Pràctica de la primavera de 2022 i exàmens fins a la primavera de 2022 de l'assignatura Compiladors (CL) del Grau en Enginyeria Informàtica (GEI) de la Facultat d'Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web de la pràctica de l'assignatura](https://www.cs.upc.edu/~cl/practica/).
