/////////////////////////////////////////////////////////////////
//
//    TypesMgr - Type System for the Asl programming language
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
////////////////////////////////////////////////////////////////

#include "TypesMgr.h"
#include <vector>
#include <string>
#include <iostream>
#include <cstddef>    // std::size_t
#include <algorithm>  // find

// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

// using namespace std;

// ======================================================================
// class TypesMgr

// ----------------------------------------------------------------------
// constructor

TypesMgr::TypesMgr() {
  // Prebuilt and insert in TypesVec the Type's of the primitive types
  TypesVec = std::vector<Type>(NumPrimitiveAndErrorTypes);
  TypesVec[ErrorTyId]     = Type(TypeKind::ErrorKind);
  TypesVec[IntegerTyId]   = Type(TypeKind::IntegerKind);
  TypesVec[FloatTyId]     = Type(TypeKind::FloatKind);
  TypesVec[BooleanTyId]   = Type(TypeKind::BooleanKind);
  TypesVec[CharacterTyId] = Type(TypeKind::CharacterKind);
  TypesVec[VoidTyId]      = Type(TypeKind::VoidKind);
}

// ----------------------------------------------------------------------
// methods to create a Type and return its TypeId

TypesMgr::TypeId TypesMgr::createErrorTy() {
  return ErrorTyId;
}

TypesMgr::TypeId TypesMgr::createIntegerTy() {
  return IntegerTyId;
}

TypesMgr::TypeId TypesMgr::createFloatTy() {
  return FloatTyId;
}

TypesMgr::TypeId TypesMgr::createBooleanTy() {
  return BooleanTyId;
}

TypesMgr::TypeId TypesMgr::createCharacterTy() {
  return CharacterTyId;
}

TypesMgr::TypeId TypesMgr::createVoidTy() {
  return VoidTyId;
}

TypesMgr::TypeId TypesMgr::createFunctionTy(const std::vector<TypeId> & paramsTypes,
					    TypeId returnType) {
  TypesVec.push_back(Type(paramsTypes, returnType));
  return TypesVec.size()-1;
}

TypesMgr::TypeId TypesMgr::createArrayTy(unsigned int size,
					 TypeId elemType) {
  TypesVec.push_back(Type{size, elemType});
  return TypesVec.size()-1;
}

// XXX 2018 ===================================================================
TypesMgr::TypeId TypesMgr::createPairTy(TypeId firstType,
				        TypeId secondType) {
  TypesVec.push_back(Type{firstType, secondType});
  return TypesVec.size()-1;
}

// XXX 2021P ==================================================================
TypesMgr::TypeId TypesMgr::createTupleTy(const std::vector<TypeId> & fieldsTypes) {
  TypesVec.push_back(Type{fieldsTypes});
  return TypesVec.size()-1;
}

// XXX 2022P ==================================================================
TypesMgr::TypeId TypesMgr::createEmptyStructTy() {
  TypesVec.push_back(Type{std::vector<std::string>(), std::vector<TypeId>()});
  return TypesVec.size()-1;
}

TypesMgr::TypeId TypesMgr::createStructTy(const std::vector<std::string> & fieldNames,
                                          const std::vector<TypeId> & fieldTypes) {
  TypesVec.push_back(Type{fieldNames, fieldTypes});
  return TypesVec.size()-1;
}

// XXX 2022F ==================================================================
TypesMgr::TypeId TypesMgr::createMatrixTy(unsigned int rows,
                                          unsigned int cols,
					 TypeId elemType) {
  TypesVec.push_back(Type{rows, cols, elemType});
  return TypesVec.size()-1;
}
// ============================================================================

// ----------------------------------------------------------------------
// accessors for working with primitive types

bool TypesMgr::isErrorTy(TypeId tid) const {
  return tid == ErrorTyId;
}

bool TypesMgr::isIntegerTy(TypeId tid) const {
  return tid == IntegerTyId;
}

bool TypesMgr::isFloatTy(TypeId tid) const {
  return tid == FloatTyId;
}

bool TypesMgr::isBooleanTy(TypeId tid) const {
  return tid == BooleanTyId;
}

bool TypesMgr::isCharacterTy(TypeId tid) const {
  return tid == CharacterTyId;
}

bool TypesMgr::isVoidTy(TypeId tid) const {
  return tid == VoidTyId;
}

bool TypesMgr::isNumericTy(TypeId tid) const {
  return (tid == IntegerTyId or tid == FloatTyId);
}

bool TypesMgr::isPrimitiveTy(TypeId tid) const {
  return (tid == IntegerTyId or tid == FloatTyId or
	  tid == BooleanTyId or tid == CharacterTyId or
	  tid == VoidTyId);
}

bool TypesMgr::isPrimitiveNonVoidTy(TypeId tid) const {
  return (isPrimitiveTy(tid) and not isVoidTy(tid));
}

// ----------------------------------------------------------------------
// accessors for working with function types

bool TypesMgr::isFunctionTy(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  return t.isFunctionTy();
}

const std::vector<TypesMgr::TypeId> & TypesMgr::getFuncParamsTypes(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isFunctionTy());
  return t.getFuncParamsTypes();
}

TypesMgr::TypeId TypesMgr::getFuncReturnType(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isFunctionTy());
  return t.getFuncReturnType();
}

std::size_t TypesMgr::getNumOfParameters(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isFunctionTy());
  return t.getNumOfParameters();
}

TypesMgr::TypeId TypesMgr::getParameterType(TypeId tid, unsigned int i) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isFunctionTy() and i < t.getNumOfParameters());
  return t.getParameterType(i);
}

bool TypesMgr::isVoidFunction(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isFunctionTy());
  TypeId tr = t.getFuncReturnType();
  return isVoidTy(tr);
}

// ----------------------------------------------------------------------
// accessors for working with array types

bool TypesMgr::isArrayTy(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  return t.isArrayTy();
}

unsigned int TypesMgr::getArraySize(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isArrayTy());
  return t.getArraySize();
}

TypesMgr::TypeId TypesMgr::getArrayElemType(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isArrayTy());
  return t.getArrayElemType();
}

// XXX 2018 ===================================================================
// ----------------------------------------------------------------------
// accessors for working with pair types
bool TypesMgr::isPairTy(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  return t.isPairTy();
}

TypesMgr::TypeId TypesMgr::getFirstPairType(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isPairTy());
  return t.getFirstPairType();
}

TypesMgr::TypeId TypesMgr::getSecondPairType(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isPairTy());
  return t.getSecondPairType();
}

// XXX 2021P ==================================================================
// ----------------------------------------------------------------------
// accessors for working with tuple types

bool TypesMgr::isTupleTy(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  return t.isTupleTy();
}

unsigned int TypesMgr::getTupleSize(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isTupleTy());
  return t.getTupleSize();
}

TypesMgr::TypeId TypesMgr::getTupleFieldType(TypeId tid, unsigned int n) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isTupleTy());
  return t.getTupleFieldType(n);
}

// XXX 2022P ==================================================================
// ----------------------------------------------------------------------
// accessors for working with struct types

void TypesMgr::addStructField(TypeId tidStruct,
                              const std::string & name, TypeId tidField) {
  Type & t = TypesVec.at(tidStruct);
  assert(t.isStructTy());
  assert(not t.existStructField(name));
  t.addStructField(name, tidField);
}

bool TypesMgr::isStructTy(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  return t.isStructTy();
}

bool TypesMgr::existStructField(TypeId tid, const std::string & name) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isStructTy());
  return t.existStructField(name);
}

TypesMgr::TypeId TypesMgr::getStructFieldTy(TypeId tid, const std::string & name) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isStructTy());
  assert(t.existStructField(name));
  return t.getStructFieldTy(name);
}

unsigned int TypesMgr::getStructFieldIndex(TypeId tid, const std::string & name) const {
	const Type & t = TypesVec.at(tid);
	assert(t.isStructTy());
	assert(t.existStructField(name));
	return t.getStructFieldIndex(name);
}

// XXX 2022F ==================================================================
// ----------------------------------------------------------------------
// accessors for working with matrix types

bool TypesMgr::isMatrixTy(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  return t.isMatrixTy();
}

unsigned int TypesMgr::getMatrixSize(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isMatrixTy());
  return t.getMatrixSize();
}

unsigned int TypesMgr::getMatrixRows(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isMatrixTy());
  return t.getMatrixRows();
}

unsigned int TypesMgr::getMatrixCols(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isMatrixTy());
  return t.getMatrixCols();
}

TypesMgr::TypeId TypesMgr::getMatrixElemType(TypeId tid) const {
  const Type & t = TypesVec.at(tid);
  assert(t.isMatrixTy());
  return t.getMatrixElemType();
}
// ============================================================================

// ----------------------------------------------------------------------
// methods for checking different compatibilities of Types

bool TypesMgr::equalTypes(TypeId tid1, TypeId tid2) const {
  if (tid1 == tid2)
    return true;
  const Type & t1 = TypesVec.at(tid1);
  const Type & t2 = TypesVec.at(tid2);
  if (t1.getTypeKind() != t2.getTypeKind())
    return false;
  if (t1.isPrimitiveTy() and t2.isPrimitiveTy())
    return true;
  if (t1.isFunctionTy()) {  // or: if (t2.isFunctionTy()) {
    if (t1.getNumOfParameters() != t2.getNumOfParameters())
      return false;
    TypeId tid1_aux;
    TypeId tid2_aux;
    for (unsigned int i = 0; i < t1.getNumOfParameters(); ++i) {
      tid1_aux = t1.getParameterType(i);
      tid2_aux = t2.getParameterType(i);
      if (not equalTypes(tid1_aux, tid2_aux))
	return false;
    }
    tid1_aux = t1.getFuncReturnType();
    tid2_aux = t2.getFuncReturnType();
    return equalTypes(tid1_aux, tid2_aux);
  }
  if (t1.isArrayTy()) {  // or: if (t2.isArrayTy()) {
    if (t1.getArraySize() != t2.getArraySize()) {
      return false;
    }
    TypeId tid1_aux = t1.getArrayElemType();
    TypeId tid2_aux = t2.getArrayElemType();
    return equalTypes(tid1_aux, tid2_aux);
  }
  // XXX 2018 =================================================================
  if (t1.isPairTy()) {  // or: if (t2.isPairTy()) {
    TypeId tid1_aux = t1.getFirstPairType();
    TypeId tid2_aux = t2.getFirstPairType();
    if (equalTypes(tid1_aux, tid2_aux)) {
      tid1_aux = t1.getSecondPairType();
      tid2_aux = t2.getSecondPairType();
      return equalTypes(tid1_aux, tid2_aux);
    }
    return false;
  }
  // XXX 2021P ================================================================
  if (t1.isTupleTy()) {  // or: if (t2.isTupleTy()) {
    if (t1.getTupleSize() != t2.getTupleSize()) {
      return false;
    }
    int nFields = t1.getTupleSize();
    for (int i = 0; i < nFields; ++i) {
      TypeId tid1_aux = t1.getTupleFieldType(i);
      TypeId tid2_aux = t2.getTupleFieldType(i);
      if (not equalTypes(tid1_aux, tid2_aux))
	return false;
    }
  }
  // XXX 2022P ================================================================
  if (t1.isStructTy()) {  // or: if (t2.isStructTy()) {
    if (t1.getStructNumFields() != t2.getStructNumFields()) {
      return false;
    }
    int nFields = t1.getStructNumFields();
    for (int i = 0; i < nFields; ++i) {
      const std::string &    name1 = t1.getStructFieldName(i);
      TypeId              tid1_aux = t1.getStructFieldTy(i);
      const std::string &    name2 = t2.getStructFieldName(i);
      TypeId              tid2_aux = t2.getStructFieldTy(i);
      if (name1 != name2 or not equalTypes(tid1_aux, tid2_aux))
	return false;
    }
    return true;
  }
  // XXX 2022F ================================================================  
  if (t1.isMatrixTy()) {  // or: if (t2.isArrayTy()) {
    if (t1.getMatrixRows() != t2.getMatrixRows() or t1.getMatrixCols() != t2.getMatrixCols()) {
      return false;
    }
    TypeId tid1_aux = t1.getMatrixElemType();
    TypeId tid2_aux = t2.getMatrixElemType();
    return equalTypes(tid1_aux, tid2_aux);
  }
  // ==========================================================================
  return false;
}

bool TypesMgr::comparableTypes(TypeId tid1, TypeId tid2,
			       const std::string & op) const {
  if ((not isPrimitiveTy(tid1)) or (not isPrimitiveTy(tid2)))
    return false;
  if (isNumericTy(tid1) and isNumericTy(tid2))
    return true;
  if (isCharacterTy(tid1) and isCharacterTy(tid2))
    return true;
  if (isBooleanTy(tid1) and isBooleanTy(tid2) and
      (op == "==" or op == "!="))
    return true;
  return false;
}

bool TypesMgr::copyableTypes(TypeId tid1, TypeId tid2) const {
  if (equalTypes(tid1, tid2))
    return true;
  if (isFloatTy(tid1) and isIntegerTy(tid2))
    return true;
  return false;
}

// ----------------------------------------------------------------------
// method to compute the size of a type (primitive type size = 1)
std::size_t TypesMgr::getSizeOfType (TypeId tid) const {
  if (isPrimitiveNonVoidTy(tid)) return 1;
  if (isArrayTy(tid)) {
    const Type & tArr = TypesVec.at(tid);
    std::size_t nElems = tArr.getArraySize();
    TypeId tElem = tArr.getArrayElemType();
    return nElems * getSizeOfType(tElem);
  }
  // XXX 2018 =================================================================
  if (isPairTy(tid)) {
    const Type & tPair = TypesVec.at(tid);
    TypeId tFirst  = tPair.getFirstPairType();
    TypeId tSecond = tPair.getFirstPairType();
    return getSizeOfType(tFirst) + getSizeOfType(tSecond);
  }
  // XXX 2021P ================================================================
  if (isTupleTy(tid)) {
    const Type & tTuple = TypesVec.at(tid);
    std::size_t nFields = tTuple.getTupleSize();
    int sizeOfTuple = 0;
    for (std::size_t i = 0; i < nFields; ++i) {
      TypeId t1 = tTuple.getTupleFieldType(i);
      sizeOfTuple += getSizeOfType(t1);
    }
    return sizeOfTuple;
  }
  // XXX 2022P ================================================================
  if (isStructTy(tid)) {
    const Type & tStruct = TypesVec.at(tid);
    std::size_t nFields = tStruct.getStructNumFields();
    int sizeOfStruct = 0;
    for (std::size_t i = 0; i < nFields; ++i) {
      TypeId t1 = tStruct.getStructFieldTy(i);
      sizeOfStruct += getSizeOfType(t1);
    }
    return sizeOfStruct;
  }
  // XXX 2022F ================================================================
  if (isMatrixTy(tid)) {
    const Type & tMat = TypesVec.at(tid);
    std::size_t nElems = tMat.getMatrixSize();
    TypeId tElem = tMat.getMatrixElemType();
    return nElems * getSizeOfType(tElem);
  }
  // ==========================================================================
  return 0;
}

// ----------------------------------------------------------------------
// methods to convert to string and print types

std::string TypesMgr::to_string(TypeId tid) const {
  if (isPrimitiveTy(tid) or isErrorTy(tid)) {
    switch (tid) {
    case ErrorTyId:     return "error";
    case IntegerTyId:   return "integer";
    case FloatTyId:     return "float";
    case BooleanTyId:   return "boolean";
    case CharacterTyId: return "character";
    case VoidTyId:      return "void";
    }
  }
  const Type & t = TypesVec.at(tid);
  if (t.isFunctionTy()) {
    TypeId tid1;
    std::string s = "function<";
    if (t.getNumOfParameters() > 0) {
      tid1 = t.getParameterType(0);
      s = s + to_string(tid1);
    }
    for (unsigned int i = 1; i < t.getNumOfParameters(); ++i) {
      tid1 = t.getParameterType(i);
      s = s + "," + to_string(tid1);
    }
    tid1 = t.getFuncReturnType();
    s = s + ">:" + to_string(tid1);
    return s;
  }
  else if (t.isArrayTy()) {
    TypeId tid1;
    std::string s = "array<" + std::to_string(t.getArraySize()) + ",";
    tid1 = t.getArrayElemType();
    s = s + to_string(tid1) +">";
    return s;
  }
  // XXX 2018 =================================================================
  else if (t.isPairTy()) {
    TypeId tFirst = t.getFirstPairType();
    TypeId tSecond = t.getSecondPairType();
    std::string s = "pair<" + to_string(tFirst) + "," + to_string(tSecond) + ">";
    return s;
  }
  // XXX 2021P ================================================================
   else if (t.isTupleTy()) {
    std::string s = "tuple<";
    std::size_t nFields = t.getTupleSize();
    for (std::size_t i = 0; i < nFields; ++i) {
      TypeId t1 = t.getTupleFieldType(i);
      if (i > 0) s += ",";
      s += to_string(t1);
    }
    s += ">";
    return s;
  }
  // XXX 2022P ================================================================
  else if (t.isStructTy()) {
    std::string s = "struct<";
    std::size_t nFields = t.getStructNumFields();
    for (std::size_t i = 0; i < nFields; ++i) {
      const std::string & name = t.getStructFieldName(i);
      TypeId                t1 = t.getStructFieldTy(i);
      if (i > 0) s += ",";
      s += name + ":" + to_string(t1);
    }
    s += ">";
    return s;
  }
  // XXX 2022F ================================================================
  else if (t.isMatrixTy()) {
    TypeId tid1;
    std::string s = "matrix<" + std::to_string(t.getMatrixRows()) + "," + std::to_string(t.getMatrixCols()) + "," ;
    tid1 = t.getMatrixElemType();
    s = s + to_string(tid1) +">";
    return s;
  }
  // ==========================================================================
  else {
    return "none";
  }
}

void TypesMgr::dump(TypeId tid, std::ostream & os) const {
  os << to_string(tid);
}


// ======================================================================
// class TypesMgr::Type

// ----------------------------------------------------------------------
// constructors

TypesMgr::Type::Type(TypeKind tid) : ID{tid} {
  assert(TypeKind::FirstPrimitiveKind < ID and
	 ID < TypeKind::LastPrimitiveKind);
}

TypesMgr::Type::Type(const std::vector<TypeId> & paramsTypes, TypeId returnType) :
  ID{TypesMgr::TypeKind::FunctionKind},
  funcParamsTy{paramsTypes},
  funcReturnTy{returnType} { }

TypesMgr::Type::Type(unsigned int arraySize, TypeId arrayElemType) :
  ID{TypesMgr::TypeKind::ArrayKind},
  arraySize{arraySize},
  arrayElemTy{arrayElemType} { }

// XXX 2018 ===================================================================
TypesMgr::Type::Type(TypeId firstType, TypeId secondType) :
  ID{TypesMgr::TypeKind::PairKind},
  firstPairTy{firstType},
  secondPairTy{secondType} { }

// XXX 2021P ==================================================================
TypesMgr::Type::Type(const std::vector<TypeId> & fieldsTypes) :
  ID{TypesMgr::TypeKind::TupleKind},
  tupleFieldsTy{fieldsTypes} { }

// XXX 2022P ==================================================================
TypesMgr::Type::Type(const std::vector<std::string> & fieldNames,
                     const std::vector<TypeId> & fieldTypes) :
  ID{TypesMgr::TypeKind::StructKind},
  structFieldNames{fieldNames},
  structFieldTypes{fieldTypes} { }

// XXX 2022F ==================================================================
TypesMgr::Type::Type(unsigned int matrixRows, unsigned int matrixCols, TypeId matrixElemType) :
  ID{TypesMgr::TypeKind::MatrixKind},
  matrixSize{matrixCols*matrixRows},
  matrixRows{matrixRows},
  matrixCols{matrixCols},
  matrixElemTy{matrixElemType} { }
// ============================================================================

// ----------------------------------------------------------------------
// accesor to get the kind

TypesMgr::TypeKind TypesMgr::Type::getTypeKind () const {
  return ID;
}

// ----------------------------------------------------------------------
// accessors for working with primitive types

bool TypesMgr::Type::isErrorTy() const {
  return ID == TypeKind::ErrorKind;
}

bool TypesMgr::Type::isIntegerTy() const {
  return ID == TypeKind::IntegerKind;
}

bool TypesMgr::Type::isFloatTy() const {
  return ID == TypeKind::FloatKind;
}

bool TypesMgr::Type::isBooleanTy() const {
  return ID == TypeKind::BooleanKind;
}

bool TypesMgr::Type::isCharacterTy() const {
  return ID == TypeKind::CharacterKind;
}

bool TypesMgr::Type::isVoidTy() const {
  return ID == TypeKind::VoidKind;
}

bool TypesMgr::Type::isNumericTy() const {
  return (ID == TypeKind::IntegerKind or
	  ID == TypeKind::FloatKind);
}

bool TypesMgr::Type::isPrimitiveTy() const {
  return (ID == TypeKind::IntegerKind or
	  ID == TypeKind::FloatKind or
	  ID == TypeKind::BooleanKind or
	  ID == TypeKind::CharacterKind or
	  ID == TypeKind::VoidKind);
}

bool TypesMgr::Type::isPrimitiveNonVoidTy() const {
  return (isPrimitiveTy() and not isVoidTy());
}

// ----------------------------------------------------------------------
// accessors for working with function types

bool TypesMgr::Type::isFunctionTy() const {
  return ID == TypeKind::FunctionKind;
}

const std::vector<TypesMgr::TypeId> & TypesMgr::Type::getFuncParamsTypes() const {
  return funcParamsTy;
}

TypesMgr::TypeId TypesMgr::Type::getFuncReturnType() const {
  return funcReturnTy;
}

std::size_t TypesMgr::Type::getNumOfParameters() const {
  return getFuncParamsTypes().size();
}

TypesMgr::TypeId TypesMgr::Type::getParameterType(unsigned int i) const {
  assert(i < getNumOfParameters());
  return getFuncParamsTypes()[i];
}

bool TypesMgr::Type::isVoidFunction() const {
  return getFuncReturnType() == TypeKind::VoidKind;
}

// ----------------------------------------------------------------------
// accessors for working with array types

bool TypesMgr::Type::isArrayTy() const {
  return ID == TypeKind::ArrayKind;
}

unsigned int TypesMgr::Type::getArraySize() const {
  return arraySize;
}

TypesMgr::TypeId TypesMgr::Type::getArrayElemType() const {
  return arrayElemTy;
}

// XXX 2018 ===================================================================
// ----------------------------------------------------------------------
// accessors for working with pair types
bool TypesMgr::Type::isPairTy() const {
  return ID == TypeKind::PairKind;
}

TypesMgr::TypeId TypesMgr::Type::getFirstPairType() const {
  return firstPairTy;
}

TypesMgr::TypeId TypesMgr::Type::getSecondPairType() const {
  return secondPairTy;
}
// XXX 2021P ==================================================================
// ----------------------------------------------------------------------
// accessors for working with tuple types

bool TypesMgr::Type::isTupleTy() const {
  return ID == TypeKind::TupleKind;
}

unsigned int TypesMgr::Type::getTupleSize() const {
  return tupleFieldsTy.size();
}

TypesMgr::TypeId TypesMgr::Type::getTupleFieldType(unsigned int i) const {
  assert(i < tupleFieldsTy.size());
  return tupleFieldsTy[i];
}

// XXX 2022P ==================================================================
// ----------------------------------------------------------------------
// accessors for working with struct types

void TypesMgr::Type::addStructField(const std::string & name, TypeId tidField) {
  assert(not existStructField(name));
  structFieldNames.push_back(name);
  structFieldTypes.push_back(tidField);
}

bool TypesMgr::Type::isStructTy() const {
  return ID == TypeKind::StructKind;
}

bool TypesMgr::Type::existStructField(const std::string & name) const {
  return std::find(structFieldNames.begin(), structFieldNames.end(), name) != structFieldNames.end();
}

unsigned int TypesMgr::Type::getStructFieldIndex(const std::string & name) const {
  unsigned int i;
  for (i = 0; i < structFieldNames.size(); i++)
  	if (structFieldNames[i] == name)
  		break;
  return i;
}

TypesMgr::TypeId TypesMgr::Type::getStructFieldTy(const std::string & name) const {
  assert(existStructField(name));
  for (unsigned int i = 0; i < getStructNumFields(); ++i) {
    if (getStructFieldName(i) == name)
      return getStructFieldTy(i);
  }
  return ErrorTyId;
}

unsigned int TypesMgr::Type::getStructNumFields() const {
  return structFieldNames.size();    // or structFieldTypes.size()
}

const std::string & TypesMgr::Type::getStructFieldName(unsigned int i) const {
  assert(i < structFieldNames.size());
  return structFieldNames[i];
}

TypesMgr::TypeId TypesMgr::Type::getStructFieldTy(unsigned int i) const {
  assert(i < structFieldTypes.size());
  return structFieldTypes[i];
}

// XXX 2022F ==================================================================
// ----------------------------------------------------------------------
// accessors for working with matrix types

bool TypesMgr::Type::isMatrixTy() const {
  return ID == TypeKind::MatrixKind;
}

unsigned int TypesMgr::Type::getMatrixSize() const {
  return matrixSize;
}

unsigned int TypesMgr::Type::getMatrixRows() const {
  return matrixRows;
}

unsigned int TypesMgr::Type::getMatrixCols() const {
  return matrixCols;
}

TypesMgr::TypeId TypesMgr::Type::getMatrixElemType() const {
  return matrixElemTy;
}
// ============================================================================
