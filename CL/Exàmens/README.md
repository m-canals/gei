Codi i execució
===============================================================================

Fem servir el codi comú de 2022 afegint-hi el codi de cada examen amb un
comentari que comença amb `XXX`.

El programa `asl/check-examples.sh` accepta un directori amb jocs de proves,
com ara `examples/2018`, com a argument opcional. El programa
`asl/check-example.sh` requereix d'un joc de proves, com ara
`examples/2018/jp_chkt_01` com a argument. Tots dos programes accepten
la TVM com a segon argument opcional.

Si `USE_HALT` (`CodeGenVisitor.cpp`) és cert, fem servir la instrucció `halt`,
i cal fer servir la TVM de 2019 als jocs de proves de 2019 perquè la TVM de
2022 no l'accepta. Si és fals simulem la instrucció `halt` amb excepcions.

Generem el codi per a structs simples (2022P): implementem la funció
`TypesMgr::getStructFieldIndex`.


Modificació dels jocs de proves
===============================================================================

Eliminem el missatge d'error `Line 8:2 error: Return with incompatible type`
a `examples/2018/jpbasic_chkt_04.err`.

Fem servir el mètode `SemErrors::incompatibleReturn` de 2022, que no fa servir
el context de l'expressió retornada, quan n'hi ha, sinó el del símbol: canviem
el número de columna als missatges d'error corresponents a
`examples/2018/jpbasic_chkt_12.err`.

Fem servir el mètode `SemErrors::less` de 2022, que mostra els missatges en
ordre (línia, columna i text): ordenem els missatges a `examples/2018` i a
`examples/2021P`.

Fem servir el mètode `SemErrors::nonIntegerIndexInArrayAccess` de 2022, que
conté `with` en lloc de `witn`: canviem la paraula `witn` per `with` a
`examples/2018` i a `examples/2019`.

Per a poder fer servir la TVM de 2019 amb els jocs de proves de 2019, que no
accepta cadenes de text, canviem les cadenes de text per caràcters i `'` per
`"` a `examples/2019`.

La gramàtica conté la funció predefinida `max` (2020P), que invalida els
programes que contenen la variable `max`: canviem la variable `max` per la
variable `m` a `jpbasic_genc_09.asl`.

La gramàtica conté la funció predefinida `sum` (2020F), que invalida els
programes que contenen la variable `sum`: canviem la variable `sum` per la
variable `s` a `examples/2022F/jp_genc_07.asl` i
`examples/2022F/jp_genc_08.asl`.

Comprovem la validesa de l'índex amb què accedim a un array (2019): evitem
l'accés a un array amb un índex invàlid a `examples/2020F/jp_genc_05.asl`.

Generem el codi per a l'accés a tuples (2018): creem
`examples/2018/jp_genc_04`.

Generem el codi per a la funció predefinida `max` i per a l'estructura
repetitiva `for` (2020P): creem `examples/2020P/jp_genc_01` i
`examples/2020P/jp_genc_02`.

Generem el codi per a les instruccions `pack` i `unpack` i per a tuples
(2021P): creem `examples/2021P/jp_genc_01`, `examples/2021P/jp_genc_02`
i `examples/2021P/jp_genc_03`.

Generem el codi per a la instrucció de mapatge d'arrays i per a structs
simples (2022P): creem `examples/2022P/jp_genc_01` i
`examples/2022P/jp_genc_02`.

Implementem les instruccions `unless`, `doWhile`, `until` i `doUntil` i
l'assignació múltiple: creem `examples/Altres`.

