//////////////////////////////////////////////////////////////////////
//
//    CodeGenVisitor - Walk the parser tree to do
//                     the generation of code
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
//////////////////////////////////////////////////////////////////////

#include "CodeGenVisitor.h"
#include "antlr4-runtime.h"
// XXX 2019 ===================================================================
#include "../common/RuntimeError.h"
#define USE_HALT 0
// ============================================================================
#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/code.h"
#include <string>
#include <cstddef>
//#define DEBUG_BUILD
#include "../common/debug.h"
#define NDEBUG
#include <cassert>

#define RESULT_ADDRESS  "_result"
#define SUCCESS_ADDRESS "_success"

CodeGenVisitor::CodeGenVisitor(
		TypesMgr       & Types,
		SymTable       & Symbols,
		TreeDecoration & Decorations) :
	Types{Types},
	Symbols{Symbols},
	Decorations{Decorations} {
}

antlrcpp::Any CodeGenVisitor::visitProgram(AslParser::ProgramContext * ctx) {
	DEBUG_ENTER();
	SymTable::ScopeId scope = getScopeDecor(ctx);
	code program;
	Symbols.pushThisScope(scope);
	
	for (const auto & function : ctx->function()) { 
	  subroutine routine = visit(function);
	  program.add_subroutine(routine);
	}
	
	Symbols.popScope();
	DEBUG_EXIT();
	return program;
}

antlrcpp::Any CodeGenVisitor::visitFunction(AslParser::FunctionContext * ctx) {
	DEBUG_ENTER();
	const std::string & functionName = ctx->ID()->getText();
	TypesMgr::TypeId functionType = getTypeDecor(ctx);
	TypesMgr::TypeId returnType = Types.getFuncReturnType(functionType);
	SymTable::ScopeId scope = getScopeDecor(ctx);
	subroutine routine(functionName);
	Symbols.pushThisScope(scope);
	codeCounters.reset();
	setCurrentFunctionTy(functionType);
	
	// Excepcions.
	if (functionName == "main") {
		routine.add_var(var{SUCCESS_ADDRESS, 1});
	} else {
		routine.add_param(SUCCESS_ADDRESS);
	}
	
	// Resultat.
	if (not Types.isVoidTy(returnType)) {
		routine.add_param(RESULT_ADDRESS);
	}

	// Paràmetres.
	if (Types.getNumOfParameters(functionType) > 0) {
		for (const auto & parameter : ctx->parameters()->parameter()) {
			const std::string & parameterName = parameter->ID()->getText();
			routine.add_param(parameterName);
		}
	}

	// Variables locals.
	for (const auto & variable : ctx->variables()->variable()) {
		TypesMgr::TypeId variableType = getTypeDecor(variable->type());
		std::size_t variableSize = Types.getSizeOfType(variableType);
		for (const auto & id : variable->ID()) {
			const std::string & variableName = id->getText();
			routine.add_var(var{variableName, variableSize});
		}
	}

	// Instruccions.
	instructionList && code = visit(ctx->statements());
	
	// Excepcions.
	code = instruction::ILOAD(SUCCESS_ADDRESS, "1")
	    || code
	    || instruction::LABEL("end");
	
	// Return implícit.
	code = code || instruction::RETURN();
	
	routine.set_instructions(code);
	Symbols.popScope();
	DEBUG_EXIT();
	return routine;
}

antlrcpp::Any CodeGenVisitor::visitStatements(AslParser::StatementsContext * ctx) {
	DEBUG_ENTER();
	instructionList code;
	
	for (const auto & statement : ctx->statement()) {
		instructionList && statementCode = visit(statement);
	  code = code || statementCode;
	}
	
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitReturn(AslParser::ReturnContext * ctx) {
	DEBUG_ENTER();
	instructionList code;
	
	if (ctx->expression()) {
		CodeAttribs && expression = visit(ctx->expression());
		TypesMgr::TypeId expressionType = getTypeDecor(ctx->expression());
		TypesMgr::TypeId functionType = getCurrentFunctionTy();
		TypesMgr::TypeId returnType = Types.getFuncReturnType(functionType);
		code = code || expression.code;
		std::string resultAddress = coerce(code, returnType, expressionType, expression.addr);
		code = code || instruction::LOAD(RESULT_ADDRESS, resultAddress);
	}
	
	code = code || instruction::RETURN();
	
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitRead(AslParser::ReadContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->leftExpression());
	TypesMgr::TypeId leftExpressionType = getTypeDecor(ctx->leftExpression());
	std::string address = "%" + codeCounters.newTEMP();
	instructionList & code = leftExpression.code;

	if (Types.isCharacterTy(leftExpressionType))
		code = code || instruction::READC(address);
	else if (Types.isBooleanTy(leftExpressionType))
		code = code || instruction::READI(address);
	else if (Types.isIntegerTy(leftExpressionType))
		code = code || instruction::READI(address);
	else if (Types.isFloatTy(leftExpressionType))
		code = code || instruction::READF(address);
	
	store(code, leftExpression.addr, leftExpression.offs, address);

	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitWriteExpression(AslParser::WriteExpressionContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	TypesMgr::TypeId expressionType = getTypeDecor(ctx->expression());
	instructionList & code = expression.code;
	
	if (Types.isCharacterTy(expressionType))
		code = code || instruction::WRITEC(expression.addr);
	else if (Types.isBooleanTy(expressionType))
		code = code || instruction::WRITEI(expression.addr);
	else if (Types.isIntegerTy(expressionType))
		code = code || instruction::WRITEI(expression.addr);
	else if (Types.isFloatTy(expressionType))
		code = code || instruction::WRITEF(expression.addr);
	
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitWriteString(AslParser::WriteStringContext * ctx) {
	DEBUG_ENTER();
	const std::string & string = ctx->STRING()->getText();
	instructionList code = instruction::WRITES(string);
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitIf(AslParser::IfContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && thenCode = visit(ctx->statements(0));
	instructionList code;

	if (ctx->statements(1)) {
		instructionList && elseCode = visit(ctx->statements(1));
		code = ifCode(expression.code, expression.addr, thenCode, elseCode);
	} else {
		code = ifCode(expression.code, expression.addr, thenCode);
	}

	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitWhile(AslParser::WhileContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && doCode = visit(ctx->statements());
	instructionList code = whileCode(expression.code, expression.addr, doCode);
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitProcedureCall(AslParser::ProcedureCallContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && call = visit(ctx->call());
	instructionList & code = call.code;
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitAssignment(AslParser::AssignmentContext * ctx) {
	DEBUG_ENTER();
	const auto & leftExpressions = ctx->leftExpressions()->leftExpression();
	const auto & expressions = ctx->expressions()->expression();
	instructionList code;
	
	for (unsigned int i = 0; i < leftExpressions.size(); i++) {
		CodeAttribs && leftExpression = visit(leftExpressions[i]);
		CodeAttribs && expression = visit(expressions[i]);
		TypesMgr::TypeId leftExpressionType = getTypeDecor(leftExpressions[i]);
		TypesMgr::TypeId expressionType = getTypeDecor(expressions[i]);
		
		code = code || leftExpression.code || expression.code;
		std::string address = coerce(code, leftExpressionType, expressionType, expression.addr);
		
		if (Types.isArrayTy(leftExpressionType) and Types.isArrayTy(expressionType))
			code = code || copyArrayCode(leftExpressionType, expressionType, leftExpression.addr, address);
		// XXX 2022F =============================================================
		else if (Types.isMatrixTy(leftExpressionType) and Types.isMatrixTy(expressionType))
			code = code || copyMatrixCode(leftExpressionType, expressionType, leftExpression.addr, address);
		// XXX ===================================================================
		else
			store(code, leftExpression.addr, leftExpression.offs, address);
	}

	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitFunctionCall(AslParser::FunctionCallContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && attributes = visit(ctx->call());
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitCall(AslParser::CallContext * ctx) {
	DEBUG_ENTER();
	const std::string & functionName = ctx->id()->getText();
	std::vector <TypesMgr::TypeId> argumentTypes;
	std::vector <std::string> argumentAddresses;
	instructionList code;

	if (ctx->expressions()) {
		for (const auto & expressionContext : ctx->expressions()->expression()) {
			CodeAttribs && expression = visit(expressionContext);
			TypesMgr::TypeId expressionType = getTypeDecor(expressionContext);
			argumentTypes.push_back(expressionType);
			argumentAddresses.push_back(expression.addr);
			code = code || expression.code;
		}
	}
		
	std::string address = call(code, functionName, argumentTypes, argumentAddresses);

	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitParenthesization(AslParser::ParenthesizationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && attributes = visit(ctx->expression());
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitBooleanUnaryOperation(AslParser::BooleanUnaryOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList & code = expression.code;
	
	if (ctx->NOT())
		code = code || instruction::NOT(resultAddress, expression.addr);

	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitBooleanBinaryOperation(AslParser::BooleanBinaryOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->expression(0));
	CodeAttribs && rightExpression = visit(ctx->expression(1));
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList && code = leftExpression.code || rightExpression.code;
	
	if (ctx->AND())
		code = code || instruction::AND(resultAddress, leftExpression.addr, rightExpression.addr);
	else if (ctx->OR())
		code = code || instruction::OR(resultAddress, leftExpression.addr, rightExpression.addr);
	
	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitArithmeticUnaryOperation(AslParser::ArithmeticUnaryOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	TypesMgr::TypeId expressionType = getTypeDecor(ctx->expression());
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList & code = expression.code;

	if (ctx->PLUS()) {
		code = code || instruction::LOAD(resultAddress, expression.addr);
	} else if (ctx->MINUS()) {
		if (Types.isFloatTy(expressionType))
			code = code || instruction::FNEG(resultAddress, expression.addr);
		else
			code = code || instruction::NEG(resultAddress, expression.addr);
	}
	
	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitArithmeticBinaryOperation(AslParser::ArithmeticBinaryOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->expression(0));
	CodeAttribs && rightExpression = visit(ctx->expression(1));
	TypesMgr::TypeId leftExpressionType = getTypeDecor(ctx->expression(0));
	TypesMgr::TypeId rightExpressionType = getTypeDecor(ctx->expression(1));
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList && code = leftExpression.code || rightExpression.code;
	
	if (ctx->MOD()) {
		code = code || instruction__MOD(resultAddress, leftExpression.addr, rightExpression.addr);
	// XXX 2021F ================================================================
	} else if (ctx->EXP()) {
		std::string base = coerce(code, Types.createFloatTy(), leftExpressionType, leftExpression.addr);
		code = code || instruction__POW(resultAddress, base, rightExpression.addr);
	// ==========================================================================
	} else if (Types.isFloatTy(leftExpressionType) or Types.isFloatTy(rightExpressionType)) {
		std::string leftOperandAddress = coerce(code, rightExpressionType, leftExpressionType, leftExpression.addr);
		std::string rightOperandAddress = coerce(code, leftExpressionType, rightExpressionType, rightExpression.addr);
		if      (ctx->MUL())   code = code || instruction::FMUL(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->PLUS())  code = code || instruction::FADD(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->MINUS()) code = code || instruction::FSUB(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->DIV())   code = code || instruction::FDIV(resultAddress, leftOperandAddress, rightOperandAddress);
	} else if (Types.isIntegerTy(leftExpressionType) and Types.isIntegerTy(rightExpressionType)) {
		if      (ctx->MUL())   code = code || instruction::MUL(resultAddress, leftExpression.addr, rightExpression.addr);
		else if (ctx->PLUS())  code = code || instruction::ADD(resultAddress, leftExpression.addr, rightExpression.addr);
		else if (ctx->MINUS()) code = code || instruction::SUB(resultAddress, leftExpression.addr, rightExpression.addr);
		else if (ctx->DIV())   code = code || instruction::DIV(resultAddress, leftExpression.addr, rightExpression.addr);
	// XXX 2018 =================================================================
	} else if (Types.isArrayTy(leftExpressionType) and Types.isArrayTy(rightExpressionType)) {
		TypesMgr::TypeId type = Types.getArrayElemType(leftExpressionType);
		unsigned int size = Types.getArraySize(leftExpressionType);
		if (Types.isFloatTy(type))
			code = code || instruction__FDOT(resultAddress, leftExpression.addr, rightExpression.addr, size);
		else
			code = code || instruction__DOT(resultAddress, leftExpression.addr, rightExpression.addr, size);
	// ==========================================================================
	}
	
	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitRelationalOperation(AslParser::RelationalOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->expression(0));
	CodeAttribs && rightExpression = visit(ctx->expression(1));
	TypesMgr::TypeId leftExpressionType = getTypeDecor(ctx->expression(0));
	TypesMgr::TypeId rightExpressionType = getTypeDecor(ctx->expression(1));
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList && code = leftExpression.code || rightExpression.code;
	
	std::string leftOperandAddress = coerce(code, rightExpressionType, leftExpressionType, leftExpression.addr);
	std::string rightOperandAddress = coerce(code, leftExpressionType, rightExpressionType, rightExpression.addr);
	
	if (Types.isFloatTy(leftExpressionType) or Types.isFloatTy(rightExpressionType)) {
		if      (ctx->LT()) code = code || instruction::FLT(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->LE()) code = code || instruction::FLE(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->EQ()) code = code || instruction::FEQ(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->GE()) code = code || instruction::FLE(resultAddress, rightOperandAddress, leftOperandAddress);
		else if (ctx->GT()) code = code || instruction::FLT(resultAddress, rightOperandAddress, leftOperandAddress);
		else if (ctx->NE()) code = code || instruction__FNE(resultAddress, leftOperandAddress, rightOperandAddress);
	} else {
		if      (ctx->LT()) code = code || instruction::LT(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->LE()) code = code || instruction::LE(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->EQ()) code = code || instruction::EQ(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->GE()) code = code || instruction::LE(resultAddress, rightOperandAddress, leftOperandAddress);
		else if (ctx->GT()) code = code || instruction::LT(resultAddress, rightOperandAddress, leftOperandAddress);
		else if (ctx->NE()) code = code || instruction__NE(resultAddress, leftOperandAddress, rightOperandAddress);
	}

	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitLiteralValue(AslParser::LiteralValueContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && attributes = visit(ctx->literal());
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitLiteral(AslParser::LiteralContext * ctx) {
	DEBUG_ENTER();
	antlr4::ParserRuleContext * context;

	if (ctx->basicLiteral())
		context = ctx->basicLiteral();
	
	CodeAttribs && attributes = visit(context);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitBasicLiteral(AslParser::BasicLiteralContext * ctx) {
	DEBUG_ENTER();
	const std::string & value = ctx->getText();
	TypesMgr::TypeId type = getTypeDecor(ctx);
	std::string address = "%" + codeCounters.newTEMP();
	instructionList code;

	if (Types.isBooleanTy(type))
		code = instruction::ILOAD(address, value == "true" ? "1" : "0");
	else if (Types.isCharacterTy(type))
		code = instruction::CHLOAD(address, value.substr(1, value.size() - 2));
	else if (Types.isIntegerTy(type))
		code = instruction::ILOAD(address, value);
	else if (Types.isFloatTy(type))
		code = instruction::FLOAD(address, value);
	
	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitLeftExpressionValue(AslParser::LeftExpressionValueContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->leftExpression());
	instructionList & code = leftExpression.code;
	std::string address = load(code, leftExpression.addr, leftExpression.offs);
	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitElement(AslParser::ElementContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && attributes = visit(ctx->id());
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitArrayElement(AslParser::ArrayElementContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && array = visit(ctx->id());
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && code = array.code || expression.code;
	// XXX 2019 =================================================================
	TypesMgr::TypeId arrayType = getTypeDecor(ctx->id());
	unsigned int size = Types.getArraySize(arrayType);
	const std::string minimumAddress = "%" + codeCounters.newTEMP();
	const std::string maximumAddress = "%" + codeCounters.newTEMP();
	const std::string evaluationAddress = "%" + codeCounters.newTEMP();
	instructionList thenCode, conditionCode;
	
	conditionCode = instruction::ILOAD(minimumAddress, "0")
	             || instruction::ILOAD(maximumAddress, std::to_string(size))
	             || instruction__NB(evaluationAddress, expression.addr, minimumAddress, maximumAddress);

	#if USE_HALT
	thenCode = throwRuntimeErrorTCode(ctx);
	#else
	thenCode = runtimeErrorCode(ctx, IndexError());
	#endif
	
	code = code || ifCode(conditionCode, evaluationAddress, thenCode);
	// ==========================================================================
	CodeAttribs attributes(array.addr, expression.addr, code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitId(AslParser::IdContext * ctx) {
	DEBUG_ENTER();
	const std::string & name = ctx->getText();
	instructionList code;
	std::string address = dereference(code, name);
	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

// XXX 2018 ===================================================================
antlrcpp::Any CodeGenVisitor::visitPairElement(AslParser::PairElementContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && pair = visit(ctx->id());
	const std::string element = ctx->PAIR_ELEMENT()->getText();
	std::string offsetAddress = "%" + codeCounters.newTEMP();
	instructionList code = pair.code
		|| instruction::ILOAD(offsetAddress, element == "first" ? "0" : "1");
	CodeAttribs attributes(pair.addr, offsetAddress, code);
	DEBUG_EXIT();
	return attributes;
}

// XXX 2019 ===================================================================
antlrcpp::Any CodeGenVisitor::visitMaxAttribute(AslParser::MaxAttributeContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && array = visit(ctx->id());
	TypesMgr::TypeId idType = getTypeDecor(ctx->id());
	TypesMgr::TypeId elementType = Types.getArrayElemType(idType);
	unsigned int size = Types.getArraySize(idType);
	std::string address = "%" + codeCounters.newTEMP();
	std::string valueAddress = "%" + codeCounters.newTEMP();
	std::string evaluationAddress = "%" + codeCounters.newTEMP();
	std::string counterAddress = "%" + codeCounters.newTEMP();
	instructionList code, conditionCode, thenCode, doCode;
	
	if (Types.isFloatTy(elementType))
		conditionCode = instruction::LOADX(valueAddress, array.addr, counterAddress)
		             || instruction::FLT(evaluationAddress, address, valueAddress);
	else
		conditionCode = instruction::LOADX(valueAddress, array.addr, counterAddress)
		             || instruction::LT(evaluationAddress, address, valueAddress);
	
	thenCode = instruction::LOAD(address, valueAddress);
	doCode = ifCode(conditionCode, evaluationAddress, thenCode);
	code = array.code
	    || instruction::ILOAD(counterAddress, "0")
	    || instruction::LOADX(address, array.addr, counterAddress)
	    || forCode(counterAddress, doCode, 1, size, 1);
	
	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

// XXX 2020P ==================================================================
antlrcpp::Any CodeGenVisitor::visitMaxCall(AslParser::MaxCallContext * ctx) {
	DEBUG_ENTER();
	TypesMgr::TypeId type = getTypeDecor(ctx);
	const auto & expressions = ctx->expressions()->expression();
	std::string address = "%" + codeCounters.newTEMP();
	instructionList code;
	
	for (unsigned int i = 0; i < expressions.size(); i++) {
		CodeAttribs && expression = visit(expressions[i]);
		TypesMgr::TypeId expressionType = getTypeDecor(expressions[i]);
		std::string argumentAddress = expression.addr;
		
		code = code || expression.code;
		argumentAddress = coerce(code, type, expressionType, argumentAddress);
		
		if (i == 0) {
			code = code || instruction::LOAD(address, argumentAddress);
		} else {
			std::string evaluationAddress = "%" + codeCounters.newTEMP();
			instructionList doCode = instruction::LOAD(address, argumentAddress);
			instructionList conditionCode = Types.isFloatTy(type) ?
				instruction::FLT(evaluationAddress, address, argumentAddress) :
				instruction::LT(evaluationAddress, address, argumentAddress);
			code = code || ifCode(conditionCode, evaluationAddress, doCode);
		}
	}

	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitFor(AslParser::ForContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && id = visit(ctx->id());
	instructionList && statementsCode = visit(ctx->statements());
	const auto & expressions = ctx->expressions()->expression();
	auto argumentCount = expressions.size();
	std::string startAddress, stopAddress, stepAddress;
	instructionList code; 
	
	if (argumentCount == 1) {
		CodeAttribs && stop = visit(expressions[0]);
		startAddress = "%" + codeCounters.newTEMP();
		stopAddress = stop.addr;
		stepAddress = "%" + codeCounters.newTEMP();
		code = code || instruction::ILOAD(startAddress, "0")
		            || stop.code
		            || instruction::ILOAD(stepAddress, "1");
	} else {
		CodeAttribs && start = visit(expressions[0]);
		CodeAttribs && stop = visit(expressions[1]);
		startAddress = start.addr;
		stopAddress = stop.addr;
		code = code || start.code
		            || stop.code;
		
		if (argumentCount == 2) {
			stepAddress = "%" + codeCounters.newTEMP();
			code = code || instruction::ILOAD(stepAddress, "1");
		} else if (argumentCount == 3) {
			CodeAttribs && step = visit(expressions[2]);
			stepAddress = step.addr;
			code = code || step.code;
		}
	}

	// TODO Step negatiu (range).
	code = code || forCode(id.addr, statementsCode,
		startAddress, stopAddress, stepAddress);

	DEBUG_EXIT();
	return code;
}

// XXX 2020F ==================================================================
antlrcpp::Any CodeGenVisitor::visitSumCall(AslParser::SumCallContext * ctx) {
	DEBUG_ENTER();
	TypesMgr::TypeId type = getTypeDecor(ctx);
	std::string address = "%" + codeCounters.newTEMP();
	instructionList code = Types.isFloatTy(type) ?
		instruction::FLOAD(address, "0") :
		instruction::ILOAD(address, "0");
	
	if (ctx->expressions()) {
		for (const auto & expressionContext : ctx->expressions()->expression()) {
			CodeAttribs && expression = visit(expressionContext);
			TypesMgr::TypeId expressionType = getTypeDecor(expressionContext);
			std::string argumentAddress = expression.addr;
			
			code = code || expression.code;
			argumentAddress = coerce(code, type, expressionType, argumentAddress);
			code = code || (Types.isFloatTy(type) ?
				instruction::FADD(address, address, argumentAddress) :
				instruction::ADD(address, address, argumentAddress));
		}
	}

	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

// XXX 2020F ==================================================================
antlrcpp::Any CodeGenVisitor::visitFilter(AslParser::FilterContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && sourceArray = visit(ctx->id(0));
	CodeAttribs && destinationArray = visit(ctx->id(1));
	CodeAttribs && function = visit(ctx->id(2));
	TypesMgr::TypeId sourceArrayType = getTypeDecor(ctx->id(0));
	TypesMgr::TypeId sourceType = Types.getArrayElemType(sourceArrayType);
	unsigned int size = Types.getArraySize(sourceArrayType);
	std::string resultAddress = "%" + codeCounters.newTEMP();
	std::string counterAddress = "%" + codeCounters.newTEMP();
	std::string argumentAddress = "%" + codeCounters.newTEMP();
	std::string address = "%" + codeCounters.newTEMP();
	instructionList && code = sourceArray.code || destinationArray.code || function.code;
	instructionList doCode;

	doCode = doCode || instruction::LOADX(argumentAddress, sourceArray.addr, counterAddress);
	resultAddress = call(doCode, function.addr, {sourceType}, {argumentAddress});
	doCode = doCode || instruction::XLOAD(destinationArray.addr, counterAddress, resultAddress)
	            || instruction::ADD(address, address, resultAddress);
	code = code || instruction::ILOAD(address, "0")
	            || forCode(counterAddress, doCode, 0, size, 1);

	CodeAttribs attr(address, "", code);
	DEBUG_EXIT();
	return attr;
}

// XXX 2021P ==================================================================
antlrcpp::Any CodeGenVisitor::visitPack(AslParser::PackContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && array = visit(ctx->id());
	TypesMgr::TypeId arrayType = getTypeDecor(ctx->id());
	TypesMgr::TypeId elementType = Types.getArrayElemType(arrayType);
	const auto & expressions = ctx->expressions()->expression();
	instructionList & code = array.code;
	
	for (unsigned int i = 0; i < expressions.size(); i++) {
		CodeAttribs && expression = visit(expressions[i]);
		TypesMgr::TypeId expressionType = getTypeDecor(expressions[i]);
		std::string offsetAddress = "%" + codeCounters.newTEMP();
		std::string valueAddress;
		code = code || expression.code;
		valueAddress = coerce(code, elementType, expressionType, expression.addr);
		code = code || instruction::ILOAD(offsetAddress, std::to_string(i));
		code = code || instruction::XLOAD(array.addr, offsetAddress, valueAddress);
	}

	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitUnpack(AslParser::UnpackContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && array = visit(ctx->id());
	TypesMgr::TypeId arrayType = getTypeDecor(ctx->id());
	TypesMgr::TypeId elementType = Types.getArrayElemType(arrayType);
	const auto & leftExpressions = ctx->leftExpressions()->leftExpression();
	instructionList & code = array.code;
	
	for (unsigned int i = 0; i < leftExpressions.size(); i++) {
		CodeAttribs && leftExpression = visit(leftExpressions[i]);
		TypesMgr::TypeId leftExpressionType = getTypeDecor(leftExpressions[i]);
		std::string offsetAddress = "%" + codeCounters.newTEMP();
		std::string valueAddress = "%" + codeCounters.newTEMP();
		code = code || leftExpression.code;
		code = code || instruction::ILOAD(offsetAddress, std::to_string(i));
		code = code || instruction::LOADX(valueAddress, array.addr, offsetAddress);
		valueAddress = coerce(code, leftExpressionType, elementType, valueAddress);
		store(code, leftExpression.addr, leftExpression.offs, valueAddress);
	}
	
	DEBUG_EXIT();
	return code;
}
		
antlrcpp::Any CodeGenVisitor::visitTupleElement(AslParser::TupleElementContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && tuple = visit(ctx->id());
	const std::string index = ctx->NATURAL_NUMBER()->getText();
	std::string offsetAddress = "%" + codeCounters.newTEMP();
	instructionList && code = tuple.code ||
		instruction::ILOAD(offsetAddress, index);
	CodeAttribs attributes(tuple.addr, offsetAddress, code);
	DEBUG_EXIT();
	return attributes;
}

// XXX 2021F ==================================================================
antlrcpp::Any CodeGenVisitor::visitMap(AslParser::MapContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && sourceArray = visit(ctx->id(0));
	CodeAttribs && destinationArray = visit(ctx->id(1));
	CodeAttribs && function = visit(ctx->id(2));
	TypesMgr::TypeId sourceArrayType = getTypeDecor(ctx->id(0));
	TypesMgr::TypeId sourceType = Types.getArrayElemType(sourceArrayType);
	TypesMgr::TypeId destinationArrayType = getTypeDecor(ctx->id(1));
	TypesMgr::TypeId destinationType = Types.getArrayElemType(destinationArrayType);
	TypesMgr::TypeId functionType = getTypeDecor(ctx->id(2));
	TypesMgr::TypeId resultType = Types.getFuncReturnType(functionType);
	unsigned int size = Types.getArraySize(sourceArrayType);
	std::string resultAddress = "%" + codeCounters.newTEMP();
	std::string counterAddress = "%" + codeCounters.newTEMP();
	std::string argumentAddress = "%" + codeCounters.newTEMP();
	instructionList && code = sourceArray.code || destinationArray.code || function.code;
	instructionList doCode;

	doCode = doCode || instruction::LOADX(argumentAddress, sourceArray.addr, counterAddress);
	resultAddress = call(doCode, function.addr, {sourceType}, {argumentAddress});
	resultAddress = coerce(doCode, destinationType, resultType, resultAddress);
	doCode = doCode || instruction::XLOAD(destinationArray.addr, counterAddress, resultAddress);
	code = code || forCode(counterAddress, doCode, 0, size, 1);

	DEBUG_EXIT();
	return code;
}

// XXX 2022P ==================================================================
antlrcpp::Any CodeGenVisitor::visitArrayMap(AslParser::ArrayMapContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && condition = visit(ctx->expression(0));
	CodeAttribs && thenExpression = visit(ctx->expression(1));
	CodeAttribs && elseEXpression = visit(ctx->expression(2));
	CodeAttribs && destinationArray = visit(ctx->id(0));
	CodeAttribs && variable = visit(ctx->id(1));
	CodeAttribs && sourceArray = visit(ctx->id(2));
	TypesMgr::TypeId thenExpressionType = getTypeDecor(ctx->expression(1));
	TypesMgr::TypeId elseExpressionType = getTypeDecor(ctx->expression(2));
	TypesMgr::TypeId destinationArrayType = getTypeDecor(ctx->id(0));
	TypesMgr::TypeId destinationType = Types.getArrayElemType(destinationArrayType);
	TypesMgr::TypeId variableType = getTypeDecor(ctx->id(1));
	TypesMgr::TypeId sourceArrayType = getTypeDecor(ctx->id(2));
	TypesMgr::TypeId sourceType = Types.getArrayElemType(sourceArrayType);
	std::string address, counterAddress = "%" + codeCounters.newTEMP();
	unsigned int size = Types.getArraySize(sourceArrayType);
	instructionList code, thenCode, elseCode, doCode;
	
	address = thenExpression.addr;
	thenCode = thenCode || thenExpression.code;
	address = coerce(thenCode, destinationType, thenExpressionType, address);
	thenCode = thenCode || instruction::XLOAD(destinationArray.addr, counterAddress, address);
	
	address = elseEXpression.addr;
	elseCode = elseCode || elseEXpression.code;
	address = coerce(elseCode, destinationType, elseExpressionType, address);
	elseCode = elseCode || instruction::XLOAD(destinationArray.addr, counterAddress, address);
	
	address = "%" + codeCounters.newTEMP();
	doCode = doCode || instruction::LOADX(address, sourceArray.addr, counterAddress);
	address = coerce(doCode, variableType, sourceType, address);
	doCode = doCode || instruction::LOAD(variable.addr, address);
	doCode = doCode || ifCode(condition.code, condition.addr, thenCode, elseCode);

	code = code || sourceArray.code
	            || destinationArray.code
	            || forCode(counterAddress, doCode, 0, size, 1);

	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitStructElement(AslParser::StructElementContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && structure = visit(ctx->id());
	TypesMgr::TypeId structureType = getTypeDecor(ctx->id());
	const std::string & fieldName = ctx->ID()->getText();
	unsigned int index = Types.getStructFieldIndex(structureType, fieldName);
	std::string offsetAddress = "%" + codeCounters.newTEMP();
	instructionList && code = structure.code ||
		instruction::ILOAD(offsetAddress, std::to_string(index));
	CodeAttribs attributes(structure.addr, offsetAddress, code);
	DEBUG_EXIT();
	return attributes;
}

// XXX 2022F ==================================================================
antlrcpp::Any CodeGenVisitor::visitFactorial(AslParser::FactorialContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	const std::string resultAddress = "%" + codeCounters.newTEMP();
	const std::string zeroAddress = "%" + codeCounters.newTEMP();
	const std::string oneAddress = "%" + codeCounters.newTEMP();
	const std::string counterAddress = "%" + codeCounters.newTEMP();
	const std::string startAddress = "%" + codeCounters.newTEMP();
	const std::string stopAddress = "%" + codeCounters.newTEMP();
	const std::string stepAddress = "%" + codeCounters.newTEMP();
	const std::string evaluationAddress = "%" + codeCounters.newTEMP();
	instructionList conditionCode =
	     instruction::ILOAD(zeroAddress, "0")
	  || instruction::LT(evaluationAddress, expression.addr, zeroAddress);
	instructionList thenCode =
	     instruction::HALT(code::INVALID_INTEGER_OPERAND);
	instructionList doCode = 
	     instruction::MUL(resultAddress, resultAddress, counterAddress);
	instructionList && code =
	     expression.code
	  || ifCode(conditionCode, evaluationAddress, thenCode)
		|| instruction::ILOAD(resultAddress, "1")
		|| instruction::ILOAD(startAddress, "1")
		|| instruction::ILOAD(oneAddress, "1")
		|| instruction::ADD(stopAddress, expression.addr, oneAddress)
		|| instruction::ILOAD(stepAddress, "1")
		|| forCode(counterAddress, doCode, startAddress, stopAddress, stepAddress);
	CodeAttribs attributes(resultAddress, "", code); 
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitMatrixElement(AslParser::MatrixElementContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && matrix = visit(ctx->id());
	CodeAttribs && firstExpression = visit(ctx->expression(0));
	CodeAttribs && secondExpression = visit(ctx->expression(1));
	TypesMgr::TypeId matrixType = getTypeDecor(ctx->id());
	unsigned int rows = Types.getMatrixRows(matrixType);
	unsigned int columns = Types.getMatrixCols(matrixType);
	const std::string columnsAddress = "%" + codeCounters.newTEMP();
	const std::string offsetAddress = "%" + codeCounters.newTEMP();
	instructionList && code =
		matrix.code ||
		firstExpression.code ||
		outOfRangeCode(firstExpression.addr, rows) ||
		secondExpression.code ||
		outOfRangeCode(secondExpression.addr, columns) ||
		instruction::ILOAD(columnsAddress, std::to_string(columns)) ||
		instruction::MUL(offsetAddress, firstExpression.addr, columnsAddress) ||
		instruction::ADD(offsetAddress, offsetAddress, secondExpression.addr);
	CodeAttribs attributes(matrix.addr, offsetAddress, code);
	DEBUG_EXIT();
	return attributes;
}

// XXX Altres =================================================================
antlrcpp::Any CodeGenVisitor::visitUnless(AslParser::UnlessContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && doCode = visit(ctx->statements(0));
	instructionList code;

	if (ctx->statements(1)) {
		instructionList && elseCode = visit(ctx->statements(1));
		code = unlessCode(expression.code, expression.addr, doCode, elseCode);
	} else {
		code = unlessCode(expression.code, expression.addr, doCode);
	}

	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitDoUntil(AslParser::DoUntilContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && statementsCode = visit(ctx->statements());
	instructionList code = doUntilCode(expression.code, expression.addr, statementsCode);
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitDoWhile(AslParser::DoWhileContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && statementsCode = visit(ctx->statements());
	instructionList code = doWhileCode(expression.code, expression.addr, statementsCode);
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitUntil(AslParser::UntilContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && statementsCode = visit(ctx->statements());
	instructionList code = untilCode(expression.code, expression.addr, statementsCode);
	DEBUG_EXIT();
	return code;
}
// ============================================================================

std::string CodeGenVisitor::coerce(
		instructionList & code,
		TypesMgr::TypeId destinationType,
		TypesMgr::TypeId sourceType,
		const std::string & sourceAddress) {
	if (Types.isFloatTy(destinationType) and Types.isIntegerTy(sourceType)) {
		const std::string address = "%" + codeCounters.newTEMP();
		code = code || instruction::FLOAT(address, sourceAddress);
		return address;
	} else {
		return sourceAddress;
	}
}

std::string CodeGenVisitor::reference(
		instructionList & code,
		const std::string & name) {
	TypesMgr::TypeId type = Symbols.getType(name);
	if (Symbols.isLocalVarClass(name) and (Types.isArrayTy(type)
	// XXX 2022F ===============================================================
	    or Types.isMatrixTy(type)
	// =========================================================================
	)) {
		const std::string address = "%" + codeCounters.newTEMP();
		code = code || instruction::ALOAD(address, name);
		return address;
	} else {
		return name;
	}
}

std::string CodeGenVisitor::dereference(
		instructionList & code,
		const std::string & name) {
	TypesMgr::TypeId type = Symbols.getType(name);
	if (Symbols.isParameterClass(name) and (Types.isArrayTy(type)
	// XXX 2022F ===============================================================
	    or Types.isMatrixTy(type)
	// =========================================================================
	)) {
		const std::string address = "%" + codeCounters.newTEMP();
		code = code || instruction::LOAD(address, name);
		return address;
	} else {
		return name;
	}
}

std::string CodeGenVisitor::load(
		instructionList & code,
		const std::string & sourceAddress,
		const std::string & offsetAddress) {
	if (offsetAddress == "") {
		return sourceAddress;
	} else {
		const std::string address = "%" + codeCounters.newTEMP();
		code = code || instruction::LOADX(address, sourceAddress, offsetAddress);
		return address;
	}
}

void CodeGenVisitor::store(
		instructionList & code,
		const std::string & destinationAddress,
		const std::string & offsetAddress,
		const std::string & sourceAddress) {
	if (offsetAddress == "") {
		code = code || instruction::LOAD(destinationAddress, sourceAddress);
	} else {
		code = code || instruction::XLOAD(destinationAddress, offsetAddress, sourceAddress);
	}
}

std::string CodeGenVisitor::call(
 		instructionList & code,
		const std::string & functionName,
		std::vector <TypesMgr::TypeId> argumentTypes,
		std::vector <std::string> argumentAddresses) {
	TypesMgr::TypeId functionType = Symbols.getType(functionName);
	TypesMgr::TypeId returnType = Types.getFuncReturnType(functionType);
	std::size_t parameterCount = Types.getNumOfParameters(functionType);
	bool isNonVoid = Types.isPrimitiveNonVoidTy(returnType);
	std::string address;
	
	// Excepcions.
	code = code || instruction::PUSH();
	
	if (isNonVoid) {
		address = "%" + codeCounters.newTEMP();
		code = code || instruction::PUSH();
	}
	
	for (std::size_t i = 0; i < parameterCount; i++) {
		TypesMgr::TypeId parameterType = Types.getParameterType(functionType, i);
		TypesMgr::TypeId argumentType = argumentTypes[i];
		const std::string & argumentAddress = argumentAddresses[i];
		std::string address = argumentAddress;
		address = coerce(code, parameterType, argumentType, address);
		address = reference(code, address);
		code = code || instruction::PUSH(address);
	}
	
	code = code || instruction::CALL(functionName);
	
	for (std::size_t i = 0; i < parameterCount; i++) {
		code = code || instruction::POP();
	}

	if (isNonVoid) {
		code = code || instruction::POP(address);
	}
	
	// Excepcions.
	code = code || instruction::POP(SUCCESS_ADDRESS)
	            || instruction::FJUMP(SUCCESS_ADDRESS, "end");
	
	return address;
}

instructionList CodeGenVisitor::copyArrayCode(
		TypesMgr::TypeId destinationType,
		TypesMgr::TypeId sourceType,
		const std::string & destinationAddress,
		const std::string & sourceAddress) {
	unsigned int destinationSize = Types.getArraySize(destinationType);
	unsigned int sourceSize = Types.getArraySize(sourceType);
	unsigned int size = std::min(destinationSize, sourceSize);
	std::string address = "%" + codeCounters.newTEMP();
	std::string counterAddress = "%" + codeCounters.newTEMP();
	instructionList doCode = instruction::LOADX(address, sourceAddress, counterAddress);
	address = coerce(doCode, destinationType, sourceType, address);
	doCode = doCode || instruction::XLOAD(destinationAddress, counterAddress, address);
	return forCode(counterAddress, doCode, 0, size, 1);
}

// XXX 2022F =================================================================
instructionList CodeGenVisitor::copyMatrixCode(
		TypesMgr::TypeId destinationType,
		TypesMgr::TypeId sourceType,
		const std::string & destinationAddress,
		const std::string & sourceAddress) {
	unsigned int destinationSize = Types.getMatrixSize(destinationType);
	unsigned int sourceSize = Types.getMatrixSize(sourceType);
	unsigned int size = std::min(destinationSize, sourceSize);
	std::string address = "%" + codeCounters.newTEMP();
	std::string counterAddress = "%" + codeCounters.newTEMP();
	instructionList doCode = instruction::LOADX(address, sourceAddress, counterAddress);
	address = coerce(doCode, destinationType, sourceType, address);
	doCode = doCode || instruction::XLOAD(destinationAddress, counterAddress, address);
	return forCode(counterAddress, doCode, 0, size, 1);
}

instructionList CodeGenVisitor::outOfRangeCode(
		const std::string & address,
		unsigned int size) {
	const std::string zeroAddress = "%" + codeCounters.newTEMP();
	const std::string sizeAddress = "%" + codeCounters.newTEMP();
	const std::string evaluationAddress = "%" + codeCounters.newTEMP();
	instructionList conditionCode =
		instruction__NB(evaluationAddress, address, zeroAddress, sizeAddress);
	instructionList thenCode =
		instruction::HALT(code::INDEX_OUT_OF_RANGE);
	return instruction::ILOAD(zeroAddress, "0")
	    || instruction::ILOAD(sizeAddress, std::to_string(size))
	    || ifCode(conditionCode, evaluationAddress, thenCode);
}
// ============================================================================

instructionList CodeGenVisitor::ifCode(
		instructionList & conditionCode,
		const std::string & evaluationAddress,
		instructionList & thenCode) {
	const std::string label = codeCounters.newLabelIF();
	return conditionCode
	    || instruction::FJUMP(evaluationAddress, "endif" + label)
	    || thenCode
	    || instruction::LABEL("endif" + label);
}

instructionList CodeGenVisitor::ifCode(
		instructionList & conditionCode,
		const std::string & evaluationAddress,
		instructionList & thenCode,
		instructionList & elseCode) {
	const std::string label = codeCounters.newLabelIF();
	return conditionCode
	    || instruction::FJUMP(evaluationAddress, "else" + label)
	    || thenCode
	    || instruction::UJUMP("endif" + label)
	    || instruction::LABEL("else" + label)
	    || elseCode
	    || instruction::LABEL("endif" + label);
}

instructionList CodeGenVisitor::whileCode(
		instructionList       & conditionCode,
		const std::string     & evaluationAddress,
		instructionList       & doCode) {
	const std::string label = codeCounters.newLabelWHILE();
	return instruction::LABEL("while" + label)
	    || conditionCode
	    || instruction::FJUMP(evaluationAddress, "endwhile" + label)
	    || doCode
	    || instruction::UJUMP("while" + label)
	    || instruction::LABEL("endwhile" + label);
}

// XXX Altres =================================================================
instructionList CodeGenVisitor::doWhileCode(
		instructionList & conditionCode,
		const std::string & evaluationAddress,
		instructionList & doCode) {
	const std::string label = codeCounters.newLabelWHILE();
	return instruction::LABEL("do" + label)
	    || doCode
	    || conditionCode
	    || instruction::FJUMP(evaluationAddress, "endwhile" + label)
	    || instruction::UJUMP("do" + label)
	    || instruction::LABEL("endwhile" + label);
}

instructionList CodeGenVisitor::untilCode(
		instructionList & conditionCode,
		const std::string & evaluationAddress,
		instructionList & doCode) {
	const std::string label = codeCounters.newLabelWHILE();
	return instruction::LABEL("until" + label)
	    || conditionCode
	    || instruction::FJUMP(evaluationAddress, "do" + label)
	    || instruction::UJUMP("enduntil" + label) 
      || instruction::LABEL("do" + label)
      || doCode
      || instruction::UJUMP("until" + label)
      || instruction::LABEL("enduntil" + label);
}

instructionList CodeGenVisitor::doUntilCode(
		instructionList & conditionCode,
		const std::string & evaluationAddress,
		instructionList & doCode) {
	const std::string label = codeCounters.newLabelWHILE();
	return instruction::LABEL("do" + label)
	    || doCode
	    || conditionCode
	    || instruction::FJUMP(evaluationAddress, "do" + label);
}

instructionList CodeGenVisitor::unlessCode(
		instructionList & conditionCode,
		const std::string & evaluationAddress,
		instructionList & doCode) {
	const std::string label = codeCounters.newLabelIF();
	return conditionCode
	    || instruction::FJUMP(evaluationAddress, "then" + label)
	    || instruction::UJUMP("endif" + label)
	    || instruction::LABEL("then" + label)
	    || doCode
	    || instruction::LABEL("endif" + label);
}

instructionList CodeGenVisitor::unlessCode(
		instructionList & conditionCode,
		const std::string & evaluationAddress,
		instructionList & doCode,
		instructionList & elseCode) {
	const std::string label = codeCounters.newLabelIF();
	return conditionCode
	    || instruction::FJUMP(evaluationAddress, "then" + label)
	    || elseCode
	    || instruction::UJUMP("endif" + label)
	    || instruction::LABEL("then" + label)
	    || doCode
	    || instruction::LABEL("endif" + label);
}
// ============================================================================

instructionList CodeGenVisitor::forCode(
		const std::string & counterAddress,
		instructionList & doCode,
		const std::string & startAddress,
		const std::string & stopAddress,
		const std::string & stepAddress) {
	const std::string evaluationAddress = "%" + codeCounters.newTEMP();
	
	instructionList conditionCode =
		instruction::LT(evaluationAddress, counterAddress, stopAddress);
	instructionList whileDoCode =
		doCode ||
		instruction::ADD(counterAddress, counterAddress, stepAddress);
	
	return instruction::LOAD(counterAddress, startAddress)
      || whileCode(conditionCode, evaluationAddress, whileDoCode);
}

instructionList CodeGenVisitor::forCode(
		const std::string & counterAddress,
		instructionList & doCode,
		unsigned int start,
		unsigned int stop,
		unsigned int step) {
	const std::string startAddress = "%" + codeCounters.newTEMP();
	const std::string stopAddress = "%" + codeCounters.newTEMP();
	const std::string stepAddress = "%" + codeCounters.newTEMP();
	return instruction::ILOAD(startAddress, std::to_string(start))
      || instruction::ILOAD(stopAddress, std::to_string(stop))
      || instruction::ILOAD(stepAddress, std::to_string(step))
      || forCode(counterAddress, doCode, startAddress, stopAddress, stepAddress);
}

instructionList CodeGenVisitor::runtimeErrorCode(
		antlr4::ParserRuleContext * context,
		const RuntimeError & error) {
	return instruction::WRITES("\"" + error.message(context) + "\\n\"")
	    || instruction::ILOAD(SUCCESS_ADDRESS, "0")
	    || instruction::UJUMP("end");
}

instructionList CodeGenVisitor::instruction__NE(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3) {
	const std::string a4 = "%" + codeCounters.newTEMP();
	return instruction::EQ(a4, a2, a3) 
	    || instruction::NOT(a1, a4);
}

instructionList CodeGenVisitor::instruction__FNE(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3) {
	const std::string a4 = "%" + codeCounters.newTEMP();
	return instruction::FEQ(a4, a2, a3) 
	    || instruction::NOT(a1, a4);
}

instructionList CodeGenVisitor::instruction__INC(
		const std::string & a1) {
	const std::string a2 = "%" + codeCounters.newTEMP();
	return instruction::ILOAD(a2, "1")
	    || instruction::ADD(a1, a1, a2);
}

instructionList CodeGenVisitor::instruction__DEC(
		const std::string & a1) {
	const std::string a2 = "%" + codeCounters.newTEMP();
	return instruction::ILOAD(a2, "1")
	    || instruction::SUB(a1, a1, a2);
}

instructionList CodeGenVisitor::instruction__B(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3,
		const std::string & a4) {
	const std::string a5 = "%" + codeCounters.newTEMP();
	const std::string a6 = "%" + codeCounters.newTEMP();
	return instruction::LE(a5, a3, a2)
	    || instruction::LT(a6, a2, a4)
	    || instruction::AND(a1, a5, a6);
}

instructionList CodeGenVisitor::instruction__NB(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3,
		const std::string & a4) {
	const std::string a5 = "%" + codeCounters.newTEMP();
	const std::string a6 = "%" + codeCounters.newTEMP();
	return instruction::LT(a5, a2, a3)
	    || instruction::LE(a6, a4, a2)
	    || instruction::OR(a1, a5, a6);
}

instructionList CodeGenVisitor::instruction__MOD(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3) {
	const std::string a4 = "%" + codeCounters.newTEMP();
	const std::string a5 = "%" + codeCounters.newTEMP();
	return instruction::DIV(a4, a2, a3)
	    || instruction::MUL(a5, a4, a3)
	    || instruction::SUB(a1, a2, a5);
}

// XXX 2018 ===================================================================
instructionList CodeGenVisitor::instruction__DOT(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3,
		unsigned int size) {
	const std::string counterAddress = "%" + codeCounters.newTEMP();
	const std::string address1 = "%" + codeCounters.newTEMP();
	const std::string address2 = "%" + codeCounters.newTEMP();
	const std::string address3 = "%" + codeCounters.newTEMP();

	instructionList	doCode =
		instruction::LOADX(address1, a2, counterAddress) ||
		instruction::LOADX(address2, a3, counterAddress) ||
		instruction::MUL(address3, address1, address2) ||
		instruction::ADD(a1, a1, address3);
	instructionList code =
		instruction::ILOAD(a1, "0") ||
		forCode(counterAddress, doCode, 0, size, 1);
	
	return code;
}

instructionList CodeGenVisitor::instruction__FDOT(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3,
		unsigned int size) {
	const std::string counterAddress = "%" + codeCounters.newTEMP();
	const std::string address1 = "%" + codeCounters.newTEMP();
	const std::string address2 = "%" + codeCounters.newTEMP();
	const std::string address3 = "%" + codeCounters.newTEMP();
	
	instructionList doCode =
		instruction::LOADX(address1, a2, counterAddress) ||
		instruction::LOADX(address2, a3, counterAddress) ||
		instruction::FMUL(address3, address1, address2) ||
		instruction::FADD(a1, a1, address3);
	instructionList code =
		instruction::FLOAD(a1, "0.0") ||
		forCode(counterAddress, doCode, 0, size, 1);
	
	return code;
}

// XXX 2021F ==================================================================
instructionList CodeGenVisitor::instruction__POW(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3) {
	/*
	const std::string zeroAddress = "%" + codeCounters.newTEMP();
	const std::string oneAddress = "%" + codeCounters.newTEMP();
	const std::string counterAddress = "%" + codeCounters.newTEMP();
	
	instructionList doCode = instruction::FMUL(a1, a1, a2);
	
	return instruction::FLOAD(a1, "1.0")
	    || instruction::ILOAD(zeroAddress, "0")
	    || instruction::ILOAD(oneAddress, "1")
	    || forCode(counterAddress, doCode, zeroAddress, a3, oneAddress);
	*/
	
	/*
	b ** n = b ** (2 ** k * ak + ... + 2 ** 0 * a0)
		     = b ** (2 ** k * ak) * ... * b ** (2 ** 0 * a0)

	pow(b, n):
		r = 1
		while n > 0
			if n % 2 == 1
				r = r * b
			b = b * b
			n = n / 2
		return r
	
	Cost lineal (en comptes de pseudo-lineal).
	*/

	const std::string zeroAddress = "%" + codeCounters.newTEMP();
	const std::string oneAddress = "%" + codeCounters.newTEMP();
	const std::string twoAddress = "%" + codeCounters.newTEMP();
	const std::string powerAddress = "%" + codeCounters.newTEMP();
	const std::string baseAddress = "%" + codeCounters.newTEMP();
	const std::string ifEvaluationAddress = "%" + codeCounters.newTEMP();
	const std::string whileEvaluationAddress = "%" + codeCounters.newTEMP();
	const std::string address = "%" + codeCounters.newTEMP();
	
	instructionList ifConditionCode =
		instruction__MOD(address, powerAddress, twoAddress) ||
		instruction::EQ(ifEvaluationAddress, address, oneAddress);
	instructionList thenCode =
		instruction::FMUL(a1, a1, baseAddress);
	instructionList whileConditionCode =
		instruction::LT(whileEvaluationAddress, zeroAddress, powerAddress);
	instructionList doCode =
		ifCode(ifConditionCode, ifEvaluationAddress, thenCode) ||
		instruction::FMUL(baseAddress, baseAddress, baseAddress) ||
		instruction::DIV(powerAddress, powerAddress, twoAddress);
	
	return instruction::FLOAD(a1, "1.0")
	    || instruction::ILOAD(zeroAddress, "0")
	    || instruction::ILOAD(oneAddress, "1")
	    || instruction::ILOAD(twoAddress, "2")
	    || instruction::LOAD(baseAddress, a2)
	    || instruction::LOAD(powerAddress, a3)
	    || whileCode(whileConditionCode, whileEvaluationAddress, doCode);
}

TypesMgr::TypeId CodeGenVisitor::getCurrentFunctionTy() const {
	return currFunctionType;
}

void CodeGenVisitor::setCurrentFunctionTy(TypesMgr::TypeId type) {
	currFunctionType = type;
}

SymTable::ScopeId CodeGenVisitor::getScopeDecor(antlr4::ParserRuleContext * ctx) const {
	return Decorations.getScope(ctx);
}
TypesMgr::TypeId CodeGenVisitor::getTypeDecor(antlr4::ParserRuleContext * ctx) const {
	return Decorations.getType(ctx);
}

CodeGenVisitor::CodeAttribs::CodeAttribs(
		const std::string & addr,
		const std::string & offs,
		instructionList & code) :
	addr{addr}, offs{offs}, code{code} {
}

CodeGenVisitor::CodeAttribs::CodeAttribs(
		const std::string & addr,
		const std::string & offs,
		instructionList && code) :
	addr{addr}, offs{offs}, code{code} {
}
