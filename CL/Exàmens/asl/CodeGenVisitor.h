//////////////////////////////////////////////////////////////////////
//
//    CodeGenVisitor - Walk the parser tree to do
//                     the generation of code
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
//////////////////////////////////////////////////////////////////////

#pragma once
#include "antlr4-runtime.h"
#include "AslBaseVisitor.h"
#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/code.h"
#include <string>

class CodeGenVisitor final : public AslBaseVisitor {
	public:
		CodeGenVisitor(
			TypesMgr & Types,
			SymTable & Symbols,
			TreeDecoration & Decorations);

		antlrcpp::Any visitProgram(AslParser::ProgramContext * ctx);
		antlrcpp::Any visitFunction(AslParser::FunctionContext * ctx);
		antlrcpp::Any visitStatements(AslParser::StatementsContext * ctx);
		antlrcpp::Any visitReturn(AslParser::ReturnContext * ctx);
		antlrcpp::Any visitRead(AslParser::ReadContext * ctx);
		antlrcpp::Any visitWriteExpression(AslParser::WriteExpressionContext * ctx);
		antlrcpp::Any visitWriteString(AslParser::WriteStringContext * ctx);
		antlrcpp::Any visitIf(AslParser::IfContext * ctx);
		antlrcpp::Any visitWhile(AslParser::WhileContext * ctx);
		antlrcpp::Any visitProcedureCall(AslParser::ProcedureCallContext * ctx);
		antlrcpp::Any visitAssignment(AslParser::AssignmentContext * ctx);
		antlrcpp::Any visitFunctionCall(AslParser::FunctionCallContext * ctx);
		antlrcpp::Any visitCall(AslParser::CallContext * ctx);
		antlrcpp::Any visitParenthesization(AslParser::ParenthesizationContext * ctx);
		antlrcpp::Any visitBooleanUnaryOperation(AslParser::BooleanUnaryOperationContext * ctx);
		antlrcpp::Any visitBooleanBinaryOperation(AslParser::BooleanBinaryOperationContext * ctx);
		antlrcpp::Any visitArithmeticUnaryOperation(AslParser::ArithmeticUnaryOperationContext * ctx);
		antlrcpp::Any visitArithmeticBinaryOperation(AslParser::ArithmeticBinaryOperationContext * ctx);
		antlrcpp::Any visitRelationalOperation(AslParser::RelationalOperationContext * ctx);
		antlrcpp::Any visitLiteralValue(AslParser::LiteralValueContext * ctx);
		antlrcpp::Any visitLiteral(AslParser::LiteralContext * ctx);
		antlrcpp::Any visitBasicLiteral(AslParser::BasicLiteralContext * ctx);
		antlrcpp::Any visitLeftExpressionValue(AslParser::LeftExpressionValueContext * ctx);
		antlrcpp::Any visitElement(AslParser::ElementContext * ctx);
		antlrcpp::Any visitArrayElement(AslParser::ArrayElementContext * ctx);
		antlrcpp::Any visitId(AslParser::IdContext * ctx);
		// XXX 2018 ================================================================
		antlrcpp::Any visitPairElement(AslParser::PairElementContext * ctx);
		// XXX 2019 ================================================================
		antlrcpp::Any visitMaxAttribute(AslParser::MaxAttributeContext * ctx);
		// XXX 2020P ===============================================================
		antlrcpp::Any visitMaxCall(AslParser::MaxCallContext * ctx);
		antlrcpp::Any visitFor(AslParser::ForContext * ctx);
		// XXX 2020F ===============================================================
		antlrcpp::Any visitSumCall(AslParser::SumCallContext * ctx);
		antlrcpp::Any visitFilter(AslParser::FilterContext * ctx);
		// XXX 2021P ===============================================================
		antlrcpp::Any visitPack(AslParser::PackContext * ctx);
		antlrcpp::Any visitUnpack(AslParser::UnpackContext * ctx);
		antlrcpp::Any visitTupleElement(AslParser::TupleElementContext * ctx);
		// XXX 2021F ===============================================================
		antlrcpp::Any visitMap(AslParser::MapContext * ctx);
		// XXX 2022P ===============================================================		
		antlrcpp::Any visitArrayMap(AslParser::ArrayMapContext * ctx);
		antlrcpp::Any visitStructElement(AslParser::StructElementContext * ctx);
		// XXX 2022F ===============================================================		
		antlrcpp::Any visitFactorial(AslParser::FactorialContext * ctx);
		antlrcpp::Any visitMatrixElement(AslParser::MatrixElementContext * ctx);
		// XXX Altres =============================================================
		antlrcpp::Any visitUnless(AslParser::UnlessContext * ctx);
		antlrcpp::Any visitDoUntil(AslParser::DoUntilContext * ctx);
		antlrcpp::Any visitDoWhile(AslParser::DoWhileContext * ctx);
		antlrcpp::Any visitUntil(AslParser::UntilContext * ctx);
		// ========================================================================

	private:
		TypesMgr & Types;
		SymTable & Symbols;
		TreeDecoration & Decorations;
		counters codeCounters;
		TypesMgr::TypeId currFunctionType;

		std::string coerce(
			instructionList & code,
			TypesMgr::TypeId destinationType,
			TypesMgr::TypeId sourceType,
			const std::string & sourceAddress);
		
		std::string reference(
			instructionList & code,
			const std::string & name);
		
		std::string dereference(
			instructionList & code,
			const std::string & name);
		
		std::string load(
			instructionList & code,
			const std::string & sourceAddress,
			const std::string & offsetAddress);
		
		void store(
			instructionList & code,
			const std::string & destinationAddress,
			const std::string & offsetAddress,
			const std::string & sourceAddress);
		
		std::string call(
	 		instructionList & code,
			const std::string & functionName,
			std::vector <TypesMgr::TypeId> argumentTypes,
			std::vector <std::string> argumentAddresses);
		
		instructionList copyArrayCode(
			TypesMgr::TypeId destinationType,
			TypesMgr::TypeId sourceType,
			const std::string & destinationAddress,
			const std::string & sourceAddress);
		// XXX 2022F ==============================================================			
		instructionList copyMatrixCode(
			TypesMgr::TypeId destinationType,
			TypesMgr::TypeId sourceType,
			const std::string & destinationAddress,
			const std::string & sourceAddress);
		
		instructionList outOfRangeCode(
			const std::string & address,
			unsigned int size);
		// ========================================================================
		
		instructionList forCode(
			const std::string & counterAddress,
			instructionList & doCode,
			unsigned int start,
			unsigned int stop,
			unsigned int step);

		instructionList forCode(
			const std::string & counterAddress,
			instructionList & bodyCode,
			const std::string & startAddress,
			const std::string & stopAddress,
			const std::string & stepAddress);
		
		instructionList whileCode(
			instructionList & conditionCode,
			const std::string & evaluationAddress,
			instructionList & doCode);
		
		// XXX Altres =============================================================
		instructionList doWhileCode(
			instructionList & conditionCode,
			const std::string & evaluationAddress,
			instructionList & doCode);

		instructionList untilCode(
			instructionList & condition,
			const std::string & evaluationAddress,
			instructionList & doCode);

		instructionList doUntilCode(
			instructionList & condition,
			const std::string & evaluationAddress,
			instructionList & doCode);

		instructionList unlessCode(
			instructionList & conditionCode,
			const std::string & evaluationAddress,
			instructionList & doCode);

		instructionList unlessCode(
			instructionList & conditionCode,
			const std::string & evaluationAddress,
			instructionList & doCode,
			instructionList & elseCode);
		// ========================================================================

		instructionList ifCode(
			instructionList & conditionCode,
			const std::string & evaluationAddress,
			instructionList & thenCode);

		instructionList ifCode(
			instructionList & conditionCode,
			const std::string & evaluationAddress,
			instructionList & doCode,
			instructionList & elseCode);

		class RuntimeError {
			public:
				virtual std::string message(antlr4::ParserRuleContext * context) const {
					const std::string line = std::to_string(context->getStart()->getLine());
					const std::string column = std::to_string(context->getStart()->getCharPositionInLine());
					return "Runtime Error: Line " + line + ":" + column + ".";
				};
		};
		
		// XXX 2019 ================================================================
		class IndexError : public RuntimeError {
			public:
				std::string message(antlr4::ParserRuleContext * context) const {
					return RuntimeError::message(context)
					     + " Access out of range: " + context->getText();
				}
		};
		// ========================================================================

		instructionList runtimeErrorCode(
			antlr4::ParserRuleContext * context,
			const RuntimeError & error);

		// a1 = not (a2 == a3)
		instructionList instruction__NE(
			const std::string & a1,
			const std::string & a2,
			const std::string & a3);
		
		// a1 = not (a2 ==. a3)
		instructionList instruction__FNE(
			const std::string & a1,
			const std::string & a2,
			const std::string & a3);
		
		// a1 = a1 + 1
		instructionList instruction__INC(
			const std::string & a1);
		
		// a1 = a1 - 1
		instructionList instruction__DEC(
			const std::string & a1);
		
		// a1 = (a3 <= a2) and (a2 < a4)
		instructionList instruction__B(
			const std::string & a1,
			const std::string & a2,
			const std::string & a3,
			const std::string & a4);
		
		// a1 = (a2 < a3) or (a4 <= a2)
		instructionList instruction__NB(
			const std::string & a1,
			const std::string & a2,
			const std::string & a3,
			const std::string & a4);
		
		// a1 = a2 % a3 = a2 - a3 * (a2 / a3)
		instructionList instruction__MOD(
			const std::string & a1,
			const std::string & a2,
			const std::string & a3);
		
		// XXX 2018 ===================================================================		
		instructionList instruction__DOT(
			const std::string & a1,
			const std::string & a2,
			const std::string & a3,
			unsigned int size);
		
		instructionList instruction__FDOT(
			const std::string & a1,
			const std::string & a2,
			const std::string & a3,
			unsigned int size);
		
		// XXX 2021F ==================================================================
		// a1 = a2 ** a3
		instructionList instruction__POW(
			const std::string & a1,
			const std::string & a2,
			const std::string & a3);
		// =========================================================================

		TypesMgr::TypeId getCurrentFunctionTy() const;
		void setCurrentFunctionTy(TypesMgr::TypeId type);
		
		SymTable::ScopeId getScopeDecor(antlr4::ParserRuleContext * ctx) const;
		TypesMgr::TypeId getTypeDecor(antlr4::ParserRuleContext * ctx) const;

		class CodeAttribs {
			public:
				std::string addr;
				std::string offs;
				instructionList code;
				
				CodeAttribs(
					const std::string & addr,
					const std::string & offs,
					instructionList & code);
				CodeAttribs(
					const std::string & addr,
					const std::string & offs,
					instructionList && code);
		};
};
