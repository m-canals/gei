//////////////////////////////////////////////////////////////////////
//
//    TypeCheckVisitor - Walk the parser tree to do the semantic
//                       typecheck for the Asl programming language
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library //is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
//////////////////////////////////////////////////////////////////////

#include "TypeCheckVisitor.h"
#include "antlr4-runtime.h"
#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/SemErrors.h"
#include <iostream>
#include <string>
//#define DEBUG_BUILD
#include "../common/debug.h"
#include <cstdarg>
#include <vector>

TypeCheckVisitor::TypeCheckVisitor(
		TypesMgr & Types,
		SymTable & Symbols,
		TreeDecoration & Decorations,
		SemErrors & Errors) :
	Types{Types},
	Symbols{Symbols},
	Decorations{Decorations},
	Errors{Errors} {
}

antlrcpp::Any TypeCheckVisitor::visitProgram(AslParser::ProgramContext * ctx) {
	DEBUG_ENTER();
	SymTable::ScopeId scope = getScopeDecor(ctx);
	Symbols.pushThisScope(scope);
	
	for (const auto & function : ctx->function())
		visit(function);
	
	if (Symbols.noMainProperlyDeclared())
		Errors.noMainProperlyDeclared(ctx);
	
	Symbols.popScope();
	Errors.print();
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitFunction(AslParser::FunctionContext * ctx) {
	DEBUG_ENTER();
	SymTable::ScopeId scope = getScopeDecor(ctx);
	TypesMgr::TypeId type = getTypeDecor(ctx);
	Symbols.pushThisScope(scope);
	setCurrentFunctionTy(type);
	visit(ctx->variables());
	visit(ctx->statements());
	//Symbols.print();
	print("%_func %t\n\n", type);
	Symbols.popScope();
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitVariable(AslParser::VariableContext * ctx) {
	DEBUG_ENTER();
	TypesMgr::TypeId type = getTypeDecor(ctx->type());
	print("%_var %t\n", type);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitReturn(AslParser::ReturnContext * ctx) {
	DEBUG_ENTER();
	TypesMgr::TypeId functionType = getCurrentFunctionTy();
	TypesMgr::TypeId t1 = Types.getFuncReturnType(functionType);
	TypesMgr::TypeId t2;
	
	if (ctx->expression()) {
		visit(ctx->expression());
		t2 = getTypeDecor(ctx->expression());
	} else {
		t2 = Types.createVoidTy();
	}
	
	if (not Types.isErrorTy(t2) and not Types.copyableTypes(t1, t2))
		Errors.incompatibleReturn(ctx->RETURN());
	
	print("%_return %t\n", t2);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitRead(AslParser::ReadContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->leftExpression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->leftExpression());
	
	if (not Types.isErrorTy(t1)) {
		if (not Types.isPrimitiveTy(t1) and not Types.isFunctionTy(t1))
			Errors.readWriteRequireBasic(ctx);
		if (not getIsLValueDecor(ctx->leftExpression()))
			Errors.nonReferenceableExpression(ctx);
	}
	
	print("%_read %t\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitWriteExpression(AslParser::WriteExpressionContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isPrimitiveTy(t1))
		Errors.readWriteRequireBasic(ctx);
	
	print("%_write %t\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitIf(AslParser::IfContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isBooleanTy(t1))
		Errors.booleanRequired(ctx);
	
	visit(ctx->statements(0));
	if (ctx->statements(1))
		visit(ctx->statements(1));
	
	print("%_if %t\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitWhile(AslParser::WhileContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isBooleanTy(t1))
		Errors.booleanRequired(ctx);
	
	visit(ctx->statements());
	
	print("%_while %t\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitAssignment(AslParser::AssignmentContext * ctx) {
	DEBUG_ENTER();
	const auto & leftExpressionContexts = ctx->leftExpressions()->leftExpression();
	const auto & expressionContexts = ctx->expressions()->expression();
	std::vector <TypesMgr::TypeId> leftExpressionTypes, expressionTypes;
	
	for (const auto & context : leftExpressionContexts) {
		visit(context);
		TypesMgr::TypeId t1 = getTypeDecor(context);
		leftExpressionTypes.push_back(t1);
	}
	
	for (const auto & context : expressionContexts) {
		visit(context);
		TypesMgr::TypeId t1 = getTypeDecor(context);
		expressionTypes.push_back(t1);
	}
	
	if (leftExpressionContexts.size() == expressionContexts.size()) {
		for (unsigned int i = 0; i < leftExpressionContexts.size(); i++) {
			TypesMgr::TypeId t1 = leftExpressionTypes[i];
			TypesMgr::TypeId t2 = expressionTypes[i];
			if (not Types.isErrorTy(t1) and
					not getIsLValueDecor(leftExpressionContexts[i]))
				Errors.nonReferenceableLeftExpr(leftExpressionContexts[i]);
			if (not Types.isErrorTy(t1) and not Types.isErrorTy(t2) and
					not Types.copyableTypes(t1, t2))
				Errors.incompatibleAssignment(ctx->ASSIGN());
		}
	} else {
		Errors.numberOfAssignments(ctx);
	}

	print("%_%T = %T\n", leftExpressionTypes, expressionTypes);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitCall(AslParser::CallContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id());
	std::vector <AslParser::ExpressionContext *> expressions;
	std::vector <TypesMgr::TypeId> expressionTypes;

	if (ctx->expressions()) {
		expressions = ctx->expressions()->expression();
		for (const auto & expression : expressions) {
			visit(expression);
			TypesMgr::TypeId expressionType = getTypeDecor(expression);
			expressionTypes.push_back(expressionType);
		}
	}
	
	if (Types.isErrorTy(t1)) {
		putTypeDecor(ctx, t1);
	} else if (Types.isFunctionTy(t1)) {
		if (expressions.size() != Types.getNumOfParameters(t1)) {
			Errors.numberOfParameters(ctx);
		} else {
			for (unsigned int i = 0; i < expressions.size(); i++) {
				TypesMgr::TypeId t2 = Types.getParameterType(t1, i);
				TypesMgr::TypeId t3 = expressionTypes[i];
				if (not Types.isErrorTy(t3) and not Types.copyableTypes(t2, t3))
					Errors.incompatibleParameter(expressions[i], i + 1, ctx);
			}
		}
		putTypeDecor(ctx, Types.getFuncReturnType(t1));
	} else {
		Errors.isNotCallable(ctx->id());
		putTypeDecor(ctx, Types.createErrorTy());
	}

	print("%_%t (%T)\n", t1, expressionTypes);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitFunctionCall(AslParser::FunctionCallContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->call());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->call());
	
	if (not Types.isErrorTy(t1) and Types.isVoidTy(t1)) {
		Errors.isNotFunction(ctx);
		putTypeDecor(ctx, Types.createErrorTy());
	} else {
		putTypeDecor(ctx, t1);
	}
	
	putIsLValueDecor(ctx, false);
	
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitParenthesization(AslParser::ParenthesizationContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	putTypeDecor(ctx, t1);
	putIsLValueDecor(ctx, false);
	
	print("%_(%t)\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitBooleanUnaryOperation(AslParser::BooleanUnaryOperationContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isBooleanTy(t1))
		Errors.incompatibleOperator(ctx->op);
	
	putTypeDecor(ctx, Types.createBooleanTy());
	putIsLValueDecor(ctx, false);
	
	print("%_%o %t\n", ctx->op, t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitBooleanBinaryOperation(AslParser::BooleanBinaryOperationContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression(0));
	visit(ctx->expression(1));
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression(0));
	TypesMgr::TypeId t2 = getTypeDecor(ctx->expression(1));
	
	if ((not Types.isErrorTy(t1) and not Types.isBooleanTy(t1)) or
	    (not Types.isErrorTy(t2) and not Types.isBooleanTy(t2)))
		Errors.incompatibleOperator(ctx->op);
	
	putTypeDecor(ctx, Types.createBooleanTy());
	putIsLValueDecor(ctx, false);
	
	print("%_%t %o %t\n", t1, ctx->op, t2);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitArithmeticUnaryOperation(AslParser::ArithmeticUnaryOperationContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isNumericTy(t1)) {
		Errors.incompatibleOperator(ctx->op);
		putTypeDecor(ctx, Types.createErrorTy());
	} else {
		putTypeDecor(ctx, t1);
	}
	
	putIsLValueDecor(ctx, false);

	print("%_%o %t\n", ctx->op, t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitArithmeticBinaryOperation(AslParser::ArithmeticBinaryOperationContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression(0));
	visit(ctx->expression(1));
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression(0));
	TypesMgr::TypeId t2 = getTypeDecor(ctx->expression(1));

	if (ctx->MUL()) {
		if (Types.isErrorTy(t1) and Types.isErrorTy(t2))
			putTypeDecor(ctx, Types.createIntegerTy());
		else if (Types.isErrorTy(t1) and Types.isIntegerTy(t2))
			putTypeDecor(ctx, Types.createIntegerTy());
		else if (Types.isIntegerTy(t1) and Types.isErrorTy(t2))
			putTypeDecor(ctx, Types.createIntegerTy());
		else if (Types.isIntegerTy(t1) and Types.isIntegerTy(t2))
			putTypeDecor(ctx, Types.createIntegerTy());
		else if (Types.isErrorTy(t1) and Types.isFloatTy(t2))
			putTypeDecor(ctx, Types.createFloatTy());
		else if (Types.isFloatTy(t1) and Types.isErrorTy(t2))
			putTypeDecor(ctx, Types.createFloatTy());
		else if (Types.isFloatTy(t1) and Types.isFloatTy(t2))
			putTypeDecor(ctx, Types.createFloatTy());
		else if (Types.isIntegerTy(t1) and Types.isFloatTy(t2))
			putTypeDecor(ctx, Types.createFloatTy());
		else if (Types.isFloatTy(t1) and Types.isIntegerTy(t2))
			putTypeDecor(ctx, Types.createFloatTy());
		// XXX 2018 ===============================================================
		else if (Types.isErrorTy(t1) and Types.isArrayTy(t2) and
		         Types.isIntegerTy(Types.getArrayElemType(t2)))
			putTypeDecor(ctx, Types.createIntegerTy());
		else if (Types.isErrorTy(t2) and Types.isArrayTy(t1) and
		         Types.isIntegerTy(Types.getArrayElemType(t1)))
			putTypeDecor(ctx, Types.createIntegerTy());
		else if (Types.isArrayTy(t1) and Types.isIntegerTy(Types.getArrayElemType(t1)) and
		         Types.isArrayTy(t2) and Types.isIntegerTy(Types.getArrayElemType(t2)) and
		         Types.getArraySize(t1) == Types.getArraySize(t2))
			putTypeDecor(ctx, Types.createIntegerTy());
		else if (Types.isErrorTy(t1) and Types.isArrayTy(t2) and
		         Types.isFloatTy(Types.getArrayElemType(t2)))
			putTypeDecor(ctx, Types.createFloatTy());
		else if (Types.isErrorTy(t2) and Types.isArrayTy(t1) and
		         Types.isFloatTy(Types.getArrayElemType(t1)))
			putTypeDecor(ctx, Types.createFloatTy());
		else if (Types.isArrayTy(t1) and Types.isFloatTy(Types.getArrayElemType(t1)) and
		         Types.isArrayTy(t2) and Types.isFloatTy(Types.getArrayElemType(t2)) and
		         Types.getArraySize(t1) == Types.getArraySize(t2))
			putTypeDecor(ctx, Types.createFloatTy());
		// ========================================================================
		else {
			Errors.incompatibleOperator(ctx->op);
			putTypeDecor(ctx, Types.createIntegerTy());
		}
	} else if (ctx->DIV() or ctx->PLUS() or ctx->MINUS()) {
		if ((not Types.isErrorTy(t1) and not Types.isNumericTy(t1)) or
		    (not Types.isErrorTy(t2) and not Types.isNumericTy(t2)))
			Errors.incompatibleOperator(ctx->op);
		if (Types.isFloatTy(t1) or Types.isFloatTy(t2))
			putTypeDecor(ctx, Types.createFloatTy());
		else
			putTypeDecor(ctx, Types.createIntegerTy());
	// XXX 2021F ================================================================
	} else if(ctx->EXP()) {
		if ((not Types.isErrorTy(t1) and not Types.isNumericTy(t1)) or
		    (not Types.isErrorTy(t2) and not Types.isIntegerTy(t2)))
			Errors.incompatibleOperator(ctx->op);
		putTypeDecor(ctx, Types.createFloatTy());
	// ==========================================================================
	} else if (ctx->MOD()) {
		if ((not Types.isErrorTy(t1) and not Types.isIntegerTy(t1)) or
		    (not Types.isErrorTy(t2) and not Types.isIntegerTy(t2)))
			Errors.incompatibleOperator(ctx->op);
		putTypeDecor(ctx, Types.createIntegerTy());
	}
	
	putIsLValueDecor(ctx, false);
	
	print("%_%t %o %t\n", t1, ctx->op, t2);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitRelationalOperation(AslParser::RelationalOperationContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression(0));
	visit(ctx->expression(1));
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression(0));
	TypesMgr::TypeId t2 = getTypeDecor(ctx->expression(1));
	
	if (not Types.isErrorTy(t1) and not Types.isErrorTy(t2) and
	    not Types.comparableTypes(t1, t2, ctx->op->getText()))
		Errors.incompatibleOperator(ctx->op);
	
	putTypeDecor(ctx, Types.createBooleanTy());
	putIsLValueDecor(ctx, false);
	
	print("%_%t %o %t\n", t1, ctx->op, t2);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitLiteralValue(AslParser::LiteralValueContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->literal());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->literal());
	putTypeDecor(ctx, t1);
	putIsLValueDecor(ctx, false);
	print("%_%t\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitLiteral(AslParser::LiteralContext * ctx) {
	DEBUG_ENTER();
	TypesMgr::TypeId t1;
	
	if (ctx->basicLiteral()) {
		visit(ctx->basicLiteral());
		t1 = getTypeDecor(ctx->basicLiteral());
	}
	
	putTypeDecor(ctx, t1);
	putIsLValueDecor(ctx, false);
	
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitBasicLiteral(AslParser::BasicLiteralContext * ctx) {
	DEBUG_ENTER();
	TypesMgr::TypeId t1;
	
	if (ctx->BOOLEAN_VALUE()) t1 = Types.createBooleanTy();
	else if (ctx->CHARACTER()) t1 = Types.createCharacterTy();
	else if (ctx->NATURAL_NUMBER()) t1 = Types.createIntegerTy();
	else if (ctx->REAL_NUMBER()) t1 = Types.createFloatTy();
	
	putTypeDecor(ctx, t1);
	putIsLValueDecor(ctx, false);
	
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitLeftExpressionValue(AslParser::LeftExpressionValueContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->leftExpression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->leftExpression());
	bool l1 = getIsLValueDecor(ctx->leftExpression());
	
	putTypeDecor(ctx, t1);
	putIsLValueDecor(ctx, l1);
	
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitElement(AslParser::ElementContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id());
	bool l1 = getIsLValueDecor(ctx->id());
	
	putTypeDecor(ctx, t1);
	putIsLValueDecor(ctx, l1);
	
	print("%_%t\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitArrayElement(AslParser::ArrayElementContext * ctx) {
	DEBUG_ENTER();
	// Array.
	visit(ctx->id());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id());
	bool l1 = getIsLValueDecor(ctx->id());
	
	if (Types.isErrorTy(t1)) {
		putTypeDecor(ctx, t1);
	} else if (Types.isArrayTy(t1)) {
		putTypeDecor(ctx, Types.getArrayElemType(t1));
	} else {
		Errors.nonArrayInArrayAccess(ctx->id());
		putTypeDecor(ctx, Types.createErrorTy());
	}

	putIsLValueDecor(ctx, l1);

	// Index.
	visit(ctx->expression());
	TypesMgr::TypeId t2 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t2) and not Types.isIntegerTy(t2)) {
		Errors.nonIntegerIndexInArrayAccess(ctx->expression());
	}

	print("%_%t [%t]\n", t1, t2);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitId(AslParser::IdContext * ctx) {
	DEBUG_ENTER();
	std::string name = ctx->getText();
	
	if (Symbols.findInStack(name) == -1) {
		Errors.undeclaredIdent(ctx->ID());
		putTypeDecor(ctx, Types.createErrorTy());
		putIsLValueDecor(ctx, true);
	} else {
		putTypeDecor(ctx, Symbols.getType(name));
		putIsLValueDecor(ctx, not Symbols.isFunctionClass(name));
	}
	
	DEBUG_EXIT();
	return 0;
}

// XXX 2018 ===================================================================
antlrcpp::Any TypeCheckVisitor::visitPairElement(AslParser::PairElementContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id());
	const std::string element = ctx->PAIR_ELEMENT()->getText();
	bool l1 = getIsLValueDecor(ctx->id());

	if (Types.isErrorTy(t1)) {
		putTypeDecor(ctx, t1);
	} else if (Types.isPairTy(t1)) {
		if (element == "first")
			putTypeDecor(ctx, Types.getFirstPairType(t1));
		else if (element == "second")
			putTypeDecor(ctx, Types.getSecondPairType(t1));
	} else {
		Errors.nonPairInPairAccess(ctx->id());
		putTypeDecor(ctx, Types.createErrorTy());
	}

	putIsLValueDecor(ctx, l1);
	
	print("%_%t.%s\n", t1, element);
	DEBUG_EXIT();
	return 0;
}

// XXX 2019 ===================================================================
antlrcpp::Any TypeCheckVisitor::visitMaxAttribute(AslParser::MaxAttributeContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id());
	TypesMgr::TypeId t2 = Types.isArrayTy(t1) ? Types.getArrayElemType(t1) : Types.createErrorTy();
	
	if (not Types.isErrorTy(t1) and not (Types.isArrayTy(t1) and
	 (Types.isCharacterTy(t2) or Types.isNumericTy(t2))))
		Errors.incompatibleOperator(ctx->op);
	
	putTypeDecor(ctx, t2);
	putIsLValueDecor(ctx, false);
	
	print("%_%t.max\n", t1);
	DEBUG_EXIT();
	return 0;
}

// XXX 2020P ==================================================================
antlrcpp::Any TypeCheckVisitor::visitFor(AslParser::ForContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id());
	TypesMgr::TypeId type = getTypeDecor(ctx->id());
	std::vector <TypesMgr::TypeId> types;
	
	if (not Types.isErrorTy(type) and not Types.isIntegerTy(type))
		Errors.forRequireIntegerVar(ctx->id());

	if (ctx->expressions()) {
		for (const auto & context : ctx->expressions()->expression()) {
			visit(context);
			TypesMgr::TypeId argumentType = getTypeDecor(context);
			types.push_back(argumentType);
			
			if (not Types.isErrorTy(argumentType) and not Types.isIntegerTy(argumentType))
				Errors.forRequireIntegerExpr(context);
		}
	}

	const auto argumentCount = types.size();

	if (argumentCount < 1 or argumentCount > 3)
		Errors.numberOfRangeExpressions(ctx);

	visit(ctx->statements());

	print("%_for %t in range(%T)\n", type, types);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitMaxCall(AslParser::MaxCallContext * ctx) {
	DEBUG_ENTER();
	std::vector <TypesMgr::TypeId> types;

	if (ctx->expressions()) {
		for (const auto & context : ctx->expressions()->expression()) {
			visit(context);
			TypesMgr::TypeId type = getTypeDecor(context);
			types.push_back(type);
		}
	}
	
	unsigned int argumentCount = types.size();
	TypeCheck typeCheck = checkTypes(types);

	if (argumentCount < 2) {
		Errors.numberOfMaxArguments(ctx);
		putTypeDecor(ctx, Types.createErrorTy());
	} else if (typeCheck.allCharacter) {
		putTypeDecor(ctx, Types.createCharacterTy());
	} else if (typeCheck.allInteger) {
		putTypeDecor(ctx, Types.createIntegerTy());
	} else if (typeCheck.allNumeric) {
		putTypeDecor(ctx, Types.createFloatTy());
	} else {
		Errors.incompatibleMaxArguments(ctx);
		putTypeDecor(ctx, Types.createErrorTy());
	}

	putIsLValueDecor(ctx, false);

	print("%_max(%T)\n", types);
	DEBUG_EXIT();
	return 0;
}

// XXX 2020F ==================================================================
antlrcpp::Any TypeCheckVisitor::visitSumCall(AslParser::SumCallContext * ctx) {
	DEBUG_ENTER();
	std::vector <TypesMgr::TypeId> types;
	
	if (ctx->expressions()) {
		for (const auto & context : ctx->expressions()->expression()) {
			visit(context);
			TypesMgr::TypeId type = getTypeDecor(context);
			types.push_back(type);
		}
	}
	
	unsigned int argumentCount = types.size();
	TypeCheck typeCheck = checkTypes(types);

	if (argumentCount == 0) {
		putTypeDecor(ctx, Types.createIntegerTy());
	} else if (typeCheck.allInteger) {
		putTypeDecor(ctx, Types.createIntegerTy());
	} else if (typeCheck.allNumeric) {
		putTypeDecor(ctx, Types.createFloatTy());
	} else {
		Errors.incompatibleOperator(ctx->op);
		putTypeDecor(ctx, Types.createIntegerTy());
	}

	putIsLValueDecor(ctx, false);
	
	print("%_sum(%T)\n", types);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitFilter(AslParser::FilterContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id(0));
	visit(ctx->id(1));
	visit(ctx->id(2));
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id(0));
	TypesMgr::TypeId t2 = getTypeDecor(ctx->id(1));
	TypesMgr::TypeId t3 = getTypeDecor(ctx->id(2));

	if ((not Types.isErrorTy(t1) and not Types.isArrayTy(t1)) ||
	    (not Types.isErrorTy(t2) and not Types.isArrayTy(t2)) ||
	    (Types.isArrayTy(t2) and not Types.isBooleanTy(Types.getArrayElemType(t2))) ||
	    (Types.isArrayTy(t1) and Types.isArrayTy(t2) and Types.getArraySize(t1) != Types.getArraySize(t2)) ||
	    (not Types.isErrorTy(t3) and not Types.isFunctionTy(t3)) ||
	    (Types.isFunctionTy(t3) and not Types.isBooleanTy(Types.getFuncReturnType(t3))) ||
	    (Types.isFunctionTy(t3) and Types.getNumOfParameters(t3) != 1) ||
	    (Types.isArrayTy(t1) and Types.isFunctionTy(t3) and not Types.copyableTypes(Types.getParameterType(t3, 0), Types.getArrayElemType(t1))))
		Errors.incompatibleOperator(ctx->op);

	putTypeDecor(ctx, Types.createIntegerTy());
	
	print("%_filter %t into %t using %t\n", t1, t2, t3);
	DEBUG_EXIT();
	return 0;
}

// XXX 2021P ==================================================================
antlrcpp::Any TypeCheckVisitor::visitPack(AslParser::PackContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id());
	std::vector <TypesMgr::TypeId> t;

	for (const auto & context : ctx->expressions()->expression()) {
		visit(context);
		t.push_back(getTypeDecor(context));
	}
	
	if (not Types.isErrorTy(t1) and not Types.isArrayTy(t1)) {
		Errors.packUnpackWithNonArray(ctx);
	} else if (Types.isArrayTy(t1)) {
		TypesMgr::TypeId t2 = Types.getArrayElemType(t1);
		if (t.size() != Types.getArraySize(t1))
			Errors.packUnpackNumberOfItemsMismatch(ctx);
		for (unsigned int i = 0; i < t.size(); i++)
			if (not Types.isErrorTy(t[i]) and not Types.copyableTypes(t2, t[i]))
				Errors.packUnpackIncompatibleTypes(ctx, i + 1);
	}

	print("%_pack %T into %t\n", t, t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitUnpack(AslParser::UnpackContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id());
	std::vector <TypesMgr::TypeId> t;
	
	for (const auto & context : ctx->leftExpressions()->leftExpression()) {
		visit(context);
		t.push_back(getTypeDecor(context));
	}
	
	if (not Types.isErrorTy(t1) and not Types.isArrayTy(t1)) {
		Errors.packUnpackWithNonArray(ctx);
	} else if (Types.isArrayTy(t1)) {
		TypesMgr::TypeId t2 = Types.getArrayElemType(t1);
		if (t.size() != Types.getArraySize(t1))
			Errors.packUnpackNumberOfItemsMismatch(ctx);
		for (unsigned int i = 0; i < t.size(); i++)
			if (not Types.isErrorTy(t[i]) and not Types.copyableTypes(t[i], t2))
				Errors.packUnpackIncompatibleTypes(ctx, i + 1);
	}

	print("%_unpack %t into %T\n", t1, t);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitTupleElement(AslParser::TupleElementContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id());
	bool l1 = getIsLValueDecor(ctx->id());

	if (not Types.isErrorTy(t1) and not Types.isTupleTy(t1)) {
		Errors.nonTupleInTupleAccess(ctx);
		putTypeDecor(ctx, Types.createErrorTy());
	} else if (Types.isTupleTy(t1)) {
		unsigned int field = std::stoul(ctx->NATURAL_NUMBER()->getText());
		if (field >= Types.getTupleSize(t1)) {
			Errors.nonExistentFieldInTuple(ctx);
			putTypeDecor(ctx, Types.createErrorTy());
		} else {
			TypesMgr::TypeId t2 = Types.getTupleFieldType(t1, field);
			putTypeDecor(ctx, t2);
		}
	}
	
	putIsLValueDecor(ctx, l1);
	
	print("%_%t{%s}\n", t1, ctx->NATURAL_NUMBER()->getText());
	DEBUG_EXIT();
	return 0;
}

// XXX 2021F ==================================================================
antlrcpp::Any TypeCheckVisitor::visitMap(AslParser::MapContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id(0));
	visit(ctx->id(1));
	visit(ctx->id(2));
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id(0));
	TypesMgr::TypeId t2 = getTypeDecor(ctx->id(1));
	TypesMgr::TypeId t3 = getTypeDecor(ctx->id(2));

	if ((not Types.isErrorTy(t1) and not Types.isArrayTy(t1))
	 || (not Types.isErrorTy(t2) and not Types.isArrayTy(t2))
	 || (not Types.isErrorTy(t3) and not Types.isFunctionTy(t3))
	 || (Types.isArrayTy(t1) and Types.isArrayTy(t2) and Types.getArraySize(t1) != Types.getArraySize(t2))
	 || (Types.isFunctionTy(t3) and Types.getNumOfParameters(t3) != 1)
	 || (Types.isArrayTy(t1) and Types.isFunctionTy(t3) and not Types.copyableTypes(Types.getParameterType(t3, 0), Types.getArrayElemType(t1)))
	 || (Types.isArrayTy(t2) and Types.isFunctionTy(t3) and not Types.copyableTypes(Types.getArrayElemType(t2), Types.getFuncReturnType(t3))))
		Errors.incompatibleMapOperands(ctx);

	putTypeDecor(ctx, Types.createIntegerTy());
	
	print("%_map %t into %t using %t\n", t1, t2, t3);
	DEBUG_EXIT();
	return 0;
}

// XXX 2022P ==================================================================
antlrcpp::Any TypeCheckVisitor::visitArrayMap(AslParser::ArrayMapContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id(0));
	visit(ctx->expression(0));
	visit(ctx->expression(1));
	visit(ctx->expression(2));
	visit(ctx->id(1));
	visit(ctx->id(2));
	TypesMgr::TypeId destinationType = getTypeDecor(ctx->id(0));
	TypesMgr::TypeId conditionType = getTypeDecor(ctx->expression(0));
	TypesMgr::TypeId trueType = getTypeDecor(ctx->expression(1));
	TypesMgr::TypeId falseType = getTypeDecor(ctx->expression(2));
	TypesMgr::TypeId variableType = getTypeDecor(ctx->id(1));
	TypesMgr::TypeId sourceType = getTypeDecor(ctx->id(2));
	
	if (not Types.isErrorTy(conditionType) and not Types.isBooleanTy(conditionType))
		Errors.mapWithNonBooleanCondition(ctx);

	if (Types.isArrayTy(destinationType)) {
		TypesMgr::TypeId destinationElementType = Types.getArrayElemType(destinationType);
		
		if (not Types.isErrorTy(trueType) and not Types.copyableTypes(destinationElementType, trueType))
				Errors.mapWithIncompatibleValues(ctx);
		if (not Types.isErrorTy(falseType) and not Types.copyableTypes(destinationElementType, falseType))
				Errors.mapWithIncompatibleValues(ctx);
		
		if (Types.isArrayTy(sourceType)) {
			TypesMgr::TypeId sourceElementType = Types.getArrayElemType(sourceType);
			
			if (not Types.isErrorTy(variableType) and not Types.copyableTypes(variableType, sourceElementType))
				Errors.mapWithIncompatibleControlVar(ctx);
			if (Types.getArraySize(sourceType) != Types.getArraySize(destinationType))
				Errors.mapWithNonArraysOrDifferentSizes(ctx);
		} else if (not Types.isErrorTy(sourceType) ) {
			Errors.mapWithNonArraysOrDifferentSizes(ctx);
		}
	} else if (not Types.isErrorTy(destinationType) ) {
		Errors.mapWithNonArraysOrDifferentSizes(ctx);
	}

	print("%_%t = [%t ? %t : %t for %t in %t]\n",
		destinationType, conditionType, trueType, falseType, variableType, sourceType);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitStructElement(AslParser::StructElementContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->id());
	TypesMgr::TypeId type;
	TypesMgr::TypeId idType = getTypeDecor(ctx->id());
	bool isLValue = getIsLValueDecor(ctx->id());
	const std::string & fieldName = ctx->ID()->getText();

	if (Types.isStructTy(idType)) {
		if (Types.existStructField(idType, fieldName)) {
			type = Types.getStructFieldTy(idType, fieldName);
		} else {
			Errors.structAccessWithNonExistentField(ctx);
			type = Types.createErrorTy();
		}
	} else {
		if (not Types.isErrorTy(idType))
			Errors.structAccessWithNonStruct(ctx);
		type = Types.createErrorTy();
	}
	
	putTypeDecor(ctx, type);
	putIsLValueDecor(ctx, isLValue);

	print("%_%t.%s\n", idType, fieldName);
	DEBUG_EXIT();
	return 0;
}

// XXX 2022F ==================================================================
antlrcpp::Any TypeCheckVisitor::visitFactorial(AslParser::FactorialContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isIntegerTy(t1))
		Errors.incompatibleOperator(ctx->op);
	
	putTypeDecor(ctx, Types.createIntegerTy());
	putIsLValueDecor(ctx, false);
	
	print("%_(%t)\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitMatrixElement(AslParser::MatrixElementContext * ctx) {
	DEBUG_ENTER();
	// Matrix.
	visit(ctx->id());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->id());
	bool l1 = getIsLValueDecor(ctx->id());
	
	if (Types.isErrorTy(t1)) {
		putTypeDecor(ctx, t1);
	} else if (Types.isMatrixTy(t1)) {
		putTypeDecor(ctx, Types.getMatrixElemType(t1));
	} else {
		Errors.nonMatrixInMatrixAccess(ctx->id());
		putTypeDecor(ctx, Types.createErrorTy());
	}

	putIsLValueDecor(ctx, l1);

	// Indices.
	visit(ctx->expression(0));
	visit(ctx->expression(1));
	TypesMgr::TypeId t2 = getTypeDecor(ctx->expression(0));
	TypesMgr::TypeId t3 = getTypeDecor(ctx->expression(1));
	
	if (not Types.isErrorTy(t2) and not Types.isIntegerTy(t2)) {
		Errors.nonIntegerIndexInMatrixAccess(ctx->expression(0));
	}
	
	if (not Types.isErrorTy(t3) and not Types.isIntegerTy(t3)) {
		Errors.nonIntegerIndexInMatrixAccess(ctx->expression(1));
	}

	print("%_%t [%t,%t]\n", t1, t2, t3);
	DEBUG_EXIT();
	return 0;
}

// XXX Altres =================================================================
antlrcpp::Any TypeCheckVisitor::visitUnless(AslParser::UnlessContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isBooleanTy(t1))
		Errors.booleanRequired(ctx);
	
	visit(ctx->statements(0));
	if (ctx->statements(1))
		visit(ctx->statements(1));
	
	print("%_unless %t\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitDoUntil(AslParser::DoUntilContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isBooleanTy(t1))
		Errors.booleanRequired(ctx);
	
	visit(ctx->statements());
	
	print("%_dountil %t\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitDoWhile(AslParser::DoWhileContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isBooleanTy(t1))
		Errors.booleanRequired(ctx);
	
	visit(ctx->statements());
	
	print("%_dowhile %t\n", t1);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any TypeCheckVisitor::visitUntil(AslParser::UntilContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->expression());
	TypesMgr::TypeId t1 = getTypeDecor(ctx->expression());
	
	if (not Types.isErrorTy(t1) and not Types.isBooleanTy(t1))
		Errors.booleanRequired(ctx);
	
	visit(ctx->statements());
	
	print("%_until %t\n", t1);
	DEBUG_EXIT();
	return 0;
}
// ============================================================================

void TypeCheckVisitor::print(const std::string & format, ...) const {
	#ifdef DEBUG_BUILD
		va_list arguments;
		va_start(arguments, format);
		std::string string;
		bool escape = false;
		for (char character : format) {
			if (escape) {
				escape = false;
				if (character == '%')
					string += character;
				else if (character == 't')
					string += Types.to_string(va_arg(arguments, TypesMgr::TypeId));
				else if (character == 'T') {
					std::vector <TypesMgr::TypeId> & types = va_arg(arguments, std::vector <TypesMgr::TypeId>);
					for (unsigned int i = 0; i < types.size(); i++)
						string += (i == 0 ? "" : ", ") + Types.to_string(types[i]);
				} else if (character == 'o')
					string += va_arg(arguments, antlr4::Token *)->getText();
				else if (character == 's')
					string += va_arg(arguments, std::string);
				else if (character == '_')
					string += std::string(_i_ - 4, ' ');
			} else if (character == '%') {
				escape = true;
			} else {
				string += character;
			}
		}
		std::cerr << string;
		va_end(arguments);
	#endif
}

TypeCheckVisitor::TypeCheck TypeCheckVisitor::checkTypes(
		const std::vector <TypesMgr::TypeId> & types) {
	TypeCheck check;
	check.anyBoolean = false;
	check.anyCharacter = false;
	check.anyInteger = false;
	check.anyFloat = false;

	for (const auto & type : types) {
		if (Types.isCharacterTy(type)) check.anyCharacter = true;
		else if (Types.isIntegerTy(type)) check.anyInteger = true; 
		else if (Types.isFloatTy(type)) check.anyFloat = true;
		else if (Types.isBooleanTy(type)) check.anyBoolean = true;
	}

	check.allBoolean = not (check.anyCharacter or check.anyInteger or check.anyFloat);
	check.allCharacter = not (check.anyBoolean or check.anyInteger or check.anyFloat);
	check.allInteger = not (check.anyBoolean or check.anyCharacter or check.anyFloat);
	check.allFloat = not (check.anyBoolean or check.anyCharacter or check.anyInteger);
	check.anyNumeric = check.anyInteger or check.anyFloat;
	check.allNumeric = not (check.anyBoolean or check.anyCharacter);
	return check;
}

TypesMgr::TypeId TypeCheckVisitor::getCurrentFunctionTy() const {
	return currFunctionType;
}

void TypeCheckVisitor::setCurrentFunctionTy(TypesMgr::TypeId type) {
	currFunctionType = type;
}

SymTable::ScopeId TypeCheckVisitor::getScopeDecor(antlr4::ParserRuleContext * ctx) {
	return Decorations.getScope(ctx);
}

TypesMgr::TypeId TypeCheckVisitor::getTypeDecor(antlr4::ParserRuleContext * ctx) {
	return Decorations.getType(ctx);
}

bool TypeCheckVisitor::getIsLValueDecor(antlr4::ParserRuleContext * ctx) {
	return Decorations.getIsLValue(ctx);
}

void TypeCheckVisitor::putScopeDecor(antlr4::ParserRuleContext * ctx, SymTable::ScopeId s) {
	Decorations.putScope(ctx, s);
}

void TypeCheckVisitor::putTypeDecor(antlr4::ParserRuleContext * ctx, TypesMgr::TypeId t) {
	Decorations.putType(ctx, t);
}

void TypeCheckVisitor::putIsLValueDecor(antlr4::ParserRuleContext * ctx, bool b) {
	Decorations.putIsLValue(ctx, b);
}
