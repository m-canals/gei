//////////////////////////////////////////////////////////////////////
//
//    TypeCheckVisitor - Walk the parser tree to do the semantic
//                       typecheck for the Asl programming language
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
//////////////////////////////////////////////////////////////////////

#pragma once
#include "antlr4-runtime.h"
#include "AslBaseVisitor.h"
#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/SemErrors.h"

class TypeCheckVisitor final : public AslBaseVisitor {
	public:
		TypeCheckVisitor(
			TypesMgr & Types,
			SymTable & Symbols,
			TreeDecoration & Decorations,
			SemErrors & Errors);

		antlrcpp::Any visitProgram(AslParser::ProgramContext * ctx);
		antlrcpp::Any visitFunction(AslParser::FunctionContext * ctx);
		antlrcpp::Any visitVariable(AslParser::VariableContext * ctx);
		antlrcpp::Any visitReturn(AslParser::ReturnContext * ctx);
		antlrcpp::Any visitRead(AslParser::ReadContext * ctx);
		antlrcpp::Any visitWriteExpression(AslParser::WriteExpressionContext * ctx);
		antlrcpp::Any visitCall(AslParser::CallContext * ctx);
		antlrcpp::Any visitIf(AslParser::IfContext * ctx);
		antlrcpp::Any visitWhile(AslParser::WhileContext * ctx);
		antlrcpp::Any visitAssignment(AslParser::AssignmentContext * ctx);
		antlrcpp::Any visitElement(AslParser::ElementContext * ctx);
		antlrcpp::Any visitArrayElement(AslParser::ArrayElementContext * ctx);
		antlrcpp::Any visitFunctionCall(AslParser::FunctionCallContext * ctx);
		antlrcpp::Any visitLeftExpressionValue(AslParser::LeftExpressionValueContext * ctx);
		antlrcpp::Any visitParenthesization(AslParser::ParenthesizationContext * ctx);
		antlrcpp::Any visitBooleanUnaryOperation(AslParser::BooleanUnaryOperationContext * ctx);
		antlrcpp::Any visitBooleanBinaryOperation(AslParser::BooleanBinaryOperationContext * ctx);
		antlrcpp::Any visitArithmeticUnaryOperation(AslParser::ArithmeticUnaryOperationContext * ctx);
		antlrcpp::Any visitArithmeticBinaryOperation(AslParser::ArithmeticBinaryOperationContext * ctx);
		antlrcpp::Any visitRelationalOperation(AslParser::RelationalOperationContext * ctx);
		antlrcpp::Any visitLiteralValue(AslParser::LiteralValueContext * ctx);
		antlrcpp::Any visitLiteral(AslParser::LiteralContext * ctx);
		antlrcpp::Any visitBasicLiteral(AslParser::BasicLiteralContext * ctx);
		antlrcpp::Any visitId(AslParser::IdContext * ctx);
		// XXX 2018 ===============================================================
		antlrcpp::Any visitPairElement(AslParser::PairElementContext * ctx);
		// XXX 2019 ===============================================================
		antlrcpp::Any visitMaxAttribute(AslParser::MaxAttributeContext * ctx);
		// XXX 2020P ==============================================================
		antlrcpp::Any visitFor(AslParser::ForContext * ctx);
		antlrcpp::Any visitMaxCall(AslParser::MaxCallContext * ctx);
		// XXX 2020F ==============================================================
		antlrcpp::Any visitSumCall(AslParser::SumCallContext * ctx);
		antlrcpp::Any visitFilter(AslParser::FilterContext * ctx);
		// XXX 2021P ==============================================================
		antlrcpp::Any visitPack(AslParser::PackContext * ctx);
		antlrcpp::Any visitUnpack(AslParser::UnpackContext * ctx);
		antlrcpp::Any visitTupleElement(AslParser::TupleElementContext * ctx);
		// XXX 2021F ==============================================================
		antlrcpp::Any visitMap(AslParser::MapContext * ctx);
		// XXX 2022P ==============================================================
		antlrcpp::Any visitArrayMap(AslParser::ArrayMapContext * ctx);
		antlrcpp::Any visitStructElement(AslParser::StructElementContext * ctx);
		// XXX 2022F ==============================================================
		antlrcpp::Any visitFactorial(AslParser::FactorialContext * ctx);
		antlrcpp::Any visitMatrixElement(AslParser::MatrixElementContext * ctx);
		// XXX Altres =============================================================
		antlrcpp::Any visitUnless(AslParser::UnlessContext * ctx);
		antlrcpp::Any visitDoUntil(AslParser::DoUntilContext * ctx);
		antlrcpp::Any visitDoWhile(AslParser::DoWhileContext * ctx);
		antlrcpp::Any visitUntil(AslParser::UntilContext * ctx);
		// ========================================================================

	private:
		TypesMgr & Types;
		SymTable & Symbols;
		TreeDecoration & Decorations;
		SemErrors & Errors;
		TypesMgr::TypeId currFunctionType;
		
		void print (const std::string & format, ...) const;

		struct TypeCheck {
			bool anyBoolean;
			bool anyCharacter;
			bool anyInteger;
			bool anyFloat;
			bool anyNumeric;
			bool allBoolean;
			bool allCharacter;
			bool allInteger;
			bool allFloat;
			bool allNumeric;
		};

		TypeCheck checkTypes(const std::vector <TypesMgr::TypeId> & types);

		TypesMgr::TypeId getCurrentFunctionTy() const;
		void setCurrentFunctionTy(TypesMgr::TypeId type);
		
		SymTable::ScopeId getScopeDecor(antlr4::ParserRuleContext * ctx);
		TypesMgr::TypeId getTypeDecor(antlr4::ParserRuleContext * ctx);
		bool getIsLValueDecor(antlr4::ParserRuleContext * ctx);
		
		void putScopeDecor(antlr4::ParserRuleContext * ctx, SymTable::ScopeId s);
		void putTypeDecor(antlr4::ParserRuleContext * ctx, TypesMgr::TypeId t);
		void putIsLValueDecor(antlr4::ParserRuleContext * ctx, bool b);
};
