//////////////////////////////////////////////////////////////////////
//
//    Asl - Another simple language (grammar)
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
//////////////////////////////////////////////////////////////////////

grammar Asl;

//////////////////////////////////////////////////
/// Parser Rules
//////////////////////////////////////////////////

program : function+ EOF
	;

function
	: FUNC ID '(' (parameters)? ')' (':' basicType)?
	  variables
	  statements
	  ENDFUNC
	;

parameters
	: parameter (',' parameter)*
	;

parameter
	: ID ':' type
	;

variables
	: (variable)*
	;

variable
	: VAR ID (',' ID)* ':' type
	;

type
	: basicType
	| arrayType
	// XXX 2018 =================================================================
	| pairType
	// XXX 2021P ================================================================
	| tupleType
	// XXX 2022P ================================================================
	| structType
	// XXX 2022P ================================================================
	| matrixType
	// ==========================================================================
	;

arrayType
	: ARRAY '[' NATURAL_NUMBER ']' OF basicType
	;

// XXX 2018 ===================================================================
pairType
	: PAIR basicType ',' basicType ENDPAIR
	;

// XXX 2021P ==================================================================
tupleType
	: '{' basicType (',' basicType)* '}'
	;

// XXX 2022P ==================================================================
structType
	: STRUCT '{' fields '}'
	;

fields
	: field (',' field)*
	;

field
	: ID ':' basicType
	;

// XXX 2022F ==================================================================
matrixType
	: MATRIX '[' NATURAL_NUMBER ',' NATURAL_NUMBER ']' OF basicType
	;
// ============================================================================

basicType
	: BOOL
	| CHAR
	| INT
	| FLOAT
	;

statements
	: (statement)*
	;

statement
	: IF expression THEN statements (ELSE statements)? ENDIF       # if
	| WHILE expression DO statements ENDWHILE                      # while
	| call ';'                                                     # procedureCall
	| READ leftExpression ';'                                      # read
	| WRITE expression ';'                                         # writeExpression
	| WRITE STRING ';'                                             # writeString
	| RETURN expression? ';'                                       # return
	// XXX 2020P ================================================================
	| FOR id IN RANGE '(' expressions? ')' DO statements ENDFOR    # for
	// XXX 2021P ================================================================
	| PACK expressions INTO id ';'                                 # pack
	| UNPACK id INTO leftExpressions ';'                           # unpack
	// XXX 2021F ================================================================
	| MAP id INTO id USING id ';'                                  # map
	// XXX 2022P ================================================================
	| id ASSIGN '[' expression '?' expression ':' expression
	  FOR id IN id ']' ';'                                         # arrayMap
	// XXX Altres ===============================================================
	| leftExpressions ASSIGN expressions ';'                       # assignment
	// | leftExpressions (op = ...) expressions ';'                # augmentedAssignment
	| UNLESS expression DO statements (ELSE statements)? ENDUNLESS # unless
	| UNTIL expression DO statements ENDUNTIL                      # until
	| DO statements WHILE expression ';'                           # doWhile
	| DO statements UNTIL expression ';'                           # doUntil
	// | BREAK NATURAL_NUMBER ';'                                  # break
	// | ID ':'                                                    # label
	// | GOTO id ';'                                               # goto
	// | TRY statements (CATCH exception statements)* ENDTRY       # try
	// | SWITCH id (CASE NATURAL_NUMBER: statements)*
	//   DEFAULT: statements ENDSWITCH                             # switch
	// ==========================================================================
	;

call
	: id '(' expressions? ')'
	;

expressions
	: expression (',' expression)*
	;

expression
	// XXX 2020F ================================================================
	: (op = FILTER) id INTO id USING id                            # filter
	// XXX Altres ===============================================================
	// | (op = REDUCE) id (',' expression)? USING id               # reduce
	// XXX 2019 =================================================================
	| id (op = MAX_ATTRIBUTE)                                      # maxAttribute
	// XXX 2022F ================================================================
	| expression (op = FACT)                                       # factorial
	// ==========================================================================
	| '(' expression ')'                                           # parenthesization
	| (op = NOT) expression                                        # booleanUnaryOperation
	| (op = PLUS | op = MINUS) expression                          # arithmeticUnaryOperation
	// XXX Altres ===============================================================
	// | (op = INC | op = DEC) leftExpression                      # prefixOperation
	// | leftExpression (op = INC | op = DEC)                      # postfixOperation
	// XXX 2021F ================================================================
	| expression (op = EXP) expression                             # arithmeticBinaryOperation
	// ==========================================================================                                       
	| expression (op = MUL | op = DIV | op = MOD ) expression      # arithmeticBinaryOperation
	| expression (op = PLUS | op = MINUS) expression               # arithmeticBinaryOperation
	| expression (op = LT | op = LE | op = EQ |
	              op = GE | op = GT | op = NE) expression          # relationalOperation
	| expression (op = AND) expression                             # booleanBinaryOperation
	| expression (op = OR) expression                              # booleanBinaryOperation
	| literal                                                      # literalValue
	| leftExpression                                               # leftExpressionValue
	// XXX 2020P ================================================================
	| (op = MAX) '(' expressions? ')'                              # maxCall
	// XXX 2020F ================================================================
	| (op = SUM) '(' expressions? ')'                              # sumCall
	// ==========================================================================
	| call                                                         # functionCall
	;

// XXX 2021P Altres ===========================================================
leftExpressions
	: leftExpression (',' leftExpression)*
	;
// ============================================================================

leftExpression
	: id                                                           # element
	| id '[' expression ']'                                        # arrayElement
	// XXX 2018 =================================================================
	| id '.' PAIR_ELEMENT                                          # pairElement
	// XXX 2021P ================================================================
	| id '{' NATURAL_NUMBER '}'                                    # tupleElement
	// XXX 2022P ================================================================
	| id '.' ID                                                    # structElement
	// XXX 2022F ================================================================
	| id '[' expression ',' expression ']'                         # matrixElement
	// ==========================================================================
	;

id
	: ID
	;

literal
	: basicLiteral
	// XXX Altres ===============================================================
	// | arrayLiteral
	// | pairLiteral
	// | tupleLiteral
	// | structLiteral
	// ==========================================================================
	;

basicLiteral
	: BOOLEAN_VALUE
	| CHARACTER
	| NATURAL_NUMBER
	| REAL_NUMBER
	;

/*
// XXX Altres =================================================================
arrayLiteral
	: '[' basicLiteral (',' basicLiteral)* ']'
	;

pairLiteral
	: '<' basicLiteral ',' basicLiteral '>'
	;

tupleLiteral
	: '(' basicLiteral (',' basicLiteral)* ')'
	;

structLiteral
	: '{' basicLiteral (',' basicLiteral)* '}'
	;
	;
// ============================================================================
*/

//////////////////////////////////////////////////
/// Lexer Rules
//////////////////////////////////////////////////

ASSIGN         : '=' ;
LT             : '<' ;
LE             : '<=' ;
EQ             : '==' ;
GE             : '>=' ;
GT             : '>' ;
NE             : '!=' ;
PLUS           : '+' ;
MUL            : '*';
MINUS          : '-' ;
DIV            : '/' ;
MOD            : '%' ;
NOT            : 'not' ;
AND            : 'and' ;
OR             : 'or' ;
BOOL           : 'bool' ;
CHAR           : 'char' ;
INT            : 'int' ;
FLOAT          : 'float' ;
VAR            : 'var';
IF             : 'if' ;
THEN           : 'then' ;
ELSE           : 'else' ;
ENDIF          : 'endif' ;
WHILE          : 'while' ;
DO             : 'do' ;
ENDWHILE       : 'endwhile' ;
FUNC           : 'func' ;
ENDFUNC        : 'endfunc' ;
READ           : 'read' ;
WRITE          : 'write' ;
RETURN         : 'return' ;
ARRAY          : 'array' ;
OF             : 'of' ;
// XXX 2018 ===================================================================
PAIR           : 'pair' ;
ENDPAIR        : 'endpair' ;
PAIR_ELEMENT   : 'first' | 'second' ;
// XXX 2019 ===================================================================
MAX_ATTRIBUTE  : '.max' ;
// XXX 2020P 2022P ============================================================
FOR            : 'for' ;
IN             : 'in' ;
// XXX 2020P ==================================================================
RANGE          : 'range' ;
ENDFOR         : 'endfor' ;
MAX            : 'max' ;
// XXX 2020F ==================================================================
SUM            : 'sum' ;
FILTER         : 'filter' ;
USING          : 'using' ;
// XXX 2021P ==================================================================
PACK           : 'pack' ;
UNPACK         : 'unpack' ;
INTO           : 'into' ;
// XXX 2021F ==================================================================
EXP            : '**' ;
MAP            : 'map' ;
// XXX 2022P ==================================================================
STRUCT         : 'struct' ;
// XXX 2022F ==================================================================
FACT           : '!' ;
MATRIX         : 'matrix' ;
// XXX Altres =================================================================
UNLESS         : 'unless' ;
ENDUNLESS      : 'endunless' ;
UNTIL          : 'until' ;
ENDUNTIL       : 'enduntil' ;
// BREAK       : 'break' ;
// LABEL       : 'label' ;
// GOTO        : 'goto' ;
// TRY         : 'try' ;
// CATCH       : 'catch' ;
// ENDTRY      : 'endtry' ;
// ============================================================================
BOOLEAN_VALUE  : 'true' | 'false' ;
NATURAL_NUMBER : DIGIT+ ;
CHARACTER      : '\'' (DIGIT | LETTER | MISC_SYM | '\\n' | '\\t') '\'' ;
REAL_NUMBER    : DIGIT+ '.' DIGIT+ ;
STRING         : '"' (ESC_SEQ | ~('\\' | '"'))* '"' ;
ID             : LETTER (LETTER | '_' | DIGIT)* ;

fragment
ESC_SEQ      : '\\' ('b' | 't' | 'n' | 'f' | 'r' | '"' | '\'' | '\\') ;
fragment
LETTER       : 'a'..'z' | 'A'..'Z' ;
fragment
DIGIT        : '0'..'9' ;
fragment
MISC_SYM     : ' ' | '!' | '"' | '#' | '$' | '%' | '&' | '('    |
               ')' | '*' | '+' | ',' | '-' | '.' | '/' | ':'    |
               ';' | '<' | '=' | '>' | '?' | '@' | '[' | '\\\\' |
               ']' | '^' | '_' | '`' | '{' | '|' | '}' | '~'    ;

COMMENT      : '//' ~('\n' | '\r')* '\r'? '\n' -> skip ;
WS           : (' ' | '\t' | '\r' | '\n')+     -> skip ;
