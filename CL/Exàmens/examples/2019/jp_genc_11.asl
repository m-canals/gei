func init(aI : array[10] of int,
          k : int)
  var i : int

  i = 0;
  while i < 10 do
    aI[i] = i*i + k; i = i + 1;
  endwhile
endfunc

func f(a : array[10] of int)
  var i,x : int

  i = 0;
  while i < 15 do
    x = a[i+1];
    write 'a'; write '['; write i+1;
    write ']'; write '='; write x; write '\n';
    i = i + 1;
  endwhile
  write 'b'; write 'a'; write 'd'; write ' '; write 'm'; write 'e'; write 's'; write 's'; write 'a'; write 'g'; write 'e'; write '!'; write '\n';
endfunc

func main()
  var b,c: array[10] of int

  init(b, 5);
  c = b;
  f(c);
endfunc
