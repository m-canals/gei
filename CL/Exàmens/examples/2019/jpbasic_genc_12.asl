func f(v: array[10] of int)
  var c: array[10] of int
  var i: int

  c = v;

  write 'e'; write 'n'; write ' '; write 'f'; write '.'; write ' '; write 'c'; write ':'; write ' ';
  i = 0;
  while i < 10 do
    write c[i]; write ' ';
    i = i+1;
  endwhile
  write '\n';
endfunc


func g(v: array[10] of int)
  var d: array[10] of int
  var i: int
  i = 0;
  while i < 10 do
    d[i] = -1;
    i = i+1;
  endwhile

  v = d;

endfunc


func main()
  var a, b: array[10] of int
  var i, j: int
  i = 0;
  while i < 10 do
    a[i] = i;
    b[i] = 0;
    i = i+1;
  endwhile

  b = a;

  write 'd'; write 'e'; write 's'; write 'p'; write 'r'; write 'e'; write 's'; write ' '; write 'd'; write 'e'; write ' '; write 'b'; write '='; write 'a'; write '.'; write ' '; write 'b'; write ':'; write ' ';
  i = 0;
  while i < 10 do
    write b[i]; write ' ';
    i = i+1;
  endwhile
  write '\n';
  write 'd'; write 'e'; write 's'; write 'p'; write 'r'; write 'e'; write 's'; write ' '; write 'd'; write 'e'; write ' '; write 'b'; write '='; write 'a'; write '.'; write ' '; write 'a'; write ':'; write ' ';
  i = 0;
  while i < 10 do
    write a[i]; write ' ';
    i = i+1;
  endwhile
  write '\n';

  f(a);

  write 'd'; write 'e'; write 's'; write 'p'; write 'r'; write 'e'; write 's'; write ' '; write 'd'; write 'e'; write ' '; write 'f'; write '('; write 'a'; write ')'; write '.'; write ' '; write 'a'; write ':'; write ' ';
  i = 0;
  while i < 10 do
    write a[i]; write ' ';
    i = i+1;
  endwhile
  write '\n';
  
  g(a);

  write 'd'; write 'e'; write 's'; write 'p'; write 'r'; write 'e'; write 's'; write ' '; write 'd'; write 'e'; write ' '; write 'g'; write '('; write 'a'; write ')'; write '.'; write ' '; write 'a'; write ':'; write ' ';
  i = 0;
  while i < 10 do
    write a[i]; write ' ';
    i = i+1;
  endwhile
  write '\n';  
endfunc
