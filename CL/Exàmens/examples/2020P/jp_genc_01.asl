func main()
	write max(1, 2);
	write max(2, 1);
  write max(1, 2, 3);
  write max(1, 3, 2);
  write max(2, 1, 3);
  write max(2, 3, 1);
  write max(3, 1, 2);
  write max(3, 2, 1);
  write max(1, 1.1);
  write max(1.1, 1);
  write max(1.1, 1.1);
  write max('a', 'b');
  write max('b', 'a');
  write '\n';
endfunc
