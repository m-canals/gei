func main()
	var i : int
	for i in range(3) do write(i+1); endfor
	for i in range(1,4) do write(i); endfor
	for i in range(2, 7, 2) do write(i/2); endfor
	for i in range(-4, -1) do write(i+5); endfor
	for i in range(-1, 2) do write(i+2); endfor
	for i in range(3, 0, -1) do write(i); endfor
  write '\n';
endfunc
