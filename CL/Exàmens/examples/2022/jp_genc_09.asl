func fact(n: int): int
    var f: int
    f = 1;
    while n>1 do
        f = f*n;
	n = n-1;
    endwhile
    return f;
endfunc

func main()
    var m, i, f : int
    var end : bool
    i = 0;
    end = true;    
    read m;
    if i <= m then
        end = false;
    else
        i = 0;
    endif
    while not end do
	write i;
	write "!=";
	write fact(i);
	write "\n";
	if i == m then
	    end = true;
	else
   	    i = i+1;
	endif
    endwhile
endfunc
