func main()
	var a : array [3] of float
	var b : array [3] of int
	var c : array [3] of char
	var d : array [3] of bool
	var i : int
	pack 1, 2, 3 into a;
	pack 1, 2, 3 into b;
	pack '1', '2', '3' into c;
	pack true, false, true into d;
	i = 0; while i < 3 do write a[i]; i = i + 1; endwhile write "\n";
	i = 0; while i < 3 do write b[i]; i = i + 1; endwhile write "\n";
	i = 0; while i < 3 do write c[i]; i = i + 1; endwhile write "\n";
	i = 0; while i < 3 do write d[i]; i = i + 1; endwhile write "\n";
endfunc
