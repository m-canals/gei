func main()
	var a : array [3] of float
	var b : array [3] of int
	var c : array [3] of char
	var d : array [3] of bool
	var a2, a3 : float
	var b1, b2, b3 : int
	var c1, c2, c3 : char
	pack 1, 2, 3 into a;
	pack 1, 2, 3 into b;
	pack '1', '2', '3' into c;
	pack true, false, true into d;
	unpack a into a[0], a2, a3;
	unpack b into b1, b2, b3;
	unpack c into c1, c2, c3;
	unpack d into d[0], d[1], d[2];
	write a[0]; write a2; write a3; write "\n";
	write b1; write b2; write b3; write "\n";
	write c1; write c2; write c3; write "\n";
	write d[0]; write d[1]; write d[2]; write "\n";
endfunc
