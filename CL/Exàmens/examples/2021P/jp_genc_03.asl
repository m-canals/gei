func main()
	var s: {bool, int, float, char}
	s{0} = true;
	s{1} = 1;
	s{2} = 2;
	s{3} = '\n';
	if s{0} then
		write s{1} + s{2};
		write s{3};
	endif
endfunc
