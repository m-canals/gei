func main()
	var a : float
	var b, c : int
	a, b, c = 1, 2, 3;
	// Assignació múltiple però no paral·lela sinó seqüencial.
	a, b, c = b, c, b;
	write a;
	write b;
	write c;
	write "\n";
endfunc
