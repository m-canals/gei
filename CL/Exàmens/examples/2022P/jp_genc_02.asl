func main()
	var s: struct {i: int, b: bool, f: float, c: char}
	s.b = true;
	s.i = 1;
	s.f = 2;
	s.c = '\n';
	if s.b then
		write s.i + s.f;
		write s.c;
	endif
endfunc
