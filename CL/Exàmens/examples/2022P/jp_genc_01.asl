func main()
	var a : array [3] of int
	var b, c : array [3] of float
	var d : array [3] of char
	var i : int
	var x : int
	var y : float
	var z : char
	a = [true ? 1 : 0 for x in a];
	b = [false ? 0 : x + 1 for x in a];
	c = [false ? 0 : x + 2 for y in a];
	d = [x == 0 ? '0' : '4' for x in a];
	i = 0; while i < 3 do write a[i]; i = i + 1; endwhile write "\n";
	i = 0; while i < 3 do write b[i]; i = i + 1; endwhile write "\n";
	i = 0; while i < 3 do write c[i]; i = i + 1; endwhile write "\n";
	i = 0; while i < 3 do write d[i]; i = i + 1; endwhile write "\n";
endfunc
