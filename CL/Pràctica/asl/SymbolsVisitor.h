//////////////////////////////////////////////////////////////////////
//
//    SymbolsVisitor - Walk the parser tree to register symbols
//                     for the Asl programming language
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
//////////////////////////////////////////////////////////////////////

#pragma once
#include "antlr4-runtime.h"
#include "AslBaseVisitor.h"
#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/SemErrors.h"

class SymbolsVisitor final : public AslBaseVisitor {
	public:
		SymbolsVisitor(
			TypesMgr & Types,
			SymTable & Symbols,
			TreeDecoration & Decorations,
			SemErrors & Errors);
		
		antlrcpp::Any visitProgram(AslParser::ProgramContext * ctx);
		antlrcpp::Any visitFunction(AslParser::FunctionContext * ctx);
		antlrcpp::Any visitParameter(AslParser::ParameterContext * ctx);
		antlrcpp::Any visitVariable(AslParser::VariableContext * ctx);
		antlrcpp::Any visitType(AslParser::TypeContext * ctx);
		antlrcpp::Any visitBasicType(AslParser::BasicTypeContext * ctx);
		antlrcpp::Any visitArrayType(AslParser::ArrayTypeContext * ctx);

	private:
		TypesMgr & Types;
		SymTable & Symbols;
		TreeDecoration & Decorations;
		SemErrors & Errors;
		
		SymTable::ScopeId getScopeDecor(antlr4::ParserRuleContext * ctx);
		TypesMgr::TypeId getTypeDecor(antlr4::ParserRuleContext * ctx);
		
		void putScopeDecor(antlr4::ParserRuleContext * ctx, SymTable::ScopeId scope);
		void putTypeDecor(antlr4::ParserRuleContext * ctx, TypesMgr::TypeId type);
};
