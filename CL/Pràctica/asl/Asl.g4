//////////////////////////////////////////////////////////////////////
//
//    Asl - Another simple language (grammar)
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
//////////////////////////////////////////////////////////////////////

grammar Asl;

//////////////////////////////////////////////////
/// Parser Rules
//////////////////////////////////////////////////

program : function+ EOF
	;

function
	: FUNC ID '(' (parameters)? ')' (':' basicType)?
	  variables
	  statements
	  ENDFUNC
	;

parameters
	: parameter (',' parameter)*
	;

parameter
	: ID ':' type
	;

variables
	: (variable)*
	;

variable
	: VAR ID (',' ID)* ':' type
	;

type
	: basicType
	| arrayType
	;

arrayType
	: ARRAY '[' NATURAL_NUMBER ']' OF basicType
	;

basicType
	: BOOL
	| CHAR
	| INT
	| FLOAT
	;

statements
	: (statement)*
	;

statement
	: leftExpression ASSIGN expression ';'                         # assignment
	| IF expression THEN statements (ELSE statements)? ENDIF       # if
	| WHILE expression DO statements ENDWHILE                      # while
	| call ';'                                                     # procedureCall
	| READ leftExpression ';'                                      # read
	| WRITE expression ';'                                         # writeExpression
	| WRITE STRING ';'                                             # writeString
	| RETURN expression? ';'                                       # return
	;

call
	: id '(' expressions? ')'
	;

expressions
	: expression (',' expression)*
	;

expression
	: '(' expression ')'                                           # parenthesization
	| (op = NOT) expression                                        # booleanUnaryOperation
	| (op = PLUS | op = MINUS) expression                          # arithmeticUnaryOperation
	| expression (op = MUL | op = DIV | op = MOD ) expression      # arithmeticBinaryOperation
	| expression (op = PLUS | op = MINUS) expression               # arithmeticBinaryOperation
	| expression (op = LT | op = LE | op = EQ |
	              op = GE | op = GT | op = NE) expression          # relationalOperation
	| expression (op = AND) expression                             # booleanBinaryOperation
	| expression (op = OR) expression                              # booleanBinaryOperation
	| literal                                                      # literalValue
	| leftExpression                                               # leftExpressionValue
	| call                                                         # functionCall
	;

leftExpression
	: id                                                           # element
	| id '[' expression ']'                                        # arrayElement
	;

id
	: ID
	;

literal
	: basicLiteral
	;

basicLiteral
	: BOOLEAN_VALUE
	| CHARACTER
	| NATURAL_NUMBER
	| REAL_NUMBER
	;

//////////////////////////////////////////////////
/// Lexer Rules
//////////////////////////////////////////////////

ASSIGN         : '=' ;
LT             : '<' ;
LE             : '<=' ;
EQ             : '==' ;
GE             : '>=' ;
GT             : '>' ;
NE             : '!=' ;
PLUS           : '+' ;
MUL            : '*';
MINUS          : '-' ;
DIV            : '/' ;
MOD            : '%' ;
NOT            : 'not' ;
AND            : 'and' ;
OR             : 'or' ;
BOOL           : 'bool' ;
CHAR           : 'char' ;
INT            : 'int' ;
FLOAT          : 'float' ;
VAR            : 'var';
IF             : 'if' ;
THEN           : 'then' ;
ELSE           : 'else' ;
ENDIF          : 'endif' ;
WHILE          : 'while' ;
DO             : 'do' ;
ENDWHILE       : 'endwhile' ;
FUNC           : 'func' ;
ENDFUNC        : 'endfunc' ;
READ           : 'read' ;
WRITE          : 'write' ;
RETURN         : 'return' ;
ARRAY          : 'array' ;
OF             : 'of' ;
BOOLEAN_VALUE  : 'true' | 'false' ;
NATURAL_NUMBER : DIGIT+ ;
CHARACTER      : '\'' (DIGIT | LETTER | MISC_SYM | '\\n' | '\\t') '\'' ;
REAL_NUMBER    : DIGIT+ '.' DIGIT+ ;
STRING         : '"' (ESC_SEQ | ~('\\' | '"'))* '"' ;
ID             : LETTER (LETTER | '_' | DIGIT)* ;

fragment
ESC_SEQ      : '\\' ('b' | 't' | 'n' | 'f' | 'r' | '"' | '\'' | '\\') ;
fragment
LETTER       : 'a'..'z' | 'A'..'Z' ;
fragment
DIGIT        : '0'..'9' ;
fragment
MISC_SYM     : ' ' | '!' | '"' | '#' | '$' | '%' | '&' | '('    |
               ')' | '*' | '+' | ',' | '-' | '.' | '/' | ':'    |
               ';' | '<' | '=' | '>' | '?' | '@' | '[' | '\\\\' |
               ']' | '^' | '_' | '`' | '{' | '|' | '}' | '~'    ;

COMMENT      : '//' ~('\n' | '\r')* '\r'? '\n' -> skip ;
WS           : (' ' | '\t' | '\r' | '\n')+     -> skip ;
