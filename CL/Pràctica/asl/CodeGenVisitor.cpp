//////////////////////////////////////////////////////////////////////
//
//    CodeGenVisitor - Walk the parser tree to do
//                     the generation of code
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
//////////////////////////////////////////////////////////////////////

#include "CodeGenVisitor.h"
#include "antlr4-runtime.h"
#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/code.h"
#include <string>
#include <cstddef>
//#define DEBUG_BUILD
#include "../common/debug.h"

#define RESULT_ADDRESS  "_result"

CodeGenVisitor::CodeGenVisitor(
		TypesMgr       & Types,
		SymTable       & Symbols,
		TreeDecoration & Decorations) :
	Types{Types},
	Symbols{Symbols},
	Decorations{Decorations} {
}

antlrcpp::Any CodeGenVisitor::visitProgram(AslParser::ProgramContext * ctx) {
	DEBUG_ENTER();
	SymTable::ScopeId scope = getScopeDecor(ctx);
	code program;
	Symbols.pushThisScope(scope);
	
	for (const auto & function : ctx->function()) { 
	  subroutine routine = visit(function);
	  program.add_subroutine(routine);
	}
	
	Symbols.popScope();
	DEBUG_EXIT();
	return program;
}

antlrcpp::Any CodeGenVisitor::visitFunction(AslParser::FunctionContext * ctx) {
	DEBUG_ENTER();
	const std::string & functionName = ctx->ID()->getText();
	TypesMgr::TypeId functionType = getTypeDecor(ctx);
	TypesMgr::TypeId returnType = Types.getFuncReturnType(functionType);
	SymTable::ScopeId scope = getScopeDecor(ctx);
	subroutine routine(functionName);
	Symbols.pushThisScope(scope);
	codeCounters.reset();
	setCurrentFunctionTy(functionType);
	
	// Resultat.
	if (not Types.isVoidTy(returnType)) {
		routine.add_param(RESULT_ADDRESS);
	}

	// Paràmetres.
	if (Types.getNumOfParameters(functionType) > 0) {
		for (const auto & parameter : ctx->parameters()->parameter()) {
			const std::string & parameterName = parameter->ID()->getText();
			routine.add_param(parameterName);
		}
	}

	// Variables locals.
	for (const auto & variable : ctx->variables()->variable()) {
		TypesMgr::TypeId variableType = getTypeDecor(variable->type());
		std::size_t variableSize = Types.getSizeOfType(variableType);
		for (const auto & id : variable->ID()) {
			const std::string & variableName = id->getText();
			routine.add_var(var{variableName, variableSize});
		}
	}

	// Instruccions.
	instructionList && code = visit(ctx->statements());

	// Return implícit.
	code = code || instruction::RETURN();
	
	routine.set_instructions(code);
	Symbols.popScope();
	DEBUG_EXIT();
	return routine;
}

antlrcpp::Any CodeGenVisitor::visitStatements(AslParser::StatementsContext * ctx) {
	DEBUG_ENTER();
	instructionList code;
	
	for (const auto & statement : ctx->statement()) {
		instructionList && statementCode = visit(statement);
	  code = code || statementCode;
	}
	
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitReturn(AslParser::ReturnContext * ctx) {
	DEBUG_ENTER();
	instructionList code;
	
	if (ctx->expression()) {
		CodeAttribs && expression = visit(ctx->expression());
		TypesMgr::TypeId expressionType = getTypeDecor(ctx->expression());
		TypesMgr::TypeId functionType = getCurrentFunctionTy();
		TypesMgr::TypeId returnType = Types.getFuncReturnType(functionType);
		code = code || expression.code;
		std::string resultAddress = coerce(code, returnType, expressionType, expression.addr);
		code = code || instruction::LOAD(RESULT_ADDRESS, resultAddress);
	}
	
	code = code || instruction::RETURN();
	
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitRead(AslParser::ReadContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->leftExpression());
	TypesMgr::TypeId leftExpressionType = getTypeDecor(ctx->leftExpression());
	std::string address = "%" + codeCounters.newTEMP();
	instructionList & code = leftExpression.code;

	if (Types.isCharacterTy(leftExpressionType))
		code = code || instruction::READC(address);
	else if (Types.isBooleanTy(leftExpressionType))
		code = code || instruction::READI(address);
	else if (Types.isIntegerTy(leftExpressionType))
		code = code || instruction::READI(address);
	else if (Types.isFloatTy(leftExpressionType))
		code = code || instruction::READF(address);
	
	store(code, leftExpression.addr, leftExpression.offs, address);

	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitWriteExpression(AslParser::WriteExpressionContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	TypesMgr::TypeId expressionType = getTypeDecor(ctx->expression());
	instructionList & code = expression.code;
	
	if (Types.isCharacterTy(expressionType))
		code = code || instruction::WRITEC(expression.addr);
	else if (Types.isBooleanTy(expressionType))
		code = code || instruction::WRITEI(expression.addr);
	else if (Types.isIntegerTy(expressionType))
		code = code || instruction::WRITEI(expression.addr);
	else if (Types.isFloatTy(expressionType))
		code = code || instruction::WRITEF(expression.addr);
	
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitWriteString(AslParser::WriteStringContext * ctx) {
	DEBUG_ENTER();
	const std::string & string = ctx->STRING()->getText();
	instructionList code = instruction::WRITES(string);
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitIf(AslParser::IfContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && thenCode = visit(ctx->statements(0));
	instructionList code;

	if (ctx->statements(1)) {
		instructionList && elseCode = visit(ctx->statements(1));
		code = ifCode(expression.code, expression.addr, thenCode, elseCode);
	} else {
		code = ifCode(expression.code, expression.addr, thenCode);
	}

	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitWhile(AslParser::WhileContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && doCode = visit(ctx->statements());
	instructionList code = whileCode(expression.code, expression.addr, doCode);
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitProcedureCall(AslParser::ProcedureCallContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && call = visit(ctx->call());
	instructionList & code = call.code;
	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitAssignment(AslParser::AssignmentContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->leftExpression());
	CodeAttribs && expression = visit(ctx->expression());
	TypesMgr::TypeId leftExpressionType = getTypeDecor(ctx->leftExpression());
	TypesMgr::TypeId expressionType = getTypeDecor(ctx->expression());
	instructionList && code = leftExpression.code || expression.code;;
	
	std::string address = coerce(code, leftExpressionType, expressionType, expression.addr);
	
	if (Types.isArrayTy(leftExpressionType) and Types.isArrayTy(expressionType))
		code = code || copyArrayCode(leftExpressionType, expressionType, leftExpression.addr, address);
	else
		store(code, leftExpression.addr, leftExpression.offs, address);

	DEBUG_EXIT();
	return code;
}

antlrcpp::Any CodeGenVisitor::visitFunctionCall(AslParser::FunctionCallContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && attributes = visit(ctx->call());
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitCall(AslParser::CallContext * ctx) {
	DEBUG_ENTER();
	const std::string & functionName = ctx->id()->getText();
	std::vector <TypesMgr::TypeId> argumentTypes;
	std::vector <std::string> argumentAddresses;
	instructionList code;

	if (ctx->expressions()) {
		for (const auto & expressionContext : ctx->expressions()->expression()) {
			CodeAttribs && expression = visit(expressionContext);
			TypesMgr::TypeId expressionType = getTypeDecor(expressionContext);
			argumentTypes.push_back(expressionType);
			argumentAddresses.push_back(expression.addr);
			code = code || expression.code;
		}
	}
		
	std::string address = call(code, functionName, argumentTypes, argumentAddresses);

	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitParenthesization(AslParser::ParenthesizationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && attributes = visit(ctx->expression());
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitBooleanUnaryOperation(AslParser::BooleanUnaryOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList & code = expression.code;
	
	if (ctx->NOT())
		code = code || instruction::NOT(resultAddress, expression.addr);

	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitBooleanBinaryOperation(AslParser::BooleanBinaryOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->expression(0));
	CodeAttribs && rightExpression = visit(ctx->expression(1));
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList && code = leftExpression.code || rightExpression.code;
	
	if (ctx->AND())
		code = code || instruction::AND(resultAddress, leftExpression.addr, rightExpression.addr);
	else if (ctx->OR())
		code = code || instruction::OR(resultAddress, leftExpression.addr, rightExpression.addr);
	
	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitArithmeticUnaryOperation(AslParser::ArithmeticUnaryOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && expression = visit(ctx->expression());
	TypesMgr::TypeId expressionType = getTypeDecor(ctx->expression());
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList & code = expression.code;

	if (ctx->PLUS()) {
		code = code || instruction::LOAD(resultAddress, expression.addr);
	} else if (ctx->MINUS()) {
		if (Types.isFloatTy(expressionType))
			code = code || instruction::FNEG(resultAddress, expression.addr);
		else
			code = code || instruction::NEG(resultAddress, expression.addr);
	}
	
	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitArithmeticBinaryOperation(AslParser::ArithmeticBinaryOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->expression(0));
	CodeAttribs && rightExpression = visit(ctx->expression(1));
	TypesMgr::TypeId leftExpressionType = getTypeDecor(ctx->expression(0));
	TypesMgr::TypeId rightExpressionType = getTypeDecor(ctx->expression(1));
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList && code = leftExpression.code || rightExpression.code;
	
	if (ctx->MOD()) {
		code = code || instruction__MOD(resultAddress, leftExpression.addr, rightExpression.addr);
	} else if (Types.isFloatTy(leftExpressionType) or Types.isFloatTy(rightExpressionType)) {
		std::string leftOperandAddress = coerce(code, rightExpressionType, leftExpressionType, leftExpression.addr);
		std::string rightOperandAddress = coerce(code, leftExpressionType, rightExpressionType, rightExpression.addr);
		if      (ctx->MUL())   code = code || instruction::FMUL(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->PLUS())  code = code || instruction::FADD(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->MINUS()) code = code || instruction::FSUB(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->DIV())   code = code || instruction::FDIV(resultAddress, leftOperandAddress, rightOperandAddress);
	} else if (Types.isIntegerTy(leftExpressionType) and Types.isIntegerTy(rightExpressionType)) {
		if      (ctx->MUL())   code = code || instruction::MUL(resultAddress, leftExpression.addr, rightExpression.addr);
		else if (ctx->PLUS())  code = code || instruction::ADD(resultAddress, leftExpression.addr, rightExpression.addr);
		else if (ctx->MINUS()) code = code || instruction::SUB(resultAddress, leftExpression.addr, rightExpression.addr);
		else if (ctx->DIV())   code = code || instruction::DIV(resultAddress, leftExpression.addr, rightExpression.addr);
	}
	
	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitRelationalOperation(AslParser::RelationalOperationContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->expression(0));
	CodeAttribs && rightExpression = visit(ctx->expression(1));
	TypesMgr::TypeId leftExpressionType = getTypeDecor(ctx->expression(0));
	TypesMgr::TypeId rightExpressionType = getTypeDecor(ctx->expression(1));
	std::string resultAddress = "%" + codeCounters.newTEMP();
	instructionList && code = leftExpression.code || rightExpression.code;
	
	std::string leftOperandAddress = coerce(code, rightExpressionType, leftExpressionType, leftExpression.addr);
	std::string rightOperandAddress = coerce(code, leftExpressionType, rightExpressionType, rightExpression.addr);
	
	if (Types.isFloatTy(leftExpressionType) or Types.isFloatTy(rightExpressionType)) {
		if      (ctx->LT()) code = code || instruction::FLT(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->LE()) code = code || instruction::FLE(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->EQ()) code = code || instruction::FEQ(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->GE()) code = code || instruction::FLE(resultAddress, rightOperandAddress, leftOperandAddress);
		else if (ctx->GT()) code = code || instruction::FLT(resultAddress, rightOperandAddress, leftOperandAddress);
		else if (ctx->NE()) code = code || instruction__FNE(resultAddress, leftOperandAddress, rightOperandAddress);
	} else {
		if      (ctx->LT()) code = code || instruction::LT(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->LE()) code = code || instruction::LE(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->EQ()) code = code || instruction::EQ(resultAddress, leftOperandAddress, rightOperandAddress);
		else if (ctx->GE()) code = code || instruction::LE(resultAddress, rightOperandAddress, leftOperandAddress);
		else if (ctx->GT()) code = code || instruction::LT(resultAddress, rightOperandAddress, leftOperandAddress);
		else if (ctx->NE()) code = code || instruction__NE(resultAddress, leftOperandAddress, rightOperandAddress);
	}

	CodeAttribs attributes(resultAddress, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitLiteralValue(AslParser::LiteralValueContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && attributes = visit(ctx->literal());
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitLiteral(AslParser::LiteralContext * ctx) {
	DEBUG_ENTER();
	antlr4::ParserRuleContext * context;

	if (ctx->basicLiteral())
		context = ctx->basicLiteral();
	
	CodeAttribs && attributes = visit(context);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitBasicLiteral(AslParser::BasicLiteralContext * ctx) {
	DEBUG_ENTER();
	const std::string & value = ctx->getText();
	TypesMgr::TypeId type = getTypeDecor(ctx);
	std::string address = "%" + codeCounters.newTEMP();
	instructionList code;

	if (Types.isBooleanTy(type))
		code = instruction::ILOAD(address, value == "true" ? "1" : "0");
	else if (Types.isCharacterTy(type))
		code = instruction::CHLOAD(address, value.substr(1, value.size() - 2));
	else if (Types.isIntegerTy(type))
		code = instruction::ILOAD(address, value);
	else if (Types.isFloatTy(type))
		code = instruction::FLOAD(address, value);
	
	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitLeftExpressionValue(AslParser::LeftExpressionValueContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && leftExpression = visit(ctx->leftExpression());
	instructionList & code = leftExpression.code;
	std::string address = load(code, leftExpression.addr, leftExpression.offs);
	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitElement(AslParser::ElementContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && attributes = visit(ctx->id());
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitArrayElement(AslParser::ArrayElementContext * ctx) {
	DEBUG_ENTER();
	CodeAttribs && array = visit(ctx->id());
	CodeAttribs && expression = visit(ctx->expression());
	instructionList && code = array.code || expression.code;
	CodeAttribs attributes(array.addr, expression.addr, code);
	DEBUG_EXIT();
	return attributes;
}

antlrcpp::Any CodeGenVisitor::visitId(AslParser::IdContext * ctx) {
	DEBUG_ENTER();
	const std::string & name = ctx->getText();
	instructionList code;
	std::string address = dereference(code, name);
	CodeAttribs attributes(address, "", code);
	DEBUG_EXIT();
	return attributes;
}

std::string CodeGenVisitor::coerce(
		instructionList & code,
		TypesMgr::TypeId destinationType,
		TypesMgr::TypeId sourceType,
		const std::string & sourceAddress) {
	if (Types.isFloatTy(destinationType) and Types.isIntegerTy(sourceType)) {
		const std::string address = "%" + codeCounters.newTEMP();
		code = code || instruction::FLOAT(address, sourceAddress);
		return address;
	} else {
		return sourceAddress;
	}
}

std::string CodeGenVisitor::reference(
		instructionList & code,
		const std::string & name) {
	TypesMgr::TypeId type = Symbols.getType(name);
	if (Symbols.isLocalVarClass(name) and Types.isArrayTy(type)) {
		const std::string address = "%" + codeCounters.newTEMP();
		code = code || instruction::ALOAD(address, name);
		return address;
	} else {
		return name;
	}
}

std::string CodeGenVisitor::dereference(
		instructionList & code,
		const std::string & name) {
	TypesMgr::TypeId type = Symbols.getType(name);
	if (Symbols.isParameterClass(name) and Types.isArrayTy(type)) {
		const std::string address = "%" + codeCounters.newTEMP();
		code = code || instruction::LOAD(address, name);
		return address;
	} else {
		return name;
	}
}

std::string CodeGenVisitor::load(
		instructionList & code,
		const std::string & sourceAddress,
		const std::string & offsetAddress) {
	if (offsetAddress == "") {
		return sourceAddress;
	} else {
		const std::string address = "%" + codeCounters.newTEMP();
		code = code || instruction::LOADX(address, sourceAddress, offsetAddress);
		return address;
	}
}

void CodeGenVisitor::store(
		instructionList & code,
		const std::string & destinationAddress,
		const std::string & offsetAddress,
		const std::string & sourceAddress) {
	if (offsetAddress == "") {
		code = code || instruction::LOAD(destinationAddress, sourceAddress);
	} else {
		code = code || instruction::XLOAD(destinationAddress, offsetAddress, sourceAddress);
	}
}

std::string CodeGenVisitor::call(
 		instructionList & code,
		const std::string & functionName,
		std::vector <TypesMgr::TypeId> argumentTypes,
		std::vector <std::string> argumentAddresses) {
	TypesMgr::TypeId functionType = Symbols.getType(functionName);
	TypesMgr::TypeId returnType = Types.getFuncReturnType(functionType);
	std::size_t parameterCount = Types.getNumOfParameters(functionType);
	bool isNonVoid = Types.isPrimitiveNonVoidTy(returnType);
	std::string address;
		
	if (isNonVoid) {
		address = "%" + codeCounters.newTEMP();
		code = code || instruction::PUSH();
	}
	
	for (std::size_t i = 0; i < parameterCount; i++) {
		TypesMgr::TypeId parameterType = Types.getParameterType(functionType, i);
		TypesMgr::TypeId argumentType = argumentTypes[i];
		const std::string & argumentAddress = argumentAddresses[i];
		std::string address = argumentAddress;
		address = coerce(code, parameterType, argumentType, address);
		address = reference(code, address);
		code = code || instruction::PUSH(address);
	}
	
	code = code || instruction::CALL(functionName);
	
	for (std::size_t i = 0; i < parameterCount; i++) {
		code = code || instruction::POP();
	}

	if (isNonVoid) {
		code = code || instruction::POP(address);
	}
	
	return address;
}

instructionList CodeGenVisitor::copyArrayCode(
		TypesMgr::TypeId destinationType,
		TypesMgr::TypeId sourceType,
		const std::string & destinationAddress,
		const std::string & sourceAddress) {
	unsigned int destinationSize = Types.getArraySize(destinationType);
	unsigned int sourceSize = Types.getArraySize(sourceType);
	unsigned int size = std::min(destinationSize, sourceSize);
	std::string address = "%" + codeCounters.newTEMP();
	std::string counterAddress = "%" + codeCounters.newTEMP();
	instructionList doCode = instruction::LOADX(address, sourceAddress, counterAddress);
	address = coerce(doCode, destinationType, sourceType, address);
	doCode = doCode || instruction::XLOAD(destinationAddress, counterAddress, address);
	return forCode(counterAddress, doCode, 0, size, 1);
}

instructionList CodeGenVisitor::ifCode(
		instructionList & conditionCode,
		const std::string & evaluationAddress,
		instructionList & thenCode) {
	const std::string label = codeCounters.newLabelIF();
	return conditionCode
	    || instruction::FJUMP(evaluationAddress, "endif" + label)
	    || thenCode
	    || instruction::LABEL("endif" + label);
}

instructionList CodeGenVisitor::ifCode(
		instructionList & conditionCode,
		const std::string & evaluationAddress,
		instructionList & thenCode,
		instructionList & elseCode) {
	const std::string label = codeCounters.newLabelIF();
	return conditionCode
	    || instruction::FJUMP(evaluationAddress, "else" + label)
	    || thenCode
	    || instruction::UJUMP("endif" + label)
	    || instruction::LABEL("else" + label)
	    || elseCode
	    || instruction::LABEL("endif" + label);
}

instructionList CodeGenVisitor::whileCode(
		instructionList       & conditionCode,
		const std::string     & evaluationAddress,
		instructionList       & doCode) {
	const std::string label = codeCounters.newLabelWHILE();
	return instruction::LABEL("while" + label)
	    || conditionCode
	    || instruction::FJUMP(evaluationAddress, "endwhile" + label)
	    || doCode
	    || instruction::UJUMP("while" + label)
	    || instruction::LABEL("endwhile" + label);
}

instructionList CodeGenVisitor::forCode(
		const std::string & counterAddress,
		instructionList & doCode,
		const std::string & startAddress,
		const std::string & stopAddress,
		const std::string & stepAddress) {
	const std::string evaluationAddress = "%" + codeCounters.newTEMP();
	
	instructionList conditionCode =
		instruction::LT(evaluationAddress, counterAddress, stopAddress);
	instructionList whileDoCode =
		doCode ||
		instruction::ADD(counterAddress, counterAddress, stepAddress);
	
	return instruction::LOAD(counterAddress, startAddress)
      || whileCode(conditionCode, evaluationAddress, whileDoCode);
}

instructionList CodeGenVisitor::forCode(
		const std::string & counterAddress,
		instructionList & doCode,
		unsigned int start,
		unsigned int stop,
		unsigned int step) {
	const std::string startAddress = "%" + codeCounters.newTEMP();
	const std::string stopAddress = "%" + codeCounters.newTEMP();
	const std::string stepAddress = "%" + codeCounters.newTEMP();
	return instruction::ILOAD(startAddress, std::to_string(start))
      || instruction::ILOAD(stopAddress, std::to_string(stop))
      || instruction::ILOAD(stepAddress, std::to_string(step))
      || forCode(counterAddress, doCode, startAddress, stopAddress, stepAddress);
}

instructionList CodeGenVisitor::instruction__NE(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3) {
	const std::string a4 = "%" + codeCounters.newTEMP();
	return instruction::EQ(a4, a2, a3) 
	    || instruction::NOT(a1, a4);
}

instructionList CodeGenVisitor::instruction__FNE(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3) {
	const std::string a4 = "%" + codeCounters.newTEMP();
	return instruction::FEQ(a4, a2, a3) 
	    || instruction::NOT(a1, a4);
}

instructionList CodeGenVisitor::instruction__MOD(
		const std::string & a1,
		const std::string & a2,
		const std::string & a3) {
	const std::string a4 = "%" + codeCounters.newTEMP();
	const std::string a5 = "%" + codeCounters.newTEMP();
	return instruction::DIV(a4, a2, a3)
	    || instruction::MUL(a5, a4, a3)
	    || instruction::SUB(a1, a2, a5);
}

TypesMgr::TypeId CodeGenVisitor::getCurrentFunctionTy() const {
	return currFunctionType;
}

void CodeGenVisitor::setCurrentFunctionTy(TypesMgr::TypeId type) {
	currFunctionType = type;
}

SymTable::ScopeId CodeGenVisitor::getScopeDecor(antlr4::ParserRuleContext * ctx) const {
	return Decorations.getScope(ctx);
}
TypesMgr::TypeId CodeGenVisitor::getTypeDecor(antlr4::ParserRuleContext * ctx) const {
	return Decorations.getType(ctx);
}

CodeGenVisitor::CodeAttribs::CodeAttribs(
		const std::string & addr,
		const std::string & offs,
		instructionList & code) :
	addr{addr}, offs{offs}, code{code} {
}

CodeGenVisitor::CodeAttribs::CodeAttribs(
		const std::string & addr,
		const std::string & offs,
		instructionList && code) :
	addr{addr}, offs{offs}, code{code} {
}
