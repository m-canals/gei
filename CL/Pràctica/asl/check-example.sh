#!/usr/bin/env sh

example=${1%.asl}
tvm=${2:-../tvm/tvm-linux}
mode=$3

if expr match + "$example" '^.*/jp_chkt_[_A-Za-z0-9]*$' > /dev/null ||
   expr match + "$example" '^.*/jpbasic_chkt_[_A-Za-z0-9]*$' > /dev/null
then
	if test "$mode" = "debug"
	then
		./asl "$example.asl"
	else
		./asl "$example.asl" | grep -v '^[ ]*>>>'
	fi
	echo
	cat "$example.err"
else
	if ./asl "$example.asl" > "$example.t"
	then
		$tvm "$example.t" < "$example.in"
		echo
		cat "$example.out"
	else
		cat "$example.t"
	fi
fi

