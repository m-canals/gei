//////////////////////////////////////////////////////////////////////
//
//    SymbolsVisitor - Walk the parser tree to register symbols
//                     for the Asl programming language
//
//    Copyright (C) 2017-2022  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.
//
//////////////////////////////////////////////////////////////////////

#include "SymbolsVisitor.h"
#include "antlr4-runtime.h"
#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/SemErrors.h"
#include <iostream>
#include <string>
#include <vector>
#include <cstddef>
//#define DEBUG_BUILD
#include "../common/debug.h"

SymbolsVisitor::SymbolsVisitor(
		TypesMgr & Types,
		SymTable & Symbols,
		TreeDecoration & Decorations,
		SemErrors & Errors) :
	Types{Types},
	Symbols{Symbols},
	Decorations{Decorations},
	Errors{Errors} {
}

antlrcpp::Any SymbolsVisitor::visitProgram(AslParser::ProgramContext * ctx) {
	DEBUG_ENTER();
	SymTable::ScopeId scope = Symbols.pushNewScope(SymTable::GLOBAL_SCOPE_NAME);
	putScopeDecor(ctx, scope);
	
	for (const auto & function : ctx->function()) {
		visit(function);
	}
	
	Symbols.popScope();
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any SymbolsVisitor::visitFunction(AslParser::FunctionContext * ctx) {
	DEBUG_ENTER();
	const std::string & functionName = ctx->ID()->getText();
	bool redefined = Symbols.findInCurrentScope(functionName);
	
	if (redefined) {
  		Errors.declaredIdent(ctx->ID());
	}

	SymTable::ScopeId scope = Symbols.pushNewScope(functionName);
	putScopeDecor(ctx, scope);

	std::vector <TypesMgr::TypeId> parameterTypes;
	TypesMgr::TypeId returnType, functionType;
	
	if (ctx->parameters()) {
		visit(ctx->parameters());
		for (const auto & parameter : ctx->parameters()->parameter()) {
			TypesMgr::TypeId parameterType = getTypeDecor(parameter->type());
			parameterTypes.push_back(parameterType);
		}
	}
  
	if (ctx->basicType()) {
		visit(ctx->basicType());
		returnType = getTypeDecor(ctx->basicType());
	} else {
		returnType = Types.createVoidTy();
	}
  
	functionType = Types.createFunctionTy(parameterTypes, returnType);
	putTypeDecor(ctx, functionType);
  
	visit(ctx->variables());
  
	Symbols.popScope();
  
	if (not redefined) {
  		Symbols.addFunction(functionName, functionType);
	}

	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any SymbolsVisitor::visitParameter(AslParser::ParameterContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->type());
	TypesMgr::TypeId type = getTypeDecor(ctx->type());
	const std::string & name = ctx->ID()->getText();
	
	if (Symbols.findInCurrentScope(name)) {
		Errors.declaredIdent(ctx->ID());
	} else {
	  Symbols.addParameter(name, type);
	}
	
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any SymbolsVisitor::visitVariable(AslParser::VariableContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->type());
	TypesMgr::TypeId type = getTypeDecor(ctx->type());
	
	for (const auto & id : ctx->ID()) {
		const std::string & name = id->getText();
		if (Symbols.findInCurrentScope(name)) {
		  Errors.declaredIdent(id);
		} else {
		  Symbols.addLocalVar(name, type);
		}
	}
	
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any SymbolsVisitor::visitType(AslParser::TypeContext * ctx) {
	DEBUG_ENTER();
	visitChildren(ctx);
	TypesMgr::TypeId type;
	
	if (ctx->basicType()) type = getTypeDecor(ctx->basicType());
	else if (ctx->arrayType()) type = getTypeDecor(ctx->arrayType());

	putTypeDecor(ctx, type);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any SymbolsVisitor::visitBasicType(AslParser::BasicTypeContext * ctx) {
	DEBUG_ENTER();
	TypesMgr::TypeId type;
	
	if (ctx->BOOL()) type = Types.createBooleanTy();
	else if (ctx->CHAR()) type = Types.createCharacterTy();
	else if (ctx->INT()) type = Types.createIntegerTy();
	else if (ctx->FLOAT()) type = Types.createFloatTy();
	else type = Types.createErrorTy();
	
	putTypeDecor(ctx, type);
	DEBUG_EXIT();
	return 0;
}

antlrcpp::Any SymbolsVisitor::visitArrayType(AslParser::ArrayTypeContext * ctx) {
	DEBUG_ENTER();
	visit(ctx->basicType());
	TypesMgr::TypeId basicType = getTypeDecor(ctx->basicType());
	unsigned int size = std::stoul(ctx->NATURAL_NUMBER()->getText());
	TypesMgr::TypeId type = Types.createArrayTy(size, basicType);
	putTypeDecor(ctx, type);
	DEBUG_EXIT();
	return 0;
}

SymTable::ScopeId SymbolsVisitor::getScopeDecor(antlr4::ParserRuleContext * ctx) {
	return Decorations.getScope(ctx);
}

TypesMgr::TypeId SymbolsVisitor::getTypeDecor(antlr4::ParserRuleContext * ctx) {
	return Decorations.getType(ctx);
}

void SymbolsVisitor::putScopeDecor(antlr4::ParserRuleContext * ctx, SymTable::ScopeId s) {
	Decorations.putScope(ctx, s);
}

void SymbolsVisitor::putTypeDecor(antlr4::ParserRuleContext * ctx, TypesMgr::TypeId t) {
	Decorations.putType(ctx, t);
}
