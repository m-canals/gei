
@.str.f = constant [3 x i8] c"%g\00"
@.str.c = constant [3 x i8] c"%c\00"
@.str.s.1 = constant [2 x i8] c"\0A\00"


@.global.c.addr = common dso_local global i8 0


define dso_local i32 @read_chars(i8* %a) {
  .entry:
    %_result.addr = alloca i32
    %a.addr = alloca i8*
    %i.addr = alloca i32
    store i8* %a, i8** %a.addr
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    br label %while1
  while1:
    %.temp.2 = trunc i64 10 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.3 = icmp slt i32 %i.1, %.temp.2
    br i1 %.temp.3, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.4 = load i8*, i8** %a.addr
    call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.c, i64 0, i64 0), i8* @.global.c.addr)
    %.temp.5 = load i8, i8* @.global.c.addr
    %i.2 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.2 to i64
    %.arrPtr.1 = getelementptr inbounds i8, i8* %.temp.4, i64 %.idx64.1
    store i8 %.temp.5, i8* %.arrPtr.1
    %.temp.6 = load i8*, i8** %a.addr
    %i.3 = load i32, i32* %i.addr
    %.idx64.2 = sext i32 %i.3 to i64
    %.arrPtr.2 = getelementptr inbounds i8, i8* %.temp.6, i64 %.idx64.2
    %.temp.7 = load i8, i8* %.arrPtr.2
    %.temp.8 = trunc i32 46 to i8
    %.temp.10 = icmp eq i8 %.temp.7, %.temp.8
    %.temp.9 = xor i1 %.temp.10, 1
    br i1 %.temp.9, label %.br.cont.2, label %else1
  .br.cont.2:
    %.temp.11 = trunc i64 1 to i32
    %i.4 = load i32, i32* %i.addr
    %.temp.12 = add i32 %i.4, %.temp.11
    store i32 %.temp.12, i32* %i.addr
    br label %endif1
  else1:
    %i.5 = load i32, i32* %i.addr
    store i32 %i.5, i32* %_result.addr
    %_result.1 = load i32, i32* %_result.addr
    ret i32 %_result.1
  endif1:
    br label %while1
  endwhile1:
    %.temp.13 = trunc i64 10 to i32
    store i32 %.temp.13, i32* %_result.addr
    %_result.2 = load i32, i32* %_result.addr
    ret i32 %_result.2
  .dead.code.1:
    %_result.3 = load i32, i32* %_result.addr
    ret i32 %_result.3
}

define dso_local void @find_vowels(i32 %n, i8* %vc, i1* %vb) {
  .entry:
    %n.addr = alloca i32
    %vc.addr = alloca i8*
    %vb.addr = alloca i1*
    %c.addr = alloca i8
    store i32 %n, i32* %n.addr
    store i8* %vc, i8** %vc.addr
    store i1* %vb, i1** %vb.addr
    br label %while1
  while1:
    %.temp.1 = trunc i64 0 to i32
    %n.1 = load i32, i32* %n.addr
    %.temp.2 = icmp slt i32 %.temp.1, %n.1
    br i1 %.temp.2, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.3 = load i8*, i8** %vc.addr
    %.temp.4 = trunc i64 1 to i32
    %n.2 = load i32, i32* %n.addr
    %.temp.5 = sub i32 %n.2, %.temp.4
    %.idx64.1 = sext i32 %.temp.5 to i64
    %.arrPtr.1 = getelementptr inbounds i8, i8* %.temp.3, i64 %.idx64.1
    %.temp.6 = load i8, i8* %.arrPtr.1
    store i8 %.temp.6, i8* %c.addr
    %.temp.7 = load i1*, i1** %vb.addr
    %.temp.8 = trunc i64 1 to i32
    %n.3 = load i32, i32* %n.addr
    %.temp.9 = sub i32 %n.3, %.temp.8
    %.temp.10 = trunc i32 97 to i8
    %c.1 = load i8, i8* %c.addr
    %.temp.11 = icmp eq i8 %c.1, %.temp.10
    %.temp.12 = trunc i32 101 to i8
    %c.2 = load i8, i8* %c.addr
    %.temp.13 = icmp eq i8 %c.2, %.temp.12
    %.temp.14 = or i1 %.temp.11, %.temp.13
    %.temp.15 = trunc i32 105 to i8
    %c.3 = load i8, i8* %c.addr
    %.temp.16 = icmp eq i8 %c.3, %.temp.15
    %.temp.17 = or i1 %.temp.14, %.temp.16
    %.temp.18 = trunc i32 111 to i8
    %c.4 = load i8, i8* %c.addr
    %.temp.19 = icmp eq i8 %c.4, %.temp.18
    %.temp.20 = or i1 %.temp.17, %.temp.19
    %.temp.21 = trunc i32 117 to i8
    %c.5 = load i8, i8* %c.addr
    %.temp.22 = icmp eq i8 %c.5, %.temp.21
    %.temp.23 = or i1 %.temp.20, %.temp.22
    %.idx64.2 = sext i32 %.temp.9 to i64
    %.arrPtr.2 = getelementptr inbounds i1, i1* %.temp.7, i64 %.idx64.2
    store i1 %.temp.23, i1* %.arrPtr.2
    %.temp.24 = trunc i64 1 to i32
    %n.4 = load i32, i32* %n.addr
    %.temp.25 = sub i32 %n.4, %.temp.24
    store i32 %.temp.25, i32* %n.addr
    br label %while1
  endwhile1:
    ret void
}

define dso_local float @write_consonants(i32 %n, i8* %vc, i1* %vb) {
  .entry:
    %_result.addr = alloca float
    %n.addr = alloca i32
    %vc.addr = alloca i8*
    %vb.addr = alloca i1*
    %k.addr = alloca float
    %i.addr = alloca i32
    store i32 %n, i32* %n.addr
    store i8* %vc, i8** %vc.addr
    store i1* %vb, i1** %vb.addr
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    %.temp.2 = trunc i64 0 to i32
    %.temp.3 = sitofp i32 %.temp.2 to float
    store float %.temp.3, float* %k.addr
    br label %while1
  while1:
    %i.1 = load i32, i32* %i.addr
    %n.1 = load i32, i32* %n.addr
    %.temp.5 = icmp eq i32 %i.1, %n.1
    %.temp.4 = xor i1 %.temp.5, 1
    br i1 %.temp.4, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.6 = load i1*, i1** %vb.addr
    %i.2 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.2 to i64
    %.arrPtr.1 = getelementptr inbounds i1, i1* %.temp.6, i64 %.idx64.1
    %.temp.7 = load i1, i1* %.arrPtr.1
    br i1 %.temp.7, label %.br.cont.2, label %else1
  .br.cont.2:
    %.temp.8 = trunc i64 1 to i32
    %.temp.10 = sitofp i32 %.temp.8 to float
    %k.1 = load float, float* %k.addr
    %.temp.9 = fadd float %k.1, %.temp.10
    store float %.temp.9, float* %k.addr
    br label %endif1
  else1:
    %.temp.11 = load i8*, i8** %vc.addr
    %i.3 = load i32, i32* %i.addr
    %.idx64.2 = sext i32 %i.3 to i64
    %.arrPtr.2 = getelementptr inbounds i8, i8* %.temp.11, i64 %.idx64.2
    %.temp.12 = load i8, i8* %.arrPtr.2
    %.wrtc.i32.1 = zext i8 %.temp.12 to i32
    call i32 @putchar(i32 %.wrtc.i32.1)
    br label %endif1
  endif1:
    %.temp.13 = trunc i64 1 to i32
    %i.4 = load i32, i32* %i.addr
    %.temp.14 = add i32 %i.4, %.temp.13
    store i32 %.temp.14, i32* %i.addr
    br label %while1
  endwhile1:
    %.temp.15 = trunc i32 10 to i8
    %.wrtc.i32.2 = zext i8 %.temp.15 to i32
    call i32 @putchar(i32 %.wrtc.i32.2)
    %.temp.16 = trunc i64 100 to i32
    %.temp.18 = sitofp i32 %.temp.16 to float
    %k.2 = load float, float* %k.addr
    %.temp.17 = fmul float %.temp.18, %k.2
    %n.2 = load i32, i32* %n.addr
    %.temp.20 = sitofp i32 %n.2 to float
    %.temp.19 = fdiv float %.temp.17, %.temp.20
    store float %.temp.19, float* %_result.addr
    %_result.1 = load float, float* %_result.addr
    ret float %_result.1
  .dead.code.1:
    %_result.2 = load float, float* %_result.addr
    ret float %_result.2
}

define dso_local i32 @main() {
  .entry:
    %a.addr = alloca [10 x i8]
    %b.addr = alloca [10 x i1]
    %n.addr = alloca i32
    %p.addr = alloca float
    %.temp.2 = getelementptr inbounds [10 x i8], [10 x i8]* %a.addr, i64 0, i64 0
    %.temp.1 = call i32 @read_chars(i8* %.temp.2)
    store i32 %.temp.1, i32* %n.addr
    %n.1 = load i32, i32* %n.addr
    %.temp.3 = getelementptr inbounds [10 x i8], [10 x i8]* %a.addr, i64 0, i64 0
    %.temp.4 = getelementptr inbounds [10 x i1], [10 x i1]* %b.addr, i64 0, i64 0
    call void @find_vowels(i32 %n.1, i8* %.temp.3, i1* %.temp.4)
    %n.2 = load i32, i32* %n.addr
    %.temp.6 = getelementptr inbounds [10 x i8], [10 x i8]* %a.addr, i64 0, i64 0
    %.temp.7 = getelementptr inbounds [10 x i1], [10 x i1]* %b.addr, i64 0, i64 0
    %.temp.5 = call float @write_consonants(i32 %n.2, i8* %.temp.6, i1* %.temp.7)
    store float %.temp.5, float* %p.addr
    %p.1 = load float, float* %p.addr
    %.wrtf.double.1 = fpext float %p.1 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.1)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)
declare dso_local i32 @putchar(i32)
declare dso_local i32 @__isoc99_scanf(i8*, ...)


