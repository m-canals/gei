
@.str.i = constant [3 x i8] c"%d\00"
@.str.c = constant [3 x i8] c"%c\00"
@.str.s.1 = constant [10 x i8] c"en f. c: \00"
@.str.s.2 = constant [20 x i8] c"despres de b=a. b: \00"
@.str.s.3 = constant [20 x i8] c"despres de b=a. a: \00"
@.str.s.4 = constant [21 x i8] c"despres de f(a). a: \00"
@.str.s.5 = constant [21 x i8] c"despres de g(a). a: \00"




define dso_local void @f(i32* %v) {
  .entry:
    %v.addr = alloca i32*
    %c.addr = alloca [10 x i32]
    %i.addr = alloca i32
    store i32* %v, i32** %v.addr
    %.temp.1 = load i32*, i32** %v.addr
    %.temp.3 = trunc i64 0 to i32
    %.idx64.1 = sext i32 %.temp.3 to i64
    %.arrPtr.1 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.1
    %.temp.2 = load i32, i32* %.arrPtr.1
    %.idx64.2 = sext i32 %.temp.3 to i64
    %.arrPtr.2 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.2
    store i32 %.temp.2, i32* %.arrPtr.2
    %.temp.5 = trunc i64 1 to i32
    %.idx64.3 = sext i32 %.temp.5 to i64
    %.arrPtr.3 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.3
    %.temp.4 = load i32, i32* %.arrPtr.3
    %.idx64.4 = sext i32 %.temp.5 to i64
    %.arrPtr.4 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.4
    store i32 %.temp.4, i32* %.arrPtr.4
    %.temp.7 = trunc i64 2 to i32
    %.idx64.5 = sext i32 %.temp.7 to i64
    %.arrPtr.5 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.5
    %.temp.6 = load i32, i32* %.arrPtr.5
    %.idx64.6 = sext i32 %.temp.7 to i64
    %.arrPtr.6 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.6
    store i32 %.temp.6, i32* %.arrPtr.6
    %.temp.9 = trunc i64 3 to i32
    %.idx64.7 = sext i32 %.temp.9 to i64
    %.arrPtr.7 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.7
    %.temp.8 = load i32, i32* %.arrPtr.7
    %.idx64.8 = sext i32 %.temp.9 to i64
    %.arrPtr.8 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.8
    store i32 %.temp.8, i32* %.arrPtr.8
    %.temp.11 = trunc i64 4 to i32
    %.idx64.9 = sext i32 %.temp.11 to i64
    %.arrPtr.9 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.9
    %.temp.10 = load i32, i32* %.arrPtr.9
    %.idx64.10 = sext i32 %.temp.11 to i64
    %.arrPtr.10 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.10
    store i32 %.temp.10, i32* %.arrPtr.10
    %.temp.13 = trunc i64 5 to i32
    %.idx64.11 = sext i32 %.temp.13 to i64
    %.arrPtr.11 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.11
    %.temp.12 = load i32, i32* %.arrPtr.11
    %.idx64.12 = sext i32 %.temp.13 to i64
    %.arrPtr.12 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.12
    store i32 %.temp.12, i32* %.arrPtr.12
    %.temp.15 = trunc i64 6 to i32
    %.idx64.13 = sext i32 %.temp.15 to i64
    %.arrPtr.13 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.13
    %.temp.14 = load i32, i32* %.arrPtr.13
    %.idx64.14 = sext i32 %.temp.15 to i64
    %.arrPtr.14 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.14
    store i32 %.temp.14, i32* %.arrPtr.14
    %.temp.17 = trunc i64 7 to i32
    %.idx64.15 = sext i32 %.temp.17 to i64
    %.arrPtr.15 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.15
    %.temp.16 = load i32, i32* %.arrPtr.15
    %.idx64.16 = sext i32 %.temp.17 to i64
    %.arrPtr.16 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.16
    store i32 %.temp.16, i32* %.arrPtr.16
    %.temp.19 = trunc i64 8 to i32
    %.idx64.17 = sext i32 %.temp.19 to i64
    %.arrPtr.17 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.17
    %.temp.18 = load i32, i32* %.arrPtr.17
    %.idx64.18 = sext i32 %.temp.19 to i64
    %.arrPtr.18 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.18
    store i32 %.temp.18, i32* %.arrPtr.18
    %.temp.21 = trunc i64 9 to i32
    %.idx64.19 = sext i32 %.temp.21 to i64
    %.arrPtr.19 = getelementptr inbounds i32, i32* %.temp.1, i64 %.idx64.19
    %.temp.20 = load i32, i32* %.arrPtr.19
    %.idx64.20 = sext i32 %.temp.21 to i64
    %.arrPtr.20 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.20
    store i32 %.temp.20, i32* %.arrPtr.20
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.22 = trunc i64 0 to i32
    store i32 %.temp.22, i32* %i.addr
    br label %while1
  while1:
    %.temp.23 = trunc i64 10 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.24 = icmp slt i32 %i.1, %.temp.23
    br i1 %.temp.24, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %i.2 = load i32, i32* %i.addr
    %.idx64.21 = sext i32 %i.2 to i64
    %.arrPtr.21 = getelementptr inbounds [10 x i32], [10 x i32]* %c.addr, i64 0, i64 %.idx64.21
    %.temp.25 = load i32, i32* %.arrPtr.21
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.25)
    %.temp.26 = trunc i32 32 to i8
    %.wrtc.i32.1 = zext i8 %.temp.26 to i32
    call i32 @putchar(i32 %.wrtc.i32.1)
    %.temp.27 = trunc i64 1 to i32
    %i.3 = load i32, i32* %i.addr
    %.temp.28 = add i32 %i.3, %.temp.27
    store i32 %.temp.28, i32* %i.addr
    br label %while1
  endwhile1:
    %.temp.29 = trunc i32 10 to i8
    %.wrtc.i32.2 = zext i8 %.temp.29 to i32
    call i32 @putchar(i32 %.wrtc.i32.2)
    ret void
}

define dso_local void @g(i32* %v) {
  .entry:
    %v.addr = alloca i32*
    %d.addr = alloca [10 x i32]
    %i.addr = alloca i32
    store i32* %v, i32** %v.addr
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    br label %while1
  while1:
    %.temp.2 = trunc i64 10 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.3 = icmp slt i32 %i.1, %.temp.2
    br i1 %.temp.3, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.4 = trunc i64 1 to i32
    %.temp.5 = sub i32 0, %.temp.4
    %i.2 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.2 to i64
    %.arrPtr.1 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.1
    store i32 %.temp.5, i32* %.arrPtr.1
    %.temp.6 = trunc i64 1 to i32
    %i.3 = load i32, i32* %i.addr
    %.temp.7 = add i32 %i.3, %.temp.6
    store i32 %.temp.7, i32* %i.addr
    br label %while1
  endwhile1:
    %.temp.8 = load i32*, i32** %v.addr
    %.temp.10 = trunc i64 0 to i32
    %.idx64.2 = sext i32 %.temp.10 to i64
    %.arrPtr.2 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.2
    %.temp.9 = load i32, i32* %.arrPtr.2
    %.idx64.3 = sext i32 %.temp.10 to i64
    %.arrPtr.3 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.3
    store i32 %.temp.9, i32* %.arrPtr.3
    %.temp.12 = trunc i64 1 to i32
    %.idx64.4 = sext i32 %.temp.12 to i64
    %.arrPtr.4 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.4
    %.temp.11 = load i32, i32* %.arrPtr.4
    %.idx64.5 = sext i32 %.temp.12 to i64
    %.arrPtr.5 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.5
    store i32 %.temp.11, i32* %.arrPtr.5
    %.temp.14 = trunc i64 2 to i32
    %.idx64.6 = sext i32 %.temp.14 to i64
    %.arrPtr.6 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.6
    %.temp.13 = load i32, i32* %.arrPtr.6
    %.idx64.7 = sext i32 %.temp.14 to i64
    %.arrPtr.7 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.7
    store i32 %.temp.13, i32* %.arrPtr.7
    %.temp.16 = trunc i64 3 to i32
    %.idx64.8 = sext i32 %.temp.16 to i64
    %.arrPtr.8 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.8
    %.temp.15 = load i32, i32* %.arrPtr.8
    %.idx64.9 = sext i32 %.temp.16 to i64
    %.arrPtr.9 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.9
    store i32 %.temp.15, i32* %.arrPtr.9
    %.temp.18 = trunc i64 4 to i32
    %.idx64.10 = sext i32 %.temp.18 to i64
    %.arrPtr.10 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.10
    %.temp.17 = load i32, i32* %.arrPtr.10
    %.idx64.11 = sext i32 %.temp.18 to i64
    %.arrPtr.11 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.11
    store i32 %.temp.17, i32* %.arrPtr.11
    %.temp.20 = trunc i64 5 to i32
    %.idx64.12 = sext i32 %.temp.20 to i64
    %.arrPtr.12 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.12
    %.temp.19 = load i32, i32* %.arrPtr.12
    %.idx64.13 = sext i32 %.temp.20 to i64
    %.arrPtr.13 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.13
    store i32 %.temp.19, i32* %.arrPtr.13
    %.temp.22 = trunc i64 6 to i32
    %.idx64.14 = sext i32 %.temp.22 to i64
    %.arrPtr.14 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.14
    %.temp.21 = load i32, i32* %.arrPtr.14
    %.idx64.15 = sext i32 %.temp.22 to i64
    %.arrPtr.15 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.15
    store i32 %.temp.21, i32* %.arrPtr.15
    %.temp.24 = trunc i64 7 to i32
    %.idx64.16 = sext i32 %.temp.24 to i64
    %.arrPtr.16 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.16
    %.temp.23 = load i32, i32* %.arrPtr.16
    %.idx64.17 = sext i32 %.temp.24 to i64
    %.arrPtr.17 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.17
    store i32 %.temp.23, i32* %.arrPtr.17
    %.temp.26 = trunc i64 8 to i32
    %.idx64.18 = sext i32 %.temp.26 to i64
    %.arrPtr.18 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.18
    %.temp.25 = load i32, i32* %.arrPtr.18
    %.idx64.19 = sext i32 %.temp.26 to i64
    %.arrPtr.19 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.19
    store i32 %.temp.25, i32* %.arrPtr.19
    %.temp.28 = trunc i64 9 to i32
    %.idx64.20 = sext i32 %.temp.28 to i64
    %.arrPtr.20 = getelementptr inbounds [10 x i32], [10 x i32]* %d.addr, i64 0, i64 %.idx64.20
    %.temp.27 = load i32, i32* %.arrPtr.20
    %.idx64.21 = sext i32 %.temp.28 to i64
    %.arrPtr.21 = getelementptr inbounds i32, i32* %.temp.8, i64 %.idx64.21
    store i32 %.temp.27, i32* %.arrPtr.21
    ret void
}

define dso_local i32 @main() {
  .entry:
    %a.addr = alloca [10 x i32]
    %b.addr = alloca [10 x i32]
    %i.addr = alloca i32
    %j.addr = alloca i32
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    br label %while1
  while1:
    %.temp.2 = trunc i64 10 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.3 = icmp slt i32 %i.1, %.temp.2
    br i1 %.temp.3, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %i.2 = load i32, i32* %i.addr
    %i.3 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.2 to i64
    %.arrPtr.1 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.1
    store i32 %i.3, i32* %.arrPtr.1
    %.temp.4 = trunc i64 0 to i32
    %i.4 = load i32, i32* %i.addr
    %.idx64.2 = sext i32 %i.4 to i64
    %.arrPtr.2 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.2
    store i32 %.temp.4, i32* %.arrPtr.2
    %.temp.5 = trunc i64 1 to i32
    %i.5 = load i32, i32* %i.addr
    %.temp.6 = add i32 %i.5, %.temp.5
    store i32 %.temp.6, i32* %i.addr
    br label %while1
  endwhile1:
    %.temp.8 = trunc i64 0 to i32
    %.idx64.3 = sext i32 %.temp.8 to i64
    %.arrPtr.3 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.3
    %.temp.7 = load i32, i32* %.arrPtr.3
    %.idx64.4 = sext i32 %.temp.8 to i64
    %.arrPtr.4 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.4
    store i32 %.temp.7, i32* %.arrPtr.4
    %.temp.10 = trunc i64 1 to i32
    %.idx64.5 = sext i32 %.temp.10 to i64
    %.arrPtr.5 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.5
    %.temp.9 = load i32, i32* %.arrPtr.5
    %.idx64.6 = sext i32 %.temp.10 to i64
    %.arrPtr.6 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.6
    store i32 %.temp.9, i32* %.arrPtr.6
    %.temp.12 = trunc i64 2 to i32
    %.idx64.7 = sext i32 %.temp.12 to i64
    %.arrPtr.7 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.7
    %.temp.11 = load i32, i32* %.arrPtr.7
    %.idx64.8 = sext i32 %.temp.12 to i64
    %.arrPtr.8 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.8
    store i32 %.temp.11, i32* %.arrPtr.8
    %.temp.14 = trunc i64 3 to i32
    %.idx64.9 = sext i32 %.temp.14 to i64
    %.arrPtr.9 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.9
    %.temp.13 = load i32, i32* %.arrPtr.9
    %.idx64.10 = sext i32 %.temp.14 to i64
    %.arrPtr.10 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.10
    store i32 %.temp.13, i32* %.arrPtr.10
    %.temp.16 = trunc i64 4 to i32
    %.idx64.11 = sext i32 %.temp.16 to i64
    %.arrPtr.11 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.11
    %.temp.15 = load i32, i32* %.arrPtr.11
    %.idx64.12 = sext i32 %.temp.16 to i64
    %.arrPtr.12 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.12
    store i32 %.temp.15, i32* %.arrPtr.12
    %.temp.18 = trunc i64 5 to i32
    %.idx64.13 = sext i32 %.temp.18 to i64
    %.arrPtr.13 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.13
    %.temp.17 = load i32, i32* %.arrPtr.13
    %.idx64.14 = sext i32 %.temp.18 to i64
    %.arrPtr.14 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.14
    store i32 %.temp.17, i32* %.arrPtr.14
    %.temp.20 = trunc i64 6 to i32
    %.idx64.15 = sext i32 %.temp.20 to i64
    %.arrPtr.15 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.15
    %.temp.19 = load i32, i32* %.arrPtr.15
    %.idx64.16 = sext i32 %.temp.20 to i64
    %.arrPtr.16 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.16
    store i32 %.temp.19, i32* %.arrPtr.16
    %.temp.22 = trunc i64 7 to i32
    %.idx64.17 = sext i32 %.temp.22 to i64
    %.arrPtr.17 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.17
    %.temp.21 = load i32, i32* %.arrPtr.17
    %.idx64.18 = sext i32 %.temp.22 to i64
    %.arrPtr.18 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.18
    store i32 %.temp.21, i32* %.arrPtr.18
    %.temp.24 = trunc i64 8 to i32
    %.idx64.19 = sext i32 %.temp.24 to i64
    %.arrPtr.19 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.19
    %.temp.23 = load i32, i32* %.arrPtr.19
    %.idx64.20 = sext i32 %.temp.24 to i64
    %.arrPtr.20 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.20
    store i32 %.temp.23, i32* %.arrPtr.20
    %.temp.26 = trunc i64 9 to i32
    %.idx64.21 = sext i32 %.temp.26 to i64
    %.arrPtr.21 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.21
    %.temp.25 = load i32, i32* %.arrPtr.21
    %.idx64.22 = sext i32 %.temp.26 to i64
    %.arrPtr.22 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.22
    store i32 %.temp.25, i32* %.arrPtr.22
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.s.2, i64 0, i64 0))
    %.temp.27 = trunc i64 0 to i32
    store i32 %.temp.27, i32* %i.addr
    br label %while2
  while2:
    %.temp.28 = trunc i64 10 to i32
    %i.6 = load i32, i32* %i.addr
    %.temp.29 = icmp slt i32 %i.6, %.temp.28
    br i1 %.temp.29, label %.br.cont.2, label %endwhile2
  .br.cont.2:
    %i.7 = load i32, i32* %i.addr
    %.idx64.23 = sext i32 %i.7 to i64
    %.arrPtr.23 = getelementptr inbounds [10 x i32], [10 x i32]* %b.addr, i64 0, i64 %.idx64.23
    %.temp.30 = load i32, i32* %.arrPtr.23
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.30)
    %.temp.31 = trunc i32 32 to i8
    %.wrtc.i32.1 = zext i8 %.temp.31 to i32
    call i32 @putchar(i32 %.wrtc.i32.1)
    %.temp.32 = trunc i64 1 to i32
    %i.8 = load i32, i32* %i.addr
    %.temp.33 = add i32 %i.8, %.temp.32
    store i32 %.temp.33, i32* %i.addr
    br label %while2
  endwhile2:
    %.temp.34 = trunc i32 10 to i8
    %.wrtc.i32.2 = zext i8 %.temp.34 to i32
    call i32 @putchar(i32 %.wrtc.i32.2)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.s.3, i64 0, i64 0))
    %.temp.35 = trunc i64 0 to i32
    store i32 %.temp.35, i32* %i.addr
    br label %while3
  while3:
    %.temp.36 = trunc i64 10 to i32
    %i.9 = load i32, i32* %i.addr
    %.temp.37 = icmp slt i32 %i.9, %.temp.36
    br i1 %.temp.37, label %.br.cont.3, label %endwhile3
  .br.cont.3:
    %i.10 = load i32, i32* %i.addr
    %.idx64.24 = sext i32 %i.10 to i64
    %.arrPtr.24 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.24
    %.temp.38 = load i32, i32* %.arrPtr.24
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.38)
    %.temp.39 = trunc i32 32 to i8
    %.wrtc.i32.3 = zext i8 %.temp.39 to i32
    call i32 @putchar(i32 %.wrtc.i32.3)
    %.temp.40 = trunc i64 1 to i32
    %i.11 = load i32, i32* %i.addr
    %.temp.41 = add i32 %i.11, %.temp.40
    store i32 %.temp.41, i32* %i.addr
    br label %while3
  endwhile3:
    %.temp.42 = trunc i32 10 to i8
    %.wrtc.i32.4 = zext i8 %.temp.42 to i32
    call i32 @putchar(i32 %.wrtc.i32.4)
    %.temp.43 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 0
    call void @f(i32* %.temp.43)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.s.4, i64 0, i64 0))
    %.temp.44 = trunc i64 0 to i32
    store i32 %.temp.44, i32* %i.addr
    br label %while4
  while4:
    %.temp.45 = trunc i64 10 to i32
    %i.12 = load i32, i32* %i.addr
    %.temp.46 = icmp slt i32 %i.12, %.temp.45
    br i1 %.temp.46, label %.br.cont.4, label %endwhile4
  .br.cont.4:
    %i.13 = load i32, i32* %i.addr
    %.idx64.25 = sext i32 %i.13 to i64
    %.arrPtr.25 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.25
    %.temp.47 = load i32, i32* %.arrPtr.25
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.47)
    %.temp.48 = trunc i32 32 to i8
    %.wrtc.i32.5 = zext i8 %.temp.48 to i32
    call i32 @putchar(i32 %.wrtc.i32.5)
    %.temp.49 = trunc i64 1 to i32
    %i.14 = load i32, i32* %i.addr
    %.temp.50 = add i32 %i.14, %.temp.49
    store i32 %.temp.50, i32* %i.addr
    br label %while4
  endwhile4:
    %.temp.51 = trunc i32 10 to i8
    %.wrtc.i32.6 = zext i8 %.temp.51 to i32
    call i32 @putchar(i32 %.wrtc.i32.6)
    %.temp.52 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 0
    call void @g(i32* %.temp.52)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.s.5, i64 0, i64 0))
    %.temp.53 = trunc i64 0 to i32
    store i32 %.temp.53, i32* %i.addr
    br label %while5
  while5:
    %.temp.54 = trunc i64 10 to i32
    %i.15 = load i32, i32* %i.addr
    %.temp.55 = icmp slt i32 %i.15, %.temp.54
    br i1 %.temp.55, label %.br.cont.5, label %endwhile5
  .br.cont.5:
    %i.16 = load i32, i32* %i.addr
    %.idx64.26 = sext i32 %i.16 to i64
    %.arrPtr.26 = getelementptr inbounds [10 x i32], [10 x i32]* %a.addr, i64 0, i64 %.idx64.26
    %.temp.56 = load i32, i32* %.arrPtr.26
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.56)
    %.temp.57 = trunc i32 32 to i8
    %.wrtc.i32.7 = zext i8 %.temp.57 to i32
    call i32 @putchar(i32 %.wrtc.i32.7)
    %.temp.58 = trunc i64 1 to i32
    %i.17 = load i32, i32* %i.addr
    %.temp.59 = add i32 %i.17, %.temp.58
    store i32 %.temp.59, i32* %i.addr
    br label %while5
  endwhile5:
    %.temp.60 = trunc i32 10 to i8
    %.wrtc.i32.8 = zext i8 %.temp.60 to i32
    call i32 @putchar(i32 %.wrtc.i32.8)
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)
declare dso_local i32 @putchar(i32)


