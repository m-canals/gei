
@.str.i = constant [3 x i8] c"%d\00"
@.str.s.1 = constant [3 x i8] c"!=\00"
@.str.s.2 = constant [2 x i8] c"\0A\00"


@.global.i.addr = common dso_local global i32 0


define dso_local i32 @fact(i32 %n) {
  .entry:
    %_result.addr = alloca i32
    %n.addr = alloca i32
    %f.addr = alloca i32
    store i32 %n, i32* %n.addr
    %.temp.1 = trunc i64 1 to i32
    store i32 %.temp.1, i32* %f.addr
    br label %while1
  while1:
    %.temp.2 = trunc i64 1 to i32
    %n.1 = load i32, i32* %n.addr
    %.temp.3 = icmp slt i32 %.temp.2, %n.1
    br i1 %.temp.3, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %f.1 = load i32, i32* %f.addr
    %n.2 = load i32, i32* %n.addr
    %.temp.4 = mul i32 %f.1, %n.2
    store i32 %.temp.4, i32* %f.addr
    %.temp.5 = trunc i64 1 to i32
    %n.3 = load i32, i32* %n.addr
    %.temp.6 = sub i32 %n.3, %.temp.5
    store i32 %.temp.6, i32* %n.addr
    br label %while1
  endwhile1:
    %f.2 = load i32, i32* %f.addr
    store i32 %f.2, i32* %_result.addr
    %_result.1 = load i32, i32* %_result.addr
    ret i32 %_result.1
  .dead.code.1:
    %_result.2 = load i32, i32* %_result.addr
    ret i32 %_result.2
}

define dso_local i32 @main() {
  .entry:
    %max.addr = alloca i32
    %i.addr = alloca i32
    %f.addr = alloca i32
    %end.addr = alloca i1
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    %.temp.2 = trunc i64 1 to i1
    store i1 %.temp.2, i1* %end.addr
    call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32* @.global.i.addr)
    %.temp.3 = load i32, i32* @.global.i.addr
    store i32 %.temp.3, i32* %max.addr
    %i.1 = load i32, i32* %i.addr
    %max.1 = load i32, i32* %max.addr
    %.temp.4 = icmp sle i32 %i.1, %max.1
    br i1 %.temp.4, label %.br.cont.1, label %else1
  .br.cont.1:
    %.temp.5 = trunc i64 0 to i1
    store i1 %.temp.5, i1* %end.addr
    br label %endif1
  else1:
    %.temp.6 = trunc i64 0 to i32
    store i32 %.temp.6, i32* %i.addr
    br label %endif1
  endif1:
    br label %while1
  while1:
    %end.1 = load i1, i1* %end.addr
    %.temp.7 = xor i1 %end.1, 1
    br i1 %.temp.7, label %.br.cont.2, label %endwhile1
  .br.cont.2:
    %i.2 = load i32, i32* %i.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %i.2)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.s.1, i64 0, i64 0))
    %i.3 = load i32, i32* %i.addr
    %.temp.8 = call i32 @fact(i32 %i.3)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.8)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.2, i64 0, i64 0))
    %i.4 = load i32, i32* %i.addr
    %max.2 = load i32, i32* %max.addr
    %.temp.9 = icmp eq i32 %i.4, %max.2
    br i1 %.temp.9, label %.br.cont.3, label %else2
  .br.cont.3:
    %.temp.10 = trunc i64 1 to i1
    store i1 %.temp.10, i1* %end.addr
    br label %endif2
  else2:
    %.temp.11 = trunc i64 1 to i32
    %i.5 = load i32, i32* %i.addr
    %.temp.12 = add i32 %i.5, %.temp.11
    store i32 %.temp.12, i32* %i.addr
    br label %endif2
  endif2:
    br label %while1
  endwhile1:
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)
declare dso_local i32 @__isoc99_scanf(i8*, ...)


