
@.str.f = constant [3 x i8] c"%g\00"
@.str.s.1 = constant [2 x i8] c"\0A\00"




define dso_local void @f1(i32 %a, i32 %b) {
  .entry:
    %a.addr = alloca i32
    %b.addr = alloca i32
    %c.addr = alloca float
    %d.addr = alloca i32
    %found.addr = alloca i1
    store i32 %a, i32* %a.addr
    store i32 %b, i32* %b.addr
    %.temp.1 = trunc i64 0 to i1
    store i1 %.temp.1, i1* %found.addr
    %.temp.2 = fptrunc double 0.7 to float
    %a.1 = load i32, i32* %a.addr
    %.temp.4 = sitofp i32 %a.1 to float
    %.temp.3 = fadd float %.temp.4, %.temp.2
    store float %.temp.3, float* %c.addr
    %.temp.5 = trunc i64 0 to i32
    store i32 %.temp.5, i32* %d.addr
    %a.2 = load i32, i32* %a.addr
    %d.1 = load i32, i32* %d.addr
    %.temp.6 = add i32 %a.2, %d.1
    %.temp.8 = sitofp i32 %.temp.6 to float
    %c.1 = load float, float* %c.addr
    %.temp.7 = fcmp olt float %c.1, %.temp.8
    %found.1 = load i1, i1* %found.addr
    %.temp.9 = xor i1 %found.1, 1
    %.temp.10 = or i1 %.temp.7, %.temp.9
    br i1 %.temp.10, label %while1, label %endif1
  while1:
    %.temp.11 = trunc i64 0 to i32
    %b.1 = load i32, i32* %b.addr
    %.temp.12 = icmp slt i32 %.temp.11, %b.1
    br i1 %.temp.12, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.13 = trunc i64 1 to i32
    %b.2 = load i32, i32* %b.addr
    %.temp.14 = sub i32 %b.2, %.temp.13
    store i32 %.temp.14, i32* %b.addr
    %.temp.15 = trunc i64 1 to i1
    store i1 %.temp.15, i1* %found.addr
    br label %while1
  endwhile1:
    br label %endif1
  endif1:
    %.temp.16 = trunc i64 11 to i32
    %b.3 = load i32, i32* %b.addr
    %.temp.17 = icmp sle i32 %b.3, %.temp.16
    br i1 %.temp.17, label %.br.cont.2, label %endif2
  .br.cont.2:
    %.temp.18 = trunc i64 2 to i32
    %.temp.20 = sitofp i32 %.temp.18 to float
    %c.2 = load float, float* %c.addr
    %.temp.19 = fmul float %.temp.20, %c.2
    %.temp.21 = trunc i64 1 to i32
    %.temp.23 = sitofp i32 %.temp.21 to float
    %.temp.22 = fadd float %.temp.19, %.temp.23
    store float %.temp.22, float* %c.addr
    br label %endif2
  endif2:
    %c.3 = load float, float* %c.addr
    %.wrtf.double.1 = fpext float %c.3 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.1)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    ret void
}

define dso_local i32 @main() {
  .entry:
    %a.addr = alloca i32
    %b.addr = alloca i32
    %.temp.1 = trunc i64 4 to i32
    store i32 %.temp.1, i32* %a.addr
    %.temp.2 = trunc i64 2 to i32
    %a.1 = load i32, i32* %a.addr
    %.temp.3 = mul i32 %.temp.2, %a.1
    %.temp.4 = trunc i64 1 to i32
    %.temp.5 = add i32 %.temp.3, %.temp.4
    store i32 %.temp.5, i32* %b.addr
    %b.1 = load i32, i32* %b.addr
    %.temp.6 = trunc i64 3 to i32
    %b.2 = load i32, i32* %b.addr
    %.temp.7 = add i32 %.temp.6, %b.2
    call void @f1(i32 %b.1, i32 %.temp.7)
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)


