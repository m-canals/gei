
@.str.i = constant [3 x i8] c"%d\00"
@.str.c = constant [3 x i8] c"%c\00"




define dso_local i32 @main() {
  .entry:
    %a.addr = alloca i32
    %b.addr = alloca i32
    %c.addr = alloca i32
    %.temp.1 = trunc i64 13 to i32
    store i32 %.temp.1, i32* %a.addr
    %.temp.2 = trunc i64 4 to i32
    store i32 %.temp.2, i32* %b.addr
    %a.1 = load i32, i32* %a.addr
    %b.1 = load i32, i32* %b.addr
    %.temp.4 = sdiv i32 %a.1, %b.1
    %b.2 = load i32, i32* %b.addr
    %.temp.5 = mul i32 %.temp.4, %b.2
    %a.2 = load i32, i32* %a.addr
    %.temp.3 = sub i32 %a.2, %.temp.5
    store i32 %.temp.3, i32* %c.addr
    %c.1 = load i32, i32* %c.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %c.1)
    %.temp.6 = trunc i32 46 to i8
    %.wrtc.i32.1 = zext i8 %.temp.6 to i32
    call i32 @putchar(i32 %.wrtc.i32.1)
    %.temp.7 = trunc i32 10 to i8
    %.wrtc.i32.2 = zext i8 %.temp.7 to i32
    call i32 @putchar(i32 %.wrtc.i32.2)
    %a.3 = load i32, i32* %a.addr
    %.temp.8 = sub i32 0, %a.3
    %b.3 = load i32, i32* %b.addr
    %.temp.10 = sdiv i32 %.temp.8, %b.3
    %b.4 = load i32, i32* %b.addr
    %.temp.11 = mul i32 %.temp.10, %b.4
    %.temp.9 = sub i32 %.temp.8, %.temp.11
    store i32 %.temp.9, i32* %c.addr
    %c.2 = load i32, i32* %c.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %c.2)
    %.temp.12 = trunc i32 46 to i8
    %.wrtc.i32.3 = zext i8 %.temp.12 to i32
    call i32 @putchar(i32 %.wrtc.i32.3)
    %.temp.13 = trunc i32 10 to i8
    %.wrtc.i32.4 = zext i8 %.temp.13 to i32
    call i32 @putchar(i32 %.wrtc.i32.4)
    %b.5 = load i32, i32* %b.addr
    %.temp.14 = sub i32 0, %b.5
    %a.4 = load i32, i32* %a.addr
    %.temp.16 = sdiv i32 %a.4, %.temp.14
    %.temp.17 = mul i32 %.temp.16, %.temp.14
    %a.5 = load i32, i32* %a.addr
    %.temp.15 = sub i32 %a.5, %.temp.17
    store i32 %.temp.15, i32* %c.addr
    %c.3 = load i32, i32* %c.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %c.3)
    %.temp.18 = trunc i32 46 to i8
    %.wrtc.i32.5 = zext i8 %.temp.18 to i32
    call i32 @putchar(i32 %.wrtc.i32.5)
    %.temp.19 = trunc i32 10 to i8
    %.wrtc.i32.6 = zext i8 %.temp.19 to i32
    call i32 @putchar(i32 %.wrtc.i32.6)
    %.temp.20 = trunc i64 3 to i32
    %a.6 = load i32, i32* %a.addr
    %.temp.21 = add i32 %a.6, %.temp.20
    %b.6 = load i32, i32* %b.addr
    %.temp.23 = sdiv i32 %.temp.21, %b.6
    %b.7 = load i32, i32* %b.addr
    %.temp.24 = mul i32 %.temp.23, %b.7
    %.temp.22 = sub i32 %.temp.21, %.temp.24
    store i32 %.temp.22, i32* %c.addr
    %c.4 = load i32, i32* %c.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %c.4)
    %.temp.25 = trunc i32 46 to i8
    %.wrtc.i32.7 = zext i8 %.temp.25 to i32
    call i32 @putchar(i32 %.wrtc.i32.7)
    %.temp.26 = trunc i32 10 to i8
    %.wrtc.i32.8 = zext i8 %.temp.26 to i32
    call i32 @putchar(i32 %.wrtc.i32.8)
    %a.7 = load i32, i32* %a.addr
    %.temp.27 = sub i32 0, %a.7
    %.temp.28 = trunc i64 3 to i32
    %.temp.29 = sub i32 %.temp.27, %.temp.28
    %b.8 = load i32, i32* %b.addr
    %.temp.30 = sub i32 0, %b.8
    %.temp.32 = sdiv i32 %.temp.29, %.temp.30
    %.temp.33 = mul i32 %.temp.32, %.temp.30
    %.temp.31 = sub i32 %.temp.29, %.temp.33
    store i32 %.temp.31, i32* %c.addr
    %c.5 = load i32, i32* %c.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %c.5)
    %.temp.34 = trunc i32 46 to i8
    %.wrtc.i32.9 = zext i8 %.temp.34 to i32
    call i32 @putchar(i32 %.wrtc.i32.9)
    %.temp.35 = trunc i32 10 to i8
    %.wrtc.i32.10 = zext i8 %.temp.35 to i32
    call i32 @putchar(i32 %.wrtc.i32.10)
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)
declare dso_local i32 @putchar(i32)


