
@.str.i = constant [3 x i8] c"%d\00"
@.str.s.1 = constant [2 x i8] c"\0A\00"
@.str.s.2 = constant [3 x i8] c"z:\00"
@.str.s.3 = constant [3 x i8] c"x[\00"
@.str.s.4 = constant [3 x i8] c"]=\00"




define dso_local i32 @x2(i32* %a) {
  .entry:
    %_result.addr = alloca i32
    %a.addr = alloca i32*
    %i.addr = alloca i32
    %n.addr = alloca i32
    store i32* %a, i32** %a.addr
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %n.addr
    %.temp.2 = trunc i64 0 to i32
    store i32 %.temp.2, i32* %i.addr
    br label %while1
  while1:
    %.temp.3 = trunc i64 10 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.4 = icmp slt i32 %i.1, %.temp.3
    br i1 %.temp.4, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.5 = load i32*, i32** %a.addr
    %i.2 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.2 to i64
    %.arrPtr.1 = getelementptr inbounds i32, i32* %.temp.5, i64 %.idx64.1
    %.temp.6 = load i32, i32* %.arrPtr.1
    %.temp.7 = trunc i64 80 to i32
    %.temp.8 = icmp slt i32 %.temp.6, %.temp.7
    br i1 %.temp.8, label %.br.cont.2, label %endif1
  .br.cont.2:
    %.temp.9 = trunc i64 1 to i32
    %n.1 = load i32, i32* %n.addr
    %.temp.10 = add i32 %n.1, %.temp.9
    store i32 %.temp.10, i32* %n.addr
    br label %endif1
  endif1:
    %.temp.11 = load i32*, i32** %a.addr
    %.temp.12 = load i32*, i32** %a.addr
    %i.3 = load i32, i32* %i.addr
    %.idx64.2 = sext i32 %i.3 to i64
    %.arrPtr.2 = getelementptr inbounds i32, i32* %.temp.12, i64 %.idx64.2
    %.temp.13 = load i32, i32* %.arrPtr.2
    %.temp.14 = trunc i64 2 to i32
    %.temp.15 = mul i32 %.temp.13, %.temp.14
    %i.4 = load i32, i32* %i.addr
    %.idx64.3 = sext i32 %i.4 to i64
    %.arrPtr.3 = getelementptr inbounds i32, i32* %.temp.11, i64 %.idx64.3
    store i32 %.temp.15, i32* %.arrPtr.3
    %.temp.16 = load i32*, i32** %a.addr
    %i.5 = load i32, i32* %i.addr
    %.idx64.4 = sext i32 %i.5 to i64
    %.arrPtr.4 = getelementptr inbounds i32, i32* %.temp.16, i64 %.idx64.4
    %.temp.17 = load i32, i32* %.arrPtr.4
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.17)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.18 = trunc i64 1 to i32
    %i.6 = load i32, i32* %i.addr
    %.temp.19 = add i32 %i.6, %.temp.18
    store i32 %.temp.19, i32* %i.addr
    br label %while1
  endwhile1:
    %n.2 = load i32, i32* %n.addr
    store i32 %n.2, i32* %_result.addr
    %_result.1 = load i32, i32* %_result.addr
    ret i32 %_result.1
  .dead.code.1:
    %_result.2 = load i32, i32* %_result.addr
    ret i32 %_result.2
}

define dso_local i32 @main() {
  .entry:
    %x.addr = alloca [10 x i32]
    %i.addr = alloca i32
    %z.addr = alloca i32
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    br label %while1
  while1:
    %.temp.2 = trunc i64 10 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.3 = icmp slt i32 %i.1, %.temp.2
    br i1 %.temp.3, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.4 = trunc i64 77 to i32
    %i.2 = load i32, i32* %i.addr
    %.temp.5 = add i32 %.temp.4, %i.2
    %i.3 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.3 to i64
    %.arrPtr.1 = getelementptr inbounds [10 x i32], [10 x i32]* %x.addr, i64 0, i64 %.idx64.1
    store i32 %.temp.5, i32* %.arrPtr.1
    %.temp.6 = trunc i64 1 to i32
    %i.4 = load i32, i32* %i.addr
    %.temp.7 = add i32 %i.4, %.temp.6
    store i32 %.temp.7, i32* %i.addr
    br label %while1
  endwhile1:
    %.temp.8 = trunc i64 0 to i32
    store i32 %.temp.8, i32* %i.addr
    br label %while2
  while2:
    %.temp.9 = trunc i64 10 to i32
    %i.5 = load i32, i32* %i.addr
    %.temp.10 = icmp slt i32 %i.5, %.temp.9
    br i1 %.temp.10, label %.br.cont.2, label %endwhile2
  .br.cont.2:
    %i.6 = load i32, i32* %i.addr
    %.idx64.2 = sext i32 %i.6 to i64
    %.arrPtr.2 = getelementptr inbounds [10 x i32], [10 x i32]* %x.addr, i64 0, i64 %.idx64.2
    %.temp.11 = load i32, i32* %.arrPtr.2
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.11)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.12 = trunc i64 1 to i32
    %i.7 = load i32, i32* %i.addr
    %.temp.13 = add i32 %i.7, %.temp.12
    store i32 %.temp.13, i32* %i.addr
    br label %while2
  endwhile2:
    %.temp.15 = getelementptr inbounds [10 x i32], [10 x i32]* %x.addr, i64 0, i64 0
    %.temp.14 = call i32 @x2(i32* %.temp.15)
    store i32 %.temp.14, i32* %z.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.s.2, i64 0, i64 0))
    %z.1 = load i32, i32* %z.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %z.1)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.16 = trunc i64 0 to i32
    store i32 %.temp.16, i32* %i.addr
    br label %while3
  while3:
    %.temp.17 = trunc i64 10 to i32
    %i.8 = load i32, i32* %i.addr
    %.temp.18 = icmp slt i32 %i.8, %.temp.17
    br i1 %.temp.18, label %.br.cont.3, label %endwhile3
  .br.cont.3:
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.s.3, i64 0, i64 0))
    %i.9 = load i32, i32* %i.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %i.9)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.s.4, i64 0, i64 0))
    %i.10 = load i32, i32* %i.addr
    %.idx64.3 = sext i32 %i.10 to i64
    %.arrPtr.3 = getelementptr inbounds [10 x i32], [10 x i32]* %x.addr, i64 0, i64 %.idx64.3
    %.temp.19 = load i32, i32* %.arrPtr.3
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.19)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.20 = trunc i64 1 to i32
    %i.11 = load i32, i32* %i.addr
    %.temp.21 = add i32 %i.11, %.temp.20
    store i32 %.temp.21, i32* %i.addr
    br label %while3
  endwhile3:
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)


