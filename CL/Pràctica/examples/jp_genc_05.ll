
@.str.i = constant [3 x i8] c"%d\00"
@.str.f = constant [3 x i8] c"%g\00"
@.str.s.1 = constant [2 x i8] c"\0A\00"




define dso_local i1 @f(i32 %a, float %b) {
  .entry:
    %_result.addr = alloca i1
    %a.addr = alloca i32
    %b.addr = alloca float
    %x.addr = alloca i32
    %y.addr = alloca i1
    %z.addr = alloca [10 x i32]
    store i32 %a, i32* %a.addr
    store float %b, float* %b.addr
    %.temp.1 = trunc i64 9 to i32
    %.temp.2 = trunc i64 67 to i32
    %a.1 = load i32, i32* %a.addr
    %.temp.3 = add i32 %a.1, %.temp.2
    %.idx64.1 = sext i32 %.temp.1 to i64
    %.arrPtr.1 = getelementptr inbounds [10 x i32], [10 x i32]* %z.addr, i64 0, i64 %.idx64.1
    store i32 %.temp.3, i32* %.arrPtr.1
    %.temp.4 = trunc i64 34 to i32
    store i32 %.temp.4, i32* %x.addr
    %.temp.5 = trunc i64 3 to i32
    %.temp.6 = trunc i64 56 to i32
    %.temp.7 = trunc i64 9 to i32
    %.idx64.2 = sext i32 %.temp.7 to i64
    %.arrPtr.2 = getelementptr inbounds [10 x i32], [10 x i32]* %z.addr, i64 0, i64 %.idx64.2
    %.temp.8 = load i32, i32* %.arrPtr.2
    %.temp.9 = add i32 %.temp.6, %.temp.8
    %.idx64.3 = sext i32 %.temp.5 to i64
    %.arrPtr.3 = getelementptr inbounds [10 x i32], [10 x i32]* %z.addr, i64 0, i64 %.idx64.3
    store i32 %.temp.9, i32* %.arrPtr.3
    %.temp.10 = trunc i64 3 to i32
    %.idx64.4 = sext i32 %.temp.10 to i64
    %.arrPtr.4 = getelementptr inbounds [10 x i32], [10 x i32]* %z.addr, i64 0, i64 %.idx64.4
    %.temp.11 = load i32, i32* %.arrPtr.4
    %.temp.13 = sitofp i32 %.temp.11 to float
    %b.1 = load float, float* %b.addr
    %.temp.12 = fcmp olt float %b.1, %.temp.13
    br i1 %.temp.12, label %.br.cont.1, label %else1
  .br.cont.1:
    %.temp.14 = trunc i64 78 to i32
    store i32 %.temp.14, i32* %x.addr
    %b.2 = load float, float* %b.addr
    %.wrtf.double.1 = fpext float %b.2 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.1)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    br label %endif1
  else1:
    %.temp.15 = trunc i64 99 to i32
    store i32 %.temp.15, i32* %x.addr
    br label %endif1
  endif1:
    %.temp.16 = trunc i64 3 to i32
    %.idx64.5 = sext i32 %.temp.16 to i64
    %.arrPtr.5 = getelementptr inbounds [10 x i32], [10 x i32]* %z.addr, i64 0, i64 %.idx64.5
    %.temp.17 = load i32, i32* %.arrPtr.5
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.17)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.18 = trunc i64 1 to i1
    store i1 %.temp.18, i1* %_result.addr
    %_result.1 = load i1, i1* %_result.addr
    ret i1 %_result.1
  .dead.code.1:
    %_result.2 = load i1, i1* %_result.addr
    ret i1 %_result.2
}

define dso_local void @fz(i32 %r) {
  .entry:
    %r.addr = alloca i32
    store i32 %r, i32* %r.addr
    br label %while1
  while1:
    %.temp.1 = trunc i64 0 to i32
    %r.1 = load i32, i32* %r.addr
    %.temp.2 = icmp slt i32 %.temp.1, %r.1
    br i1 %.temp.2, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.3 = trunc i64 1 to i32
    %r.2 = load i32, i32* %r.addr
    %.temp.4 = sub i32 %r.2, %.temp.3
    store i32 %.temp.4, i32* %r.addr
    br label %while1
  endwhile1:
    ret void
}

define dso_local i32 @main() {
  .entry:
    %a.addr = alloca i32
    %.temp.2 = trunc i64 3 to i32
    %.temp.3 = trunc i64 2 to i32
    %.temp.4 = sitofp i32 %.temp.3 to float
    %.temp.1 = call i1 @f(i32 %.temp.2, float %.temp.4)
    br i1 %.temp.1, label %.br.cont.1, label %endif1
  .br.cont.1:
    %.temp.5 = fptrunc double 3.7 to float
    %a.1 = load i32, i32* %a.addr
    %.temp.7 = sitofp i32 %a.1 to float
    %.temp.6 = fadd float %.temp.7, %.temp.5
    %.temp.8 = trunc i64 4 to i32
    %.temp.10 = sitofp i32 %.temp.8 to float
    %.temp.9 = fadd float %.temp.6, %.temp.10
    %.wrtf.double.1 = fpext float %.temp.9 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.1)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    br label %endif1
  endif1:
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)


