
@.str.i = constant [3 x i8] c"%d\00"
@.str.f = constant [3 x i8] c"%g\00"
@.str.s.1 = constant [2 x i8] c"\0A\00"




define dso_local i32 @main() {
  .entry:
    %a.addr = alloca i32
    %b.addr = alloca i32
    %end.addr = alloca i1
    %pi.addr = alloca float
    %.temp.1 = trunc i64 12 to i32
    store i32 %.temp.1, i32* %a.addr
    %.temp.2 = trunc i64 5 to i32
    %a.1 = load i32, i32* %a.addr
    %.temp.3 = mul i32 %a.1, %.temp.2
    %.temp.4 = trunc i64 1 to i32
    %a.2 = load i32, i32* %a.addr
    %.temp.5 = add i32 %a.2, %.temp.4
    %a.3 = load i32, i32* %a.addr
    %.temp.6 = mul i32 %a.3, %.temp.5
    %.temp.7 = add i32 %.temp.3, %.temp.6
    store i32 %.temp.7, i32* %b.addr
    %a.4 = load i32, i32* %a.addr
    %b.1 = load i32, i32* %b.addr
    %.temp.8 = icmp sle i32 %a.4, %b.1
    %.temp.9 = trunc i64 0 to i32
    %a.5 = load i32, i32* %a.addr
    %.temp.10 = icmp eq i32 %a.5, %.temp.9
    %.temp.11 = xor i1 %.temp.10, 1
    %.temp.12 = and i1 %.temp.8, %.temp.11
    store i1 %.temp.12, i1* %end.addr
    %.temp.13 = fptrunc double 3.3 to float
    %.temp.14 = trunc i64 1 to i32
    %a.6 = load i32, i32* %a.addr
    %.temp.15 = sdiv i32 %.temp.14, %a.6
    %.temp.17 = sitofp i32 %.temp.15 to float
    %.temp.16 = fadd float %.temp.13, %.temp.17
    %.temp.18 = fptrunc double 2.0 to float
    %.temp.19 = fneg float %.temp.18
    %a.7 = load i32, i32* %a.addr
    %.temp.21 = sitofp i32 %a.7 to float
    %.temp.20 = fdiv float %.temp.19, %.temp.21
    %.temp.22 = fsub float %.temp.16, %.temp.20
    store float %.temp.22, float* %pi.addr
    %a.8 = load i32, i32* %a.addr
    %b.2 = load i32, i32* %b.addr
    %.temp.23 = icmp eq i32 %a.8, %b.2
    %end.1 = load i1, i1* %end.addr
    %.temp.24 = or i1 %.temp.23, %end.1
    %.wrti.i32.1 = zext i1 %.temp.24 to i32
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.wrti.i32.1)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %a.9 = load i32, i32* %a.addr
    %b.3 = load i32, i32* %b.addr
    %.temp.25 = mul i32 %a.9, %b.3
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.25)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.26 = trunc i64 2 to i32
    %.temp.28 = sitofp i32 %.temp.26 to float
    %pi.1 = load float, float* %pi.addr
    %.temp.27 = fmul float %.temp.28, %pi.1
    %.wrtf.double.1 = fpext float %.temp.27 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.1)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)


