
@.str.f = constant [3 x i8] c"%g\00"
@.str.s.1 = constant [2 x i8] c"\0A\00"


@.global.f.addr = common dso_local global float 0.000000e+00


define dso_local void @f(float* %a) {
  .entry:
    %a.addr = alloca float*
    store float* %a, float** %a.addr
    %.temp.1 = load float*, float** %a.addr
    %.temp.2 = trunc i64 5 to i32
    %.temp.3 = load float*, float** %a.addr
    %.temp.4 = trunc i64 5 to i32
    %.idx64.1 = sext i32 %.temp.4 to i64
    %.arrPtr.1 = getelementptr inbounds float, float* %.temp.3, i64 %.idx64.1
    %.temp.5 = load float, float* %.arrPtr.1
    %.temp.6 = trunc i64 10 to i32
    %.temp.8 = sitofp i32 %.temp.6 to float
    %.temp.7 = fmul float %.temp.5, %.temp.8
    %.idx64.2 = sext i32 %.temp.2 to i64
    %.arrPtr.2 = getelementptr inbounds float, float* %.temp.1, i64 %.idx64.2
    store float %.temp.7, float* %.arrPtr.2
    ret void
}

define dso_local i32 @main() {
  .entry:
    %b.addr = alloca [10 x float]
    %c.addr = alloca [10 x float]
    %.temp.1 = trunc i64 5 to i32
    call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), float* @.global.f.addr)
    %.temp.2 = load float, float* @.global.f.addr
    %.idx64.1 = sext i32 %.temp.1 to i64
    %.arrPtr.1 = getelementptr inbounds [10 x float], [10 x float]* %b.addr, i64 0, i64 %.idx64.1
    store float %.temp.2, float* %.arrPtr.1
    %.temp.3 = trunc i64 7 to i32
    %.temp.4 = trunc i64 5 to i32
    %.idx64.2 = sext i32 %.temp.4 to i64
    %.arrPtr.2 = getelementptr inbounds [10 x float], [10 x float]* %b.addr, i64 0, i64 %.idx64.2
    %.temp.5 = load float, float* %.arrPtr.2
    %.idx64.3 = sext i32 %.temp.3 to i64
    %.arrPtr.3 = getelementptr inbounds [10 x float], [10 x float]* %c.addr, i64 0, i64 %.idx64.3
    store float %.temp.5, float* %.arrPtr.3
    %.temp.6 = trunc i64 5 to i32
    %.idx64.4 = sext i32 %.temp.6 to i64
    %.arrPtr.4 = getelementptr inbounds [10 x float], [10 x float]* %b.addr, i64 0, i64 %.idx64.4
    %.temp.7 = load float, float* %.arrPtr.4
    %.wrtf.double.1 = fpext float %.temp.7 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.1)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.8 = trunc i64 7 to i32
    %.idx64.5 = sext i32 %.temp.8 to i64
    %.arrPtr.5 = getelementptr inbounds [10 x float], [10 x float]* %c.addr, i64 0, i64 %.idx64.5
    %.temp.9 = load float, float* %.arrPtr.5
    %.wrtf.double.2 = fpext float %.temp.9 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.2)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.10 = getelementptr inbounds [10 x float], [10 x float]* %b.addr, i64 0, i64 0
    call void @f(float* %.temp.10)
    %.temp.11 = trunc i64 5 to i32
    %.idx64.6 = sext i32 %.temp.11 to i64
    %.arrPtr.6 = getelementptr inbounds [10 x float], [10 x float]* %b.addr, i64 0, i64 %.idx64.6
    %.temp.12 = load float, float* %.arrPtr.6
    %.wrtf.double.3 = fpext float %.temp.12 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.3)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    %.temp.13 = trunc i64 7 to i32
    %.idx64.7 = sext i32 %.temp.13 to i64
    %.arrPtr.7 = getelementptr inbounds [10 x float], [10 x float]* %c.addr, i64 0, i64 %.idx64.7
    %.temp.14 = load float, float* %.arrPtr.7
    %.wrtf.double.4 = fpext float %.temp.14 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.4)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)
declare dso_local i32 @__isoc99_scanf(i8*, ...)


