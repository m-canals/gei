
@.str.f = constant [3 x i8] c"%g\00"
@.str.c = constant [3 x i8] c"%c\00"


@.global.f.addr = common dso_local global float 0.000000e+00


define dso_local float @one() {
  .entry:
    %_result.addr = alloca float
    %.temp.1 = trunc i64 1 to i32
    %.temp.2.i64.1 = zext i64 %.temp.1 to i64
    %.temp.2 = trunc i32 %.temp.2.i64.1 to i32
    %.temp.3 = sitofp i32 %.temp.2 to float
    store float %.temp.3, float* %_result.addr
    %_result.1 = load float, float* %_result.addr
    ret float %_result.1
  .dead.code.1:
    %_result.2 = load float, float* %_result.addr
    ret float %_result.2
}

define dso_local void @sort(float* %v) {
  .entry:
    %v.addr = alloca float*
    %i.addr = alloca i32
    %j.addr = alloca i32
    %jmin.addr = alloca i32
    %aux.addr = alloca float
    store float* %v, float** %v.addr
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    br label %while2
  while2:
    %.temp.2 = trunc i64 20 to i32
    %.temp.3 = trunc i64 1 to i32
    %.temp.4 = sub i32 %.temp.2, %.temp.3
    %i.1 = load i32, i32* %i.addr
    %.temp.5 = icmp slt i32 %i.1, %.temp.4
    br i1 %.temp.5, label %.br.cont.1, label %endwhile2
  .br.cont.1:
    %i.2 = load i32, i32* %i.addr
    store i32 %i.2, i32* %jmin.addr
    %.temp.6 = trunc i64 1 to i32
    %i.3 = load i32, i32* %i.addr
    %.temp.7 = add i32 %i.3, %.temp.6
    store i32 %.temp.7, i32* %j.addr
    br label %while1
  while1:
    %.temp.8 = trunc i64 20 to i32
    %j.1 = load i32, i32* %j.addr
    %.temp.9 = icmp slt i32 %j.1, %.temp.8
    br i1 %.temp.9, label %.br.cont.2, label %endwhile1
  .br.cont.2:
    %.temp.10 = load float*, float** %v.addr
    %j.2 = load i32, i32* %j.addr
    %.idx64.1 = sext i32 %j.2 to i64
    %.arrPtr.1 = getelementptr inbounds float, float* %.temp.10, i64 %.idx64.1
    %.temp.11 = load float, float* %.arrPtr.1
    %.temp.12 = load float*, float** %v.addr
    %jmin.1 = load i32, i32* %jmin.addr
    %.idx64.2 = sext i32 %jmin.1 to i64
    %.arrPtr.2 = getelementptr inbounds float, float* %.temp.12, i64 %.idx64.2
    %.temp.13 = load float, float* %.arrPtr.2
    %.temp.14 = fcmp olt float %.temp.11, %.temp.13
    br i1 %.temp.14, label %.br.cont.3, label %endif1
  .br.cont.3:
    %j.3 = load i32, i32* %j.addr
    store i32 %j.3, i32* %jmin.addr
    br label %endif1
  endif1:
    %.temp.15 = trunc i64 1 to i32
    %j.4 = load i32, i32* %j.addr
    %.temp.16 = add i32 %j.4, %.temp.15
    store i32 %.temp.16, i32* %j.addr
    br label %while1
  endwhile1:
    %jmin.2 = load i32, i32* %jmin.addr
    %i.4 = load i32, i32* %i.addr
    %.temp.18 = icmp eq i32 %jmin.2, %i.4
    %.temp.17 = xor i1 %.temp.18, 1
    br i1 %.temp.17, label %.br.cont.4, label %endif2
  .br.cont.4:
    %.temp.19 = load float*, float** %v.addr
    %i.5 = load i32, i32* %i.addr
    %.idx64.3 = sext i32 %i.5 to i64
    %.arrPtr.3 = getelementptr inbounds float, float* %.temp.19, i64 %.idx64.3
    %.temp.20 = load float, float* %.arrPtr.3
    store float %.temp.20, float* %aux.addr
    %.temp.21 = load float*, float** %v.addr
    %.temp.22 = load float*, float** %v.addr
    %jmin.3 = load i32, i32* %jmin.addr
    %.idx64.4 = sext i32 %jmin.3 to i64
    %.arrPtr.4 = getelementptr inbounds float, float* %.temp.22, i64 %.idx64.4
    %.temp.23 = load float, float* %.arrPtr.4
    %i.6 = load i32, i32* %i.addr
    %.idx64.5 = sext i32 %i.6 to i64
    %.arrPtr.5 = getelementptr inbounds float, float* %.temp.21, i64 %.idx64.5
    store float %.temp.23, float* %.arrPtr.5
    %.temp.24 = load float*, float** %v.addr
    %jmin.4 = load i32, i32* %jmin.addr
    %aux.1 = load float, float* %aux.addr
    %.idx64.6 = sext i32 %jmin.4 to i64
    %.arrPtr.6 = getelementptr inbounds float, float* %.temp.24, i64 %.idx64.6
    store float %aux.1, float* %.arrPtr.6
    br label %endif2
  endif2:
    %.temp.25 = trunc i64 1 to i32
    %i.7 = load i32, i32* %i.addr
    %.temp.26 = add i32 %i.7, %.temp.25
    store i32 %.temp.26, i32* %i.addr
    br label %while2
  endwhile2:
    ret void
}

define dso_local void @evenPositivesAndSort(float* %v) {
  .entry:
    %v.addr = alloca float*
    %i.addr = alloca i32
    store float* %v, float** %v.addr
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    br label %while1
  while1:
    %.temp.2 = trunc i64 20 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.3 = icmp slt i32 %i.1, %.temp.2
    br i1 %.temp.3, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.4 = load float*, float** %v.addr
    %i.2 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.2 to i64
    %.arrPtr.1 = getelementptr inbounds float, float* %.temp.4, i64 %.idx64.1
    %.temp.5 = load float, float* %.arrPtr.1
    %.temp.6 = trunc i64 0 to i32
    %.temp.8 = sitofp i32 %.temp.6 to float
    %.temp.7 = fcmp olt float %.temp.8, %.temp.5
    br i1 %.temp.7, label %.br.cont.2, label %endif1
  .br.cont.2:
    %.temp.9 = load float*, float** %v.addr
    %.temp.10 = call float @one()
    %i.3 = load i32, i32* %i.addr
    %.idx64.2 = sext i32 %i.3 to i64
    %.arrPtr.2 = getelementptr inbounds float, float* %.temp.9, i64 %.idx64.2
    store float %.temp.10, float* %.arrPtr.2
    br label %endif1
  endif1:
    %.temp.11 = trunc i64 1 to i32
    %i.4 = load i32, i32* %i.addr
    %.temp.12 = add i32 %i.4, %.temp.11
    store i32 %.temp.12, i32* %i.addr
    br label %while1
  endwhile1:
    %.temp.13 = load float*, float** %v.addr
    call void @sort(float* %.temp.13)
    ret void
}

define dso_local i32 @main() {
  .entry:
    %af.addr = alloca [20 x float]
    %i.addr = alloca i32
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    br label %while1
  while1:
    %.temp.2 = trunc i64 20 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.3 = icmp slt i32 %i.1, %.temp.2
    br i1 %.temp.3, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), float* @.global.f.addr)
    %.temp.4 = load float, float* @.global.f.addr
    %i.2 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.2 to i64
    %.arrPtr.1 = getelementptr inbounds [20 x float], [20 x float]* %af.addr, i64 0, i64 %.idx64.1
    store float %.temp.4, float* %.arrPtr.1
    %.temp.5 = trunc i64 1 to i32
    %i.3 = load i32, i32* %i.addr
    %.temp.6 = add i32 %i.3, %.temp.5
    store i32 %.temp.6, i32* %i.addr
    br label %while1
  endwhile1:
    %.temp.7 = getelementptr inbounds [20 x float], [20 x float]* %af.addr, i64 0, i64 0
    call void @evenPositivesAndSort(float* %.temp.7)
    %.temp.8 = trunc i64 0 to i32
    store i32 %.temp.8, i32* %i.addr
    br label %while2
  while2:
    %.temp.9 = trunc i64 20 to i32
    %i.4 = load i32, i32* %i.addr
    %.temp.10 = icmp slt i32 %i.4, %.temp.9
    br i1 %.temp.10, label %.br.cont.2, label %endwhile2
  .br.cont.2:
    %i.5 = load i32, i32* %i.addr
    %.idx64.2 = sext i32 %i.5 to i64
    %.arrPtr.2 = getelementptr inbounds [20 x float], [20 x float]* %af.addr, i64 0, i64 %.idx64.2
    %.temp.11 = load float, float* %.arrPtr.2
    %.temp.12 = call float @one()
    %.temp.14 = fcmp oeq float %.temp.11, %.temp.12
    %.temp.13 = xor i1 %.temp.14, 1
    br i1 %.temp.13, label %.br.cont.3, label %else1
  .br.cont.3:
    %i.6 = load i32, i32* %i.addr
    %.idx64.3 = sext i32 %i.6 to i64
    %.arrPtr.3 = getelementptr inbounds [20 x float], [20 x float]* %af.addr, i64 0, i64 %.idx64.3
    %.temp.15 = load float, float* %.arrPtr.3
    %.wrtf.double.1 = fpext float %.temp.15 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.1)
    %.temp.16 = trunc i32 32 to i8
    %.wrtc.i32.1 = zext i8 %.temp.16 to i32
    call i32 @putchar(i32 %.wrtc.i32.1)
    %.temp.17 = trunc i64 1 to i32
    %i.7 = load i32, i32* %i.addr
    %.temp.18 = add i32 %i.7, %.temp.17
    store i32 %.temp.18, i32* %i.addr
    br label %endif1
  else1:
    %.temp.19 = trunc i32 10 to i8
    %.wrtc.i32.2 = zext i8 %.temp.19 to i32
    call i32 @putchar(i32 %.wrtc.i32.2)
    ret i32 0
  endif1:
    br label %while2
  endwhile2:
    %.temp.20 = trunc i32 10 to i8
    %.wrtc.i32.3 = zext i8 %.temp.20 to i32
    call i32 @putchar(i32 %.wrtc.i32.3)
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)
declare dso_local i32 @putchar(i32)
declare dso_local i32 @__isoc99_scanf(i8*, ...)


