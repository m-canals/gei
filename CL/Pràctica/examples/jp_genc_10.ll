
@.str.i = constant [3 x i8] c"%d\00"
@.str.s.1 = constant [2 x i8] c"\0A\00"




define dso_local i32 @prod_escalar(i32* %a1, i32* %a2) {
  .entry:
    %_result.addr = alloca i32
    %a1.addr = alloca i32*
    %a2.addr = alloca i32*
    %i.addr = alloca i32
    %s.addr = alloca i32
    store i32* %a1, i32** %a1.addr
    store i32* %a2, i32** %a2.addr
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %s.addr
    br label %while1
  while1:
    %.temp.2 = trunc i64 10 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.3 = icmp slt i32 %i.1, %.temp.2
    br i1 %.temp.3, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.4 = load i32*, i32** %a1.addr
    %i.2 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.2 to i64
    %.arrPtr.1 = getelementptr inbounds i32, i32* %.temp.4, i64 %.idx64.1
    %.temp.5 = load i32, i32* %.arrPtr.1
    %.temp.6 = load i32*, i32** %a2.addr
    %i.3 = load i32, i32* %i.addr
    %.idx64.2 = sext i32 %i.3 to i64
    %.arrPtr.2 = getelementptr inbounds i32, i32* %.temp.6, i64 %.idx64.2
    %.temp.7 = load i32, i32* %.arrPtr.2
    %.temp.8 = mul i32 %.temp.5, %.temp.7
    %s.1 = load i32, i32* %s.addr
    %.temp.9 = add i32 %s.1, %.temp.8
    store i32 %.temp.9, i32* %s.addr
    %.temp.10 = trunc i64 1 to i32
    %i.4 = load i32, i32* %i.addr
    %.temp.11 = add i32 %i.4, %.temp.10
    store i32 %.temp.11, i32* %i.addr
    br label %while1
  endwhile1:
    %s.2 = load i32, i32* %s.addr
    store i32 %s.2, i32* %_result.addr
    %_result.1 = load i32, i32* %_result.addr
    ret i32 %_result.1
  .dead.code.1:
    %_result.2 = load i32, i32* %_result.addr
    ret i32 %_result.2
}

define dso_local i32 @main() {
  .entry:
    %i.addr = alloca i32
    %v1.addr = alloca [10 x i32]
    %v2.addr = alloca [10 x i32]
    %.temp.1 = trunc i64 0 to i32
    store i32 %.temp.1, i32* %i.addr
    br label %while1
  while1:
    %.temp.2 = trunc i64 10 to i32
    %i.1 = load i32, i32* %i.addr
    %.temp.3 = icmp slt i32 %i.1, %.temp.2
    br i1 %.temp.3, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %i.2 = load i32, i32* %i.addr
    %.temp.4 = sub i32 0, %i.2
    %i.3 = load i32, i32* %i.addr
    %.idx64.1 = sext i32 %i.3 to i64
    %.arrPtr.1 = getelementptr inbounds [10 x i32], [10 x i32]* %v1.addr, i64 0, i64 %.idx64.1
    store i32 %.temp.4, i32* %.arrPtr.1
    %i.4 = load i32, i32* %i.addr
    %i.5 = load i32, i32* %i.addr
    %.temp.5 = mul i32 %i.4, %i.5
    %i.6 = load i32, i32* %i.addr
    %.idx64.2 = sext i32 %i.6 to i64
    %.arrPtr.2 = getelementptr inbounds [10 x i32], [10 x i32]* %v2.addr, i64 0, i64 %.idx64.2
    store i32 %.temp.5, i32* %.arrPtr.2
    %.temp.6 = trunc i64 1 to i32
    %i.7 = load i32, i32* %i.addr
    %.temp.7 = add i32 %i.7, %.temp.6
    store i32 %.temp.7, i32* %i.addr
    br label %while1
  endwhile1:
    %.temp.9 = getelementptr inbounds [10 x i32], [10 x i32]* %v1.addr, i64 0, i64 0
    %.temp.10 = getelementptr inbounds [10 x i32], [10 x i32]* %v2.addr, i64 0, i64 0
    %.temp.8 = call i32 @prod_escalar(i32* %.temp.9, i32* %.temp.10)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.8)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.1, i64 0, i64 0))
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)


