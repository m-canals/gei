
@.str.i = constant [3 x i8] c"%d\00"
@.str.f = constant [3 x i8] c"%g\00"
@.str.s.1 = constant [7 x i8] c"h\0A\09l\\a\00"
@.str.s.2 = constant [2 x i8] c"\0A\00"


@.global.i.addr = common dso_local global i32 0
@.global.f.addr = common dso_local global float 0.000000e+00


define dso_local i1 @f(i32 %a, float %f) {
  .entry:
    %_result.addr = alloca i1
    %a.addr = alloca i32
    %f.addr = alloca float
    %x.addr = alloca i32
    %b.addr = alloca i1
    %z.addr = alloca [10 x i32]
    store i32 %a, i32* %a.addr
    store float %f, float* %f.addr
    %.temp.1 = trunc i64 5 to i32
    call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32* @.global.i.addr)
    %.temp.2 = load i32, i32* @.global.i.addr
    %.idx64.1 = sext i32 %.temp.1 to i64
    %.arrPtr.1 = getelementptr inbounds [10 x i32], [10 x i32]* %z.addr, i64 0, i64 %.idx64.1
    store i32 %.temp.2, i32* %.arrPtr.1
    %.temp.3 = trunc i64 5 to i32
    %.idx64.2 = sext i32 %.temp.3 to i64
    %.arrPtr.2 = getelementptr inbounds [10 x i32], [10 x i32]* %z.addr, i64 0, i64 %.idx64.2
    %.temp.4 = load i32, i32* %.arrPtr.2
    %.temp.5 = trunc i64 88 to i32
    %.temp.7 = sitofp i32 %.temp.5 to float
    %f.1 = load float, float* %f.addr
    %.temp.6 = fmul float %.temp.7, %f.1
    %.temp.9 = sitofp i32 %.temp.4 to float
    %.temp.8 = fsub float %.temp.9, %.temp.6
    %.wrtf.double.1 = fpext float %.temp.8 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.1)
    call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32* @.global.i.addr)
    %.readi.global.i.1 = load i32, i32* @.global.i.addr
    %.readi.i1.cmp1.1 = icmp eq i32 %.readi.global.i.1, 0
    %.temp.10 = xor i1 %.readi.i1.cmp1.1, 1
    store i1 %.temp.10, i1* %b.addr
    call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), float* @.global.f.addr)
    %.temp.11 = load float, float* @.global.f.addr
    store float %.temp.11, float* %f.addr
    %b.1 = load i1, i1* %b.addr
    br i1 %b.1, label %.br.cont.1, label %endif1
  .br.cont.1:
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.s.1, i64 0, i64 0))
    %f.2 = load float, float* %f.addr
    %.temp.12 = fneg float %f.2
    %.temp.13 = fneg float %.temp.12
    %.temp.14 = fneg float %.temp.13
    %.wrtf.double.2 = fpext float %.temp.14 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.2)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.2, i64 0, i64 0))
    br label %endif1
  endif1:
    %.temp.15 = trunc i64 1 to i1
    store i1 %.temp.15, i1* %_result.addr
    %_result.1 = load i1, i1* %_result.addr
    ret i1 %_result.1
  .dead.code.1:
    %_result.2 = load i1, i1* %_result.addr
    ret i1 %_result.2
}

define dso_local float @fz(i32 %r, float %u) {
  .entry:
    %_result.addr = alloca float
    %r.addr = alloca i32
    %u.addr = alloca float
    store i32 %r, i32* %r.addr
    store float %u, float* %u.addr
    br label %while1
  while1:
    %.temp.1 = fptrunc double 0.01 to float
    %r.1 = load i32, i32* %r.addr
    %.temp.3 = sitofp i32 %r.1 to float
    %.temp.2 = fcmp olt float %.temp.1, %.temp.3
    br i1 %.temp.2, label %.br.cont.1, label %endwhile1
  .br.cont.1:
    %.temp.4 = trunc i64 1 to i32
    %r.2 = load i32, i32* %r.addr
    %.temp.5 = sub i32 %r.2, %.temp.4
    store i32 %.temp.5, i32* %r.addr
    br label %while1
  endwhile1:
    %.temp.6 = trunc i64 0 to i32
    %r.3 = load i32, i32* %r.addr
    %.temp.7 = icmp eq i32 %r.3, %.temp.6
    br i1 %.temp.7, label %.br.cont.2, label %endif1
  .br.cont.2:
    %.temp.9 = trunc i64 55555 to i32
    %.temp.10 = trunc i64 5 to i32
    %.temp.11 = sub i32 0, %.temp.10
    %.temp.12 = trunc i64 4 to i32
    %.temp.13 = sdiv i32 %.temp.11, %.temp.12
    %.temp.14 = sitofp i32 %.temp.13 to float
    %.temp.8 = call i1 @f(i32 %.temp.9, float %.temp.14)
    br label %endif1
  endif1:
    %.temp.15 = trunc i64 3 to i32
    %r.4 = load i32, i32* %r.addr
    %.temp.16 = add i32 %r.4, %.temp.15
    %.temp.18 = sitofp i32 %.temp.16 to float
    %u.1 = load float, float* %u.addr
    %.temp.17 = fmul float %.temp.18, %u.1
    store float %.temp.17, float* %_result.addr
    %_result.1 = load float, float* %_result.addr
    ret float %_result.1
  .dead.code.1:
    %_result.2 = load float, float* %_result.addr
    ret float %_result.2
}

define dso_local i32 @main() {
  .entry:
    %a.addr = alloca i32
    %q.addr = alloca float
    %.temp.1 = trunc i64 1 to i32
    %.temp.2 = sub i32 0, %.temp.1
    %.temp.3 = sitofp i32 %.temp.2 to float
    store float %.temp.3, float* %q.addr
    %.temp.5 = trunc i64 3 to i32
    %.temp.6 = trunc i64 4 to i32
    %.temp.7 = add i32 %.temp.5, %.temp.6
    %.temp.9 = trunc i64 4444 to i32
    %.temp.10 = trunc i64 3 to i32
    %.temp.12 = sitofp i32 %.temp.10 to float
    %q.1 = load float, float* %q.addr
    %.temp.11 = fadd float %q.1, %.temp.12
    %.temp.8 = call float @fz(i32 %.temp.9, float %.temp.11)
    %.temp.4 = call float @fz(i32 %.temp.7, float %.temp.8)
    store float %.temp.4, float* %q.addr
    %.temp.13 = fptrunc double 3.7 to float
    %q.2 = load float, float* %q.addr
    %.temp.14 = fadd float %q.2, %.temp.13
    %.temp.15 = trunc i64 4 to i32
    %.temp.17 = sitofp i32 %.temp.15 to float
    %.temp.16 = fadd float %.temp.14, %.temp.17
    %.wrtf.double.1 = fpext float %.temp.16 to double
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.f, i64 0, i64 0), double %.wrtf.double.1)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.s.2, i64 0, i64 0))
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)
declare dso_local i32 @__isoc99_scanf(i8*, ...)


