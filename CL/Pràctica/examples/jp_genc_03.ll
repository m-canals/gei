
@.str.i = constant [3 x i8] c"%d\00"
@.str.s.1 = constant [7 x i8] c"x*y*2=\00"
@.str.s.2 = constant [3 x i8] c".\0A\00"


@.global.i.addr = common dso_local global i32 0


define dso_local i32 @mul(i32 %a, i32 %b) {
  .entry:
    %_result.addr = alloca i32
    %a.addr = alloca i32
    %b.addr = alloca i32
    store i32 %a, i32* %a.addr
    store i32 %b, i32* %b.addr
    %a.1 = load i32, i32* %a.addr
    %b.1 = load i32, i32* %b.addr
    %.temp.1 = mul i32 %a.1, %b.1
    store i32 %.temp.1, i32* %_result.addr
    %_result.1 = load i32, i32* %_result.addr
    ret i32 %_result.1
  .dead.code.1:
    %_result.2 = load i32, i32* %_result.addr
    ret i32 %_result.2
}

define dso_local i32 @main() {
  .entry:
    %x.addr = alloca i32
    %y.addr = alloca i32
    call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32* @.global.i.addr)
    %.temp.1 = load i32, i32* @.global.i.addr
    store i32 %.temp.1, i32* %x.addr
    call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32* @.global.i.addr)
    %.temp.2 = load i32, i32* @.global.i.addr
    store i32 %.temp.2, i32* %y.addr
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.s.1, i64 0, i64 0))
    %x.1 = load i32, i32* %x.addr
    %y.1 = load i32, i32* %y.addr
    %.temp.3 = call i32 @mul(i32 %x.1, i32 %y.1)
    %.temp.4 = trunc i64 2 to i32
    %.temp.5 = mul i32 %.temp.3, %.temp.4
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.i, i64 0, i64 0), i32 %.temp.5)
    call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.s.2, i64 0, i64 0))
    ret i32 0
}


declare dso_local i32 @printf(i8*, ...)
declare dso_local i32 @__isoc99_scanf(i8*, ...)


