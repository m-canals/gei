#version 330 core

in vec3 vertColor;

out vec3 fragColor;

void main() {
	fragColor = vertColor;
}
