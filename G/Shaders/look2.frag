#version 330 core

in vec4 vs_color;

out vec4 fragColor;

void main() {
	fragColor = vs_color;
}
