#version 330 core

const vec3 red = vec3(1, 0, 0);

out vec3 fragColor;

void main() {
	fragColor = red;
}
