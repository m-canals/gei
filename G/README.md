Apunts de la tardor de 2021 i exercicis fins la tardor de 2021 de l'assignatura Gràfics (G) del Grau en Enginyeria Informàtica (GEI) de la Facultat de Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web nou de l'assignatura](https://sites.google.com/upc.edu/grafics-fib/) i al [web vell de l'assignatura](https://www.cs.upc.edu/~virtual/G/).

Exercicis amb test:

 - Oscillate (no coincideix)
 - Checkerboard 1 (un model no coincideix del tot)
 - Checkerboard 4 (un model no coincideix del tot)
 - Cstripes (un model no coincideix)
 - Lego (no coincideix del tot)
 - Look (no coincideix del tot)
 - Light Optics (no concideix del tot)
 - Plasma (no concideix del tot)
 - Look 2 (no coincideix del tot)
 - Sphere (no coincideix del tot)

Exercicis sense test:

 - Hallu
 - Marble
 - Extrude
 - Grass (la textura no coincideix)
 - Spikes
 - Explode 2
 - Progressive
 - Oscillating Shrink
 - Area
 - Voxelize
 - Spherical points
 - Profile
 - Disco Sphere
 - Solidtex
 - Inking
 - Glossy
 - Projective Texture Mapping (la textura no coincideix)
 - Skymap (la textura no coincideix si els eixos coincideixen)
 - Ironed
 - Retall
 - Rot
 - Pacman
 - Is a Floor
 - Show color
 - Inverter
