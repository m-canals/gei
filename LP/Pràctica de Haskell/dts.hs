import Data.List (sortBy, groupBy, intercalate, (\\))
import Data.Maybe (fromJust, isNothing)
import Data.Ord (comparing)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

--------------------------------------------------------------------------------
-- Metadades
--------------------------------------------------------------------------------

-- Fóra bo de desar les metadades en un altre fitxer i interpretar-les, però
-- s'ha de fer tot en aquest, tot i que que es poden incloure al fitxer de
-- dades.

-- | Retorna el nombre de valors que pot prendre una columna en funció de
-- l'índex de la columna.
columnValues :: Int -> Int
columnValues 0 = 2
columnValues 1 = 6
columnValues 2 = 4
columnValues 3 = 10
columnValues 4 = 2
columnValues 5 = 9
columnValues 6 = 4
columnValues 7 = 3
columnValues 8 = 2
columnValues 9 = 12
columnValues 10 = 2
columnValues 11 = 7
columnValues 12 = 4
columnValues 13 = 4
columnValues 14 = 9
columnValues 15 = 9
columnValues 16 = 2
columnValues 17 = 4
columnValues 18 = 3
columnValues 19 = 8
columnValues 20 = 9
columnValues 21 = 6
columnValues 22 = 7

-- | Retorna el nom d'una columna en funció de l'índex de la columna.
columnName :: Int -> String
columnName 0 = "edible?"
columnName 1 = "cap-shape"
columnName 2 = "cap-surface"
columnName 3 = "cap-color"
columnName 4 = "bruises?"
columnName 5 = "odor"
columnName 6 = "gill-attachment"
columnName 7 = "gill-spacing"
columnName 8 = "gill-size"
columnName 9 = "gill-color"
columnName 10 = "stalk-shape"
columnName 11 = "stalk-root"
columnName 12 = "stalk-surface-above-ring"
columnName 13 = "stalk-surface-below-ring"
columnName 14 = "stalk-color-above-ring"
columnName 15 = "stalk-color-below-ring"
columnName 16 = "veil-type"
columnName 17 = "veil-color"
columnName 18 = "ring-number"
columnName 19 = "ring-type"
columnName 20 = "spore-print-color"
columnName 21 = "population"
columnName 22 = "habitat"

-- | Retorna l'índex d'un valor d'una columna en funció de l'índex de la columna
-- i l'identificador textual del valor.
valueIndex :: Int -> [Char] -> Int
valueIndex 0 "e" = 0
valueIndex 0 "p" = 1
valueIndex 1 "b" = 0
valueIndex 1 "c" = 1
valueIndex 1 "x" = 2
valueIndex 1 "f" = 3
valueIndex 1 "k" = 4
valueIndex 1 "s" = 5
valueIndex 2 "f" = 0
valueIndex 2 "g" = 1
valueIndex 2 "y" = 2
valueIndex 2 "s" = 3
valueIndex 3 "n" = 0
valueIndex 3 "b" = 1
valueIndex 3 "c" = 2
valueIndex 3 "g" = 3
valueIndex 3 "r" = 4
valueIndex 3 "p" = 5
valueIndex 3 "u" = 6
valueIndex 3 "e" = 7
valueIndex 3 "w" = 8
valueIndex 3 "y" = 9
valueIndex 4 "t" = 0
valueIndex 4 "f" = 1
valueIndex 5 "a" = 0
valueIndex 5 "l" = 1
valueIndex 5 "c" = 2
valueIndex 5 "y" = 3
valueIndex 5 "f" = 4
valueIndex 5 "m" = 5
valueIndex 5 "n" = 6
valueIndex 5 "p" = 7
valueIndex 5 "s" = 8
valueIndex 6 "a" = 0
valueIndex 6 "d" = 1
valueIndex 6 "f" = 2
valueIndex 6 "n" = 3
valueIndex 7 "c" = 0
valueIndex 7 "w" = 1
valueIndex 7 "d" = 2
valueIndex 8 "b" = 0
valueIndex 8 "n" = 1
valueIndex 9 "k" = 0
valueIndex 9 "n" = 1
valueIndex 9 "b" = 2
valueIndex 9 "h" = 3
valueIndex 9 "g" = 4
valueIndex 9 "r" = 5
valueIndex 9 "o" = 6
valueIndex 9 "p" = 7
valueIndex 9 "u" = 8
valueIndex 9 "e" = 9
valueIndex 9 "w" = 10
valueIndex 9 "y" = 11
valueIndex 10 "e" = 0
valueIndex 10 "t" = 1
valueIndex 11 "b" = 0
valueIndex 11 "c" = 1
valueIndex 11 "u" = 2
valueIndex 11 "e" = 3
valueIndex 11 "z" = 4
valueIndex 11 "r" = 5
valueIndex 11 "?" = 6
valueIndex 12 "f" = 0
valueIndex 12 "y" = 1
valueIndex 12 "k" = 2
valueIndex 12 "s" = 3
valueIndex 13 "f" = 0
valueIndex 13 "y" = 1
valueIndex 13 "k" = 2
valueIndex 13 "s" = 3
valueIndex 14 "n" = 0
valueIndex 14 "b" = 1
valueIndex 14 "c" = 2
valueIndex 14 "g" = 3
valueIndex 14 "o" = 4
valueIndex 14 "p" = 5
valueIndex 14 "e" = 6
valueIndex 14 "w" = 7
valueIndex 14 "y" = 8
valueIndex 15 "n" = 0
valueIndex 15 "b" = 1
valueIndex 15 "c" = 2
valueIndex 15 "g" = 3
valueIndex 15 "o" = 4
valueIndex 15 "p" = 5
valueIndex 15 "e" = 6
valueIndex 15 "w" = 7
valueIndex 15 "y" = 8
valueIndex 16 "p" = 0
valueIndex 16 "u" = 1
valueIndex 17 "n" = 0
valueIndex 17 "o" = 1
valueIndex 17 "w" = 2
valueIndex 17 "y" = 3
valueIndex 18 "n" = 0
valueIndex 18 "o" = 1
valueIndex 18 "t" = 2
valueIndex 19 "c" = 0
valueIndex 19 "e" = 1
valueIndex 19 "f" = 2
valueIndex 19 "l" = 3
valueIndex 19 "n" = 4
valueIndex 19 "p" = 5
valueIndex 19 "s" = 6
valueIndex 19 "z" = 7
valueIndex 20 "k" = 0
valueIndex 20 "n" = 1
valueIndex 20 "b" = 2
valueIndex 20 "h" = 3
valueIndex 20 "r" = 4
valueIndex 20 "o" = 5
valueIndex 20 "u" = 6
valueIndex 20 "w" = 7
valueIndex 20 "y" = 8
valueIndex 21 "a" = 0
valueIndex 21 "c" = 1
valueIndex 21 "n" = 2
valueIndex 21 "s" = 3
valueIndex 21 "v" = 4
valueIndex 21 "y" = 5
valueIndex 22 "g" = 0
valueIndex 22 "l" = 1
valueIndex 22 "m" = 2
valueIndex 22 "p" = 3
valueIndex 22 "u" = 4
valueIndex 22 "w" = 5
valueIndex 22 "d" = 6
-- Conceptualment potser s'hauria de fer servir la mònada Maybe, però, en aquest
-- cas, no fer-la servir simplifica els càlculs.
valueIndex _ _ = -1

-- | Retorna el nom d'un valor d'una columna en funció de l'índex de la columna
-- i de l'índex del valor.
valueName :: Int -> Int -> [Char]
valueName 0 0 = "edible"
valueName 0 1 = "poisonous"
valueName 1 0 = "bell"
valueName 1 1 = "conical"
valueName 1 2 = "convex"
valueName 1 3 = "flat"
valueName 1 4 = "knobbed"
valueName 1 5 = "sunken"
valueName 2 0 = "fibrous"
valueName 2 1 = "grooves"
valueName 2 2 = "scaly"
valueName 2 3 = "smooth"
valueName 3 0 = "brown"
valueName 3 1 = "buff"
valueName 3 2 = "cinnamon"
valueName 3 3 = "gray"
valueName 3 4 = "green"
valueName 3 5 = "pink"
valueName 3 6 = "purple"
valueName 3 7 = "red"
valueName 3 8 = "white"
valueName 3 9 = "yellow"
valueName 4 0 = "bruises"
valueName 4 1 = "no"
valueName 5 0 = "almond"
valueName 5 1 = "anise"
valueName 5 2 = "creosote"
valueName 5 3 = "fishy"
valueName 5 4 = "foul"
valueName 5 5 = "musty"
valueName 5 6 = "none"
valueName 5 7 = "pungent"
valueName 5 8 = "spicy"
valueName 6 0 = "attached"
valueName 6 1 = "descending"
valueName 6 2 = "free"
valueName 6 3 = "notched"
valueName 7 0 = "close"
valueName 7 1 = "crowded"
valueName 7 2 = "distant"
valueName 8 0 = "broad"
valueName 8 1 = "narrow"
valueName 9 0 = "black"
valueName 9 1 = "brown"
valueName 9 2 = "buff"
valueName 9 3 = "chocolate"
valueName 9 4 = "gray"
valueName 9 5 = "green"
valueName 9 6 = "orange"
valueName 9 7 = "pink"
valueName 9 8 = "purple"
valueName 9 9 = "red"
valueName 9 10 = "white"
valueName 9 11 = "yellow"
valueName 10 0 = "enlarging"
valueName 10 1 = "tapering"
valueName 11 0 = "bulbous"
valueName 11 1 = "club"
valueName 11 2 = "cup"
valueName 11 3 = "equal"
valueName 11 4 = "rhizomorphs"
valueName 11 5 = "rooted"
valueName 11 6 = "missing"
valueName 12 0 = "fibrous"
valueName 12 1 = "scaly"
valueName 12 2 = "silky"
valueName 12 3 = "smooth"
valueName 13 0 = "fibrous"
valueName 13 1 = "scaly"
valueName 13 2 = "silky"
valueName 13 3 = "smooth"
valueName 14 0 = "brown"
valueName 14 1 = "buff"
valueName 14 2 = "cinnamon"
valueName 14 3 = "gray"
valueName 14 4 = "orange"
valueName 14 5 = "pink"
valueName 14 6 = "red"
valueName 14 7 = "white"
valueName 14 8 = "yellow"
valueName 15 0 = "brown"
valueName 15 1 = "buff"
valueName 15 2 = "cinnamon"
valueName 15 3 = "gray"
valueName 15 4 = "orange"
valueName 15 5 = "pink"
valueName 15 6 = "red"
valueName 15 7 = "white"
valueName 15 8 = "yellow"
valueName 16 0 = "partial"
valueName 16 1 = "universal"
valueName 17 0 = "brown"
valueName 17 1 = "orange"
valueName 17 2 = "white"
valueName 17 3 = "yellow"
valueName 18 0 = "none"
valueName 18 1 = "one"
valueName 18 2 = "two"
valueName 19 0 = "cobwebby"
valueName 19 1 = "evanescent"
valueName 19 2 = "flaring"
valueName 19 3 = "large"
valueName 19 4 = "none"
valueName 19 5 = "pendant"
valueName 19 6 = "sheathing"
valueName 19 7 = "zone"
valueName 20 0 = "black"
valueName 20 1 = "brown"
valueName 20 2 = "buff"
valueName 20 3 = "chocolate"
valueName 20 4 = "green"
valueName 20 5 = "orange"
valueName 20 6 = "purple"
valueName 20 7 = "white"
valueName 20 8 = "yellow"
valueName 21 0 = "abundant"
valueName 21 1 = "clustered"
valueName 21 2 = "numerous"
valueName 21 3 = "scattered"
valueName 21 4 = "several"
valueName 21 5 = "solitary"
valueName 22 0 = "grasses"
valueName 22 1 = "leaves"
valueName 22 2 = "meadows"
valueName 22 3 = "paths"
valueName 22 4 = "urban"
valueName 22 5 = "waste"
valueName 22 6 = "woods"

-- | Retorna el nom de cada valor que pot prendre una columna en funció de
-- l'índex de la columna.
valueNames :: Int -> [[Char]]
valueNames column =
 map (valueName column) [0 .. columnValues column - 1]

--------------------------------------------------------------------------------
-- Funcions generals
--------------------------------------------------------------------------------

-- | Sigui c un caràcter, el caràcter separador, i s una cadena de caràcters,
-- la cadena de caràcters separada, split c s retorna una llista amb les
-- cadenes de caràcters resultants de dividir s per les posicions on hi ha c.
split :: (Foldable t, Eq a) => a -> t a -> [[a]]
split separator =
 foldr (\x y @ (a : b) -> if x == separator then [] : y else (x : a) : b) [[]]

-- | Sigui l una llista, e un element del mateix tipus que els elements de la
-- llista l i p una posició entre zero i la mida de la llista l, insert l e p
-- retorna una llista amb els elements de la llista l, en el mateix ordre, i amb
-- l'element e a la posició p, és a dir, una llista tal que els elements de la
-- primera posició a la p - 1 són els elements de la primera posició a la p - 1
-- de la llista l, l'element a la posició p és l'element e i els elements de la
-- posició p + 1 a la darrera són els elements de la posició p a la darrera de
-- la llista l.
insert :: [a] -> a -> Int -> [a]
insert list value index =
 before ++ [value] ++ after
 where
  (before, after) = splitAt index list

-- | Sigui l una llista d'elements ordenables, retorna la posició del primer
-- element màxim de la llista l.
indexOfMaximum :: Ord a => [a] -> Int
indexOfMaximum (x : xs) =
 i
 where
  (i, _, _) = foldl (\(i, j, x) y ->
   if y > x then (j, j + 1, y) else (i, j + 1, x)) (0, 1, x) xs

--------------------------------------------------------------------------------
-- Definició, representació textual i creació de l'arbre de decisions
--------------------------------------------------------------------------------

-- | Un arbre de decisions és un arbre tal que cada branca representa una
-- decisió en funció d'una columna i se separa en tantes branques com valors pot
-- prendre la columna i cada fulla representa la predicció de la proporció de
-- files que seran de cada classe en funció de les decisions preses.
data DecisionTree = Prediction Int [Double] | Decision Int [DecisionTree]

-- | Retorna la representació textual d'un valor d'una decisió.
showDecisionValue :: [Char] -> DecisionTree -> [Char] -> [Char]
showDecisionValue identation tree name  =
 identation ++ " " ++ name ++ "\n" ++ (showDecisionTree (identation ++ "  ") tree)

-- | Retorna la representació textual d'un arbre de decisions.
showDecisionTree :: [Char] -> DecisionTree -> [Char]
showDecisionTree identation (Prediction column proportions) =
 strProp
 where
  indices = [0 .. length proportions - 1]
  strProp = concat $ map (\index -> identation ++ (valueName column index) ++ " " ++ (show $ proportions !! index) ++ "\n") indices
showDecisionTree identation (Decision column trees)  =
 identation ++ (columnName column) ++ "\n" ++ values
 where
  indices = [0 .. length trees - 1]
  values = concat $ map (\index -> showDecisionValue identation (trees !! index) (valueName column index)) indices

instance Show DecisionTree where
 show tree = showDecisionTree "" tree

-- | Actualiza el nombre d'ocurrències de cada classe per a cada valor d'una
-- columna.
updateOccurrences :: Num a => Int -> Int -> [[a]] -> [Int] -> [[a]]
updateOccurrences classColumn column result row =
 a ++ [a' ++ [b' + 1] ++ c'] ++ c
 where
  (a, b : c) = splitAt value result
  (a', b' : c') = splitAt classValue b
  classValue = row !! classColumn
  value = row !! column

-- | Retorna una llista que conté, per a cada valor d'una columna, una llista
-- que conté, per a cada valor de la columna de classificació, el nombre de
-- files d'aquella classe.
getOccurrences :: Num a => [[Int]] -> Int -> Int -> [[a]]
getOccurrences rows classColumn column =
 occurrencesList
 where
  numberOfClassAttributeValues = columnValues classColumn
  numberOfAttributeValues = columnValues column
  emptyOccurrencesList = take numberOfAttributeValues $ repeat emptyOccurrences
  emptyOccurrences = take numberOfClassAttributeValues $ repeat 0
  occurrencesList = foldl (updateOccurrences classColumn column) emptyOccurrencesList rows

-- | Retorna l'índex de la columna i les ocurrències de cada classe per a cada
-- valor de la columna de la columna d'una llista de columnes que permet decidir
-- la classe de les files d'una llista de files amb més encert.
getBestColumn :: (Num a, Ord a) => [[Int]] -> [Int] -> Int -> (Int, [[a]])
getBestColumn rows columns classColumn =
 (columns !! index, occurrences !! index)
 where
  occurrences = map (getOccurrences rows classColumn) columns
  maximumNumberOfClassifiableRows = map (sum . map maximum) occurrences
  index = indexOfMaximum maximumNumberOfClassifiableRows

-- | Retorna les files d'una llista de files agrupades en funció del valor d'una
-- columna.
getGroupedRows :: [[Int]] -> Int -> [[[Int]]]
getGroupedRows rows index =
 foldl (\x y -> insert x [] y) groupedRows excluded
 where
  sortedRows = sortBy (comparing (\x -> x !! index)) rows
  groupedRows = groupBy (\x y -> x !! index == y !! index) sortedRows
  all = [0 .. columnValues index - 1]
  included = map (\x -> x !! 0 !! index) groupedRows
  excluded = all \\ included

-- | Retorna una predicció per a un valor, si el valor no apareix a cap fila,
-- totes les files amb aquest valor pertanyen a la mateixa classe o si no resten
-- columnes de decisió; retorna una decisió, si el valor apareix en alguna fila,
-- no totes les files amb aquest valor pertanyen a la mateixa classe i resta
-- alguna columna de decisió.
getDecisionOrPrediction :: Integral a => [[Int]] -> [Int] -> Int -> [a] -> DecisionTree
getDecisionOrPrediction rows columns classColumn occurrences =
 if (length $ filter (/= 0) occurrences) <= 1 || null columns
 then Prediction classColumn (map (\x -> fromIntegral x / fromIntegral (sum occurrences)) occurrences)
 else getDecisionTree rows columns classColumn

-- | Retorna un arbre de decisions en funció d'una taula de dades, una llista de
-- columnes de decisió i una columna de classificació.
getDecisionTree :: [[Int]] -> [Int] -> Int -> DecisionTree
getDecisionTree rows columns classColumn =
 Decision index decisions
 where
  (index, occurrences) = getBestColumn rows columns classColumn
  groupedRows = getGroupedRows rows index
  decisions = map (\i -> getDecisionOrPrediction (groupedRows !! i) (columns \\ [index]) classColumn (occurrences !! i)) [0 .. columnValues index - 1]

--------------------------------------------------------------------------------
-- Processament i validació de les dades d'entrada
--------------------------------------------------------------------------------

-- | Interpreta una cadena de text com una fila de dades.
getRow :: [Char] -> [Int]
getRow line =
 row
 where
  values = split ',' line
  columns = [0 .. length values - 1]
  row = zipWith (\column value -> valueIndex column value) columns values

-- | Interpreta una cadena de text com una taula de dades.
getData :: [Char] -> [[Int]]
getData contents =
 map getRow $ lines contents

-- | Retorna el primer error d'un valor d'una fila de dades, si n'hi ha.
getFirstValueError :: Int -> Int -> Int -> Maybe [Char]
getFirstValueError value rowIndex columnIndex =
 if value < 0
 then Just ("bad value (row " ++ (show rowIndex) ++ ", column " ++ (show columnIndex) ++ ")")
 else Nothing

-- | Retorna el primer error d'una fila de dades, si n'hi ha.
getFirstRowError :: [Int] -> Int -> Int -> Maybe [Char]
getFirstRowError row rowIndex maximumColumnIndex =
 if length row - 1 < maximumColumnIndex
 then Just ("not enough columns (row " ++ (show rowIndex) ++ ")")
 else if not $ null errors
 then error
 else Nothing
 where
  indices = [0 .. length row - 1]
  valueErrors = map (\i -> getFirstValueError (row !! i) rowIndex i) indices
  errors = filter (not . isNothing) valueErrors
  error = head errors

-- | Retorna el primer error d'una taula de dades, si n'hi ha.
getFirstDataError :: [[Int]] -> Int -> Maybe [Char]
getFirstDataError rows maximumColumnIndex =
 if not $ null errors
 then error
 else Nothing
 where
  indices = [0 .. length rows - 1]
  rowErrors = map (\i -> getFirstRowError (rows !! i) i maximumColumnIndex) indices
  errors = filter (not . isNothing) rowErrors
  error = head errors

--------------------------------------------------------------------------------
-- Funcions d'entrada i sortida i funció principal
--------------------------------------------------------------------------------

-- | Demana una decisió mentre la decisió no és vàlida; quan és vàlida mostra
-- la predicció en funció de la decisió presa. 
printDecision :: [DecisionTree] -> IO ()
printDecision decisionTrees = do
 putStr ("decision (0-" ++ (show $ length decisionTrees - 1) ++ "): ")
 hFlush stdout
 line <- getLine
 let input = readMaybe line :: Maybe Int
 if isNothing input then do
  putStrLn "bad decision"
  printDecision decisionTrees
 else do
  let index = fromJust input
  if 0 <= index && index < (length decisionTrees) then
   printPrediction (decisionTrees !! index)
  else do
   putStrLn "bad decision"
   printDecision decisionTrees

-- | Mostra una predicció que varia en funció de les decisions preses.
printPrediction :: DecisionTree -> IO ()
printPrediction (Prediction column proportions) = do
 let names = valueNames column
 let namesAndProportions = zipWith (\name proportion -> name ++ " (" ++ (show proportion) ++ ")") names proportions
 let prediction = intercalate ", " namesAndProportions
 putStrLn ("prediction: " ++ prediction)
 return ()
printPrediction (Decision column decisionTrees) = do
 let name = columnName column
 let names = valueNames column
 let indices = [0 .. length names - 1]
 let values = concat $ zipWith (\index name -> " " ++ (show index) ++ ") " ++ name ++ "\n") indices names
 putStr ("attribute: " ++ name ++ "\n" ++ values)
 printDecision decisionTrees

-- | Funció principal.
main :: IO ()
main = do
 -- Paràmetres: índex de la columna de classificació
 -- i de cada columna de decisió i ubicació del fitxer de dades.
 let classColumn = 0
 let columns = [1 .. 22]
 let filepath = "agaricus-lepiota.data"
 contents <- readFile filepath
 let rows = getData contents
 let maximumColumnIndex = maximum (classColumn : columns)
 let error = getFirstDataError rows maximumColumnIndex
 if isNothing error then do
  let decisionTree = getDecisionTree rows columns classColumn
  putStrLn $ show decisionTree
  printPrediction decisionTree
 else do
  let message = fromJust error
  putStrLn message

