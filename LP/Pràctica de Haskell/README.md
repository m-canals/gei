# Arbres de decisió

Aquest programa classifica instàncies d'una classe de dades en funció d'un
atribut, mitjançant un arbre de decisions, i permet de consultar la proporció
d'instàncies d'una classe, respecte del total, els atributs de les quals prenen
els valors indicats per l'usuari, minimitzant el nombre d'atributs necessaris
per a determinar aquestes proporcions.

Si no hi ha dues instàncies iguals, és a dir, amb el mateix atribut de
classificació i els mateixos atributs de decisió, llavors la proporció
d'instàncies és 1.0 en una classe i 0.0 a les altres classes, és a dir, en
funció dels atributs considerats podem determinar la classe, el valor de
l'atribut de classificació, amb certesa.

Si un dels valors d'un atribut no apareix a cap instància, llavors la proporció
d'instàncies amb aquest valor és NaN a totes les classes, és a dir, no és
computable.

Cal remarcar que:

 * L'atribut de classificació pot ser qualsevol dels atributs.
 * L'atribut de classificació pot prendre més de dos valors.
 * Una predicció no prediu la classe d'una instància amb els atributs que ha
   indicat l'usuari, sinó que prediu, per a cada classe, la proporció
   d'instàncies amb aquests atributs; d'aquesta manera, l'usuari pot fer una
   interpretació adequada en funció de les proporcions, sense perdre informació.
 * El conjunt d'atributs de decisió pot ser qualsevol subconjunt no nul dels
   atributs.
 * Es comprova que les dades del fitxer de dades i les dades demanades a
   l'usuari durant el recorregut interactiu de l'arbre siguin vàlides.

Els arguments, atribut de classificació i atributs de decisió, es poden canviar
a la funció principal. També es pot modificar les metadades per a jugar amb els
dominis dels atributs.

## Estructura del fitxer de dades

Un fitxer de dades és una successió de línies i cada línia és una successió de
fragments de text, separats per una coma; conté els valors, representats com a
fragments de text, que prenen els atributs de cada instància, representada com
una línia, d'un conjunt d'instàncies d'una classe de dades, representat com un
fitxer. La posició d'un valor, respecte de la seva fila, és l'atribut d'aquest
valor. Anomenem taula al fitxer, fila a la línia, columna a l'atribut i valor al
fragment de text.

## Metadades del fitxer de dades

El programa inclou les metadades del fitxer de dades, tot i que convindria de
posar-les en un fitxer separat per a una major generalització, passant-les com a
argument a la funció de construcció de l'arbre, a la funció parametritzada
equivalent a la funció sense paràmetres de generació de la representació textual
de l'arbre (show) i a la funció que recorre l'arbre de manera interactiva.

Aquestes metadades són, per a cada atribut de la classe, el nom i les dades dels
valors que pot prendre aquest atribut. Les dades d'un valor són el nom i
l'identificador amb el qual es representa al fitxer de dades.

## Errors al fitxer de dades

El programa detecta els següents errors:

 * El valor de la columna COLUMNA de la fila FILA no és un dels valors vàlids
   per a aquesta columna.
   
		bad value (row FILA, column COLUMNA)
 
 * La fila FILA no té alguna de les columnes que es vol fer servir:
 
		not enough columns (row FILA)

## Construcció de l'arbre de decisions

Un arbre de decisions és un arbre tal que cada branca representa una decisió en
funció d'un atribut i se separa en tantes branques com valors pot prendre aquest
atribut (la posició de la branca indica el valor) i cada fulla representa la
predicció de la proporció d'instàncies que són de cada classe en funció de les
decisions preses. Representem un atribut amb un nombre enter que n'indica la
posició i les proporcions amb nombres reals.

	data DecisionTree = Prediction Int [Double] | Decision Int [DecisionTree]

Sigui I la llista d'instàncies, D la llista no buida d'atributs de decisió i les
classes els valors que pot prendre l'atribut de classificació, llavors l'arbre
de decisions es construeix de la següent manera.

Determinem quin és el millor atribut de decisió; en cas d'empat, és el primer
dels millors. L'anomenem m.

Per a cada valor v dels valors que pot prendre l'atribut m, sigui I' la llista
d'instàncies de la llista I tals que l'atribut m de les quals pren v com a
valor i D' la llista D sense l'atribut m:
 
 * Si totes les instàncies de I' són d'una mateixa classe, llavors prediem la
   proporció 1.0 per a aquesta classe i 0.0 per a les altres classes i retornem
   aquesta predicció.
 
 * Si I' és buida, llavors, per a cada classe, prediem la proporció NaN (no
   computable) i retornem aquesta predicció.
 
 * Si D' és buida, llavors, per a cada classe, prediem la proporció
   d'instàncies de I' que són d'aquesta classe i retornem aquesta predicció.

 * En cas contrari, retornem el resultat d'aplicar l'algorisme canviant I per
   I' i D per D'.

```
getDecisionOrPrediction rows columns classColumn occurrences =
if (length $ filter (/= 0) occurrences) <= 1 || null columns
then Prediction (map (\x -> fromIntegral x / fromIntegral (sum occurrences)) occurrences)
else getDecisionTree rows columns classColumn

getDecisionTree rows columns classColumn =
Decision index decisions
where
(index, occurrences) = getBestColumn rows columns classColumn
groupedRows = getGroupedRows rows index
decisions = map (\i ->
getDecisionOrPrediction (groupedRows !! i) (columns \\ [index]) classColumn (occurrences !! i)) [0 .. columnValues index - 1]
```

El cost és quasi-lineal respecte de la mida de la llista d'instàncies, ja que
ordena la llista per a poder fer la agrupació.

### Determinació del millor atribut

Determinem quin és el millor atribut de decisió de la següent manera.

Per a cada atribut d de la llista D, agrupem les instàncies de la llista I en
funció d'aquest atribut. Llavors, per a cada grup, calculem el nombre
d'instàncies d'aquest grup que pertanyen a cada classe; considerem per a aquest
grup el nombre d'instàncies de la classe amb més instàncies, per tal de
maximitzar el grau de correspondència amb una classe. Finalment, sumem el
nombre d'instàncies dels grups de cada atribut i triem l'atribut amb el nombre
més gran.
 
Cal tenir en compte que a l'hora d'implementar-ho no agrupem les instàncies,
sinó que recorrem la llista:

	getBestColumn rows columns classColumn =
	(columns !! index, occurrences !! index)
	where
	occurrences = map (getOccurrences rows classColumn) columns
	maximumNumberOfClassifiableRows = map (sum . map maximum) occurrences
	index = indexOfMaximum maximumNumberOfClassifiableRows

El cost és lineal respecte de la mida de la llista d'instàncies, ja que recorre
la llista una vegada per a cada atribut de decisió restant.

En cas d'empat, es tria el primer atribut dels millors atributs, ja que el cost
de fer això és constant. Si ho féssim de qualsevol altra manera, el cost
temporal seria major o igual, tot i que el cost espacial podria ser menor. Per
tant, prioritzem la rapidesa. Es podria passar la funció per a triar l'atribut
en cas d'empat com a argument, d'aquesta manera el programa seria més
configurable.

## Representació textual de l'arbre de decisions

Utilitzem la classe Show per a representar l'arbre:

	showDecisionValue identation tree name  =
	identation ++ " " ++ name ++ "\n" ++ (showDecisionTree (identation ++ "  ") tree)

	showDecisionTree identation (Prediction column proportions) =
	values
	where
	indices = [0 .. length proportions - 1]
	values = concat $ map (\index -> identation ++ (valueName column index) ++ " " ++ (show $ proportions !! index) ++ "\n") indices
	showDecisionTree identation (Decision column trees)  =
	identation ++ (columnName column) ++ "\n" ++ values
	where
	indices = [0 .. length trees - 1]
	values = concat $ map (\index -> showDecisionValue identation (trees !! index) (valueName column index)) indices

	instance Show DecisionTree where
	show tree = showDecisionTree "" tree

El cost és lineal respecte de la mida de l'arbre, ja que visita tots els nodes.

## Recorregut interactiu de l'arbre de decisions

Per a obtenir una predicció, recorrem l'arbre de decisions de la següent manera,
començant pel node arrel.

Sigui n un node:
  
 * Si el node n correspon a una predicció, hem acabat.
  
 * Si el node n correspon a una decisió, demanem a l'usuari el valor de
   l'atribut de decisió i retornem el resultat d'aplicar l'algorisme canviant
   el node n pel node corresponent al valor indicat per l'usuari.

```
printDecision decisionTrees = do
...
let index = ...
...
printPrediction (decisionTrees !! index)
...

printPrediction (Prediction proportions) = do
...
return ()
printPrediction (Decision column decisionTrees) = do
...
printDecision decisionTrees
```

El cost és lineal respecte de la profunditat de l'arbre, ja que recorre l'arbre
des de l'arrel fins a la fulla més llunyana, en el pitjor dels casos.
