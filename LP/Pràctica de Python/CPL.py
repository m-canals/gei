from antlr4 import CommonTokenStream, InputStream
from antlr4.error.ErrorListener import ErrorListener
from CPLEvaluator import CPLEvaluator, CPLHelpMessage, CPLShortHelpMessage, CPLUndefinedVariable
from getopt import getopt, GetoptError
from CPLLexer import CPLLexer
from CPLParser import CPLParser
from random import seed
from sys import argv, stderr, stdin, stdout


class CPLSyntaxError(Exception):
    """Excepció per als errors de sintaxi."""
    pass


class CPL():
    """Intèrpret de codi en CPL (Convex Polygon Language)."""
    # www.antlr.org/api/Java/org/antlr/v4/runtime/ANTLRErrorListener.html
    class _ErrorListener(ErrorListener):
        """Gestor d'errors. Llança una excepció en lloc de mostrar els
        missatges d'error."""
        def syntaxError(self, recognizer, offendingSymbol, line, column,
                        message, exception):
            """Mètode de gestió d'errors de sintaxi."""
            raise CPLSyntaxError(message)

    def __init__(self):
        """Inicialitza l'intèrpret."""
        self._evaluator = CPLEvaluator()
        self._listener = CPL._ErrorListener()

    def _get_parser(self, string):
        """Retorna amb l'analitzador corresponent al codi."""
        input_stream = InputStream(string)
        lexer = CPLLexer(input_stream)
        # Token reongnition error,...
        lexer.removeErrorListeners()
        lexer.addErrorListener(self._listener)
        token_stream = CommonTokenStream(lexer)
        parser = CPLParser(token_stream)
        # Mismatched input, ...
        parser.removeErrorListeners()
        parser.addErrorListener(self._listener)
        return parser

    def parse(self, string):
        """Analitza una o més línies de codi i retorna amb l'arbre de sintaxi
        corresponent."""
        parser = self._get_parser(string)
        tree = parser.root()
        return tree.toStringTree(recog=parser)

    def evaluate(self, string, output=stdout, files=None):
        """Interpreta una o més línies de codi. La sortida s'escriu al fitxer
        del paràmetre «output». Si el paràmetre «files» conté una llista,
        l'ordre «write» escriu els fitxers en buffers ByteIO i afegeix una
        tupa amb el nom del fitxer i el contingut."""
        parser = self._get_parser(string)
        tree = parser.root()
        return self._evaluator.evaluate(tree, output, files)


def _print_help(file):
    """Mostra un missatge d'ajuda."""
    print(
     "Interpreta programes de manipulació de polígons convexos (CPL).\n\n"
     "Opcions:\n\n"
     " -h       Mostra aquest missatge d'ajuda.\n"
     " -s SEED  Utilitza SEED com a llavor del generador de nombres aleatoris."
     "\n"
     " -t       Mostra l'arbre de sintaxi.\n",
     file=file)


def _get_options():
    """Processa els arguments."""
    try:
        raw_options, arguments = getopt(argv[1:], "hs:t")
    except GetoptError as error:
        print(error, file=stderr)
        _print_help(stderr)
        exit(2)

    options = {
        'print_syntax': False,
        'seed': None
    }

    for option, argument in raw_options:
        if option == '-h':
            _print_help(stdout)
            exit(0)
        elif option == '-s':
            options['seed'] = argument
        elif option == '-t':
            options['print_syntax'] = True

    return options, arguments


def _evaluate_data(evaluator, data):
    """Evalua les dades."""
    evaluator.evaluate(data)


def _parse_data(evaluator, data):
    """Mostra l'arbre sintàctic de les dades."""
    print(evaluator.parse(data))


class _StdinReader():
    """Lector del canal d'entrada estàndard."""
    def read(self):
        data = stdin.readline()
        while data:
            yield data
            data = stdin.readline()


class _FileReader():
    """Lector d'un fitxer."""
    def __init__(self, file):
        self._file = file

    def read(self):
        yield self._file.read()


def _main():
    """Funció principal."""
    options, arguments = _get_options()
    evaluator = CPL()
    process = _parse_data if options['print_syntax'] else _evaluate_data
    seed(options['seed'])

    if len(arguments) > 0:
        try:
            reader = _FileReader(open(arguments[0]))
        except FileNotFoundError as error:
            print(error, file=stderr)
            exit(1)
    else:
        reader = _StdinReader()

    for data in reader.read():
        try:
            process(evaluator, data)
        except (CPLUndefinedVariable, CPLSyntaxError) as error:
            print(error, file=stderr)


if __name__ == '__main__':
    try:
        _main()
    except KeyboardInterrupt:
        pass
