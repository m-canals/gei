# Generated from CPL.g by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .CPLParser import CPLParser
else:
    from CPLParser import CPLParser

# This class defines a complete generic visitor for a parse tree produced by CPLParser.

class CPLVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by CPLParser#root.
    def visitRoot(self, ctx:CPLParser.RootContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#statement.
    def visitStatement(self, ctx:CPLParser.StatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#assignment.
    def visitAssignment(self, ctx:CPLParser.AssignmentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#drawStatement.
    def visitDrawStatement(self, ctx:CPLParser.DrawStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#helpStatement.
    def visitHelpStatement(self, ctx:CPLParser.HelpStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#seedStatement.
    def visitSeedStatement(self, ctx:CPLParser.SeedStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#printStatement.
    def visitPrintStatement(self, ctx:CPLParser.PrintStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#backgroundColorStatement.
    def visitBackgroundColorStatement(self, ctx:CPLParser.BackgroundColorStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#colorStatement.
    def visitColorStatement(self, ctx:CPLParser.ColorStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#defaultColorStatement.
    def visitDefaultColorStatement(self, ctx:CPLParser.DefaultColorStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#defaultFillStatement.
    def visitDefaultFillStatement(self, ctx:CPLParser.DefaultFillStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#defaultOutlineStatement.
    def visitDefaultOutlineStatement(self, ctx:CPLParser.DefaultOutlineStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#defaultVertexSizeStatement.
    def visitDefaultVertexSizeStatement(self, ctx:CPLParser.DefaultVertexSizeStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#fillStatement.
    def visitFillStatement(self, ctx:CPLParser.FillStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#outlineStatement.
    def visitOutlineStatement(self, ctx:CPLParser.OutlineStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#vertexSizeStatement.
    def visitVertexSizeStatement(self, ctx:CPLParser.VertexSizeStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#heightStatement.
    def visitHeightStatement(self, ctx:CPLParser.HeightStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#widthStatement.
    def visitWidthStatement(self, ctx:CPLParser.WidthStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#marginStatement.
    def visitMarginStatement(self, ctx:CPLParser.MarginStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#areaStatement.
    def visitAreaStatement(self, ctx:CPLParser.AreaStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#perimeterStatement.
    def visitPerimeterStatement(self, ctx:CPLParser.PerimeterStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#verticesStatement.
    def visitVerticesStatement(self, ctx:CPLParser.VerticesStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#centroidStatement.
    def visitCentroidStatement(self, ctx:CPLParser.CentroidStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#insideStatement.
    def visitInsideStatement(self, ctx:CPLParser.InsideStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#equilateralStatement.
    def visitEquilateralStatement(self, ctx:CPLParser.EquilateralStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#equiangularStatement.
    def visitEquiangularStatement(self, ctx:CPLParser.EquiangularStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#regularStatement.
    def visitRegularStatement(self, ctx:CPLParser.RegularStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#equalStatement.
    def visitEqualStatement(self, ctx:CPLParser.EqualStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#expression.
    def visitExpression(self, ctx:CPLParser.ExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#lowPrecedenceExpression.
    def visitLowPrecedenceExpression(self, ctx:CPLParser.LowPrecedenceExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#mediumPrecedenceExpression.
    def visitMediumPrecedenceExpression(self, ctx:CPLParser.MediumPrecedenceExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#highPrecedenceExpression.
    def visitHighPrecedenceExpression(self, ctx:CPLParser.HighPrecedenceExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#generation.
    def visitGeneration(self, ctx:CPLParser.GenerationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#bounding.
    def visitBounding(self, ctx:CPLParser.BoundingContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#parenthesizedExpression.
    def visitParenthesizedExpression(self, ctx:CPLParser.ParenthesizedExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#string.
    def visitString(self, ctx:CPLParser.StringContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#points.
    def visitPoints(self, ctx:CPLParser.PointsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#point.
    def visitPoint(self, ctx:CPLParser.PointContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#color.
    def visitColor(self, ctx:CPLParser.ColorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#rgbaColor.
    def visitRgbaColor(self, ctx:CPLParser.RgbaColorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#hexadecimalColor.
    def visitHexadecimalColor(self, ctx:CPLParser.HexadecimalColorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#size.
    def visitSize(self, ctx:CPLParser.SizeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#density.
    def visitDensity(self, ctx:CPLParser.DensityContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#pairOfNumbers.
    def visitPairOfNumbers(self, ctx:CPLParser.PairOfNumbersContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#margins.
    def visitMargins(self, ctx:CPLParser.MarginsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#boolean.
    def visitBoolean(self, ctx:CPLParser.BooleanContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#number.
    def visitNumber(self, ctx:CPLParser.NumberContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CPLParser#variable.
    def visitVariable(self, ctx:CPLParser.VariableContext):
        return self.visitChildren(ctx)



del CPLParser