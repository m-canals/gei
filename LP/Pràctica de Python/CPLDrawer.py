from PIL import Image, ImageDraw
from polygons import BoundingBox, Point


def _resizement(source, destination):
    """Retorna amb una funció que assigna punts d'un rectangle contenidor a un
    altre rectangle contenidor, mantenint la proporció d'aspece."""
    ws = source.right - source.left
    hs = source.top - source.bottom
    wd = destination.right - destination.left
    hd = destination.top - destination.bottom
    try:
        k = hd / hs if hd * ws < wd * hs else wd / ws
    except ZeroDivisionError:
        k = 1
    x_offset = (wd - ws * k) / 2
    y_offset = (hd - hs * k) / 2

    def transformation(point):
        x = destination.left + (point.x - source.left) * k + x_offset
        y = destination.top - ((point.y - source.bottom) * k + y_offset)
        return Point(x, y)
    return transformation


class CPLDrawer():
    """Dibuixant de polígons."""
    def __init__(self, width, height, margin, background_color, default_color,
                 default_fill, default_outline, default_vertex_size):
        """Inicialitza el dibuixant amb els paràmetres de la imatge."""
        self.width = width
        self.height = height
        self.background_color = background_color
        self.default_color = default_color
        self.default_fill = default_fill
        self.default_outline = default_outline
        self.default_vertex_size = default_vertex_size
        self.margin = margin

    def _fill_polygon(self, image, draw, transform, source, destination,
                      polygon, color, fill):
        """Dibuixa l'interior d'un polígon."""
        if fill[0] > 0 and fill[1] > 0:
            width = destination.right - destination.left + 1
            height = destination.top - destination.bottom + 1
            if width > 0 and height > 0:
                # Agafem una mostra per cada píxel que es veurà.
                x_samples = fill[0] * width
                y_samples = fill[1] * height
                # Distància entre mostres.
                x_offset = (source.right - source.left) / x_samples
                y_offset = (source.top - source.bottom) / y_samples
                # Si no té àrea, l'offset és zero i el bucle no acaba.
                if x_offset > 0 and y_offset > 0:
                    x = source.left + x_offset / 2
                    while x <= source.right:
                        y = source.bottom + y_offset / 2
                        while y <= source.top:
                            a = Point(x, y)
                            if polygon.contains(a):
                                b = transform(a)
                                draw.point([b.x, b.y], fill=color)
                            y += y_offset
                        x += x_offset

    def _draw_polygon(self, image, draw, transform, source, destination,
                      polygon):
        """Dibuixa un polígon."""
        color = getattr(polygon, 'color', self.default_color)
        fill = getattr(polygon, 'fill', self.default_fill)
        outline = getattr(polygon, 'outline', self.default_outline)
        w, h = getattr(polygon, 'vertex_size', self.default_vertex_size)
        points = [transform(point) for point in polygon]
        if len(points) >= 2 and outline:
            coordinates = [x for point in points for x in point]
            draw.polygon(coordinates, outline=color)
        for x, y in points:
            draw.rectangle([x - w, y - h, x + w, y + h], fill=color)
        self._fill_polygon(
            image, draw, transform, source, destination, polygon, color, fill)

    def draw(self, polygons):
        """Retorna amb un dibuix dels polígons."""
        top, right, bottom, left = self.margin
        size = (self.width + left + right, self.height + bottom + top)
        image = Image.new('RGBA', size, self.background_color)
        draw = ImageDraw.Draw(image)
        source = polygons.bounding_box()
        destination = BoundingBox(
            bottom + self.height - 1, right + self.width - 1, bottom, left)
        # Si hi ha algun polígon que no sigui buit:
        if source is not None:
            transform = _resizement(source, destination)
            for polygon in polygons:
                self._draw_polygon(
                 image, draw, transform, source, destination, polygon)
        return image
