# Generated from CPL.g by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3-")
        buf.write("\u0159\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.\t.\4")
        buf.write("/\t/\4\60\t\60\4\61\t\61\3\2\7\2d\n\2\f\2\16\2g\13\2\3")
        buf.write("\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\5\3\u0085\n\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\7\5\u008f")
        buf.write("\n\5\f\5\16\5\u0092\13\5\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3")
        buf.write("\b\5\b\u009c\n\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\6\n\u00a5")
        buf.write("\n\n\r\n\16\n\u00a6\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f")
        buf.write("\3\r\3\r\3\r\3\16\3\16\3\16\3\17\3\17\3\17\3\17\6\17\u00bb")
        buf.write("\n\17\r\17\16\17\u00bc\3\17\3\17\3\20\3\20\3\20\3\20\6")
        buf.write("\20\u00c5\n\20\r\20\16\20\u00c6\3\20\3\20\3\21\3\21\3")
        buf.write("\21\3\21\6\21\u00cf\n\21\r\21\16\21\u00d0\3\21\3\21\3")
        buf.write("\22\3\22\3\22\3\23\3\23\3\23\3\24\3\24\3\24\3\25\3\25")
        buf.write("\3\25\3\26\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\33\3\34")
        buf.write("\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\37\3\37")
        buf.write("\3\37\7\37\u0102\n\37\f\37\16\37\u0105\13\37\3 \3 \3 ")
        buf.write("\7 \u010a\n \f \16 \u010d\13 \3!\3!\3!\3!\3!\5!\u0114")
        buf.write("\n!\3\"\3\"\3\"\3#\3#\3#\3$\3$\3$\3$\3%\3%\3&\3&\7&\u0124")
        buf.write("\n&\f&\16&\u0127\13&\3&\3&\3\'\3\'\3\'\3(\3(\5(\u0130")
        buf.write("\n(\3)\3)\3)\3)\3)\5)\u0137\n)\3)\3)\3*\3*\3+\3+\3,\3")
        buf.write(",\3-\3-\3-\3-\3-\3.\3.\3.\5.\u0149\n.\3.\5.\u014c\n.\3")
        buf.write(".\5.\u014f\n.\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\61\2\2")
        buf.write("\62\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60")
        buf.write("\62\64\668:<>@BDFHJLNPRTVXZ\\^`\2\2\2\u0154\2e\3\2\2\2")
        buf.write("\4\u0084\3\2\2\2\6\u0086\3\2\2\2\b\u008a\3\2\2\2\n\u0093")
        buf.write("\3\2\2\2\f\u0095\3\2\2\2\16\u0098\3\2\2\2\20\u009d\3\2")
        buf.write("\2\2\22\u00a0\3\2\2\2\24\u00aa\3\2\2\2\26\u00ad\3\2\2")
        buf.write("\2\30\u00b0\3\2\2\2\32\u00b3\3\2\2\2\34\u00b6\3\2\2\2")
        buf.write("\36\u00c0\3\2\2\2 \u00ca\3\2\2\2\"\u00d4\3\2\2\2$\u00d7")
        buf.write("\3\2\2\2&\u00da\3\2\2\2(\u00dd\3\2\2\2*\u00e0\3\2\2\2")
        buf.write(",\u00e3\3\2\2\2.\u00e6\3\2\2\2\60\u00e9\3\2\2\2\62\u00ee")
        buf.write("\3\2\2\2\64\u00f1\3\2\2\2\66\u00f4\3\2\2\28\u00f7\3\2")
        buf.write("\2\2:\u00fc\3\2\2\2<\u00fe\3\2\2\2>\u0106\3\2\2\2@\u0113")
        buf.write("\3\2\2\2B\u0115\3\2\2\2D\u0118\3\2\2\2F\u011b\3\2\2\2")
        buf.write("H\u011f\3\2\2\2J\u0121\3\2\2\2L\u012a\3\2\2\2N\u012f\3")
        buf.write("\2\2\2P\u0131\3\2\2\2R\u013a\3\2\2\2T\u013c\3\2\2\2V\u013e")
        buf.write("\3\2\2\2X\u0140\3\2\2\2Z\u0145\3\2\2\2\\\u0152\3\2\2\2")
        buf.write("^\u0154\3\2\2\2`\u0156\3\2\2\2bd\5\4\3\2cb\3\2\2\2dg\3")
        buf.write("\2\2\2ec\3\2\2\2ef\3\2\2\2fh\3\2\2\2ge\3\2\2\2hi\7\2\2")
        buf.write("\3i\3\3\2\2\2j\u0085\5\6\4\2k\u0085\5\b\5\2l\u0085\5\n")
        buf.write("\6\2m\u0085\5\f\7\2n\u0085\5\16\b\2o\u0085\5\20\t\2p\u0085")
        buf.write("\5\22\n\2q\u0085\5\24\13\2r\u0085\5\26\f\2s\u0085\5\30")
        buf.write("\r\2t\u0085\5\32\16\2u\u0085\5\34\17\2v\u0085\5\36\20")
        buf.write("\2w\u0085\5 \21\2x\u0085\5\"\22\2y\u0085\5$\23\2z\u0085")
        buf.write("\5&\24\2{\u0085\5(\25\2|\u0085\5*\26\2}\u0085\5,\27\2")
        buf.write("~\u0085\5.\30\2\177\u0085\5\62\32\2\u0080\u0085\5\64\33")
        buf.write("\2\u0081\u0085\5\66\34\2\u0082\u0085\5\60\31\2\u0083\u0085")
        buf.write("\58\35\2\u0084j\3\2\2\2\u0084k\3\2\2\2\u0084l\3\2\2\2")
        buf.write("\u0084m\3\2\2\2\u0084n\3\2\2\2\u0084o\3\2\2\2\u0084p\3")
        buf.write("\2\2\2\u0084q\3\2\2\2\u0084r\3\2\2\2\u0084s\3\2\2\2\u0084")
        buf.write("t\3\2\2\2\u0084u\3\2\2\2\u0084v\3\2\2\2\u0084w\3\2\2\2")
        buf.write("\u0084x\3\2\2\2\u0084y\3\2\2\2\u0084z\3\2\2\2\u0084{\3")
        buf.write("\2\2\2\u0084|\3\2\2\2\u0084}\3\2\2\2\u0084~\3\2\2\2\u0084")
        buf.write("\177\3\2\2\2\u0084\u0080\3\2\2\2\u0084\u0081\3\2\2\2\u0084")
        buf.write("\u0082\3\2\2\2\u0084\u0083\3\2\2\2\u0085\5\3\2\2\2\u0086")
        buf.write("\u0087\7(\2\2\u0087\u0088\7\3\2\2\u0088\u0089\5:\36\2")
        buf.write("\u0089\7\3\2\2\2\u008a\u008b\7\4\2\2\u008b\u0090\5H%\2")
        buf.write("\u008c\u008d\7\35\2\2\u008d\u008f\5:\36\2\u008e\u008c")
        buf.write("\3\2\2\2\u008f\u0092\3\2\2\2\u0090\u008e\3\2\2\2\u0090")
        buf.write("\u0091\3\2\2\2\u0091\t\3\2\2\2\u0092\u0090\3\2\2\2\u0093")
        buf.write("\u0094\7\5\2\2\u0094\13\3\2\2\2\u0095\u0096\7\6\2\2\u0096")
        buf.write("\u0097\5^\60\2\u0097\r\3\2\2\2\u0098\u009b\7\7\2\2\u0099")
        buf.write("\u009c\5H%\2\u009a\u009c\5:\36\2\u009b\u0099\3\2\2\2\u009b")
        buf.write("\u009a\3\2\2\2\u009c\17\3\2\2\2\u009d\u009e\7\b\2\2\u009e")
        buf.write("\u009f\5N(\2\u009f\21\3\2\2\2\u00a0\u00a4\7\t\2\2\u00a1")
        buf.write("\u00a2\5`\61\2\u00a2\u00a3\7\35\2\2\u00a3\u00a5\3\2\2")
        buf.write("\2\u00a4\u00a1\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\u00a4")
        buf.write("\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8")
        buf.write("\u00a9\5N(\2\u00a9\23\3\2\2\2\u00aa\u00ab\7\n\2\2\u00ab")
        buf.write("\u00ac\5N(\2\u00ac\25\3\2\2\2\u00ad\u00ae\7\13\2\2\u00ae")
        buf.write("\u00af\5V,\2\u00af\27\3\2\2\2\u00b0\u00b1\7\f\2\2\u00b1")
        buf.write("\u00b2\5\\/\2\u00b2\31\3\2\2\2\u00b3\u00b4\7\r\2\2\u00b4")
        buf.write("\u00b5\5T+\2\u00b5\33\3\2\2\2\u00b6\u00ba\7\16\2\2\u00b7")
        buf.write("\u00b8\5`\61\2\u00b8\u00b9\7\35\2\2\u00b9\u00bb\3\2\2")
        buf.write("\2\u00ba\u00b7\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\u00ba")
        buf.write("\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\u00be\3\2\2\2\u00be")
        buf.write("\u00bf\5V,\2\u00bf\35\3\2\2\2\u00c0\u00c4\7\17\2\2\u00c1")
        buf.write("\u00c2\5`\61\2\u00c2\u00c3\7\35\2\2\u00c3\u00c5\3\2\2")
        buf.write("\2\u00c4\u00c1\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c6\u00c4")
        buf.write("\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8")
        buf.write("\u00c9\5\\/\2\u00c9\37\3\2\2\2\u00ca\u00ce\7\20\2\2\u00cb")
        buf.write("\u00cc\5`\61\2\u00cc\u00cd\7\35\2\2\u00cd\u00cf\3\2\2")
        buf.write("\2\u00ce\u00cb\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00ce")
        buf.write("\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2")
        buf.write("\u00d3\5T+\2\u00d3!\3\2\2\2\u00d4\u00d5\7\21\2\2\u00d5")
        buf.write("\u00d6\5^\60\2\u00d6#\3\2\2\2\u00d7\u00d8\7\22\2\2\u00d8")
        buf.write("\u00d9\5^\60\2\u00d9%\3\2\2\2\u00da\u00db\7\23\2\2\u00db")
        buf.write("\u00dc\5Z.\2\u00dc\'\3\2\2\2\u00dd\u00de\7\24\2\2\u00de")
        buf.write("\u00df\5:\36\2\u00df)\3\2\2\2\u00e0\u00e1\7\25\2\2\u00e1")
        buf.write("\u00e2\5:\36\2\u00e2+\3\2\2\2\u00e3\u00e4\7\26\2\2\u00e4")
        buf.write("\u00e5\5:\36\2\u00e5-\3\2\2\2\u00e6\u00e7\7\27\2\2\u00e7")
        buf.write("\u00e8\5:\36\2\u00e8/\3\2\2\2\u00e9\u00ea\7\30\2\2\u00ea")
        buf.write("\u00eb\5:\36\2\u00eb\u00ec\7\35\2\2\u00ec\u00ed\5:\36")
        buf.write("\2\u00ed\61\3\2\2\2\u00ee\u00ef\7\31\2\2\u00ef\u00f0\5")
        buf.write(":\36\2\u00f0\63\3\2\2\2\u00f1\u00f2\7\32\2\2\u00f2\u00f3")
        buf.write("\5:\36\2\u00f3\65\3\2\2\2\u00f4\u00f5\7\33\2\2\u00f5\u00f6")
        buf.write("\5:\36\2\u00f6\67\3\2\2\2\u00f7\u00f8\7\34\2\2\u00f8\u00f9")
        buf.write("\5:\36\2\u00f9\u00fa\7\35\2\2\u00fa\u00fb\5:\36\2\u00fb")
        buf.write("9\3\2\2\2\u00fc\u00fd\5<\37\2\u00fd;\3\2\2\2\u00fe\u0103")
        buf.write("\5> \2\u00ff\u0100\7 \2\2\u0100\u0102\5> \2\u0101\u00ff")
        buf.write("\3\2\2\2\u0102\u0105\3\2\2\2\u0103\u0101\3\2\2\2\u0103")
        buf.write("\u0104\3\2\2\2\u0104=\3\2\2\2\u0105\u0103\3\2\2\2\u0106")
        buf.write("\u010b\5@!\2\u0107\u0108\7!\2\2\u0108\u010a\5@!\2\u0109")
        buf.write("\u0107\3\2\2\2\u010a\u010d\3\2\2\2\u010b\u0109\3\2\2\2")
        buf.write("\u010b\u010c\3\2\2\2\u010c?\3\2\2\2\u010d\u010b\3\2\2")
        buf.write("\2\u010e\u0114\5B\"\2\u010f\u0114\5D#\2\u0110\u0114\5")
        buf.write("`\61\2\u0111\u0114\5J&\2\u0112\u0114\5F$\2\u0113\u010e")
        buf.write("\3\2\2\2\u0113\u010f\3\2\2\2\u0113\u0110\3\2\2\2\u0113")
        buf.write("\u0111\3\2\2\2\u0113\u0112\3\2\2\2\u0114A\3\2\2\2\u0115")
        buf.write("\u0116\7\36\2\2\u0116\u0117\5^\60\2\u0117C\3\2\2\2\u0118")
        buf.write("\u0119\7\37\2\2\u0119\u011a\5:\36\2\u011aE\3\2\2\2\u011b")
        buf.write("\u011c\7\"\2\2\u011c\u011d\5:\36\2\u011d\u011e\7#\2\2")
        buf.write("\u011eG\3\2\2\2\u011f\u0120\7*\2\2\u0120I\3\2\2\2\u0121")
        buf.write("\u0125\7$\2\2\u0122\u0124\5L\'\2\u0123\u0122\3\2\2\2\u0124")
        buf.write("\u0127\3\2\2\2\u0125\u0123\3\2\2\2\u0125\u0126\3\2\2\2")
        buf.write("\u0126\u0128\3\2\2\2\u0127\u0125\3\2\2\2\u0128\u0129\7")
        buf.write("%\2\2\u0129K\3\2\2\2\u012a\u012b\5^\60\2\u012b\u012c\5")
        buf.write("^\60\2\u012cM\3\2\2\2\u012d\u0130\5P)\2\u012e\u0130\5")
        buf.write("R*\2\u012f\u012d\3\2\2\2\u012f\u012e\3\2\2\2\u0130O\3")
        buf.write("\2\2\2\u0131\u0132\7&\2\2\u0132\u0133\5^\60\2\u0133\u0134")
        buf.write("\5^\60\2\u0134\u0136\5^\60\2\u0135\u0137\5^\60\2\u0136")
        buf.write("\u0135\3\2\2\2\u0136\u0137\3\2\2\2\u0137\u0138\3\2\2\2")
        buf.write("\u0138\u0139\7\'\2\2\u0139Q\3\2\2\2\u013a\u013b\7+\2\2")
        buf.write("\u013bS\3\2\2\2\u013c\u013d\5X-\2\u013dU\3\2\2\2\u013e")
        buf.write("\u013f\5X-\2\u013fW\3\2\2\2\u0140\u0141\7&\2\2\u0141\u0142")
        buf.write("\5^\60\2\u0142\u0143\5^\60\2\u0143\u0144\7\'\2\2\u0144")
        buf.write("Y\3\2\2\2\u0145\u0146\7&\2\2\u0146\u0148\5^\60\2\u0147")
        buf.write("\u0149\5^\60\2\u0148\u0147\3\2\2\2\u0148\u0149\3\2\2\2")
        buf.write("\u0149\u014b\3\2\2\2\u014a\u014c\5^\60\2\u014b\u014a\3")
        buf.write("\2\2\2\u014b\u014c\3\2\2\2\u014c\u014e\3\2\2\2\u014d\u014f")
        buf.write("\5^\60\2\u014e\u014d\3\2\2\2\u014e\u014f\3\2\2\2\u014f")
        buf.write("\u0150\3\2\2\2\u0150\u0151\7\'\2\2\u0151[\3\2\2\2\u0152")
        buf.write("\u0153\5^\60\2\u0153]\3\2\2\2\u0154\u0155\7)\2\2\u0155")
        buf.write("_\3\2\2\2\u0156\u0157\7(\2\2\u0157a\3\2\2\2\23e\u0084")
        buf.write("\u0090\u009b\u00a6\u00bc\u00c6\u00d0\u0103\u010b\u0113")
        buf.write("\u0125\u012f\u0136\u0148\u014b\u014e")
        return buf.getvalue()


class CPLParser ( Parser ):

    grammarFileName = "CPL.g"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "':='", "'draw'", "'help'", "'seed'", 
                     "'print'", "'background-color'", "'color'", "'default-color'", 
                     "'default-fill'", "'default-outline'", "'default-vertex-size'", 
                     "'fill'", "'outline'", "'vertex-size'", "'height'", 
                     "'width'", "'margin'", "'area'", "'perimeter'", "'vertices'", 
                     "'centroid'", "'inside'", "'equilateral'", "'equiangular'", 
                     "'regular'", "'equal'", "','", "'!'", "'#'", "'+'", 
                     "'*'", "'('", "')'", "'['", "']'", "'{'", "'}'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "ARGUMENT_SEPARATOR", 
                      "GENERATION_OPERATOR", "BOUNDING_OPERATOR", "UNION_OPERATOR", 
                      "INTERSECTION_OPERATOR", "LEFT_PARENTHESIS", "RIGHT_PARENTHESIS", 
                      "LEFT_BRACKET", "RIGHT_BRACKET", "LEFT_BRACE", "RIGHT_BRACE", 
                      "VARIABLE_IDENTIFIER", "NUMBER", "STRING", "HEXADECIMAL_COLOR", 
                      "WHITESPACE", "COMMENT" ]

    RULE_root = 0
    RULE_statement = 1
    RULE_assignment = 2
    RULE_drawStatement = 3
    RULE_helpStatement = 4
    RULE_seedStatement = 5
    RULE_printStatement = 6
    RULE_backgroundColorStatement = 7
    RULE_colorStatement = 8
    RULE_defaultColorStatement = 9
    RULE_defaultFillStatement = 10
    RULE_defaultOutlineStatement = 11
    RULE_defaultVertexSizeStatement = 12
    RULE_fillStatement = 13
    RULE_outlineStatement = 14
    RULE_vertexSizeStatement = 15
    RULE_heightStatement = 16
    RULE_widthStatement = 17
    RULE_marginStatement = 18
    RULE_areaStatement = 19
    RULE_perimeterStatement = 20
    RULE_verticesStatement = 21
    RULE_centroidStatement = 22
    RULE_insideStatement = 23
    RULE_equilateralStatement = 24
    RULE_equiangularStatement = 25
    RULE_regularStatement = 26
    RULE_equalStatement = 27
    RULE_expression = 28
    RULE_lowPrecedenceExpression = 29
    RULE_mediumPrecedenceExpression = 30
    RULE_highPrecedenceExpression = 31
    RULE_generation = 32
    RULE_bounding = 33
    RULE_parenthesizedExpression = 34
    RULE_string = 35
    RULE_points = 36
    RULE_point = 37
    RULE_color = 38
    RULE_rgbaColor = 39
    RULE_hexadecimalColor = 40
    RULE_size = 41
    RULE_density = 42
    RULE_pairOfNumbers = 43
    RULE_margins = 44
    RULE_boolean = 45
    RULE_number = 46
    RULE_variable = 47

    ruleNames =  [ "root", "statement", "assignment", "drawStatement", "helpStatement", 
                   "seedStatement", "printStatement", "backgroundColorStatement", 
                   "colorStatement", "defaultColorStatement", "defaultFillStatement", 
                   "defaultOutlineStatement", "defaultVertexSizeStatement", 
                   "fillStatement", "outlineStatement", "vertexSizeStatement", 
                   "heightStatement", "widthStatement", "marginStatement", 
                   "areaStatement", "perimeterStatement", "verticesStatement", 
                   "centroidStatement", "insideStatement", "equilateralStatement", 
                   "equiangularStatement", "regularStatement", "equalStatement", 
                   "expression", "lowPrecedenceExpression", "mediumPrecedenceExpression", 
                   "highPrecedenceExpression", "generation", "bounding", 
                   "parenthesizedExpression", "string", "points", "point", 
                   "color", "rgbaColor", "hexadecimalColor", "size", "density", 
                   "pairOfNumbers", "margins", "boolean", "number", "variable" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    ARGUMENT_SEPARATOR=27
    GENERATION_OPERATOR=28
    BOUNDING_OPERATOR=29
    UNION_OPERATOR=30
    INTERSECTION_OPERATOR=31
    LEFT_PARENTHESIS=32
    RIGHT_PARENTHESIS=33
    LEFT_BRACKET=34
    RIGHT_BRACKET=35
    LEFT_BRACE=36
    RIGHT_BRACE=37
    VARIABLE_IDENTIFIER=38
    NUMBER=39
    STRING=40
    HEXADECIMAL_COLOR=41
    WHITESPACE=42
    COMMENT=43

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class RootContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(CPLParser.EOF, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.StatementContext)
            else:
                return self.getTypedRuleContext(CPLParser.StatementContext,i)


        def getRuleIndex(self):
            return CPLParser.RULE_root

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRoot" ):
                return visitor.visitRoot(self)
            else:
                return visitor.visitChildren(self)




    def root(self):

        localctx = CPLParser.RootContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_root)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 99
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << CPLParser.T__1) | (1 << CPLParser.T__2) | (1 << CPLParser.T__3) | (1 << CPLParser.T__4) | (1 << CPLParser.T__5) | (1 << CPLParser.T__6) | (1 << CPLParser.T__7) | (1 << CPLParser.T__8) | (1 << CPLParser.T__9) | (1 << CPLParser.T__10) | (1 << CPLParser.T__11) | (1 << CPLParser.T__12) | (1 << CPLParser.T__13) | (1 << CPLParser.T__14) | (1 << CPLParser.T__15) | (1 << CPLParser.T__16) | (1 << CPLParser.T__17) | (1 << CPLParser.T__18) | (1 << CPLParser.T__19) | (1 << CPLParser.T__20) | (1 << CPLParser.T__21) | (1 << CPLParser.T__22) | (1 << CPLParser.T__23) | (1 << CPLParser.T__24) | (1 << CPLParser.T__25) | (1 << CPLParser.VARIABLE_IDENTIFIER))) != 0):
                self.state = 96
                self.statement()
                self.state = 101
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 102
            self.match(CPLParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def assignment(self):
            return self.getTypedRuleContext(CPLParser.AssignmentContext,0)


        def drawStatement(self):
            return self.getTypedRuleContext(CPLParser.DrawStatementContext,0)


        def helpStatement(self):
            return self.getTypedRuleContext(CPLParser.HelpStatementContext,0)


        def seedStatement(self):
            return self.getTypedRuleContext(CPLParser.SeedStatementContext,0)


        def printStatement(self):
            return self.getTypedRuleContext(CPLParser.PrintStatementContext,0)


        def backgroundColorStatement(self):
            return self.getTypedRuleContext(CPLParser.BackgroundColorStatementContext,0)


        def colorStatement(self):
            return self.getTypedRuleContext(CPLParser.ColorStatementContext,0)


        def defaultColorStatement(self):
            return self.getTypedRuleContext(CPLParser.DefaultColorStatementContext,0)


        def defaultFillStatement(self):
            return self.getTypedRuleContext(CPLParser.DefaultFillStatementContext,0)


        def defaultOutlineStatement(self):
            return self.getTypedRuleContext(CPLParser.DefaultOutlineStatementContext,0)


        def defaultVertexSizeStatement(self):
            return self.getTypedRuleContext(CPLParser.DefaultVertexSizeStatementContext,0)


        def fillStatement(self):
            return self.getTypedRuleContext(CPLParser.FillStatementContext,0)


        def outlineStatement(self):
            return self.getTypedRuleContext(CPLParser.OutlineStatementContext,0)


        def vertexSizeStatement(self):
            return self.getTypedRuleContext(CPLParser.VertexSizeStatementContext,0)


        def heightStatement(self):
            return self.getTypedRuleContext(CPLParser.HeightStatementContext,0)


        def widthStatement(self):
            return self.getTypedRuleContext(CPLParser.WidthStatementContext,0)


        def marginStatement(self):
            return self.getTypedRuleContext(CPLParser.MarginStatementContext,0)


        def areaStatement(self):
            return self.getTypedRuleContext(CPLParser.AreaStatementContext,0)


        def perimeterStatement(self):
            return self.getTypedRuleContext(CPLParser.PerimeterStatementContext,0)


        def verticesStatement(self):
            return self.getTypedRuleContext(CPLParser.VerticesStatementContext,0)


        def centroidStatement(self):
            return self.getTypedRuleContext(CPLParser.CentroidStatementContext,0)


        def equilateralStatement(self):
            return self.getTypedRuleContext(CPLParser.EquilateralStatementContext,0)


        def equiangularStatement(self):
            return self.getTypedRuleContext(CPLParser.EquiangularStatementContext,0)


        def regularStatement(self):
            return self.getTypedRuleContext(CPLParser.RegularStatementContext,0)


        def insideStatement(self):
            return self.getTypedRuleContext(CPLParser.InsideStatementContext,0)


        def equalStatement(self):
            return self.getTypedRuleContext(CPLParser.EqualStatementContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_statement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatement" ):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = CPLParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 130
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CPLParser.VARIABLE_IDENTIFIER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 104
                self.assignment()
                pass
            elif token in [CPLParser.T__1]:
                self.enterOuterAlt(localctx, 2)
                self.state = 105
                self.drawStatement()
                pass
            elif token in [CPLParser.T__2]:
                self.enterOuterAlt(localctx, 3)
                self.state = 106
                self.helpStatement()
                pass
            elif token in [CPLParser.T__3]:
                self.enterOuterAlt(localctx, 4)
                self.state = 107
                self.seedStatement()
                pass
            elif token in [CPLParser.T__4]:
                self.enterOuterAlt(localctx, 5)
                self.state = 108
                self.printStatement()
                pass
            elif token in [CPLParser.T__5]:
                self.enterOuterAlt(localctx, 6)
                self.state = 109
                self.backgroundColorStatement()
                pass
            elif token in [CPLParser.T__6]:
                self.enterOuterAlt(localctx, 7)
                self.state = 110
                self.colorStatement()
                pass
            elif token in [CPLParser.T__7]:
                self.enterOuterAlt(localctx, 8)
                self.state = 111
                self.defaultColorStatement()
                pass
            elif token in [CPLParser.T__8]:
                self.enterOuterAlt(localctx, 9)
                self.state = 112
                self.defaultFillStatement()
                pass
            elif token in [CPLParser.T__9]:
                self.enterOuterAlt(localctx, 10)
                self.state = 113
                self.defaultOutlineStatement()
                pass
            elif token in [CPLParser.T__10]:
                self.enterOuterAlt(localctx, 11)
                self.state = 114
                self.defaultVertexSizeStatement()
                pass
            elif token in [CPLParser.T__11]:
                self.enterOuterAlt(localctx, 12)
                self.state = 115
                self.fillStatement()
                pass
            elif token in [CPLParser.T__12]:
                self.enterOuterAlt(localctx, 13)
                self.state = 116
                self.outlineStatement()
                pass
            elif token in [CPLParser.T__13]:
                self.enterOuterAlt(localctx, 14)
                self.state = 117
                self.vertexSizeStatement()
                pass
            elif token in [CPLParser.T__14]:
                self.enterOuterAlt(localctx, 15)
                self.state = 118
                self.heightStatement()
                pass
            elif token in [CPLParser.T__15]:
                self.enterOuterAlt(localctx, 16)
                self.state = 119
                self.widthStatement()
                pass
            elif token in [CPLParser.T__16]:
                self.enterOuterAlt(localctx, 17)
                self.state = 120
                self.marginStatement()
                pass
            elif token in [CPLParser.T__17]:
                self.enterOuterAlt(localctx, 18)
                self.state = 121
                self.areaStatement()
                pass
            elif token in [CPLParser.T__18]:
                self.enterOuterAlt(localctx, 19)
                self.state = 122
                self.perimeterStatement()
                pass
            elif token in [CPLParser.T__19]:
                self.enterOuterAlt(localctx, 20)
                self.state = 123
                self.verticesStatement()
                pass
            elif token in [CPLParser.T__20]:
                self.enterOuterAlt(localctx, 21)
                self.state = 124
                self.centroidStatement()
                pass
            elif token in [CPLParser.T__22]:
                self.enterOuterAlt(localctx, 22)
                self.state = 125
                self.equilateralStatement()
                pass
            elif token in [CPLParser.T__23]:
                self.enterOuterAlt(localctx, 23)
                self.state = 126
                self.equiangularStatement()
                pass
            elif token in [CPLParser.T__24]:
                self.enterOuterAlt(localctx, 24)
                self.state = 127
                self.regularStatement()
                pass
            elif token in [CPLParser.T__21]:
                self.enterOuterAlt(localctx, 25)
                self.state = 128
                self.insideStatement()
                pass
            elif token in [CPLParser.T__25]:
                self.enterOuterAlt(localctx, 26)
                self.state = 129
                self.equalStatement()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AssignmentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARIABLE_IDENTIFIER(self):
            return self.getToken(CPLParser.VARIABLE_IDENTIFIER, 0)

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_assignment

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssignment" ):
                return visitor.visitAssignment(self)
            else:
                return visitor.visitChildren(self)




    def assignment(self):

        localctx = CPLParser.AssignmentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_assignment)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 132
            self.match(CPLParser.VARIABLE_IDENTIFIER)
            self.state = 133
            self.match(CPLParser.T__0)
            self.state = 134
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DrawStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def string(self):
            return self.getTypedRuleContext(CPLParser.StringContext,0)


        def ARGUMENT_SEPARATOR(self, i:int=None):
            if i is None:
                return self.getTokens(CPLParser.ARGUMENT_SEPARATOR)
            else:
                return self.getToken(CPLParser.ARGUMENT_SEPARATOR, i)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(CPLParser.ExpressionContext,i)


        def getRuleIndex(self):
            return CPLParser.RULE_drawStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDrawStatement" ):
                return visitor.visitDrawStatement(self)
            else:
                return visitor.visitChildren(self)




    def drawStatement(self):

        localctx = CPLParser.DrawStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_drawStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 136
            self.match(CPLParser.T__1)
            self.state = 137
            self.string()
            self.state = 142
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CPLParser.ARGUMENT_SEPARATOR:
                self.state = 138
                self.match(CPLParser.ARGUMENT_SEPARATOR)
                self.state = 139
                self.expression()
                self.state = 144
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class HelpStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return CPLParser.RULE_helpStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitHelpStatement" ):
                return visitor.visitHelpStatement(self)
            else:
                return visitor.visitChildren(self)




    def helpStatement(self):

        localctx = CPLParser.HelpStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_helpStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 145
            self.match(CPLParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SeedStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def number(self):
            return self.getTypedRuleContext(CPLParser.NumberContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_seedStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSeedStatement" ):
                return visitor.visitSeedStatement(self)
            else:
                return visitor.visitChildren(self)




    def seedStatement(self):

        localctx = CPLParser.SeedStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_seedStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 147
            self.match(CPLParser.T__3)
            self.state = 148
            self.number()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PrintStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def string(self):
            return self.getTypedRuleContext(CPLParser.StringContext,0)


        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_printStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrintStatement" ):
                return visitor.visitPrintStatement(self)
            else:
                return visitor.visitChildren(self)




    def printStatement(self):

        localctx = CPLParser.PrintStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_printStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 150
            self.match(CPLParser.T__4)
            self.state = 153
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CPLParser.STRING]:
                self.state = 151
                self.string()
                pass
            elif token in [CPLParser.GENERATION_OPERATOR, CPLParser.BOUNDING_OPERATOR, CPLParser.LEFT_PARENTHESIS, CPLParser.LEFT_BRACKET, CPLParser.VARIABLE_IDENTIFIER]:
                self.state = 152
                self.expression()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BackgroundColorStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def color(self):
            return self.getTypedRuleContext(CPLParser.ColorContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_backgroundColorStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBackgroundColorStatement" ):
                return visitor.visitBackgroundColorStatement(self)
            else:
                return visitor.visitChildren(self)




    def backgroundColorStatement(self):

        localctx = CPLParser.BackgroundColorStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_backgroundColorStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 155
            self.match(CPLParser.T__5)
            self.state = 156
            self.color()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ColorStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def color(self):
            return self.getTypedRuleContext(CPLParser.ColorContext,0)


        def variable(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.VariableContext)
            else:
                return self.getTypedRuleContext(CPLParser.VariableContext,i)


        def ARGUMENT_SEPARATOR(self, i:int=None):
            if i is None:
                return self.getTokens(CPLParser.ARGUMENT_SEPARATOR)
            else:
                return self.getToken(CPLParser.ARGUMENT_SEPARATOR, i)

        def getRuleIndex(self):
            return CPLParser.RULE_colorStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitColorStatement" ):
                return visitor.visitColorStatement(self)
            else:
                return visitor.visitChildren(self)




    def colorStatement(self):

        localctx = CPLParser.ColorStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_colorStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 158
            self.match(CPLParser.T__6)
            self.state = 162 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 159
                self.variable()
                self.state = 160
                self.match(CPLParser.ARGUMENT_SEPARATOR)
                self.state = 164 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==CPLParser.VARIABLE_IDENTIFIER):
                    break

            self.state = 166
            self.color()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DefaultColorStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def color(self):
            return self.getTypedRuleContext(CPLParser.ColorContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_defaultColorStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDefaultColorStatement" ):
                return visitor.visitDefaultColorStatement(self)
            else:
                return visitor.visitChildren(self)




    def defaultColorStatement(self):

        localctx = CPLParser.DefaultColorStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_defaultColorStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 168
            self.match(CPLParser.T__7)
            self.state = 169
            self.color()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DefaultFillStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def density(self):
            return self.getTypedRuleContext(CPLParser.DensityContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_defaultFillStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDefaultFillStatement" ):
                return visitor.visitDefaultFillStatement(self)
            else:
                return visitor.visitChildren(self)




    def defaultFillStatement(self):

        localctx = CPLParser.DefaultFillStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_defaultFillStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 171
            self.match(CPLParser.T__8)
            self.state = 172
            self.density()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DefaultOutlineStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def boolean(self):
            return self.getTypedRuleContext(CPLParser.BooleanContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_defaultOutlineStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDefaultOutlineStatement" ):
                return visitor.visitDefaultOutlineStatement(self)
            else:
                return visitor.visitChildren(self)




    def defaultOutlineStatement(self):

        localctx = CPLParser.DefaultOutlineStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_defaultOutlineStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 174
            self.match(CPLParser.T__9)
            self.state = 175
            self.boolean()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DefaultVertexSizeStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def size(self):
            return self.getTypedRuleContext(CPLParser.SizeContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_defaultVertexSizeStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDefaultVertexSizeStatement" ):
                return visitor.visitDefaultVertexSizeStatement(self)
            else:
                return visitor.visitChildren(self)




    def defaultVertexSizeStatement(self):

        localctx = CPLParser.DefaultVertexSizeStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_defaultVertexSizeStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 177
            self.match(CPLParser.T__10)
            self.state = 178
            self.size()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FillStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def density(self):
            return self.getTypedRuleContext(CPLParser.DensityContext,0)


        def variable(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.VariableContext)
            else:
                return self.getTypedRuleContext(CPLParser.VariableContext,i)


        def ARGUMENT_SEPARATOR(self, i:int=None):
            if i is None:
                return self.getTokens(CPLParser.ARGUMENT_SEPARATOR)
            else:
                return self.getToken(CPLParser.ARGUMENT_SEPARATOR, i)

        def getRuleIndex(self):
            return CPLParser.RULE_fillStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFillStatement" ):
                return visitor.visitFillStatement(self)
            else:
                return visitor.visitChildren(self)




    def fillStatement(self):

        localctx = CPLParser.FillStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_fillStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 180
            self.match(CPLParser.T__11)
            self.state = 184 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 181
                self.variable()
                self.state = 182
                self.match(CPLParser.ARGUMENT_SEPARATOR)
                self.state = 186 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==CPLParser.VARIABLE_IDENTIFIER):
                    break

            self.state = 188
            self.density()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class OutlineStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def boolean(self):
            return self.getTypedRuleContext(CPLParser.BooleanContext,0)


        def variable(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.VariableContext)
            else:
                return self.getTypedRuleContext(CPLParser.VariableContext,i)


        def ARGUMENT_SEPARATOR(self, i:int=None):
            if i is None:
                return self.getTokens(CPLParser.ARGUMENT_SEPARATOR)
            else:
                return self.getToken(CPLParser.ARGUMENT_SEPARATOR, i)

        def getRuleIndex(self):
            return CPLParser.RULE_outlineStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOutlineStatement" ):
                return visitor.visitOutlineStatement(self)
            else:
                return visitor.visitChildren(self)




    def outlineStatement(self):

        localctx = CPLParser.OutlineStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_outlineStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 190
            self.match(CPLParser.T__12)
            self.state = 194 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 191
                self.variable()
                self.state = 192
                self.match(CPLParser.ARGUMENT_SEPARATOR)
                self.state = 196 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==CPLParser.VARIABLE_IDENTIFIER):
                    break

            self.state = 198
            self.boolean()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VertexSizeStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def size(self):
            return self.getTypedRuleContext(CPLParser.SizeContext,0)


        def variable(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.VariableContext)
            else:
                return self.getTypedRuleContext(CPLParser.VariableContext,i)


        def ARGUMENT_SEPARATOR(self, i:int=None):
            if i is None:
                return self.getTokens(CPLParser.ARGUMENT_SEPARATOR)
            else:
                return self.getToken(CPLParser.ARGUMENT_SEPARATOR, i)

        def getRuleIndex(self):
            return CPLParser.RULE_vertexSizeStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVertexSizeStatement" ):
                return visitor.visitVertexSizeStatement(self)
            else:
                return visitor.visitChildren(self)




    def vertexSizeStatement(self):

        localctx = CPLParser.VertexSizeStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_vertexSizeStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 200
            self.match(CPLParser.T__13)
            self.state = 204 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 201
                self.variable()
                self.state = 202
                self.match(CPLParser.ARGUMENT_SEPARATOR)
                self.state = 206 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==CPLParser.VARIABLE_IDENTIFIER):
                    break

            self.state = 208
            self.size()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class HeightStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def number(self):
            return self.getTypedRuleContext(CPLParser.NumberContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_heightStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitHeightStatement" ):
                return visitor.visitHeightStatement(self)
            else:
                return visitor.visitChildren(self)




    def heightStatement(self):

        localctx = CPLParser.HeightStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_heightStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 210
            self.match(CPLParser.T__14)
            self.state = 211
            self.number()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class WidthStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def number(self):
            return self.getTypedRuleContext(CPLParser.NumberContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_widthStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWidthStatement" ):
                return visitor.visitWidthStatement(self)
            else:
                return visitor.visitChildren(self)




    def widthStatement(self):

        localctx = CPLParser.WidthStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_widthStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 213
            self.match(CPLParser.T__15)
            self.state = 214
            self.number()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class MarginStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def margins(self):
            return self.getTypedRuleContext(CPLParser.MarginsContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_marginStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMarginStatement" ):
                return visitor.visitMarginStatement(self)
            else:
                return visitor.visitChildren(self)




    def marginStatement(self):

        localctx = CPLParser.MarginStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_marginStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 216
            self.match(CPLParser.T__16)
            self.state = 217
            self.margins()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AreaStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_areaStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAreaStatement" ):
                return visitor.visitAreaStatement(self)
            else:
                return visitor.visitChildren(self)




    def areaStatement(self):

        localctx = CPLParser.AreaStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_areaStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 219
            self.match(CPLParser.T__17)
            self.state = 220
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PerimeterStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_perimeterStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPerimeterStatement" ):
                return visitor.visitPerimeterStatement(self)
            else:
                return visitor.visitChildren(self)




    def perimeterStatement(self):

        localctx = CPLParser.PerimeterStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_perimeterStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 222
            self.match(CPLParser.T__18)
            self.state = 223
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VerticesStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_verticesStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVerticesStatement" ):
                return visitor.visitVerticesStatement(self)
            else:
                return visitor.visitChildren(self)




    def verticesStatement(self):

        localctx = CPLParser.VerticesStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_verticesStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 225
            self.match(CPLParser.T__19)
            self.state = 226
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class CentroidStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_centroidStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCentroidStatement" ):
                return visitor.visitCentroidStatement(self)
            else:
                return visitor.visitChildren(self)




    def centroidStatement(self):

        localctx = CPLParser.CentroidStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_centroidStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 228
            self.match(CPLParser.T__20)
            self.state = 229
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class InsideStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(CPLParser.ExpressionContext,i)


        def ARGUMENT_SEPARATOR(self):
            return self.getToken(CPLParser.ARGUMENT_SEPARATOR, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_insideStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInsideStatement" ):
                return visitor.visitInsideStatement(self)
            else:
                return visitor.visitChildren(self)




    def insideStatement(self):

        localctx = CPLParser.InsideStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_insideStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 231
            self.match(CPLParser.T__21)
            self.state = 232
            self.expression()
            self.state = 233
            self.match(CPLParser.ARGUMENT_SEPARATOR)
            self.state = 234
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class EquilateralStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_equilateralStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEquilateralStatement" ):
                return visitor.visitEquilateralStatement(self)
            else:
                return visitor.visitChildren(self)




    def equilateralStatement(self):

        localctx = CPLParser.EquilateralStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_equilateralStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 236
            self.match(CPLParser.T__22)
            self.state = 237
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class EquiangularStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_equiangularStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEquiangularStatement" ):
                return visitor.visitEquiangularStatement(self)
            else:
                return visitor.visitChildren(self)




    def equiangularStatement(self):

        localctx = CPLParser.EquiangularStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_equiangularStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 239
            self.match(CPLParser.T__23)
            self.state = 240
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RegularStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_regularStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRegularStatement" ):
                return visitor.visitRegularStatement(self)
            else:
                return visitor.visitChildren(self)




    def regularStatement(self):

        localctx = CPLParser.RegularStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_regularStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 242
            self.match(CPLParser.T__24)
            self.state = 243
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class EqualStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(CPLParser.ExpressionContext,i)


        def ARGUMENT_SEPARATOR(self):
            return self.getToken(CPLParser.ARGUMENT_SEPARATOR, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_equalStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEqualStatement" ):
                return visitor.visitEqualStatement(self)
            else:
                return visitor.visitChildren(self)




    def equalStatement(self):

        localctx = CPLParser.EqualStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_equalStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 245
            self.match(CPLParser.T__25)
            self.state = 246
            self.expression()
            self.state = 247
            self.match(CPLParser.ARGUMENT_SEPARATOR)
            self.state = 248
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def lowPrecedenceExpression(self):
            return self.getTypedRuleContext(CPLParser.LowPrecedenceExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_expression

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression" ):
                return visitor.visitExpression(self)
            else:
                return visitor.visitChildren(self)




    def expression(self):

        localctx = CPLParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_expression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 250
            self.lowPrecedenceExpression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LowPrecedenceExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def mediumPrecedenceExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.MediumPrecedenceExpressionContext)
            else:
                return self.getTypedRuleContext(CPLParser.MediumPrecedenceExpressionContext,i)


        def UNION_OPERATOR(self, i:int=None):
            if i is None:
                return self.getTokens(CPLParser.UNION_OPERATOR)
            else:
                return self.getToken(CPLParser.UNION_OPERATOR, i)

        def getRuleIndex(self):
            return CPLParser.RULE_lowPrecedenceExpression

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLowPrecedenceExpression" ):
                return visitor.visitLowPrecedenceExpression(self)
            else:
                return visitor.visitChildren(self)




    def lowPrecedenceExpression(self):

        localctx = CPLParser.LowPrecedenceExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_lowPrecedenceExpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 252
            self.mediumPrecedenceExpression()
            self.state = 257
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,8,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 253
                    self.match(CPLParser.UNION_OPERATOR)
                    self.state = 254
                    self.mediumPrecedenceExpression() 
                self.state = 259
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class MediumPrecedenceExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def highPrecedenceExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.HighPrecedenceExpressionContext)
            else:
                return self.getTypedRuleContext(CPLParser.HighPrecedenceExpressionContext,i)


        def INTERSECTION_OPERATOR(self, i:int=None):
            if i is None:
                return self.getTokens(CPLParser.INTERSECTION_OPERATOR)
            else:
                return self.getToken(CPLParser.INTERSECTION_OPERATOR, i)

        def getRuleIndex(self):
            return CPLParser.RULE_mediumPrecedenceExpression

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMediumPrecedenceExpression" ):
                return visitor.visitMediumPrecedenceExpression(self)
            else:
                return visitor.visitChildren(self)




    def mediumPrecedenceExpression(self):

        localctx = CPLParser.MediumPrecedenceExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_mediumPrecedenceExpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 260
            self.highPrecedenceExpression()
            self.state = 265
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,9,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 261
                    self.match(CPLParser.INTERSECTION_OPERATOR)
                    self.state = 262
                    self.highPrecedenceExpression() 
                self.state = 267
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,9,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class HighPrecedenceExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def generation(self):
            return self.getTypedRuleContext(CPLParser.GenerationContext,0)


        def bounding(self):
            return self.getTypedRuleContext(CPLParser.BoundingContext,0)


        def variable(self):
            return self.getTypedRuleContext(CPLParser.VariableContext,0)


        def points(self):
            return self.getTypedRuleContext(CPLParser.PointsContext,0)


        def parenthesizedExpression(self):
            return self.getTypedRuleContext(CPLParser.ParenthesizedExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_highPrecedenceExpression

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitHighPrecedenceExpression" ):
                return visitor.visitHighPrecedenceExpression(self)
            else:
                return visitor.visitChildren(self)




    def highPrecedenceExpression(self):

        localctx = CPLParser.HighPrecedenceExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_highPrecedenceExpression)
        try:
            self.state = 273
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CPLParser.GENERATION_OPERATOR]:
                self.enterOuterAlt(localctx, 1)
                self.state = 268
                self.generation()
                pass
            elif token in [CPLParser.BOUNDING_OPERATOR]:
                self.enterOuterAlt(localctx, 2)
                self.state = 269
                self.bounding()
                pass
            elif token in [CPLParser.VARIABLE_IDENTIFIER]:
                self.enterOuterAlt(localctx, 3)
                self.state = 270
                self.variable()
                pass
            elif token in [CPLParser.LEFT_BRACKET]:
                self.enterOuterAlt(localctx, 4)
                self.state = 271
                self.points()
                pass
            elif token in [CPLParser.LEFT_PARENTHESIS]:
                self.enterOuterAlt(localctx, 5)
                self.state = 272
                self.parenthesizedExpression()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class GenerationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def GENERATION_OPERATOR(self):
            return self.getToken(CPLParser.GENERATION_OPERATOR, 0)

        def number(self):
            return self.getTypedRuleContext(CPLParser.NumberContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_generation

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGeneration" ):
                return visitor.visitGeneration(self)
            else:
                return visitor.visitChildren(self)




    def generation(self):

        localctx = CPLParser.GenerationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_generation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 275
            self.match(CPLParser.GENERATION_OPERATOR)
            self.state = 276
            self.number()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BoundingContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BOUNDING_OPERATOR(self):
            return self.getToken(CPLParser.BOUNDING_OPERATOR, 0)

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_bounding

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBounding" ):
                return visitor.visitBounding(self)
            else:
                return visitor.visitChildren(self)




    def bounding(self):

        localctx = CPLParser.BoundingContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_bounding)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 278
            self.match(CPLParser.BOUNDING_OPERATOR)
            self.state = 279
            self.expression()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ParenthesizedExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_PARENTHESIS(self):
            return self.getToken(CPLParser.LEFT_PARENTHESIS, 0)

        def expression(self):
            return self.getTypedRuleContext(CPLParser.ExpressionContext,0)


        def RIGHT_PARENTHESIS(self):
            return self.getToken(CPLParser.RIGHT_PARENTHESIS, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_parenthesizedExpression

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParenthesizedExpression" ):
                return visitor.visitParenthesizedExpression(self)
            else:
                return visitor.visitChildren(self)




    def parenthesizedExpression(self):

        localctx = CPLParser.ParenthesizedExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_parenthesizedExpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 281
            self.match(CPLParser.LEFT_PARENTHESIS)
            self.state = 282
            self.expression()
            self.state = 283
            self.match(CPLParser.RIGHT_PARENTHESIS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING(self):
            return self.getToken(CPLParser.STRING, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_string

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString" ):
                return visitor.visitString(self)
            else:
                return visitor.visitChildren(self)




    def string(self):

        localctx = CPLParser.StringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_string)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 285
            self.match(CPLParser.STRING)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PointsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_BRACKET(self):
            return self.getToken(CPLParser.LEFT_BRACKET, 0)

        def RIGHT_BRACKET(self):
            return self.getToken(CPLParser.RIGHT_BRACKET, 0)

        def point(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.PointContext)
            else:
                return self.getTypedRuleContext(CPLParser.PointContext,i)


        def getRuleIndex(self):
            return CPLParser.RULE_points

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPoints" ):
                return visitor.visitPoints(self)
            else:
                return visitor.visitChildren(self)




    def points(self):

        localctx = CPLParser.PointsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 72, self.RULE_points)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 287
            self.match(CPLParser.LEFT_BRACKET)
            self.state = 291
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CPLParser.NUMBER:
                self.state = 288
                self.point()
                self.state = 293
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 294
            self.match(CPLParser.RIGHT_BRACKET)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PointContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def number(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.NumberContext)
            else:
                return self.getTypedRuleContext(CPLParser.NumberContext,i)


        def getRuleIndex(self):
            return CPLParser.RULE_point

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPoint" ):
                return visitor.visitPoint(self)
            else:
                return visitor.visitChildren(self)




    def point(self):

        localctx = CPLParser.PointContext(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_point)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 296
            self.number()
            self.state = 297
            self.number()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ColorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def rgbaColor(self):
            return self.getTypedRuleContext(CPLParser.RgbaColorContext,0)


        def hexadecimalColor(self):
            return self.getTypedRuleContext(CPLParser.HexadecimalColorContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_color

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitColor" ):
                return visitor.visitColor(self)
            else:
                return visitor.visitChildren(self)




    def color(self):

        localctx = CPLParser.ColorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_color)
        try:
            self.state = 301
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CPLParser.LEFT_BRACE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 299
                self.rgbaColor()
                pass
            elif token in [CPLParser.HEXADECIMAL_COLOR]:
                self.enterOuterAlt(localctx, 2)
                self.state = 300
                self.hexadecimalColor()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RgbaColorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_BRACE(self):
            return self.getToken(CPLParser.LEFT_BRACE, 0)

        def number(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.NumberContext)
            else:
                return self.getTypedRuleContext(CPLParser.NumberContext,i)


        def RIGHT_BRACE(self):
            return self.getToken(CPLParser.RIGHT_BRACE, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_rgbaColor

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRgbaColor" ):
                return visitor.visitRgbaColor(self)
            else:
                return visitor.visitChildren(self)




    def rgbaColor(self):

        localctx = CPLParser.RgbaColorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_rgbaColor)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 303
            self.match(CPLParser.LEFT_BRACE)
            self.state = 304
            self.number()
            self.state = 305
            self.number()
            self.state = 306
            self.number()
            self.state = 308
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CPLParser.NUMBER:
                self.state = 307
                self.number()


            self.state = 310
            self.match(CPLParser.RIGHT_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class HexadecimalColorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def HEXADECIMAL_COLOR(self):
            return self.getToken(CPLParser.HEXADECIMAL_COLOR, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_hexadecimalColor

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitHexadecimalColor" ):
                return visitor.visitHexadecimalColor(self)
            else:
                return visitor.visitChildren(self)




    def hexadecimalColor(self):

        localctx = CPLParser.HexadecimalColorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_hexadecimalColor)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 312
            self.match(CPLParser.HEXADECIMAL_COLOR)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SizeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def pairOfNumbers(self):
            return self.getTypedRuleContext(CPLParser.PairOfNumbersContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_size

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSize" ):
                return visitor.visitSize(self)
            else:
                return visitor.visitChildren(self)




    def size(self):

        localctx = CPLParser.SizeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 82, self.RULE_size)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 314
            self.pairOfNumbers()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DensityContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def pairOfNumbers(self):
            return self.getTypedRuleContext(CPLParser.PairOfNumbersContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_density

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDensity" ):
                return visitor.visitDensity(self)
            else:
                return visitor.visitChildren(self)




    def density(self):

        localctx = CPLParser.DensityContext(self, self._ctx, self.state)
        self.enterRule(localctx, 84, self.RULE_density)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 316
            self.pairOfNumbers()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PairOfNumbersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_BRACE(self):
            return self.getToken(CPLParser.LEFT_BRACE, 0)

        def number(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.NumberContext)
            else:
                return self.getTypedRuleContext(CPLParser.NumberContext,i)


        def RIGHT_BRACE(self):
            return self.getToken(CPLParser.RIGHT_BRACE, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_pairOfNumbers

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPairOfNumbers" ):
                return visitor.visitPairOfNumbers(self)
            else:
                return visitor.visitChildren(self)




    def pairOfNumbers(self):

        localctx = CPLParser.PairOfNumbersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 86, self.RULE_pairOfNumbers)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 318
            self.match(CPLParser.LEFT_BRACE)
            self.state = 319
            self.number()
            self.state = 320
            self.number()
            self.state = 321
            self.match(CPLParser.RIGHT_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class MarginsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LEFT_BRACE(self):
            return self.getToken(CPLParser.LEFT_BRACE, 0)

        def number(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CPLParser.NumberContext)
            else:
                return self.getTypedRuleContext(CPLParser.NumberContext,i)


        def RIGHT_BRACE(self):
            return self.getToken(CPLParser.RIGHT_BRACE, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_margins

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMargins" ):
                return visitor.visitMargins(self)
            else:
                return visitor.visitChildren(self)




    def margins(self):

        localctx = CPLParser.MarginsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 88, self.RULE_margins)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 323
            self.match(CPLParser.LEFT_BRACE)
            self.state = 324
            self.number()
            self.state = 326
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
            if la_ == 1:
                self.state = 325
                self.number()


            self.state = 329
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
            if la_ == 1:
                self.state = 328
                self.number()


            self.state = 332
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==CPLParser.NUMBER:
                self.state = 331
                self.number()


            self.state = 334
            self.match(CPLParser.RIGHT_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BooleanContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def number(self):
            return self.getTypedRuleContext(CPLParser.NumberContext,0)


        def getRuleIndex(self):
            return CPLParser.RULE_boolean

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBoolean" ):
                return visitor.visitBoolean(self)
            else:
                return visitor.visitChildren(self)




    def boolean(self):

        localctx = CPLParser.BooleanContext(self, self._ctx, self.state)
        self.enterRule(localctx, 90, self.RULE_boolean)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 336
            self.number()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class NumberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(CPLParser.NUMBER, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_number

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumber" ):
                return visitor.visitNumber(self)
            else:
                return visitor.visitChildren(self)




    def number(self):

        localctx = CPLParser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 92, self.RULE_number)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 338
            self.match(CPLParser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VariableContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARIABLE_IDENTIFIER(self):
            return self.getToken(CPLParser.VARIABLE_IDENTIFIER, 0)

        def getRuleIndex(self):
            return CPLParser.RULE_variable

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVariable" ):
                return visitor.visitVariable(self)
            else:
                return visitor.visitChildren(self)




    def variable(self):

        localctx = CPLParser.VariableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 94, self.RULE_variable)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 340
            self.match(CPLParser.VARIABLE_IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





