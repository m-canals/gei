PolyBot
===============================================================================

Part 1: classe per a polígons convexos
-------------------------------------------------------------------------------

El fitxer `polygons.py` conté diferents classes:

 * La classe `Point` representa un punt, la classe `Edge` representa una aresta
   (dos vèrtexs consecutius) i la classe `Angle` representa un angle (tres
   vèrtexs consecutius).

 * La classe `BoundingBox` representa una caixa contenidora. Les classes
   `PointCollection` i `PointCollections` representen col·leccions de punts i
   col·leccions de col·leccions de punts. Permeten obtenir la caixa contenidora
   dels punts de la col·lecció o de les col·leccions, respectivament.

 * La classe `PointSet` representa un conjunt de punts. Permet crear un polígon
   simple que prengui els punts del conjunt com a vèrtexs. És una subclasse de
   les classes `set` i `PointCollection`.

 * La classe `Polygon` representa un polígon. És una sublcasse de les classes
   `list` i `PointCollection`. Permet crear un polígon convex que contingui els
   vèrtexs del polígon.
 
 * La classe `ConvexPolygon` representa un polígon convex. És una subclasse de
   la classe `Polygon`.

Les operacions més importants són:

 * Construcció d'un polígon convex: amb el mètode `simple_polygon` de la classe
   `PointSet` s'obté un polígon. El cost és quasi-lineal respecte el nombre de
   punts. Es calcula pendents en comptes d'angles i distàncies al quadrat en
   comptes de distàncies.
   
   Amb el mètode `convex_hull` de la classe `Polygon` s'obté un polígon convex.
   El cost és lineal respecte el nombre de vèrtexs. S'utilitza l'algorisme de
   Graham Scan.

 * Comprovació d'inclusió: es comprova amb el mètode `contains` de la classe
   `ConvexPolygon`, amb un polígon convex o un punt d'argument; per a fer-ho es
   determina si tots els vèrtexs del polígon o el punt són dins, és a dir, si
   són al mateix costat per a totes les arestes del polígon contenidor. El cost
   és lineal respecte el producte del nombre de vèrtexs dels polígons.

 * Obtenció del nombre de vèrtex i arestes: s'obté amb el mètode `size` de la
   classe `Polygon`, que hereta la classe `ConvexPolygon`. El cost és consant.

 * Obtenció del perímetre: s'obté amb el mètodde `perimeter` de la classe
   `Polygon`, que hereta la classe `ConvexPolygon`. El cost és lineal respecte
   el nombre de vèrtexs.

 * Obtenció de l'àrea: s'obté amb el mètode `area` de la classe `Polygon`, que
   hereta la classe `ConvexPolygon`. El cost és lineal respecte el nombre de
   vèrtexs.

 * Obtenció del centroide: s'obté amb el mètode `centroid` de la classe
   `Polygon`, que hereta la classe `ConvexPolygon`. El cost és lineal respecte
   el nombre de vèrtexs.

 * Comprovació de regularitat: es comprova amb el mètode `regular` de la classe
 `Polygon`. El cost és lineal respecte el nombre de vèrtexs.

 * Intersecció de polígons convexos: es calcula amb l'operador `&` de la classe
   `ConvexPolygon`. El cost és lineal respecte el producte del nombre de
   vèrtexs dels polígons. S'utilitza l'algorisme de Sutherland–Hodgman.
 
 * Unió de polígons convexos: es calcula amb l'operador `|` de la classe
   `ConvexPolygon`. El cost és quasi-lineal respecte la suma del nombre de
   vèrtexs dels polígons.
  
 * Càlcul de la caixa contenidors: es calcula amb el mètode `boundig_box` de la
   classe `PointCollection`, que hereta la classe `ConvexPolygon`.
   
 * Dibuix de polígons en una imatge: els polígons es dibuixen a la classe
   `CPLDrawer` en lloc d'un mètode de la classe `Polygon`, per a no crear una
   dependència a la llibreria amb què es dibuixen.

Algunes dels aspectes considerats a l'hora d'implementar la solució són:

 * Hi ha més d'una representació possible per als polígons. En una un polígon
   es caracteritza pels seus vèrtexs, mentre que en l'altra es caracteritza per
   les seves arestes: s'ha optat per la primera. També cal establir un sentit
   per a les arestes, horari o anti-horari i a adaptar els algorismes en
   conseqüència: s'utilitza el sentit anti-horari. També cal tenir en compte
   casos especials com que es tracta igual el 0.0 que el -0.0 (que pot resultar
   d'alguna operació) i la pèrdua de precisió dels nombres en coma flotant
   (potser cal establir un llindar de admissibilitat per a l'error).

 * Tot i que el programa potser podria ser més eficient si s'utilitzés tuples
   per a representar punts, s'utilitza la classe `Point` per motius didàctics.
   De fet, la millor opció seria implementar en Python una interfície per a
   utilitzar una llibreria en un llenguatge de baix nivell com ara C. Pel
   mateix motiu s'utilitzen funcions d'ordre superior com ara `map` i `filter`
   en lloc de `for` i `while`.

 * Les coordenades a l'espai de polígons es converteixen en coordenades a
   l'espai d'imatge. Si la imatge fos de la mida de l'espai de polígons i
   després es reduís, quan l'espai fos molt gran la mida inicial de la imatge
   també ho seria.

Part 2: llenguatge de programació per a treballar amb polígons convexos
-------------------------------------------------------------------------------

Anomenem el llenguatge CPL (Convex Polygon Language). El fitxer `CPL.py`
proporciona la classe `CPL` amb els mètodes `parse` i `evaluate`. El mètode
`parse` rep un programa en CPL i en retorna l'arbre sintàctic. El mètode
`evaluate` rep un programa en CPL i l'interpreta; el paràmetre `output` permet
d'indicar el fitxer on s'escriu la sortida i el paràmetre `files` permet
d'indicar la llista on s'afegeixen les imatges, buffers de tipus `BytesIO`. Els
mètodes llancen la excepció `CPLUndefinedVariable` si s'accedeix a una variable
sense definir i una excepció `CPLSyntaxError` si el programa està mal escrit
(se substitueix la classe predefinida, que rep els errors i els escriu al canal
d'errors, per una classe que els rep i llança una excepció).

Aquest fitxer es pot executar. Si rep un fitxer en CPL d'argument,
l'interpreta; si s'executa sense arguments, llegeix del canal d'entrada i
n'interpeta el codi línia a línia. Amb l'opció `-t` mostra l'arbre de sintaxi.

El fitxer `CPLEvaluator.py` conté la classe per a avaluar els programes i el
fitxer `CPLDrawer.py` conté la classe per a dibuixar els polígons. El fitxer
`CPL.g` conté la definició de la gramàtica del llenguatge.

El llenguatge té algunes capacitats addicionals:

 * Es pot canviar la llavor del generador de nombres aleatoris amb l'ordre
   `seed`.

 * Es pot canviar la mida de la imatge (alçada i amplada) amb les ordres
   `ẁidth` i `height`. Es manté la proporció d'aspecte.

 * Es pot canviar els marges de la imatge amb l'ordre `margin`.

 * Es pot canviar el color del fons de la imatge amb l'ordre
   `background-color`.

 * Es pot canviar el color predefinit dels polígons amb l'ordre
   `default-color`, a més de la d'alguns polígons amb l'ordre `color`.

 * Es pot canviar la densitat predefinida de les traces interiors amb l'ordre
   `default-fill` i la d'alguns polígons amb `fill`.

 * Es pot canviar la mida predefinida dels vèrtexs amb l'ordre
   `default-vertex-size` i la d'alguns polígons amb `vertex-size`.

 * Es pot consultar si un polígon és equilàter, equiangular o regular amb les
   ordres `equilateral`, `equiangular` i `regular`.

 * Els colors també es poden indicar en format hexadecimal i es pot indicar el
   valor del canal alfa.

Part 3: bot per a interactuar amb polígons convexos
-------------------------------------------------------------------------------

El fitxer `bot.py` conté la implementació del bot. Cal indicar el fitxer d'on
ha de llegir el token amb l'opció `-t`.

El bot rep missatges de Telegram i els interpreta com a fragments d'un
programa (la conversació sencera és un programa). Si el missatge interpretat
escriu al fitxer de sortida, n'envia el contingut en un missatge. Les imatges
generades les envia com a fotografies. Si hi ha algun error ho notifica.
