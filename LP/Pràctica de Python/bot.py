# https://python-telegram-bot.readthedocs.io/en/latest/index.html
# https://core.telegram.org/bots#6-botfather
# https://lliçons.jutge.org/python/telegram.html


from CPL import CPL, CPLHelpMessage, CPLShortHelpMessage, CPLSyntaxError, CPLUndefinedVariable
from getopt import getopt, GetoptError
from io import StringIO
from sys import argv, stderr, stdout
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater


class CPLInterpreter():
    """Classe de funcions de gestió de missatges de Telegram per a l'interpret
    de CPL."""
    def help(self, update, context):
        """Envia un missatge d'ajuda."""
        context.bot.send_message(
         chat_id=update.message.chat_id, text=CPLShortHelpMessage)

    def interpret(self, update, context):
        """Interpreta un missatge i n'envia el resultat."""
        if 'evaluator' not in context.user_data:
            context.user_data['evaluator'] = CPL()
        cpl = context.user_data['evaluator']

        if update.edited_message:
            message = update.edited_message
        else:
            message = update.message

        input = message.text
        output = StringIO()
        files = list()
        try:
            cpl.evaluate(input, output=output, files=files)
            text = output.getvalue()
            if text:
                try:
                    context.bot.send_message(
                        chat_id=message.chat_id,
                        text="✔️ Output: %s" % text)
                except BadRequest as error:
                    help(error)
            for filename, photo in files:
                context.bot.send_photo(
                    chat_id=message.chat_id,
                    photo=photo,
                    caption=filename)
        except (CPLUndefinedVariable, CPLSyntaxError) as error:
            context.bot.send_message(
                chat_id=message.chat_id,
                text="❌ Error: %s" % error)


def _print_help(file):
    """Mostra un missatge d'ajuda."""
    print(
     "Executa un bot de Telegram que interpreta programes de manipulació de "
     "polígons convexos (CPL).\n\n"
     "Opcions:\n\n"
     " -h       Mostra aquest missatge d'ajuda.\n"
     " -t FILE  Llegeix el token del fitxer FILE.\n",
     file=file)


def _get_options():
    """Processa els arguments."""
    try:
        raw_options, arguments = getopt(argv[1:], "ht:")
    except GetoptError as error:
        print(error, file=stderr)
        _print_help(stderr)
        exit(2)

    options = {
        'token_file': None,
    }

    for option, argument in raw_options:
        if option == '-h':
            _print_help(stdout)
            exit(0)
        elif option == '-t':
            options['token_file'] = argument

    return options, arguments


def _main():
    options, _ = _get_options()

    if options['token_file'] is None:
        _print_help(stdout)
        exit(2)

    try:
        with open(options['token_file']) as file:
            token = file.read().strip()
    except FileNotFoundError as error:
        print(error, file=stderr)
        exit(2)

    updater = Updater(token=token, use_context=True)
    interpreter = CPLInterpreter()
    handler = CommandHandler('start', interpreter.help)
    updater.dispatcher.add_handler(handler)
    handler = MessageHandler(Filters.text, interpreter.interpret)
    updater.dispatcher.add_handler(handler)
    updater.start_polling()


if __name__ == '__main__':
    _main()
