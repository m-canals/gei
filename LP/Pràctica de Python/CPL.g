grammar CPL ;

root
 : statement* EOF
 ;

// Accions
// =============================================================================

statement
 : assignment
 | drawStatement
 | helpStatement
 | seedStatement
 | printStatement
 | backgroundColorStatement
 | colorStatement
 | defaultColorStatement
 | defaultFillStatement
 | defaultOutlineStatement
 | defaultVertexSizeStatement
 | fillStatement
 | outlineStatement
 | vertexSizeStatement
 | heightStatement
 | widthStatement
 | marginStatement
 | areaStatement
 | perimeterStatement
 | verticesStatement
 | centroidStatement
 | equilateralStatement
 | equiangularStatement
 | regularStatement
 | insideStatement
 | equalStatement
 ;

assignment
 : VARIABLE_IDENTIFIER ':=' expression
 ;


drawStatement
 : 'draw' string (ARGUMENT_SEPARATOR expression)*
 ;

helpStatement
 : 'help'
 ;

seedStatement
 : 'seed' number
 ;

printStatement
 : 'print' (string | expression)
 ;

backgroundColorStatement
 : 'background-color' color
 ;

colorStatement
 : 'color' (variable ARGUMENT_SEPARATOR)+ color
 ;

defaultColorStatement
 : 'default-color' color
 ;

defaultFillStatement
 : 'default-fill' density
 ;

defaultOutlineStatement
 : 'default-outline' boolean
 ;

defaultVertexSizeStatement
 : 'default-vertex-size' size
 ;

fillStatement
 : 'fill' (variable ARGUMENT_SEPARATOR)+ density
 ;

outlineStatement
 : 'outline' (variable ARGUMENT_SEPARATOR)+ boolean
 ;

vertexSizeStatement
 : 'vertex-size' (variable ARGUMENT_SEPARATOR)+ size
 ;

heightStatement
 : 'height' number
 ;

widthStatement
 : 'width' number
 ;

marginStatement
 : 'margin' margins
 ;

areaStatement
 : 'area' expression
 ;

perimeterStatement
 : 'perimeter' expression
 ;

verticesStatement
 : 'vertices' expression
 ;

centroidStatement
 : 'centroid' expression
 ;

insideStatement
 : 'inside' expression ARGUMENT_SEPARATOR expression
 ;

equilateralStatement
 : 'equilateral' expression
 ;

equiangularStatement
 : 'equiangular' expression
 ;

regularStatement
 : 'regular' expression
 ;

equalStatement
 : 'equal' expression ARGUMENT_SEPARATOR expression
 ;

// Expressions
// =============================================================================

expression
 : lowPrecedenceExpression
 ;

lowPrecedenceExpression
 : mediumPrecedenceExpression (UNION_OPERATOR mediumPrecedenceExpression)*
 ;

mediumPrecedenceExpression
 : highPrecedenceExpression (INTERSECTION_OPERATOR highPrecedenceExpression)*
 ;

highPrecedenceExpression
 : generation
 | bounding
 | variable
 | points
 | parenthesizedExpression
 ;

generation
 : GENERATION_OPERATOR number
 ;

bounding
 : BOUNDING_OPERATOR expression
 ;

parenthesizedExpression
 : LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
 ;

// Constants
// =============================================================================

string
 : STRING
 ;

points
 : LEFT_BRACKET point* RIGHT_BRACKET
 ;

point
 : number number
 ;

color
 : rgbaColor
 | hexadecimalColor
 ;

rgbaColor
 : LEFT_BRACE number number number number? RIGHT_BRACE
 ;

hexadecimalColor
 : HEXADECIMAL_COLOR
 ;

size
 : pairOfNumbers
 ;

density
 : pairOfNumbers
 ;

pairOfNumbers
 : LEFT_BRACE number number RIGHT_BRACE
 ;

margins
 : LEFT_BRACE number number? number? number? RIGHT_BRACE
 ;

boolean
 : number
 ;

number
 : NUMBER
 ;

variable
 : VARIABLE_IDENTIFIER
 ;

ARGUMENT_SEPARATOR: ',' ;
GENERATION_OPERATOR: '!' ;
BOUNDING_OPERATOR: '#' ;
UNION_OPERATOR: '+';
INTERSECTION_OPERATOR: '*' ;
LEFT_PARENTHESIS: '(' ;
RIGHT_PARENTHESIS: ')' ;
LEFT_BRACKET: '[' ;
RIGHT_BRACKET: ']' ;
LEFT_BRACE: '{' ;
RIGHT_BRACE: '}' ;

VARIABLE_IDENTIFIER: [A-Za-z_][0-9A-Za-z_]* ;
NUMBER: [+-]? ([0-9]+ | [0-9]+ '.' [0-9]* | [0-9]* '.' [0-9]+) ;
STRING: '"' (~'"')* '"' ;
HEXADECIMAL_COLOR: '#' [0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f]([0-9A-Fa-f]([0-9A-Fa-f][0-9A-Fa-f])?([0-9A-Fa-f][0-9A-Fa-f])?)? ;
WHITESPACE: [\t\n\r ]+ -> skip ;
COMMENT: '//' ~[\r\n]* -> skip;

