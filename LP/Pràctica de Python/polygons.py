from math import hypot, acos
from functools import reduce


class BoundingBox():
    """Representació d'un rectangle contenidor."""
    def __init__(self, *args):
        self.top, self.right, self.bottom, self.left, *_ = args

    def __str__(self):
        return "{top: %.2f, right: %.2f, bottom: %.2f, left: %.2f}" % (
         self.top, self.right, self.bottom, self.left)

    def point_set(self):
        return PointSet({
         Point(self.left, self.top), Point(self.right, self.top),
         Point(self.left, self.bottom), Point(self.right, self.bottom)
        })


class Point():
    """Representació d'un punt al pla."""
    def __init__(self, *args):
        """Inicialitza el punt. Si no rep ningun argument s'inicialitza amb les
        coordenades de l'origen de coordenades. Si rep un punt d'argument
        s'inicialitza amb les coordenades d'aquest punt. Si rep dos coordenades
        d'argument s'inicialitza amb aquestes coordenades."""
        if len(args) == 0:
            self.x = 0
            self.y = 0
        elif isinstance(args[0], Point):
            self.x = args[0].x
            self.y = args[0].y
        else:
            self.x, self.y, *_ = args

    def __iter__(self):
        """Retorna amb un generador de les coordenades del punt."""
        yield self.x
        yield self.y

    def __eq__(self, p):
        """Indica si dos punts són iguals. Dos punts són iguals si i només si
        les seves coordenades també ho són."""
        return self.x == p.x and self.y == p.y

    def __hash__(self):
        """Retorna amb un «hash» del punt."""
        return hash((self.x, self.y))

    def __str__(self):
        """Retorna amb la representació textual del punt."""
        return "%.3f %.3f" % (self.x, self.y)

    def __add__(self, p):
        """Retorna amb un punt corresponent a la suma dels punts."""
        return Point(self.x + p.x, self.y + p.y)

    def __sub__(self, p):
        """Retorna amb un punt corresponent a la diferència dels punts."""
        return Point(self.x - p.x, self.y - p.y)

    def __mul__(self, k):
        """Retorna amb un punt corresponent al producte del punt i un
        escalar."""
        return Point(self.x * k, self.y * k)

    def slope(self):
        """Retorna amb el pendent de la recta que passa pel punt i l'origen de
        coordenades."""
        try:
            return self.y / self.x
        except ZeroDivisionError:
            return float("inf") if self.y > 0 else float("-inf")

    def farness(self, p1, p2):
        """Retorna amb el grau de llunyania del punt a la recta que passa per
        dos punts. Si és negatiu el punt és a la dreta de la recta, si és
        positiu el punt és a l'esquerra de la recta i si és zero el punt és a
        la recta."""
        return ((p2.x - p1.x) * (self.y - p1.y) -
                (p2.y - p1.y) * (self.x - p1.x))

    def distance(self):
        """Retorna amb la distància entre el punt i l'origen de coordenades."""
        return hypot(self.x, self.y)

    def square_distance(self):
        """Retorna amb la distància al quadrat entre el punt i l'origen de
        coordenades."""
        return self.x * self.x + self.y * self.y

    # http://en.wikipedia.org/wiki/Law_of_cosine
    def angle(self, p1, p2):
        """Retorna amb l'angle que formen les semirectes amb origen al punt.
        Si el destí d'una semirecta és el punt l'anglé és zero."""
        if self == p1 or self == p2:
            return 0.0
        else:
            a = (self - p1).distance()
            b = (self - p2).distance()
            c = (p2 - p1).distance()
            return acos((a * a + b * b - c * c) / (2 * a * b))


class PointCollection():
    """Representa una col·lecció de punts."""
    def bounding_box(self):
        """Retorna amb el rectangle contenidor dels punts de la col·lecció."""
        if len(self) == 0:
            return None
        else:
            x = [point.x for point in self]
            y = [point.y for point in self]
            return BoundingBox(max(y), max(x), min(y), min(x))

    def __str__(self):
        """Retorna amb la representació textual de la col·lecció de punts."""
        return ' '.join(map(str, self[::-1]))


class PointCollections(list):
    """Representa una llista de col·leccions de punts."""
    def bounding_box(self):
        """Retorna amb el rectangle contenidor dels punts de les
        col·leccions."""
        if len(self) == 0:
            return None
        else:
            boxes = [
                points.bounding_box() for points in self if len(points) > 0]
            if len(boxes) == 0:
                return None
            else:
                top = [box.top for box in boxes]
                right = [box.right for box in boxes]
                bottom = [box.bottom for box in boxes]
                left = [box.left for box in boxes]
                return BoundingBox(
                    max(top), max(right), min(bottom), min(left))


class PointSet(set, PointCollection):
    """Representa un conjunt, una col·lecció desordenada sense repeticions, de
    punts."""
    def simple_polygon(self):
        """Retorna amb el polígon simple que pren com a vèrtexs els punts. La
        complexitat de l'algorisme emprat és quasi-lineal respecte la mida del
        conjunt."""
        if len(self) == 0:
            return Polygon()
        else:
            # Prenem el punt a l'extrem dret (z); d'aquesta manera tots els
            # punts són tant o més a l'esquerra que aquest i podem establir un
            # ordre per angle (pendent). Si n'hi ha més d'un a l'extrem dret,
            # prenem el de l'extrem inferior dret per tal que sigui el primer
            # de la llista (quan el pendent és igual prenem primer punts menys
            # distants). Cal tenir en compte que a l'hora d'ordenar l'extrem
            # dret és l'extrem esquerre (reflectim els punts).
            z = reduce(
                lambda z, p: p if p.x > z.x or p.x == z.x and p.y < z.y else z,
                self)
            keys = {p: ((z-p).slope(), (z-p).square_distance()) for p in self}
            polygon = Polygon(self)
            polygon.sort(key=lambda p: keys[p])
            return polygon


class Edge(list):
    """Representa una aresta coma una llista de punts."""
    def length(self):
        """Retorna amb la longitud de l'aresta."""
        return (self[0] - self[1]).distance()

    # http://en.wikipedia.org/wiki/Line_intersection#Given_two_points_on_each_line
    def intersection(self, edge):
        """Retorna amb el punt on s'intersequen dues arestes."""
        a, b = self
        c, d = edge
        d1 = a - b
        d2 = c - d
        n1 = a.x * b.y - a.y * b.x
        n2 = c.x * d.y - c.y * d.x
        n3 = (d1.x * d2.y - d1.y * d2.x)
        x = (n1 * d2.x - n2 * d1.x) / n3
        y = (n1 * d2.y - n2 * d1.y) / n3
        return Point(x, y)


class Angle(list):
    """Representa un angle com una llista de punts el primer de la qual és el
    vèrtex."""
    def radians(self):
        """Retorna amb la mesura en radians de l'angle."""
        return Point.angle(*self)


class Polygon(list, PointCollection):
    """Representa un polígon com una llista, una col·leció ordenada, de punts
    (vèrtexs) ordenats en sentit anti-horari. Aquesta representació admet
    polígons amb dos, un i ningun vèrtex."""
    # https://en.wikipedia.org/wiki/Graham_scan
    def convex_hull(self):
        """Retorna amb l'envolupant convexa dels vèrtexs del polígon. La
        complexitat de l'algorisme emprat és lineal respecte a la mida del
        polígon."""
        if len(self) < 4:
            return ConvexPolygon(self)
        else:
            p = self
            q = {i: p[i] for i in range(3)}
            m = 2
            for k in range(3, len(p)):
                while p[k].farness(q[m-1], q[m]) < 0:
                    m -= 1
                m += 1
                q[m] = p[k]
            return ConvexPolygon([q[i] for i in range(m + 1)])

    def size(self):
        """Retorna amb la mida, el nombre de vèrtexs i arestes, del polígon."""
        return len(self)

    def edges(self):
        """Retorna amb un generador de les arestes del polígon."""
        if len(self) > 0:
            yield Edge([self[-1], self[0]])
            for i in range(1, len(self)):
                yield Edge([self[i-1], self[i]])

    def angles(self):
        """Retorna amb un generador dels angles del polígon."""
        if len(self) == 1:
            yield Angle([self[0], self[0], self[0]])
        elif len(self) > 1:
            yield Angle([self[-1], self[-2], self[0]])
            for i in range(1, len(self)):
                yield Angle([self[i-1], self[i-2], self[i]])

    # http://en.wikipedia.org/wiki/Perimeter
    def perimeter(self):
        """Retorna amb el perímetre del polígon. La complexitat de l'algorisme
        emprat és lineal respecte a la mida del polígon."""
        return sum(map(lambda edge: edge.length(), self.edges()))

    # http://en.wikipedia.org/wiki/Polygon#Area
    # http://en.wikipedia.org/wiki/Shoelace_formula
    def area(self):
        """Retorna amb l'àrea del polígon. La complexitat de l'algorisme emprat
        és lineal respecte a la mida del polígon."""
        def f(a, b):
            return a.x * b.y - b.x * a.y

        return sum(map(lambda edge: f(*edge), self.edges())) / 2

    # http://en.wikipedia.org/wiki/Polygon#Centroid
    def centroid(self):
        """Retorna amb el centroide del polígon. La complexitat de l'algorisme
        emprat és lineal respecte a la mida del polígon."""
        if len(self) == 0:
            return None
        elif len(self) == 1:
            return Point(self[0])
        elif len(self) == 2:
            return (self[0] + self[1]) * 0.5
        else:
            A = self.area()

            def fx(a, b):
                return (a.x + b.x) * (a.x * b.y - b.x * a.y)

            def fy(a, b):
                return (a.y + b.y) * (a.x * b.y - b.x * a.y)

            x = sum(map(lambda edge: fx(*edge), self.edges())) / 6 / A
            y = sum(map(lambda edge: fy(*edge), self.edges())) / 6 / A

            return Point(x, y)

    # http://en.wikipedia.org/wiki/Equilateral_polygon
    def equilateral(self, error=0.0005):
        """Indica si el polígon és equilàter. La complexitat de l'algorisme
        emprat és lineal respecte a la mida del polígon."""
        if len(self) == 0:
            return True
        else:
            edges = self.edges()
            length = next(edges).length()

            def same_length(edge):
                return abs(edge.length() - length) <= error

            return reduce(
                lambda equilateral, edge: equilateral and same_length(edge),
                edges, True)

    # http://en.wikipedia.org/wiki/Equiangular_polygon
    def equiangular(self, error=0.0005):
        """Indica si el polígon és equiangular. La complexitat de l'algorisme
        emprat és lineal respecte a la mida del polígon."""
        if len(self) == 0:
            return True
        else:
            angles = self.angles()
            radians = next(angles).radians()

            def same_radians(angle):
                return abs(angle.radians() - radians) <= error

            return reduce(
                lambda equiangular, angle: equiangular and same_radians(angle),
                angles, True)

    # http://en.wikipedia.org/wiki/Regular_polygon
    def regular(self):
        """Indica si el polígon és regular (equilàter i equiangular). La
        complexitat de l'algorisme emprat és lineal respecte a la mida del
        polígon."""
        return self.equiangular() and self.equilateral()

    def __or__(self, polygon):
        """Retorna amb el polígon corresponent a l'unió dels polígons."""
        return PointSet(self + polygon).simple_polygon()

    def __eq__(self, polygon, error=0.0005):
        """Indica si dos polígons són iguals."""
        def same_x(a, b):
            return abs(a.x - b.x) <= error

        def same_y(a, b):
            return abs(a.y - b.y) <= error

        def same_vertex(a, b):
            return same_x(a, b) and same_y(a, b)

        same_length = len(self) == len(polygon)

        return same_length and reduce(
            lambda equal, vertices: equal and same_vertex(*vertices),
            zip(self, polygon), True)


class ConvexPolygon(Polygon):
    """Representa un polígon convex. Un polígon convex és un un polígon tal que
    els seus angles interiors mesuren menys de 180°."""
    def __contains_point(self, point, error=0.0005):
        """Indica si el polígon conté un punt."""
        if len(self) == 0:
            return False
        elif len(self) == 1:
            return point == self[0]
        elif len(self) == 2:
            m = min(self[0].x, self[1].x)
            M = max(self[0].x, self[1].x)
            return point.farness(*self) == 0 and m <= point.x <= M
        else:
            def correct_side(edge): return point.farness(*edge) >= 0
            return all(map(correct_side, self.edges()))

    def contains(self, object):
        """Si rep un punt d'argument, indica si el polígon conté aquest punt.
        Si rep una col·lecció de punts, indica si el polígon conté tots els
        punts d'aquesta col·lecció de punts."""
        if isinstance(object, Point):
            return self.__contains_point(object)
        elif isinstance(object, PointCollection):
            if len(self) == 0:
                return len(object) == 0
            else:
                return reduce(
                    lambda contains, point:
                    contains and self.__contains_point(point), object, True)

    # The intersection of two convex polygons is a convex polygon.
    # http://en.wikipedia.org/wiki/Sutherland-Hodgman_clipping_algorithm
    def __intersection(self, polygon):
        """Retorna el polígon convex corresponent a la intersecció de dos
        polígons convexos. """
        output = self
        for edge in polygon.edges():
            input = output
            output = ConvexPolygon()
            for input_edge in input.edges():
                c, d = input_edge
                c_farness, d_farness = c.farness(*edge), d.farness(*edge)
                # De fora cap dins.
                if c_farness < 0 and d_farness >= 0:
                    output.append(edge.intersection(input_edge))
                    output.append(d)
                # De dins cap dins.
                elif c_farness >= 0 and d_farness >= 0:
                    output.append(d)
                # De dins cap fora.
                elif c_farness >= 0 and d_farness < 0:
                    output.append(edge.intersection(input_edge))
        return output

    def __and__(self, polygon):
        """Retorna amb el polígon convex corresponent a la intersecció de dos
        polígons convexos. La complexitat de l'algorisme emprat és lineal
        respecte al producte de la mida de cada polígon."""
        if len(polygon) == 0 or len(self) == 0:
            return ConvexPolygon()
        elif len(polygon) == 1:
            if self.contains(polygon):
                return ConvexPolygon(polygon)
            else:
                return ConvexPolygon()
        elif len(self) == 1:
            if polygon.contains(self):
                return ConvexPolygon(self)
            else:
                return ConvexPolygon()
        # Si un dígon s'interseca, amb dues arestes s'intersequen: només se
        # n'inclou una per tal de no incloure els vèrtexs repetits.
        # FIXME Arestes colineals (l'algorisme no funciona per a n < 3).
        elif len(polygon) == 2:
            points = polygon.__intersection(self)
            return PointSet(points).simple_polygon().convex_hull()
        elif len(self) == 2:
            points = self.__intersection(polygon)
            return PointSet(points).simple_polygon().convex_hull()
        else:
            points = self.__intersection(polygon)
            return PointSet(points).simple_polygon().convex_hull()

    def __or__(self, polygon):
        """Retorna amb el polígon convex corresponent a l'unió de dos polígons
        convexos. Aquest polígon és el polígon convex més petit que conté
        ambdós polígons o, de manera equivalent, l'envolupant convexa dels
        vertexs dels polígons. La complexitat de l'algorisme emprat és
        quasi-lineal respecte a la suma de la mida de cada polígon."""
        return super().__or__(polygon).convex_hull()
