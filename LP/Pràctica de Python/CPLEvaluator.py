from CPLDrawer import CPLDrawer
from CPLParser import CPLParser
from CPLVisitor import CPLVisitor
from functools import reduce
from io import BytesIO
from operator import __and__, __or__
from polygons import Point, PointCollections, PointSet
from random import random, seed

CPLShortHelpMessage = """Ordres

<Variable> := <Expressió>
seed <Nombre>
help
draw <Text>, <Expressió>, ..., <Expressió>
print <Expressió> | <Text>
background-color <Color>
color <Variable>, ..., <Variable>, <Color>
default-color <Color>
fill <Variable>, ..., <Variable>, { <Nombre> <Nombre> }
default-fill { <Nombre> <Nombre> }
vertex-size <Variable>, ..., <Variable>, { <Nombre> <Nombre> }
default-vertex-size { <Nombre> <Nombre> }
width <Nombre>
height <Nombre>
margin { <Nombre> <Nombre>? <Nombre>? <Nombre>? }
area <Expressió>
perimeter <Expressió>
vertices <Expressió>
centroid <Expressió>
inside <Expressió>, <Expressió>
equal <Expressió>, <Expressió>
equilateral <Expressió>
equiangular <Expressió>
regular <Expressió>

Colors

 #F00
 #F00F
 #FF0000
 #FF0000FF
 {1.0 0.0 0.0}
 {1.0 0.0 0.0 1.0}

Expressions

! <Mida>
# <Expressió>
<PrimeraExpressió> + <SegonaExpressió>
<PrimeraExpressió> * <SegonaExpressió>
[ <PrimerPunt> ... <DarrerPunt> ]
<Variable>"""

CPLHelpMessage = """Ordres
------

 <Variable> := <Expressió>

Assigna el polígon resultant de l'avaluació de l'expressió <Expressió> a la variable <Variable>.

 seed <Nombre>

Estableix el nombre <Nombre> com a llavor del generador de nombres aleatoris.

 help

Mostra aquest missatge d'ajuda.

 draw <Fitxer>, <PrimeraExpressió>, ..., <DarreraExpressió>

Dibuixa els polígons resultants de l'avaluació de les expressions <PrimeraExpressió>, ..., <DarreraExpressió> al fitxer <Fitxer> en format PNG.

 print <Expressió|Text>

Mostra el polígon resultant de l'avaluació de l'expressió <Expressió> o el text <Text>, delimitat per cometes dobles.

 background-color <Color>

Estableix el color <Color> com a color de fons de les imatges.

 color <PrimeraVariable>, ..., <DarreraVariable>, <Color>

Estableix el color <Color> com a color dels polígons a les variables <PrimeraVariable>, ..., <DarreraVariable>.

 default-color <Color>

Estableix el color <Color> com a color predefinit dels polígons.

 fill <PrimeraVariable>, ..., <DarreraVariable>, { <DensitatX> i <DensitatY> }

Estableix <DensitatX> i <DensitatY> com les densitats de les traces interiors dels polígons a les variables <PrimeraVariable>, ..., <DarreraVariable>.

 default-fill { <DensitatX> <DensitatY> }

Estableix <DensitatX> i <DensitatY> com les densitats predefinides de les traces interiors dels polígons.

 vertex-size <PrimeraVariable>, ..., <DarreraVariable>, { <Amplada> <Alçada> }

Estableix <Amplada> i <Alçada> com l'amplada i l'alçada dels vèrtexs dels polígons a les variables <PrimeraVariable>, ..., <DarreraVariable>.

 default-vertex-size { <Amplada> <Alçada> }

Estableix <Amplada> i <Alçada> com l'amplada i l'alçada dels vèrtexs dels polígons.

 width <Amplada>

Estableix el nombre <Amplada> com a amplada de la imatge.

 height <Alçada>

Estableix el nombre <Alçada> com a alçada de la imatge.

 margin { <Mida1> <Mida2>? <Mida3>? <Mida4>? }

Estableix les mides dels marges de la imatge. Si n'hi ha una la de tots els marges és <Mida1>; si n'hi ha dues la dels marges superior i inferior és <Mida1> i la dels marges esquerre i dret és <Mida2>; si n'hi ha tres la del marge superior és <Mida1>, la del marge inferior és <Mida2> i la dels marges esquerre i dret és <Mida3>; si n'hi ha quatre la del marge superior és <Mida1>, la del marge dret és <Mida2>, la del marge inferior és <Mida3> i la del marge esquerre és <Mida4>.

 area <Expressió>

Mostra l'àrea del polígon resultant de l'avaluació de l'expressió <Expressió>.

 perimeter <Expressió>

Mostra el perímetre del polígon resultant de l'avaluació de l'expressió <Expressió>.

 vertices <Expressió>

Mostra el nombre de vèrtexs del polígon resultant de l'avaluació de l'expressió <Expressió>.

 centroid <Expressió>

Mostra el centroide del polígon resultant de l'avaluació de l'expressió <Expressió>.

 inside <PrimeraExpressió>, <SegonaExpressió>

Mostra si el polígon resultant de l'avaluació de l'expressió <PrimeraExpressió> conté el polígon resultant de l'avaluació de l'expressió <SegonaExpressió>.

 equal <PrimeraExpressió>, <SegonaExpressió>

Mostra si el polígon resultant de l'avaluació de l'expressió <PrimeraExpressió> és el polígon resultant de l'avaluació de l'expressió <SegonaExpressió>.

 equilateral <Expressió>

Mostra si el polígon resultant de l'avaluació de l'expressió <Expressió> és equilàter.

 equiangular <Expressió>

Mostra si el polígon resultant de l'avaluació de l'expressió <Expressió> és equiangular.

 regular <Expressió>

Mostra si el polígon resultant de l'avaluació de l'expressió <Expressió> és regular.

Colors
------

Els colors es poden expressar en hexadecimal, amb un coixinet seguit de tres, quatre sis o vuit digits, o en decimal, amb tres o quatre nombres entre zero i u, separats amb espais i delimitats amb claus). Per exemple:

 #F00  #F00F  #FF0000  #FF0000FF  {1.0 0.0 0.0}  {1.0 0.0 0.0 1.0}

Expressions
-----------

Una expressió pot contenir variables, constants, operacions d'unió, operacions d'intersecció, operacions de generació aleatòria i operacions de generació del rectangle contenidor.

L'unió és menys prioritària que la intersecció i la intersecció és menys prioritària que la resta d'operacions, les variables i les constants. Per canviar l'ordre de prioritat es pot utilitzar parèntesis.

 ! <Mida>

Genera una polígon de mida <Mida> aleatòriament. Els vertexs prenen valors entre zero i u.

 # <Expressió>

Genera el polígon corresponent al rectangle contenidor del polígon resultant de l'avaluació de l'expressió <Expressió>.

<PrimeraExpressió> + <SegonaExpressió>

Genera el polígon corresponent a la unió del polígons resultants de l'avaluació de les expressions <PrimeraExpressió> i <SegonaExpressió>.

<PrimeraExpressió> * <SegonaExpressió>

Genera el polígon corresponent a la intersecció del polígons resultants"
de l'avaluació de les expressions <PrimeraExpressió> i "
<SegonaExpressió>.

 [ <PrimerPunt> ... <DarrerPunt> ]

Genera el polígon corresponent a l'envolupant convexa dels punts <PrimerPunt> ... <DarrerPunt>, separats amb espais. Un punt consta de dos nombres (x i y)."""


class CPLUndefinedVariable(Exception):
    """Excepció que es genera quan s'accedeix a una variable sense definir."""
    def __init__(self, variable):
        super().__init__("undefined variable: %s" % variable)
        self.variable = variable


class CPLEvaluator(CPLVisitor):
    """Evaluador de codi en CPL."""
    def __init__(self):
        self.variables = dict()
        self.image_width = 398
        self.image_height = 398
        self.image_margin = (1, 1, 1, 1)
        self.image_background_color = '#FFFF'
        self.image_default_color = 'black'
        self.image_default_fill = (0, 0)
        self.image_default_outline = True
        self.image_default_vertex_size = (1, 1)

    def evaluate(self, tree, output, files):
        self.files = files
        self.output = output
        self.visit(tree)
        del self.files
        del self.output

    def visitAssignment(self, context):
        variable_identifier = context.getChild(0).getText()
        polygon = self.visit(context.getChild(2))
        self.variables[variable_identifier] = polygon

    def visitSeedStatement(self, context):
        seed(self.visit(context.getChild(1)))

    # I/O Satements ===========================================================
    def visitDrawStatement(self, context):
        children = list(context.getChildren())
        filename = self.visit(children[1])
        polygons = PointCollections(map(self.visit, children[3::2]))
        image = CPLDrawer(
            self.image_width,
            self.image_height,
            self.image_margin,
            self.image_background_color,
            self.image_default_color,
            self.image_default_fill,
            self.image_default_outline,
            self.image_default_vertex_size).draw(polygons)
        if self.files is not None:
            file = BytesIO()
            self.files.append((filename, file))
            image.save(file, format='png')
            file.seek(0)
        else:
            image.save(filename, format='png')

    def visitHelpStatement(self, context):
        print(CPLHelpMessage, file=self.output)

    def visitPrintStatement(self, context):
        print(self.visit(context.getChild(1)), file=self.output)

    # Image Statements ========================================================
    def _visitAttributeStatement(self, context, attribute, default):
        children = list(context.getChildren())
        polygons = map(lambda child: self.visit(child), children[1:-1:2])
        value = self.visit(children[-1])
        for polygon in polygons:
            setattr(polygon, attribute, value)

    def visitHeightStatement(self, context):
        self.image_height = max(int(self.visit(context.getChild(1))), 1)

    def visitWidthStatement(self, context):
        self.image_width = max(int(self.visit(context.getChild(1))), 1)

    def visitMarginStatement(self, context):
        self.image_margin = self.visit(context.getChild(1))

    def visitBackgroundColorStatement(self, context):
        self.image_background_color = self.visit(context.getChild(1))

    def visitColorStatement(self, context):
        self._visitAttributeStatement(
            context, 'color', 'image_default_color')

    def visitFillStatement(self, context):
        self._visitAttributeStatement(
            context, 'fill', 'image_default_fill')

    def visitOutlineStatement(self, context):
        self._visitAttributeStatement(
            context, 'outline', 'image_default_outline')

    def visitVertexSizeStatement(self, context):
        self._visitAttributeStatement(
            context, 'vertex_size', 'image_default_vertex_size')

    def visitDefaultColorStatement(self, context):
        self.image_default_color = self.visit(context.getChild(1))

    def visitDefaultFillStatement(self, context):
        self.image_default_fill = self.visit(context.getChild(1))

    def visitDefaultOutlineStatement(self, context):
        self.image_default_outline = self.visit(context.getChild(1))

    def visitDefaultVertexSizeStatement(self, context):
        self.image_default_vertex_size = self.visit(context.getChild(1))

    # Polygon Statements ======================================================
    def visitAreaStatement(self, context):
        polygon = self.visit(context.getChild(1))
        print("%.3f" % (polygon.area()), file=self.output)

    def visitPerimeterStatement(self, context):
        polygon = self.visit(context.getChild(1))
        print("%.3f" % (polygon.perimeter()), file=self.output)

    def visitVerticesStatement(self, context):
        polygon = self.visit(context.getChild(1))
        print(polygon.size(), file=self.output)

    def visitCentroidStatement(self, context):
        polygon = self.visit(context.getChild(1))
        centroid = polygon.centroid()
        print(centroid if centroid is not None else "", file=self.output)

    def visitInsideStatement(self, context):
        polygon1 = self.visit(context.getChild(1))
        polygon2 = self.visit(context.getChild(3))
        print("yes" if polygon2.contains(polygon1) else "no", file=self.output)

    def visitEqualStatement(self, context):
        polygon1 = self.visit(context.getChild(1))
        polygon2 = self.visit(context.getChild(3))
        print("yes" if polygon2 == polygon1 else "no", file=self.output)

    def visitEquilateralStatement(self, context):
        polygon = self.visit(context.getChild(1))
        print("yes" if polygon.equilateral() else "no", file=self.output)

    def visitEquiangularStatement(self, context):
        polygon = self.visit(context.getChild(1))
        print("yes" if polygon.equiangular() else "no", file=self.output)

    def visitRegularStatement(self, context):
        polygon = self.visit(context.getChild(1))
        print("yes" if polygon.regular() else "no", file=self.output)

    # Polygon Expressions =====================================================
    def visitExpression(self, context):
        return self.visit(context.getChild(0))

    def visitLowPrecedenceExpression(self, context):
        polygons = map(self.visit, list(context.getChildren())[0::2])
        return reduce(__or__, polygons)

    def visitMediumPrecedenceExpression(self, context):
        polygons = map(self.visit, list(context.getChildren())[0::2])
        return reduce(__and__, polygons)

    def visitHighPrecedenceExpression(self, context):
        return self.visit(context.getChild(0))

    def visitGeneration(self, context):
        n = int(self.visit(context.getChild(1)))
        points = PointSet({Point(random(), random()) for i in range(n)})
        polygon = points.simple_polygon().convex_hull()
        return polygon

    def visitBounding(self, context):
        polygon = self.visit(context.getChild(1))
        bounding_box = polygon.bounding_box()
        points = bounding_box.point_set() if bounding_box else PointSet()
        bounding_polygon = points.simple_polygon().convex_hull()
        return bounding_polygon

    def visitParenthesizedExpression(self, context):
        return self.visit(context.getChild(1))

    # Constants and variables =================================================
    def visitString(self, context):
        return context.getChild(0).getText()[1:-1]

    def visitPoints(self, context):
        points = set(map(self.visit, list(context.getChildren())[1:-1]))
        return PointSet(points).simple_polygon().convex_hull()

    def visitPoint(self, context):
        x = float(self.visit(context.getChild(0)))
        y = float(self.visit(context.getChild(1)))
        return Point(x, y)

    def visitRgbaColor(self, context):
        r = int(255 * self.visit(context.getChild(1)))
        g = int(255 * self.visit(context.getChild(2)))
        b = int(255 * self.visit(context.getChild(3)))
        if context.getChildCount() == 6:
            a = int(255 * self.visit(context.getChild(4)))
        else:
            a = 255
        return (r, g, b, a)

    def visitHexadecimalColor(self, context):
        return context.getChild(0).getText()

    def visitSize(self, context):
        x, y = self.visit(context.getChild(0))
        return max(int(x), 0), max(int(y), 0)

    def visitDensity(self, context):
        return self.visit(context.getChild(0))

    def visitPairOfNumbers(self, context):
        x = self.visit(context.getChild(1))
        y = self.visit(context.getChild(2))
        return (x, y)

    def visitMargins(self, context):
        if context.getChildCount() == 3:
            top = right = bottom = left = int(self.visit(context.getChild(1)))
        elif context.getChildCount() == 4:
            top = bottom = int(self.visit(context.getChild(1)))
            right = left = int(self.visit(context.getChild(2)))
        elif context.getChildCount() == 5:
            top = int(self.visit(context.getChild(1)))
            right = left = int(self.visit(context.getChild(2)))
            bottom = int(self.visit(context.getChild(3)))
        elif context.getChildCount() == 5:
            top = int(self.visit(context.getChild(1)))
            right = int(self.visit(context.getChild(2)))
            bottom = int(self.visit(context.getChild(3)))
            left = int(self.visit(context.getChild(4)))
        return max(top, 1), max(right, 1), max(bottom, 1), max(left, 1)

    def visitBoolean(self, context):
        return bool(self.visit(context.getChild(0)))

    def visitNumber(self, context):
        return float(context.getChild(0).getText())

    def visitVariable(self, context):
        variable = context.getChild(0).getText()
        try:
            return self.variables[variable]
        except KeyError:
            raise CPLUndefinedVariable(variable)
