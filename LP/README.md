Treball dirigit i pràctiques de la tardor de 2020 de l'assignatura Llenguatges de Programació (LP) del Grau en Enginyeria Informàtica (GEI) de la Facultat de Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web de l'assignatura](https://jpetit.jutge.org/lp).
