Exercicis i resum de la primavera de 2017 de l'assignatura Estructura de Computadors (EC) del Grau en Enginyeria Informàtica (GEI) de la Facultat de Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web de l'assignatura](https://docencia.ac.upc.edu/FIB/grau/EC/).
