Qüestionatis i exercicis de la tardor de 2018 de l'assignatura Bases de Dades (BD) del Grau en Enginyeria Informàtica (GEI) de la Facultat d'Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web de l'assignatura](https://learnsql2.fib.upc.edu).
