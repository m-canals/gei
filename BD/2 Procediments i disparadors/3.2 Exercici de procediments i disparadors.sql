drop trigger t0 on empleats1;
drop trigger t1 on empleats1;
drop function p0() cascade;
drop function p1() cascade;
drop table empleats1;
drop table empleats2;
drop table missatgesExcepcions;
\q
create table empleats1 (nemp1 integer primary key, nom1 char(25), ciutat1 char(10) not null);

create table empleats2 (nemp2 integer primary key, nom2 char(25), ciutat2 char(10) not null);

create table missatgesExcepcions(
	num integer, 
	texte varchar(100)
	);

insert into missatgesExcepcions
values(1,' Els valors de l''atribut ciutat1 d''empleats1  han d''estar inclosos en els valors de ciutat2');

insert into empleats2 values(1,'joan','bcn');

CREATE FUNCTION p0()
RETURNS TRIGGER AS $$
BEGIN
	IF EXISTS (SELECT * FROM empleats2 WHERE ciutat2 = NEW.ciutat1)
	THEN
		RETURN NEW;
	END IF;
	
	RAISE EXCEPTION '%',
		(SELECT texte FROM missatgesExcepcions WHERE num = 1);
END;
$$LANGUAGE plpgsql;

CREATE TRIGGER t0
AFTER INSERT OR UPDATE OF ciutat1 ON empleats1
FOR EACH ROW EXECUTE PROCEDURE p0();


INSERT INTO empleats1 VALUES (1,'joan','bcn'); 
select * from empleats1;

