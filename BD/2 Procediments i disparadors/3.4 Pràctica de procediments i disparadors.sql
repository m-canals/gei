DROP FUNCTION assignar_individual(char(10), char(10));
drop table missatgesExcepcions;
drop table clubs_amb_mes_de_5_socis;
drop table socisclubs;
drop table socis;
drop table clubs;

\q

create table socis (
	nsoci char(10) primary key,
	sexe char(1) not null
);

create table clubs (
	nclub char(10) primary key
);

create table socisclubs (
	nsoci char(10) not null references socis, 
	nclub char(10) not null references clubs, 
	primary key(nsoci, nclub)
);

create table clubs_amb_mes_de_5_socis (
	nclub char(10) primary key references clubs
);

create table missatgesExcepcions (
	num integer, 
	texte varchar(50)
);

insert into missatgesExcepcions values(1, 'Club amb mes de 10 socis');
insert into missatgesExcepcions values(2, 'Club amb mes homes que dones');
insert into missatgesExcepcions values(3, 'Soci ja assignat a aquest club');
insert into missatgesExcepcions values(4, 'O el soci o el club no existeixen');
insert into missatgesExcepcions values(5, 'Error intern');

insert into clubs values ('escacs');
insert into clubs values ('petanca');

insert into socis values ('anna','F');

insert into socis values ('joanna','F');
insert into socis values ('josefa','F');
insert into socis values ('pere','M');

insert into socisclubs values('joanna','petanca');
insert into socisclubs values('josefa','petanca');
insert into socisclubs values('pere','petanca');

CREATE FUNCTION assignar_individual(new_nsoci char(10), new_nclub char(10))
RETURNS VOID AS $$
DECLARE
	socis INTEGER DEFAULT 0;
	homes INTEGER DEFAULT 0;
	dones INTEGER DEFAULT 0;
	soci CHAR (10);
	sexe char(1) DEFAULT NULL;
BEGIN
	SELECT s.sexe INTO sexe FROM socis s WHERE s.nsoci = new_nsoci;
	
	-- El soci no existeix.
	IF sexe IS NULL
	THEN
		RAISE EXCEPTION '%',
			(SELECT texte FROM missatgesExcepcions WHERE num = 4);
	ELSIF sexe = 'M'
	THEN
		homes = homes + 1;
	ELSE
		dones = dones + 1;
	END IF;

	-- El club no existeix.
	IF NOT EXISTS (SELECT * FROM clubs c WHERE c.nclub = new_nclub)
	THEN
		RAISE EXCEPTION '%',
			(SELECT texte FROM missatgesExcepcions WHERE num = 4);
	END IF;

	FOR soci IN SELECT sc.nsoci FROM socisclubs sc WHERE sc.nclub = new_nclub
	LOOP
	    -- El soci ja forma part del club.
		IF soci = new_nsoci
		THEN
			RAISE EXCEPTION '%',
				(SELECT texte FROM missatgesExcepcions WHERE num = 3);
		END IF;
		
		socis = socis + 1;
		
		IF (SELECT s.sexe FROM socis s WHERE s.nsoci = soci) = 'M'
		THEN
			homes = homes + 1;
		ELSE
			dones = dones + 1;
		END IF;
	END LOOP;

	-- El club tindrà més de 10 socis.
	IF socis = 10
	THEN
		RAISE EXCEPTION '%',
			(SELECT texte FROM missatgesExcepcions WHERE num = 1);
	END IF;
	
	-- El club tindrà més homes que dones.
	IF homes > dones
	THEN
		RAISE EXCEPTION '%',
			(SELECT texte FROM missatgesExcepcions WHERE num = 2);
	END IF;

	-- Si el club tindrà més de cinc socis, l'insereix a la taula corresponent.
	IF socis = 5
	THEN
		INSERT INTO clubs_amb_mes_de_5_socis VALUES (new_nclub);
	END IF;

	-- Enregistra l'assignació del soci al club.
	INSERT INTO socisclubs VALUES (new_nsoci, new_nclub);

	RETURN;
	
	EXCEPTION
	WHEN raise_exception THEN
		RAISE EXCEPTION '%', SQLERRM;
	WHEN OTHERS THEN
		RAISE EXCEPTION '%',
			(SELECT texte FROM missatgesExcepcions WHERE num = 5);
END;
$$LANGUAGE plpgsql;

insert into socis values('s1','F');
insert into socis values('s2','F');
select * from assignar_individual('anna','petanca'); 
select * from assignar_individual('s1','petanca'); 
select * from assignar_individual('s2','petanca'); 

select * from socisclubs;
select * from clubs_amb_mes_de_5_socis;
