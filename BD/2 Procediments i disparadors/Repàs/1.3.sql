DROP TRIGGER disparador ON empleats;
DROP FUNCTION procediment();
DROP TABLE empleats;
DROP TABLE dia;
DROP TABLE missatgesExcepcions;

CREATE TABLE empleats (
	nempl integer primary key,
	salari integer
);

CREATE TABLE dia (
	dia char(10)
);

INSERT INTO empleats VALUES (1,1000);
INSERT INTO empleats VALUES (2,2000);
INSERT INTO empleats VALUES (123,3000);
INSERT INTO dia VALUES ('dijous');

CREATE TABLE missatgesExcepcions (
	num integer, 
	texte varchar(50)
);

INSERT INTO missatgesExcepcions
VALUES (1, 'No es poden esborrar empleats el dijous');

CREATE FUNCTION procediment()
RETURNS TRIGGER AS $$
BEGIN
	IF (SELECT dia FROM dia) = 'dijous'
	THEN
		RAISE EXCEPTION '%',
			(SELECT texte FROM missatgesExcepcions WHERE num = 1);
	END IF;
	
	RETURN OLD;
END;
$$LANGUAGE plpgsql;

CREATE TRIGGER disparador
BEFORE DELETE ON empleats
FOR EACH STATEMENT EXECUTE PROCEDURE procediment();

DELETE FROM empleats WHERE salari <= 1000;
