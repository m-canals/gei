DROP TRIGGER disparador ON empleats;
DROP FUNCTION procediment();
DROP TABLE empleats;
DROP TABLE missatgesExcepcions;

CREATE TABLE empleats (
	nempl integer primary key,
	salari integer
);

CREATE TABLE missatgesExcepcions (
	num integer, 
	texte varchar(100)
);

INSERT INTO missatgesExcepcions
VALUES (1, 'No es pot esborrar l''empleat 123 ni modificar el seu número d''empleat');

INSERT INTO empleats VALUES (1, 1000);
INSERT INTO empleats VALUES (2, 2000);
INSERT INTO empleats VALUES (123, 3000);


CREATE FUNCTION procediment()
RETURNS TRIGGER AS $$
BEGIN
	IF OLD.nempl = 123
	THEN
		RAISE EXCEPTION '%',
			(SELECT texte FROM missatgesExcepcions WHERE num = 1);
	ELSE
		IF TG_OP = 'DELETE'
		THEN
			RETURN OLD;
		ELSIF TG_OP = 'UPDATE'
		THEN
			RETURN NEW;
		END IF;
	END IF;
END;
$$LANGUAGE plpgsql;

CREATE TRIGGER disparador
BEFORE DELETE OR UPDATE OF nempl ON empleats
FOR EACH ROW EXECUTE PROCEDURE procediment();

DELETE FROM empleats WHERE nempl = 123;
DELETE FROM empleats WHERE nempl = 1;
UPDATE empleats SET nempl = 124 WHERE nempl = 123;
UPDATE empleats SET nempl = 124 WHERE nempl = 2;
