DROP FUNCTION llistat_treb(char(8), char(8));
DROP TYPE tipus;
DROP TABLE lloguers_actius;
DROP TABLE treballadors;
DROP TABLE cotxes;
DROP TABLE missatgesExcepcions;

CREATE TABLE cotxes (
	matricula char(10) primary key,
	marca char(20) not null,
	model char(20) not null,
	categoria integer not null,
	color char(10),
	any_fab integer
);

CREATE TABLE treballadors (
	dni char(8) primary key,
	nom char(30) not null,
	sou_base real not null,
	plus real not null
);

CREATE TABLE lloguers_actius (
	matricula char(10) primary key references cotxes,
	dni char(8) not null constraint fk_treb references treballadors,
	num_dies integer not null,
	preu_total real not null
);

CREATE TABLE missatgesExcepcions(
	num integer, 
	texte varchar(50)
);
	
INSERT INTO missatgesExcepcions
VALUES (1, 'No hi ha cap tupla dins del interval demanat');
INSERT INTO missatgesExcepcions
VALUES (2, 'Error intern');

INSERT INTO cotxes VALUES ('1111111111', 'Audi', 'A4', 1, 'Vermell', 1998);
INSERT INTO cotxes VALUES ('2222222222', 'Audi', 'A3', 2, 'Blanc', 1998);
INSERT INTO cotxes VALUES ('3333333333', 'Volskwagen', 'Golf', 2, 'Blau', 1990);
INSERT INTO cotxes VALUES ('4444444444', 'Toyota', 'Corola', 3, 'groc', 1999);
INSERT INTO cotxes VALUES ('5555555555', 'Honda', 'Civic', 3, 'Vermell', 2000);
INSERT INTO cotxes VALUES ('6666666666', 'BMW', 'Mini', 2, 'Vermell', 2000);

INSERT INTO treballadors VALUES ('22222222','Joan',1700,150);

INSERT INTO lloguers_actius VALUES ('1111111111', '22222222', 7, 750);
INSERT INTO lloguers_actius VALUES ('2222222222', '22222222', 5, 550);
INSERT INTO lloguers_actius VALUES ('3333333333', '22222222', 4, 450);
INSERT INTO lloguers_actius VALUES ('4444444444', '22222222', 8, 850);
INSERT INTO lloguers_actius VALUES ('5555555555', '22222222', 2, 250);

CREATE TYPE tipus AS (
	dni CHAR(8),
	nom CHAR(30),
	sou_base REAL,
	plus REAL,
	matricula CHAR(10)
);

CREATE FUNCTION llistat_treb(primer CHAR(8), darrer CHAR(8))
RETURNS SETOF tipus AS $$
DECLARE
	fila tipus;
BEGIN
	FOR fila IN
		SELECT dni, nom, sou_base, plus, NULL
		FROM treballadors
		WHERE primer <= dni AND dni <= darrer
		ORDER BY dni ASC
	LOOP
		IF (SELECT COUNT(*) FROM lloguers_actius WHERE dni = fila.dni) >= 5
		THEN
			FOR fila.matricula IN
				SELECT matricula
				FROM lloguers_actius
				WHERE dni = fila.dni
				ORDER BY matricula ASC
			LOOP
				RETURN NEXT fila;
			END LOOP;
		ELSE
			RETURN NEXT fila;
		END IF;
	END LOOP;

	IF NOT FOUND
	THEN
		RAISE EXCEPTION '%',
			(SELECT texte FROM missatgesExcepcions WHERE num = 1);
	END IF;

	RETURN;
	
	EXCEPTION
		WHEN raise_exception THEN
			RAISE EXCEPTION '%', SQLERRM;
		WHEN OTHERS THEN
			RAISE EXCEPTION '%',
				(SELECT texte FROM missatgesExcepcions WHERE num = 2);
END;
$$LANGUAGE plpgsql;

SELECT * FROM llistat_treb('11111111', '33333333');
