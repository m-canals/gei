DROP TRIGGER ex172 ON treballadors;
DROP FUNCTION auditoria();
DROP TABLE treballadors;
DROP TABLE usuari_actual;

CREATE TABLE treballadors (
	num_treballador integer primary key,
	salari integer,
	usuari char(30)
);

CREATE TABLE usuari_actual(
	usuari char(10)
);

INSERT INTO usuari_actual VALUES ('bd0003');


CREATE FUNCTION auditoria()
RETURNS TRIGGER AS $$
BEGIN
      SELECT usuari INTO NEW.usuari FROM usuari_actual;

      RETURN NEW;
END;
$$LANGUAGE plpgsql;

CREATE TRIGGER ex172
BEFORE INSERT ON treballadors
FOR EACH ROW EXECUTE PROCEDURE auditoria();

INSERT INTO treballadors VALUES (1, 1000, NULL);
INSERT INTO treballadors VALUES (2, 2000, NULL); 
SELECT * FROM treballadors;
