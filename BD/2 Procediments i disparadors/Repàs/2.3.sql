drop function assignar_individual(char(10), char(10));
drop type tipus;
drop table socisclubs;
drop table socis;
drop table clubs_amb_mes_de_5_socis;
drop table clubs;
drop table missatgesExcepcions;


create table socis (
	nsoci char(10) primary key,
	sexe char(1) not null
);

create table clubs (
	nclub char(10) primary key
);

create table socisclubs (
	nsoci char(10) not null references socis, 
	nclub char(10) not null references clubs, 
	primary key(nsoci, nclub)
);

create table clubs_amb_mes_de_5_socis (
	nclub char(10) primary key references clubs
);

create table missatgesExcepcions (
	num integer, 
	texte varchar(50)
);

insert into missatgesExcepcions values(1, 'Club amb mes de 10 socis');
insert into missatgesExcepcions values(2, 'Club amb mes homes que dones');
insert into missatgesExcepcions values(3, 'Soci ja assignat a aquest club');
insert into missatgesExcepcions values(4, 'O el soci o el club no existeixen');
insert into missatgesExcepcions values(5, 'Error intern');

/*
insert into clubs values ('escacs');
insert into clubs values ('petanca');

insert into socis values ('anna','F');

insert into socis values ('joanna','F');
insert into socis values ('josefa','F');
insert into socis values ('pere','M');

insert into socisclubs values('joanna','petanca');
insert into socisclubs values('josefa','petanca');
insert into socisclubs values('pere','petanca');*/

create type tipus as (sexe char(1), quantitat integer);

create function assignar_individual(soci char(10), club char(10))
returns void as $$
declare
	dades tipus; 
	homes integer default 0;
	dones integer default 0;
begin
	insert into socisclubs (nsoci, nclub) values (soci, club);
	
	for dades
	in
		select sexe, count(*)
		from socis s, socisclubs sc
		where sc.nclub = club and sc.nsoci = s.nsoci
		group by sexe
	loop
		if dades.sexe = 'M'
		then
			homes = dades.quantitat;
		elseif dades.sexe = 'F'
		then
			dones = dades.quantitat;
		end if;
	end loop;

	if homes + dones > 10
	then
		raise exception '%',
			(select texte from missatgesExcepcions where num = 1);
	end if;
	
	if homes > dones
	then
		raise exception '%',
			(select texte from missatgesExcepcions where num = 2);
	end if;
	
	if homes + dones > 5 and
	   not exists (select * from clubs_amb_mes_de_5_socis where nclub = club)
	then
		insert into clubs_amb_mes_de_5_socis values (club);
	end if;
	
	return;
exception
	when raise_exception then
		raise exception '%', SQLERRM;
	when unique_violation then
		raise exception '%',
			(select texte from missatgesExcepcions where num = 3);
	when foreign_key_violation then
		raise exception '%',
			(select texte from missatgesExcepcions where num = 4);
	when others then
		raise exception '%',
			(select texte from missatgesExcepcions where num = 5);
end;
$$ language plpgsql;



/*
select * from assignar_individual('anna','escacs'); 
select * from socisclubs;
select * from clubs_amb_mes_de_5_socis;
*/

delete from socisclubs;
delete from socis;
delete from clubs;
delete from clubs_amb_mes_de_5_socis;

insert into clubs values ('1');
insert into socis values ('1','F');
insert into socis values ('2','F');
insert into socis values ('3','F');
insert into socis values ('4','F');
insert into socis values ('5','F');
insert into socis values ('6','F');
insert into socis values ('7','F');
insert into socis values ('8','F');
insert into socis values ('9','F');
insert into socis values ('10','F');
insert into socisclubs values('1','1');
insert into socisclubs values('2','1');
insert into socisclubs values('3','1');
insert into socisclubs values('4','1');
insert into socisclubs values('5','1');
insert into socisclubs values('6','1');
insert into socisclubs values('7','1');
insert into socisclubs values('8','1');

-- S'assigna un nou soci a un club que en té 8. El club no té cap home.
select * from assignar_individual('9','1'); 

-- S'assigna un nou soci a un club que en té 9. El club no té cap home.
select * from assignar_individual('10','1');

select * from socisclubs;
select * from clubs_amb_mes_de_5_socis;
