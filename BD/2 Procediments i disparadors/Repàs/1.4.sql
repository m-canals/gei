DROP TRIGGER disparador0 ON empleats;
DROP TRIGGER disparador1 ON empleats;
DROP FUNCTION procediment0();
DROP FUNCTION procediment1();
DROP TABLE empleats;
DROP TABLE temp;
DROP TABLE missatgesExcepcions;

CREATE TABLE empleats (
	nempl integer primary key,
	salari integer
);

CREATE TABLE missatgesExcepcions (
	num integer, 
	texte varchar(50)
);

INSERT INTO missatgesExcepcions
VALUES (1, 'Suma sous esborrats >  Suma sous que queden');

INSERT INTO empleats VALUES (1, 1000);
INSERT INTO empleats VALUES (2, 2500);
INSERT INTO empleats VALUES (123, 3000);

CREATE TABLE TEMP (
	x integer,
	y integer
);

-- La suma dels sous dels empleats esborrats en una instrucció delete
-- no pot ser superior a la suma dels sous dels empleats que queden a
-- la BD després de l'esborrat.

CREATE FUNCTION procediment0()
RETURNS TRIGGER AS $$
BEGIN
	DELETE FROM TEMP;
	
	INSERT INTO TEMP SELECT SUM(salari), 0 FROM empleats;
	
	RETURN NULL;
END;
$$LANGUAGE plpgsql;

CREATE FUNCTION procediment1()
RETURNS TRIGGER AS $$
BEGIN
	UPDATE TEMP SET x = x - OLD.salari, y = y + OLD.salari;
	
	IF (SELECT x - y FROM TEMP) < 0
	THEN
		RAISE EXCEPTION '%',
			(SELECT texte FROM missatgesExcepcions WHERE num = 1);
	END IF;
	
	RETURN OLD;
END;
$$LANGUAGE plpgsql;

CREATE TRIGGER disparador0
BEFORE DELETE ON empleats
FOR EACH STATEMENT EXECUTE PROCEDURE procediment0();

CREATE TRIGGER disparador1
BEFORE DELETE ON empleats
FOR EACH ROW EXECUTE PROCEDURE procediment1();

DELETE FROM empleats WHERE salari < 2500;
