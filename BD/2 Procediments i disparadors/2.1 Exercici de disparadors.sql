DROP TRIGGER ex172 ON treballadors;
DROP TABLE treballadors;;
DROP TABLE usuari_actual;
DROP FUNCTION auditoria();

CREATE TABLE treballadors (
	num_treballador INTEGER PRIMARY KEY,
	salari INTEGER,
	usuari CHARACTER (30)
);

CREATE TABLE usuari_actual(
	usuari CHARACTER (10)
);

INSERT INTO usuari_actual VALUES ('bd0003');

CREATE FUNCTION auditoria()
RETURNS TRIGGER AS $$
BEGIN
	NEW.usuari = (SELECT usuari FROM usuari_actual);
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER ex172
BEFORE INSERT ON treballadors
FOR EACH ROW EXECUTE PROCEDURE auditoria();

INSERT INTO treballadors VALUES (1, 1000, NULL);
INSERT INTO treballadors VALUES (2, 2000, NULL); 

SELECT * FROM treballadors;
