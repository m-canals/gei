DROP TRIGGER t0 ON empleats;
DROP TRIGGER t1 ON empleats;
DROP FUNCTION p0();
DROP FUNCTION p1();
DROP TABLE empleats;
DROP TABLE missatgesExcepcions;
DROP TABLE temp;

CREATE TABLE empleats (
  nempl INTEGER PRIMARY KEY,
  salari INTEGER
);

CREATE TABLE missatgesExcepcions (
	num INTEGER, 
	texte VARCHAR (50)
);

INSERT INTO empleats VALUES (1, 1000);
INSERT INTO empleats VALUES (2, 2500);
INSERT INTO empleats VALUES (123, 3000);

INSERT INTO missatgesExcepcions
VALUES (1,'Suma sous esborrats >  Suma sous que queden');

CREATE TABLE temp (
	x INTEGER,
	y INTEGER
);

CREATE FUNCTION p0()
RETURNS TRIGGER AS $$
BEGIN
	DELETE FROM temp;
	
	INSERT INTO temp (x,y) (SELECT sum(salari), 0 FROM empleats);
	
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION p1()
RETURNS TRIGGER AS $$
BEGIN
	UPDATE temp SET x = x - OLD.salari, y = y + OLD.salari;
	
	IF (SELECT y - x FROM temp) >= 0
	THEN
		RAISE EXCEPTION '%', 
			(SELECT texte FROM missatgesExcepcions WHERE num = 1);
	END IF;

	RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER t0
BEFORE DELETE ON empleats
FOR EACH STATEMENT EXECUTE PROCEDURE p0();

CREATE TRIGGER t1
BEFORE DELETE ON empleats
FOR EACH ROW EXECUTE PROCEDURE p1();

DELETE FROM empleats;
SELECT * FROM empleats;
