CREATE or replace FUNCTION p0()
RETURNS TRIGGER AS $$
BEGIN
	if (select count(*) from empleats2 where ciutat2 = old.ciutat2) = 1
	then
		delete from empleats1
		where ciutat1 = old.ciutat2;
	end if;

	if TG_OP = 'DELETE'
	then
		return old;
	else
		return new;
	end if;
END;
$$LANGUAGE plpgsql;

CREATE TRIGGER t0
before DELETE OR UPDATE OF ciutat2 ON empleats2
FOR EACH ROW EXECUTE PROCEDURE p0();
