import java.sql.*; 

class CtrlDadesPublic extends CtrlDadesPrivat
{
	public ConjuntTuples consulta(Connection c, Tuple p) throws BDException
	{
		try
		{
			ConjuntTuples ct = new ConjuntTuples();
			
			Statement s = c.createStatement();
			
			/* Esborra els despatxos del mòdul indicat que tenen una superfície
			   inferior a la indicada. */

			if (s.executeUpdate(
				"DELETE FROM despatxos " +
				"WHERE modul = '" + p.consulta(1) + "'" +
				"AND superficie < " + p.consulta(2)) == 0)
			{
				/* No hi ha cap despatx al mòdul indicat amb una superfície
				   inferior a la indicada.*/
				throw new BDException(12);
			}

			/* Retorna una fila amb la suma de les superfícies dels despatxos
			   que queden a la base de dades després de la sentència
			   d'esborrat. */
			ResultSet r = s.executeQuery(
				"SELECT COUNT(*), SUM(superficie) FROM despatxos");
				
			r.next();

			Tuple t = new Tuple();
			
			/* Si no en queda cap, ho indica. */
			t.afegir(r.getInt(1) == 0 ? "NO" : r.getString(2));

			ct.afegir(t);

			/* Retorna el nom del professor i la quantitat d'assignacions
			   finalitzades de cada professor amb alguna assignació
			   finalitzada. */
			r = s.executeQuery(
				"SELECT p.nomprof, COUNT(*) " +
				"FROM professors p, assignacions a " +
				"WHERE a.instantfi IS NOT NULL AND p.dni = a.dni " +
				"GROUP BY p.dni");
			
			while (r.next())
			{
				t = new Tuple();
	
				t.afegir(r.getString(1));
				t.afegir(r.getString(2));
				
				ct.afegir(t);
			}
			
			s.close();
			
			return ct;
		}
		catch (SQLException e)
		{
			/* Algun dels despatxos a esborrar te assignacions de professors i
			 * no es pot esborrar (foreign key constraint). */
			if (e.getSQLState().equals("23503"))
			{
				throw new BDException(13);
			}
			/* Error intern. */
			else
			{
				throw new BDException(14);
			}
		}
	}
}
