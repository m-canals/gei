import java.sql.*; 

class CtrlDadesPublic extends CtrlDadesPrivat
{
	public ConjuntTuples consulta(Connection c, Tuple p) throws BDException
	{
		try
		{
			ConjuntTuples ct = new ConjuntTuples();
		
			Statement s = c.createStatement();
			
			ResultSet r = s.executeQuery(
				"SELECT d.modul, d.numero FROM despatxos d " +
				"WHERE NOT EXISTS (SELECT * FROM assignacions a " +
				"WHERE a.modul = d.modul AND a.numero = d.numero " +
				"AND a.dni = '" + p.consulta(1) + "')");

			if (r.next())
			{
				do
				{
					Tuple t = new Tuple();
				
					t.afegir(r.getString(1));
					t.afegir(r.getString(2));
				
					ct.afegir(t);
				}
				while (r.next());
			}
			else
			{
				throw new BDException(11);
			}
			
			s.close();
			
			return ct;
		}
		catch (SQLException e)
		{
			throw new BDException(12);
		}
	}
}
