import java.sql.*; 

class CtrlDadesPublic extends CtrlDadesPrivat
{
	public ConjuntTuples consulta(Connection c, Tuple p) throws BDException
	{
		try
		{
			ConjuntTuples ct = new ConjuntTuples();
			
			PreparedStatement s = c.prepareStatement(
				"SELECT COUNT(*) FROM assignacions WHERE dni = ?");

			Integer i = 1;
			String dni = p.consulta(i);
		
			while (! dni.equals("-999"))
			{
				s.setString(1, dni);
				
				ResultSet r = s.executeQuery();
				
				r.next();
				
				Tuple t = new Tuple();
				
				t.afegir(dni);
				t.afegir(r.getString(1));
				
				ct.afegir(t);
				
				i++;
				dni = p.consulta(i);
			}
		
			s.close();

			return ct;
		}
		catch (SQLException e)
		{
			throw new BDException(11);
		}
	}
}
