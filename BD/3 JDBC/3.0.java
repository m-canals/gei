import java.sql.*; 

class CtrlDadesPublic extends CtrlDadesPrivat
{
	public ConjuntTuples consulta(Connection c, Tuple p) throws BDException
	{
		try
		{
			ConjuntTuples ct = new ConjuntTuples();
			
			Statement s = c.createStatement();
		
			ResultSet r = s.executeQuery("SELECT dni FROM professors");
		
			/* Hi ha algun professor. */
			if (r.next())
			{
				do
				{
					Tuple t = new Tuple();
					String dni = r.getString(1);
					String modul;
					String numero;
			
					Statement s1 = c.createStatement();
					ResultSet r1 = s1.executeQuery(
						"SELECT modul, numero, instantFi FROM assignacions " +
						"WHERE dni = '" + dni + "' AND instantFi IS NULL");

					/* Té una assignació no finalitzada. */
					if (r1.next())
					{
						modul = r1.getString(1);
						numero = r1.getString(2);
					}
					else
					{
					
						r1 = s1.executeQuery(
							"SELECT modul, numero, instantFi FROM assignacions " +
							"WHERE dni = '" + dni + "' AND instantFi IS NOT NULL " + 
							"ORDER BY instantFi DESC");
					
						/* Té alguna assignació finalitzada. */
						if (r1.next())
						{
							modul = r1.getString(1);
							numero = r1.getString(2);
						}
						/* No té cap assignació. */
						else
						{
							modul = "XXX";
							numero = "YYY";
						}
					}
				
					t.afegir(dni);
					t.afegir(modul);
					t.afegir(numero);
					ct.afegir(t);
				}
				while (r.next());
			}
			/* No hi ha cap professor. */
			else
			{
				throw new BDException(10);
			}
		
			return ct;
		}
		catch (SQLException exception)
		{
			throw new BDException(11);
		}
	}
}
