CREATE TABLE DEPARTAMENTS (
	NUM_DPT INTEGER,
	NOM_DPT CHAR(20),
	PLANTA INTEGER,
	EDIFICI CHAR(30),
	CIUTAT_DPT CHAR(20),
	PRIMARY KEY (NUM_DPT)
);

CREATE TABLE PROJECTES (
	NUM_PROJ INTEGER,
	NOM_PROJ CHAR(10),
	PRODUCTE CHAR(20),
	PRESSUPOST INTEGER,
	PRIMARY KEY (NUM_PROJ)
);

CREATE TABLE EMPLEATS (
	NUM_EMPL INTEGER,
	NOM_EMPL CHAR(30),
	SOU INTEGER,
	CIUTAT_EMPL CHAR(20),
	NUM_DPT INTEGER,
	NUM_PROJ INTEGER,
	PRIMARY KEY (NUM_EMPL),
	FOREIGN KEY (NUM_DPT) REFERENCES DEPARTAMENTS (NUM_DPT),
	FOREIGN KEY (NUM_PROJ) REFERENCES PROJECTES (NUM_PROJ)
);

-- Qüestió 1 --

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES (3, 'MARKETING', 3, 'VERDAGUER', 'VIC');
INSERT INTO PROJECTES VALUES (1, 'IBDTEL', 'TELEVISIO', 1000000);
INSERT INTO EMPLEATS VALUES (3, 'ROBERTO', 25000, 'MADRID', 3, 1);

/* Obté el número i nom dels departaments tals que tots els seus empleats viuen
  a Madrid i tenen algun empleat. */

--- XXX
SELECT d.num_dpt, d.nom_dpt
FROM departaments d
WHERE NOT EXISTS (
	SELECT *
	FROM empleats e
	WHERE e.num_dpt = d.num_dpt
	AND e.ciutat_empl != 'MADRID'
) AND EXISTS (
	SELECT *
	FROM empleats e
	WHERE e.num_dpt = d.num_dpt
);

SELECT d.num_dpt, d.nom_dpt
FROM departaments d, empleats e
WHERE d.num_dpt = e.num_dpt
GROUP BY d.num_dpt
HAVING NOT EXISTS (
	SELECT *
	FROM empleats e1
	WHERE e1.num_dpt = d.num_dpt
	AND e1.ciutat_empl != 'MADRID'
);

-- Qüestió 2 --

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES (3, 'MARKETING', 3, 'RIOS ROSAS', 'MADRID');
INSERT INTO EMPLEATS VALUES (3, 'ROBERTO', 25000, 'BARCELONA', 3, NULL);
INSERT INTO EMPLEATS VALUES (5, 'EULALIA', 150000, 'BARCELONA', 3, NULL);
INSERT INTO EMPLEATS VALUES (6, 'ROBERTO', 25000, 'BCN', 3, NULL);
INSERT INTO EMPLEATS VALUES (7, 'EULALIA', 150000, 'BCN', 3, NULL);

/* Obté el número i el nom dels departaments que tenen 2 o més empleats que
   viuen a la mateixa ciutat. */

--- XXX

SELECT DISTINCT d.num_dpt, d.nom_dpt
FROM departaments d, empleats e
WHERE d.num_dpt = e.num_dpt
GROUP BY d.num_dpt, e.ciutat_empl
HAVING COUNT(*) >= 2;

SELECT d.num_dpt, d.nom_dpt
FROM departaments d
WHERE (
	SELECT MAX(n.c)
	FROM (
		SELECT COUNT(*) c
		FROM empleats e
		WHERE e.num_dpt = d.num_dpt
		GROUP BY e.ciutat_empl
	) n
) >= 2;

SELECT d.num_dpt, d.nom_dpt
FROM departaments d
WHERE EXISTS (
	SELECT *
	FROM empleats e1
	WHERE e1.num_dpt = d.num_dpt
	AND EXISTS (
		SELECT *
		FROM empleats e2
		WHERE e2.num_dpt = d.num_dpt
		AND e1.num_empl != e2.num_empl
	)
);

-- Qüestió 3 --

CREATE TABLE COST_CIUTAT (
	CIUTAT_DPT CHAR(20),
    COST INTEGER,
    PRIMARY KEY (CIUTAT_DPT)
);

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO PROJECTES VALUES (3, 'PR1123', 'TELEVISIO', 600000);
INSERT INTO DEPARTAMENTS VALUES (4, 'MARKETING', 3, 'RIOS ROSAS', 'BARCELONA');
INSERT INTO EMPLEATS VALUES (3, 'ROBERTO', 100, 'MATARO', 4, 3);

/*  Insereix a la taula COST_CIUTAT el nom i la suma dels sous dels empleats
    dels departaments situats a la ciutat de cada ciutat on hi ha algun
    departament que té algun empleat. */

INSERT INTO cost_ciutat (
	SELECT d.ciutat_dpt, SUM(e.sou)
	FROM departaments d, empleats e
	WHERE d.num_dpt = e.num_dpt
	GROUP BY d.ciutat_dpt
);

-- Qüestió 4 --

/* Crea les taules següents:

    FRANGES_HORARIES(INSTANT_INICI, INSTANT_FI, NUM_EMPL)
    TASQUES_REALITZADES(NUM_TASCA, INSTANT_INICI, INSTANT_FI, NUM_EMPL,
     DESCRIPCIO)

   On cada fila de la taula franges_horaries representa un periode d'hores
   seguides què ha treballat un empleat i cada fila de la taula
   tasques_realitzades representa una tasca que un empleat ha realitzat en una
   franja horaria. 

   Cal tindre en compte que: 
   
    - No hi poden haver dues franges d'un mateix empleat que comencin i acabin
      en uns mateixos instants. 
    - L'instant fi d'una franja ha de ser més gran que l'instant d'inici més
      180.
    - Una franja horària ha de ser d'un empleat que existeixi a la base de
      dades.
    - No hi pot haver dues tasques amb el mateix número de tasca. 
    - Una tasca es fa sempre en una franja horària que existeixi a la base de
      dades.
    - La descripció d'una tasca ha de tenir un valor definit (valor no nul).
    - Els atributs instant_inici, instant_fi, num_tasca són enters.
    - L'atribut descripció ha de ser un char(50). */

CREATE TABLE FRANGES_HORARIES (
	INSTANT_INICI INTEGER,
	INSTANT_FI INTEGER CHECK (INSTANT_FI > INSTANT_INICI + 180),
	NUM_EMPL INTEGER,
	PRIMARY KEY (INSTANT_INICI, INSTANT_FI, NUM_EMPL),
	FOREIGN KEY (NUM_EMPL) REFERENCES EMPLEATS
);

CREATE TABLE TASQUES_REALITZADES (
	NUM_TASCA INTEGER,
	INSTANT_INICI INTEGER NOT NULL,
	INSTANT_FI INTEGER NOT NULL,
	NUM_EMPL INTEGER NOT NULL,
	DESCRIPCIO CHAR(50) NOT NULL,
	PRIMARY KEY (NUM_TASCA),
	FOREIGN KEY (INSTANT_INICI, INSTANT_FI, NUM_EMPL) REFERENCES FRANGES_HORARIES
);
