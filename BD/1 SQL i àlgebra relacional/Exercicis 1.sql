CREATE TABLE DEPARTAMENTS (
	NUM_DPT INTEGER,
	NOM_DPT CHAR(20),
	PLANTA INTEGER,
	EDIFICI CHAR(30),
	CIUTAT_DPT CHAR(20),
	PRIMARY KEY (NUM_DPT)
);

CREATE TABLE PROJECTES (
	NUM_PROJ INTEGER,
	NOM_PROJ CHAR(10),
	PRODUCTE CHAR(20),
	PRESSUPOST INTEGER,
	PRIMARY KEY (NUM_PROJ)
);

CREATE TABLE EMPLEATS (
	NUM_EMPL INTEGER,
	NOM_EMPL CHAR(30),
	SOU INTEGER,
	CIUTAT_EMPL CHAR(20),
	NUM_DPT INTEGER,
	NUM_PROJ INTEGER,
	PRIMARY KEY (NUM_EMPL),
	FOREIGN KEY (NUM_DPT) REFERENCES DEPARTAMENTS (NUM_DPT),
	FOREIGN KEY (NUM_PROJ) REFERENCES PROJECTES (NUM_PROJ)
);

-- Qüestió 1 ---

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES (5, 'VENDES', 3, 'MUNTANER', 'MADRID');
INSERT INTO EMPLEATS VALUES (3, 'MANEL', 250000, 'MADRID',5, NULL);

/* Obté el número i el nom de cada departament situat a Madrid que té algun
   empleat que guanya més de 200.000 €. */

SELECT DISTINCT d.num_dpt, d.nom_dpt
FROM departaments d, empleats e
WHERE e.num_dpt = d.num_dpt
AND d.ciutat_dpt = 'MADRID'
AND e.sou > 200000;

-- XXX
SELECT d.num_dpt, d.nom_dpt
FROM departaments d
WHERE d.ciutat_dpt = 'MADRID'
AND EXISTS (
	SELECT *
	FROM empleats e
	WHERE e.num_dpt = d.num_dpt
	AND e.sou > 200000
);

-- Qüestió 2 ---

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES (3, 'MARKETING', 3,'RIOS ROSAS', 'BARCELONA');
INSERT INTO PROJECTES VALUES (2, 'IBDVID', 'VIDEO', 500000);
INSERT INTO EMPLEATS VALUES (2, 'ROBERTO', 25000, 'BARCELONA', 3, 2);

/* Obté el nom del departament on treballa i el nom del projecte on està
   assignat l'empleat número 2. */

SELECT d.nom_dpt, p.nom_proj
FROM empleats e, departaments d, projectes p
WHERE e.num_dpt = d.num_dpt
AND e.num_proj = p.num_proj
AND e.num_empl = 2;

-- Qüestió 3 ---

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES (5, 'VENDES' ,3, 'MUNTANER', 'MADRID');
INSERT INTO EMPLEATS VALUES (3, 'MANEL', 250000, 'MADRID', 5, NULL);

/* Obté el número, el nom i la mitjana dels sous dels empleats que hi treballen
   de cada departament situat a Madrid. */

-- XXX Cal d.nom_dpt si sabem que és únic per a cada d.num_dpt?
SELECT d.num_dpt, d.nom_dpt, AVG(e.sou) AS sou
FROM departaments d, empleats e
WHERE d.num_dpt = e.num_dpt
AND d.ciutat_dpt = 'MADRID'
GROUP BY d.num_dpt;
