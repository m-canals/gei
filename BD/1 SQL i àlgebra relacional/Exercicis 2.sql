CREATE TABLE DEPARTAMENTS (
	NUM_DPT INTEGER,
	NOM_DPT CHAR(20),
	PLANTA INTEGER,
	EDIFICI CHAR(30),
	CIUTAT_DPT CHAR(20),
	PRIMARY KEY (NUM_DPT)
);

CREATE TABLE PROJECTES (
	NUM_PROJ INTEGER,
	NOM_PROJ CHAR(10),
	PRODUCTE CHAR(20),
	PRESSUPOST INTEGER,
	PRIMARY KEY (NUM_PROJ)
);

CREATE TABLE EMPLEATS (
	NUM_EMPL INTEGER,
	NOM_EMPL CHAR(30),
	SOU INTEGER,
	CIUTAT_EMPL CHAR(20),
	NUM_DPT INTEGER,
	NUM_PROJ INTEGER,
	PRIMARY KEY (NUM_EMPL),
	FOREIGN KEY (NUM_DPT) REFERENCES DEPARTAMENTS (NUM_DPT),
	FOREIGN KEY (NUM_PROJ) REFERENCES PROJECTES (NUM_PROJ)
);

-- Qüestió 1 --

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES (3, 'MARKETING', 3, 'RIOS ROSAS', 'MADRID');
INSERT INTO PROJECTES VALUES (1, 'IBDTEL', 'TELEVISIO', 1000000);

/* Obté el número i el nom dels departaments que no tenen cap empleat que visqui
   a Madrid. */

SELECT d.num_dpt, d.nom_dpt
FROM departaments d
WHERE NOT EXISTS (
	SELECT *
	FROM empleats e
	WHERE e.num_dpt = d.num_dpt
	AND e.ciutat_empl = 'MADRID'
);

-- Qüestió 2 --

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES (5, 'VENDES', 3, 'CASTELLANA', 'MADRID');
INSERT INTO EMPLEATS VALUES (1, 'MANEL', 250000, 'MADRID', 5, NULL);
INSERT INTO EMPLEATS VALUES (3, 'JOAN', 25000, 'GIRONA', 5, NULL);

/* Obté els noms de les ciutats on hi viuen empleats però no hi ha cap
   departament. */

SELECT DISTINCT e.ciutat_empl
FROM empleats e
WHERE NOT EXISTS (
	SELECT *
	FROM departaments d
	WHERE d.ciutat_dpt = e.ciutat_empl
);

-- Qüestió 3 --

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES(3, 'MARKETING', 1, 'EDIFICI1', 'SABADELL');
INSERT INTO EMPLEATS VALUES (4, 'JOAN', 30000, 'BARCELONA', 3, NULL);
INSERT INTO EMPLEATS VALUES (5, 'PERE', 25000, 'MATARO', 3, NULL);

/* Obté el número i el nom dels departaments que tenen dos o més empleats que
   viuen a ciutats diferents. */

SELECT d.num_dpt, d.nom_dpt
FROM departaments d
WHERE (
	SELECT COUNT(DISTINCT e.ciutat_empl)
	FROM empleats e
	WHERE e.num_dpt = d.num_dpt
) >= 2;

-- Qüestió 4 --

/* Crea les taules següents:

    VENDES(NUM_VENDA, NUM_EMPL, CLIENT)
    PRODUCTES_VENUTS(NUM_VENDA, PRODUCTE, QUANTITAT, IMPORT)

   On cada fila de la taula vendes representa una venda que ha fet un empleat a
   un client i cada fila de la taula productes_venuts representa una quantitat
   de producte venut en una venda, amb un cert import.

   Cal tenir en compte que:
   
    - No hi pot haver dues vendes amb el mateix número de venda.
    - Un empleat només li pot fer una única venda a un mateix client.
    - Una venda l'ha de fer un empleat que existeixi a la base de dades
    - No hi pot haver dues vegades un mateix producte en una mateixa venda.
    - La venda d'un producte venut ha d'existir a la base de dades.
    - La quantitat de producte venut no pot ser nul·la i té 1 com a valor per
      defecte.
    - Els atributs num_venda, quantitat, import són enters.
    - Els atributs client, producte són char(30), i char(20) respectivament. */

CREATE TABLE VENDES
(
    NUM_VENDA INTEGER,
    NUM_EMPL INTEGER NOT NULL,
    CLIENT CHAR(30),
    PRIMARY KEY (NUM_VENDA),
    FOREIGN KEY (NUM_EMPL) REFERENCES EMPLEATS (NUM_EMPL),
    UNIQUE(NUM_EMPL, CLIENT)
);

CREATE TABLE PRODUCTES_VENUTS
(
    NUM_VENDA INTEGER,
    PRODUCTE CHAR(20),
    QUANTITAT INTEGER DEFAULT 1 NOT NULL,
    IMPORT INTEGER,
    PRIMARY KEY (NUM_VENDA, PRODUCTE),
    FOREIGN KEY (NUM_VENDA) REFERENCES VENDES (NUM_VENDA)
);
