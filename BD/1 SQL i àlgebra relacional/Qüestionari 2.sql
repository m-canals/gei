CREATE TABLE DEPARTAMENTS (
	NUM_DPT INTEGER,
	NOM_DPT CHAR(20),
	PLANTA INTEGER,
	EDIFICI CHAR(30),
	CIUTAT_DPT CHAR(20),
	PRIMARY KEY (NUM_DPT)
);

CREATE TABLE PROJECTES (
	NUM_PROJ INTEGER,
	NOM_PROJ CHAR(10),
	PRODUCTE CHAR(20),
	PRESSUPOST INTEGER,
	PRIMARY KEY (NUM_PROJ)
);

CREATE TABLE EMPLEATS (
	NUM_EMPL INTEGER,
	NOM_EMPL CHAR(30),
	SOU INTEGER,
	CIUTAT_EMPL CHAR(20),
	NUM_DPT INTEGER,
	NUM_PROJ INTEGER,
	PRIMARY KEY (NUM_EMPL),
	FOREIGN KEY (NUM_DPT) REFERENCES DEPARTAMENTS (NUM_DPT),
	FOREIGN KEY (NUM_PROJ) REFERENCES PROJECTES (NUM_PROJ)
);

-- Qüestió 1 ---

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES (3, 'MARKETING', 2, 'PAU CLARIS', 'BARCELONA');
INSERT INTO EMPLEATS VALUES (3, 'ROBERTO', 25000, 'MADRID', 3, NULL);
INSERT INTO EMPLEATS VALUES (4, 'JOAN', 30000, 'BARCELONA', 3, NULL);

/* Obté els noms dels empleats amb el sou més alt, ordenats descendenment. */

SELECT DISTINCT nom_empl
FROM empleats
WHERE sou = (SELECT MAX(sou) FROM empleats)
ORDER BY nom_empl DESC;

-- Qüestió 2 ---

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO DEPARTAMENTS VALUES (3, 'MARKETING', 2, 'PAU CLARIS', 'BARCELONA');
INSERT INTO PROJECTES VALUES (1, 'IBDTEL', 'TELEVISIO', 1000000);
INSERT INTO EMPLEATS VALUES (4, 'JOAN', 30000, 'BARCELONA', 3, 1);
INSERT INTO EMPLEATS VALUES (5, 'PERE', 25000, 'MATARO', 3, 1);

/* Obté el número i els nom de cada projecte que té assignats dos o més
   empleats. Ordena el resultat descendement per número de projecte. */

SELECT p.num_proj, p.nom_proj
FROM empleats e, projectes p
WHERE e.num_proj = p.num_proj
GROUP BY p.num_proj
HAVING COUNT(*) >= 2
ORDER BY p.num_proj DESC;

SELECT p.num_proj, p.nom_proj
FROM projectes p
WHERE (
	SELECT COUNT(*)
	FROM empleats e
	WHERE e.num_proj = p.num_proj
) >= 2
ORDER BY p.num_proj DESC;

-- Qüestió 3 ---

DELETE FROM EMPLEATS;
DELETE FROM PROJECTES;
DELETE FROM DEPARTAMENTS;
INSERT INTO PROJECTES VALUES (3, 'PR1123', 'TELEVISIO', 600000);
INSERT INTO DEPARTAMENTS VALUES (4, 'MARKETING', 3, 'RIOS ROSAS', 'BARCELONA');
INSERT INTO EMPLEATS VALUES (3, 'ROBERTO', 25000, 'MATARO', 4, 3);

/* Incrementa en 500.000 el pressupost dels projectes que tenen algun empleat
   que treballa a Barcelona. */

UPDATE projectes p
SET pressupost = pressupost + 500000
WHERE EXISTS (
 SELECT *
 FROM empleats e, departaments d
 WHERE e.num_dpt = d.num_dpt
 AND e.num_proj = p.num_proj
 AND d.ciutat_dpt = 'BARCELONA'
);
