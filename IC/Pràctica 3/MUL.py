#!/usr/bin/env python2.7
# -*- encoding: utf-8 -*-

import sys

def print_data(j, m, w, d, b):
    values = (m, w, d, b)
    strings = map(lambda value: format(value, '0%db' % n), values)
    print str(j) + ' ' * (1 + n/10 - len(str(j))) + ' | ' +  (' | ').join(strings)

base = int(sys.argv[3])
x = int(sys.argv[1], base)
y = int(sys.argv[2], base)
n = 16
w = 0
d = x
b = y

# Valors d'entrada no representables.
d %= 2 ** n
b %= 2 ** n

print_data('', 0, w, d, b)

for j in range(n):
    m = d * (b % 2) 
    w += m
    # Suma no representable.
    w %= 2 ** n
    d <<= 1
    # Desplaçament no representable.
    d %= 2 ** n
    b >>= 1
    print_data(j, m, w, d, b)

print
print "%d x %d = %d" % (x, y, x * y),
print x * y == w and '=' or '!=', w
