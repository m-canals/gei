Alguns exercicis de la primavera de 2021 de l'assignatura Teoria de la Computació (TC) del Grau en Enginyeria Informàtica (GEI) de la Facultat de Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web de l'assignatura](https://www.cs.upc.edu/~alvarez/tc.html).
