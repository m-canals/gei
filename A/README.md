Treball de la primavera de 2021 de l'assignatura Algorísmia (A) del Grau en Enginyeria Informàtica (GEI) de la Facultat de Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web de l'assignatura](https://www.cs.upc.edu/~mjserna/docencia/grauA/alg-GEI.html).
