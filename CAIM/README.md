Pràctiques de la tardor de 2021 de l'assignatura Cerca i Anàlisi d'Informació Massiva (CAIM) del Grau en Enginyeria Informàtica (GEI) de la Facultat d'Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web de l'assignatura](https://www.cs.upc.edu/~caim/index.html).
