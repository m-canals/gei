cursos(Cursos):-
	numCursos(Cursos).
curs(Curs):-
	cursos(Cursos),
	between(1, Cursos, Curs).

assignatures(Assignatures):-
	numAssignatures(Assignatures).
assignatura(Assignatura):-
	assignatures(Assignatures),
	between(1, Assignatures, Assignatura).

aules(Aules):-
	numAules(Aules).
aula(Aula):-
	aules(Aules),
	between(1, Aules, Aula).

professors(Professors):-
	numProfes(Professors).
professor(Professor):-
	professors(Professors),
	between(1, Professors, Professor).

% Una setmana consta de 5 dies lectius.
dies(5).
dia(Dia):-
	dies(Dies),
	between(1, Dies, Dia).

% Un dia consta de 12 hores lectives.
hores_diaries(12).
hora_diaria(Dia, Hora):-
	dia(Dia),
	hores_diaries(Hores),
	Min is (Dia - 1 ) * Hores + 1,
	Max is Min + Hores - 1,
	between(Min, Max, Hora).
hora_del_dia(Hora, HoraDelDia):-
	hora(Hora),
	hores_diaries(Hores),
	HoraDelDia is ((Hora - 1) mod Hores) + 1.

hores(Hores):-
	dies(Dies),
	hores_diaries(HoresDiaries),
	Hores is Dies * HoresDiaries.
hora(Hora):-
	hores(Hores),
	between(1, Hores, Hora).

curs_assignatura(Assignatura, Curs):-
	assig(Curs, Assignatura, _, _, _).
hores_assignatura(Assignatura, Hores):-
	assig(_, Assignatura, Hores, _, _).
aula_assignatura(Assignatura, Aula):-
	assig(_, Assignatura, _, Aules, _),
	member(Aula, Aules).
professor_assignatura(Assignatura, Professor):-
	assig(_, Assignatura, _, _, Professors),
	member(Professor, Professors).
hora_prohibida_professor(Professor, Hora):-
	horesProhibides(Professor, Hores),
	member(Hora, Hores).

% Define SAT Variables
% =============================================================================

% Com que totes les sessions d'una assignatura tenen el mateix professor,
% es fan a la mateixa aula i com a molt n'hi ha una al dia,
% les diferenciem pel dia.

% Una assignatura s'imparteix un dia.
satVariable(quinDia(Assignatura, Dia)):-
	assignatura(Assignatura),
	dia(Dia).

% Una assignatura s'imparteix una hora de la setmana.
satVariable(quinaHora(Assignatura, Hora)):-
	assignatura(Assignatura),
	hora(Hora).

% Una asignatura es impartida per un professor.
satVariable(qui(Assignatura, Professor)):-
	professor_assignatura(Assignatura,Professor).

% Una assignatura s'imparteix en una aula.
satVariable(on(Assignatura, Aula)):-
	aula_assignatura(Assignatura,Aula).

% Generate Clauses
% =============================================================================

writeClauses:- 
	si_hora_llavors_dia_de_la_hora,
	si_dia_llavors_alguna_hora_del_dia,
	una_assignatura_es_impartida_exactament_tants_dies,
	dues_sessions_no_son_impartides_al_mateix_dia,
	una_assignatura_es_impartida_exactament_en_una_aula,
	una_assignatura_es_impartida_exactament_per_un_professor,
	un_professor_no_imparteix_una_assignatura_en_una_hora_prohibida,
	un_professor_no_imparteix_dues_assignatures_diferents_a_la_mateixa_hora,
	en_una_aula_no_se_imparteix_dues_assignatures_diferents_a_la_mateixa_hora,
	un_curs_es_impartit_consecutivament,
	un_curs_es_impartit_com_a_molt_sis_hores_diaries,
	un_curs_es_impartit_sense_solapament,
	true, !.
writeClauses:- told, nl, write('writeClauses failed!'), nl, nl, halt.

% Si una assignatura s'imparteix durant una hora,
% s'imparteix el dia corresponent a aquella hora.
si_hora_llavors_dia_de_la_hora:-
	assignatura(Assignatura),
	hora_diaria(Dia, Hora),
	writeClause([-quinaHora(Assignatura, Hora), quinDia(Assignatura, Dia)]),
	fail.
si_hora_llavors_dia_de_la_hora.

% Si una assignatura s'imparteix un dia,
% s'imparteix durant alguna hora del dia.
si_dia_llavors_alguna_hora_del_dia:-
	assignatura(Assignatura),
	dia(Dia),
	findall(
		quinaHora(Assignatura, Hora),
		(hora_diaria(Dia, Hora)),
		Literals),
	writeClause([-quinDia(Assignatura, Dia) | Literals]),
	fail.
si_dia_llavors_alguna_hora_del_dia.

% Si una assignatura s'imparteix una hora d'un dia,
% no s'imparteix en cap altra hora del dia en qüestió.
dues_sessions_no_son_impartides_al_mateix_dia:-
	assignatura(Assignatura),
	hora_diaria(Dia, Hora1), hora_diaria(Dia, Hora2), Hora1 < Hora2,
	writeClause([-quinaHora(Assignatura, Hora1), -quinaHora(Assignatura, Hora2)]),
	fail.
dues_sessions_no_son_impartides_al_mateix_dia.

% Com que una assignatura s'imparteix com a molt una hora al dia,
% s'imparteix exactament tants dies com hores setmanals indicades.
una_assignatura_es_impartida_exactament_tants_dies:-
	hores_assignatura(Assignatura, Hores),
	findall(
		quinDia(Assignatura, Dia),
		(dia(Dia)),
		Literals),
	exactly(Hores, Literals),
	fail.
una_assignatura_es_impartida_exactament_tants_dies.

% Una assignatura s'imparteix exactament en una aula on es pot impartir.
una_assignatura_es_impartida_exactament_en_una_aula:-
	assignatura(Assignatura),
	findall(
		on(Assignatura, Aula),
		(aula_assignatura(Assignatura, Aula)),
		Literals),
	exactly(1, Literals),
	fail.
una_assignatura_es_impartida_exactament_en_una_aula.

% Una assignatura la imparteix exactament un professor que la pot impartir.
una_assignatura_es_impartida_exactament_per_un_professor:-
	assignatura(Assignatura),
	findall(
		qui(Assignatura, Professor),
		(professor_assignatura(Assignatura, Professor)),
		Literals),
	exactly(1, Literals),
	fail.
una_assignatura_es_impartida_exactament_per_un_professor.

% Un professor no imparteix una assignatura que s'imparteix en una hora
% en la qual no pot impartir assignatures.
un_professor_no_imparteix_una_assignatura_en_una_hora_prohibida:-
	professor_assignatura(Assignatura, Professor),
	hora_prohibida_professor(Professor, Hora),
	writeClause([
		-qui(Assignatura, Professor), -quinaHora(Assignatura, Hora)]),
	fail.
un_professor_no_imparteix_una_assignatura_en_una_hora_prohibida.

% Un professor no imparteix dues assignatures diferents
% que s'imparteixen a la mateixa hora.
un_professor_no_imparteix_dues_assignatures_diferents_a_la_mateixa_hora:-
	professor_assignatura(Assignatura1, Professor),
	professor_assignatura(Assignatura2, Professor),
	Assignatura1 \= Assignatura2,
	hora(Hora),
	writeClause([
		-qui(Assignatura1, Professor), -quinaHora(Assignatura1, Hora),
		-qui(Assignatura2, Professor), -quinaHora(Assignatura2, Hora)]),
	fail.
un_professor_no_imparteix_dues_assignatures_diferents_a_la_mateixa_hora.

% En una aula no s'imparteixen dues assignatures diferents
% que s'imparteixen a la mateixa hora.
en_una_aula_no_se_imparteix_dues_assignatures_diferents_a_la_mateixa_hora:-
	aula_assignatura(Assignatura1, Aula),
	aula_assignatura(Assignatura2, Aula),
	Assignatura1 \= Assignatura2,
	hora(Hora),
	writeClause([
		-on(Assignatura1, Aula), -quinaHora(Assignatura1, Hora),
		-on(Assignatura2, Aula), -quinaHora(Assignatura2, Hora)]),
	fail.
en_una_aula_no_se_imparteix_dues_assignatures_diferents_a_la_mateixa_hora.

% En un dia, les assignatures d'un curs s'imparteixen en hores consecutives,
% és a dir, si A1 i A2 són dues assignatures del mateix curs que s'imparteixen
% el mateix dia en hores no consecutives, A1 més aviat que A2, llavors hi ha
% una assignatura A3 del mateix curs que s'imparteix immediatament després que
% A1 i una assignatura A4 del mateix curs que s'imparteix immediatament abans
% que A2; si hi ha una diferència d'una hora entre que A1 acaba i A2 comença,
% A3 i A4 són la mateixa assignatura.
un_curs_es_impartit_consecutivament:-
	curs_assignatura(Assignatura1, Curs), hora_diaria(Dia, Hora1),
	curs_assignatura(Assignatura2, Curs), hora_diaria(Dia, Hora2),
	Hora1 < Hora2 - 1,
	negate(quinaHora(Assignatura1, Hora1), Literal1),
	negate(quinaHora(Assignatura2, Hora2), Literal2),
	findall(
		quinaHora(Assignatura3, Hora3),
		(curs_assignatura(Assignatura3, Curs), Hora3 is Hora1 + 1),
		Literals1),
	findall(
		quinaHora(Assignatura4, Hora4),
		(curs_assignatura(Assignatura4, Curs), Hora4 is Hora2 - 1),
		Literals2),
	writeClause([Literal1, Literal2 | Literals1]),
	writeClause([Literal1, Literal2 | Literals2]),
	fail.
un_curs_es_impartit_consecutivament.

% En un dia, un curs no té més de sis hores de classe; de manera equivalent,
% en un dia, un curs no té més de sis assignatures, ja que una assignatura
% s'imparteix com a molt una hora al dia.
un_curs_es_impartit_com_a_molt_sis_hores_diaries:-
	curs(Curs), dia(Dia),
	findall(
		quinDia(Assignatura, Dia),
		(curs_assignatura(Assignatura, Curs)),
		Literals),
	atMost(6, Literals),
	fail.
un_curs_es_impartit_com_a_molt_sis_hores_diaries.

% Dues assignatures del mateix curs no s'imparteixen durant la mateixa hora.
un_curs_es_impartit_sense_solapament:-
	curs_assignatura(Assignatura1, Curs),
	curs_assignatura(Assignatura2, Curs),
	Assignatura1 \= Assignatura2,
	hora(Hora),
	writeClause([-quinaHora(Assignatura1, Hora), -quinaHora(Assignatura2, Hora)]),
	fail.
un_curs_es_impartit_sense_solapament.

% Display Solution
% =============================================================================

number_length(Number, Length):-
	number_string(Number, String),
	string_length(String, Length).

write_times(_, Times):-
	Times < 1.
write_times(String, Times):-
	Times >= 1,
	write(String),
	LessTimes is Times - 1,
	write_times(String, LessTimes).

write_number(Number, PaddingString, MinimumLength):-
	number_length(Number, NumberLength),
	PaddingLength is MinimumLength - NumberLength,
	write(Number),
	write_times(PaddingString, PaddingLength).

write_table_border_top([FirstWidth | Widths]):-
	write(" ╭"),
	write_times("─", FirstWidth),
	member(Width, Widths),
	write("┬"),
	write_times("─", Width),
	fail.
write_table_border_top(_):-
	write("╮\n").

write_table_border_center([FirstWidth | Widths]):-
	write(" ├"),
	write_times("─", FirstWidth),
	member(Width, Widths),
	write("┼"),
	write_times("─", Width),
	fail.
write_table_border_center(_):-
	write("┤\n").

write_table_border_bottom([FirstWidth | Widths]):-
	write(" ╰"),
	write_times("─", FirstWidth),
	member(Width, Widths),
	write("┴"),
	write_times("─", Width),
	fail.
write_table_border_bottom(_):-
	write("╯\n").
	
% -----------------------------------------------------------------------------

escriu_hora(Hora):-
	Hora > 1,
	hora_del_dia(Hora, 1),
	!,
	write(" │ "),
	write_number(Hora, " ", 2).
escriu_hora(Hora):-
	write(" "),
	write_number(Hora, " ", 2).

escriu_hores:-
	hora(Hora),
	escriu_hora(Hora),
	fail.
escriu_hores:-
	write(" │\n").

escriu_assignatura_hora2(Assignatura):-
	number(Assignatura),
	!,
	write_number(Assignatura, " ", 2).
escriu_assignatura_hora2(ninguna):-
	write("  ").

escriu_assignatura_hora(Hora, Assignatura):-
	Hora > 1,
	hora_del_dia(Hora, 1),
	!,
	write(" │ "),
	escriu_assignatura_hora2(Assignatura).
escriu_assignatura_hora(_, Assignatura):-
	write(" "),
	escriu_assignatura_hora2(Assignatura).

escriu_text_hora(Hora, Text):-
	Hora > 1,
	hora_del_dia(Hora, 1),
	!,
	write(" │ "),
	write(Text).
escriu_text_hora(_, Text):-
	write(" "),
	write(Text).
	

% Taula 1
% -----------------------------------------------------------------------------

escriu_aula(Aula):-
	write(" │ "),
	write_number(Aula, " ", 2),
	write("          │").

% Si no n'hi ha cap no escriu res.
escriu_assignatures_aula_hora(Hora, []):-
	escriu_assignatura_hora(Hora, ninguna),
	!.

% Si n'hi ha una ho escriu de color verd.
escriu_assignatures_aula_hora(Hora, [Assignatura]):-
	write("\033[32m"),
	escriu_assignatura_hora(Hora, Assignatura),
	write("\033[0m"),
	!.

% Si n'hi ha més d'una ho escriu de color vermell.
escriu_assignatures_aula_hora(Hora, Assignatures):-
	write("\033[31m"),
	member(Assignatura, Assignatures),
	write(" "),
	escriu_assignatura_hora(Hora, Assignatura),
	fail.
escriu_assignatures_aula_hora(_, _):-
	write("\033[0m").

escriu_assignatures_aula(Model, Aula):-
	hora(Hora),
	findall(Assignatura, (
		member(on(Assignatura, Aula), Model),
		member(quinaHora(Assignatura, Hora), Model)), Assignatures),
	escriu_assignatures_aula_hora(Hora, Assignatures),
	fail.
escriu_assignatures_aula(_, _):-
	write(" │\n").

escriu_taula_1(Model):-
	write_table_border_top([13, 37, 37, 37, 37, 37]),
	write(" │ Aula ╲ Hora │"),
	escriu_hores,
	write_table_border_center([13, 37, 37, 37, 37, 37]),
	aula(Aula),
	escriu_aula(Aula),
	escriu_assignatures_aula(Model, Aula),
	fail.
escriu_taula_1(_):-
	write_table_border_bottom([13, 37, 37, 37, 37, 37]).

% Taula 2
% -----------------------------------------------------------------------------

escriu_professor(Professor):-
	write(" │ "),
	write_number(Professor, " ", 2),
	write("               │").

% Si no n'hi ha cap no escriu res.
escriu_assignatures_professor_hora(Hora, []):-
	escriu_assignatura_hora(Hora, ninguna),
	!.

% Si n'hi ha una ho escriu de color verd.
escriu_assignatures_professor_hora(Hora, [Assignatura]):-
	write("\033[32m"),
	escriu_assignatura_hora(Hora, Assignatura),
	write("\033[0m"),
	!.

% Si n'hi ha més d'una ho escriu de color vermell.
escriu_assignatures_professor_hora(Hora, Assignatures):-
	write("\033[31m"),
	member(Assignatura, Assignatures),
	write(" "),
	escriu_assignatura_hora(Hora, Assignatura),
	fail.
escriu_assignatures_professor_hora(_):-
	write("\033[0m").

escriu_assignatures_professor(Model, Professor):-
	hora(Hora),
	findall(Assignatura, (
		member(qui(Assignatura, Professor), Model),
		member(quinaHora(Assignatura, Hora), Model)), Assignatures),
	escriu_assignatures_professor_hora(Hora, Assignatures),
	fail.
escriu_assignatures_professor(_, _):-
	write(" │\n").

escriu_taula_2(Model):-
	write_table_border_top([18, 37, 37, 37, 37, 37]),
	write(" │ Professor ╲ Hora │"),
	escriu_hores,
	write_table_border_center([18, 37, 37, 37, 37, 37]),
	professor(Professor),
	escriu_professor(Professor),
	escriu_assignatures_professor(Model, Professor),
	fail.
escriu_taula_2(_):-
	write_table_border_bottom([18, 37, 37, 37, 37, 37]).

% Taula 3
% -----------------------------------------------------------------------------

escriu_curs(Curs):-
	write(" │ "),
	write_number(Curs, " ", 2),
	write("          │").

% Si no n'hi ha cap no escriu res.
escriu_assignatures_curs_hora(Hora, []):-
	escriu_assignatura_hora(Hora, ninguna),
	!.

% Si n'hi ha una ho escriu de color verd.
escriu_assignatures_curs_hora(Hora, [Assignatura]):-
	write("\033[32m"),
	escriu_assignatura_hora(Hora, Assignatura),
	write("\033[0m"),
	!.

% Si n'hi ha més d'una ho escriu de color vermell.
escriu_assignatures_curs_hora(Hora, Assignatures):-
	write("\033[31m"),
	member(Assignatura, Assignatures),
	write(" "),
	escriu_assignatura_hora(Hora, Assignatura),
	fail.
escriu_assignatures_curs_hora(_):-
	write("\033[0m").

escriu_assignatures_curs(Model, Curs):-
	hora(Hora),
	findall(Assignatura, (
		curs_assignatura(Assignatura, Curs),
		member(quinaHora(Assignatura, Hora), Model)), Assignatures),
	escriu_assignatures_curs_hora(Hora, Assignatures),
	fail.
escriu_assignatures_curs(_, _):-
	write(" │\n").

escriu_taula_3(Model):-
	write_table_border_top([13, 37, 37, 37, 37, 37]),
	write(" │ Curs ╲ Hora │"),
	escriu_hores,
	write_table_border_center([13, 37, 37, 37, 37, 37]),
	curs(Curs),
	escriu_curs(Curs),
	escriu_assignatures_curs(Model, Curs),
	fail.
escriu_taula_3(_):-
	write_table_border_bottom([13, 37, 37, 37, 37, 37]).

% Taula 4
% -----------------------------------------------------------------------------

escriu_assignatura(Assignatura):-
	write(" │ "),
	write_number(Assignatura, " ", 2),
	write("            │").

escriu_sessio_assignatura(Model, Assignatura, Hora):-
	member(quinaHora(Assignatura, Hora), Model),
	escriu_text_hora(Hora, "🞪 "),
	!.
escriu_sessio_assignatura(_, _, Hora):-
	escriu_text_hora(Hora, "  ").

escriu_sessions_assignatura(Model, Assignatura):-
	hora(Hora),
	escriu_sessio_assignatura(Model, Assignatura, Hora),
	fail.
escriu_sessions_assignatura(_, _):-
	write(" │\n").

escriu_taula_4(Model):-
	write_table_border_top([15, 37, 37, 37, 37, 37]),
	write(" │ Assig. ╲ Hora │"),
	escriu_hores,
	write_table_border_center([15, 37, 37, 37, 37, 37]),
	assignatura(Assignatura),
	escriu_assignatura(Assignatura),
	escriu_sessions_assignatura(Model, Assignatura),
	fail.
escriu_taula_4(_):-
	write_table_border_bottom([15, 37, 37, 37, 37, 37]).
	
% -----------------------------------------------------------------------------

displaySol(Model):-
	% en_una_aula_no_se_imparteix_dues_assignatures_diferents_a_la_mateixa_hora
	% [SEPARAR-HO] una_assignatura_es_impartida_exactament_en_una_aula
	escriu_taula_1(Model),
	% una_assignatura_es_impartida_exactament_per_un_professor,
	% [SEPARAR-HO] un_professor_no_imparteix_dues_assignatures_diferents_a_la_mateixa_hora
	escriu_taula_2(Model),
	% un_curs_es_impartit_consecutivament
	% un_curs_es_impartit_com_a_molt_sis_hores_diaries
	% un_curs_es_impartit_sense_solapament
	escriu_taula_3(Model),
	% [POTSER SERIA MILLOR EN LLISTES]
	% una_assignatura_es_impartida_exactament_tants_dies
	% dues_sessions_no_son_impartides_al_mateix_dia
	escriu_taula_4(Model).

