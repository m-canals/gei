display_solution_ascii(0).

color(Color):-
	c(Color, _, _, _, _).
mida(Mida):-
	size(Mida).
columna(Columna):-
	mida(Columnes),
	between(1, Columnes, Columna).
fila(Fila):-
	mida(Files),
	between(1, Files, Fila).
casella(Fila, Columna):-
	fila(Fila),
	columna(Columna).
extrem(Fila, Columna, Color):-
	c(Color, Fila, Columna, _, _).
extrem(Fila, Columna, Color):-
	c(Color, _, _, Fila, Columna).
no_extrem(Fila, Columna):-
	casella(Fila, Columna),
	\+ extrem(Fila, Columna, _).
adjacents(Fila1, Columna1, Fila2, Columna2):-
	casella(Fila1, Columna1),
	casella(Fila2, Columna2),
	member([I, J], [[0, 1], [0, -1], [1, 0], [-1, 0]]),
	Fila2 is Fila1 + I,
	Columna2 is Columna1 + J.

% Define SAT Variables
% =============================================================================

satVariable(connectada_a(Fila1, Columna1, Fila2, Columna2)):-
	casella(Fila1, Columna1),
	casella(Fila2, Columna2).

satVariable(de_color(Fila, Columna, Color)):-
	casella(Fila, Columna),
	color(Color).

% Generate Clauses
% =============================================================================

writeClauses:-
	la_connexio_es_bidireccional,
	dues_caselles_connectades_son_del_mateix_color,
	una_casella_que_es_un_extrem_es_del_color_indicat,
	una_casella_es_exactament_de_un_color,
	una_casella_que_es_un_extrem_esta_connectada_a_exactament_una_casella,
	una_casella_que_no_es_un_extrem_esta_connectada_a_exactament_dues_caselles,
	true, !.
writeClauses:- told, nl, write('writeClauses failed!'), nl, nl, halt.

% Si els dos extrems d'un color no són el mateix extrem,
% un extrem té exactament una connexió.
% Cadascuna de les altres caselles té exactament dues connexions:
% si en tingués una seria un extrem i si en tingués més de dues
% hi hauria una superposició. D'aquesta manera
% totes les caselles estan connectades, però hi  podria haver caselles
% connectades formant un cicle, en que totes tindrien dues connexions
% però no tindrien color, fet que contradiria la restricció que
% estableix que totes les caselles han de tenir un color;
% per tant no hi ha aquests cicles i les solucions són correctes.

% TODO Optimització: es podria aprofitar
% que la relació de connexió és simètrica
% (la connexió és bidireccional)
% per a provar d'ometre clàusules.

la_connexio_es_bidireccional:-
	adjacents(Fila1, Columna1, Fila2, Columna2),
	writeClause([
		-connectada_a(Fila1, Columna1, Fila2, Columna2),
		connectada_a(Fila2, Columna2, Fila1, Columna1)]),
	fail.
la_connexio_es_bidireccional.

dues_caselles_connectades_son_del_mateix_color:-
	adjacents(Fila1, Columna1, Fila2, Columna2),
	color(Color),
	writeClause([
		-de_color(Fila1, Columna1, Color),
		-connectada_a(Fila1, Columna1, Fila2, Columna2),
		de_color(Fila2, Columna2, Color)]),
	fail.
dues_caselles_connectades_son_del_mateix_color.

una_casella_que_es_un_extrem_es_del_color_indicat:-
	extrem(Fila, Columna, Color),
	writeClause([de_color(Fila, Columna, Color)]),
	fail.
una_casella_que_es_un_extrem_es_del_color_indicat.

una_casella_es_exactament_de_un_color:-
	casella(Fila, Columna),
	findall(
		de_color(Fila, Columna, Color),
		color(Color),
		Literals),
	exactly(1, Literals),
	fail.
una_casella_es_exactament_de_un_color.

una_casella_que_es_un_extrem_esta_connectada_a_exactament_una_casella:-
	extrem(Fila1, Columna2, _),
	findall(
		connectada_a(Fila1, Columna1, Fila2, Columna2),
		adjacents(Fila1, Columna1, Fila2, Columna2),
		Literals),
	exactly(1, Literals),
	fail.
una_casella_que_es_un_extrem_esta_connectada_a_exactament_una_casella.

una_casella_que_no_es_un_extrem_esta_connectada_a_exactament_dues_caselles:-
	no_extrem(Fila1, Columna1),
	findall(
		connectada_a(Fila1, Columna1, Fila2, Columna2),
		adjacents(Fila1, Columna1, Fila2, Columna2),
		Literals),
	exactly(2, Literals),
	fail.
una_casella_que_no_es_un_extrem_esta_connectada_a_exactament_dues_caselles.

% Display Solution
% =============================================================================

colorCode(blue, 69).
colorCode(brown, 138).
colorCode(red, 196).
colorCode(cyan, 51).
colorCode(green, 46).
colorCode(yellow, 226).
colorCode(pink, 201).
colorCode(violet, 90).
colorCode(orange, 208).
colorCode(darkblue, 21).
colorCode(darkgreen, 28).
colorCode(darkred, 88).
colorCode(darkcyan, 30).
colorCode(white, 15).
colorCode(grey, 8).

% ASCII.

writeInputSq(X,Y):-
	c(Color,X,Y,_,_),
	setColor(Color),
	write(' o'),
	!.
writeInputSq(X,Y):-
	c(Color,_,_,X,Y),
	setColor(Color),
	write(' o'),
	!.
writeInputSq(_,_):-
	resetColor,
	write(' ·'),
	!.

setColor(Color):-
	colorCode(Color,Code),
	put(27),
	write('[0;38;5;'),
	write(Code),
	write('m'),
	!.
resetColor:-
	put(27),
	write('[0m'),
	!.

displaySol(_):-
	display_solution_ascii(1),
	nl, 
	write('Input:   '),
	fila(X),
	nl,
	columna(Y),
	writeInputSq(X,Y),
	fail. 
displaySol(M):-
	display_solution_ascii(1),
	nl,
	nl,
	write('Solution:'),
	fila(X),
	nl,
	columna(Y),
	member(de_color(X,Y,Color),M),
	setColor(Color),
	write(' o'),
	fail. 
displaySol(_):-
	display_solution_ascii(1),
	resetColor,
	!.

% UTF-8.

displaySol(Model):-
	display_solution_ascii(0),
	fila(Fila),
	nl,
	columna(Columna),
	member(de_color(Fila, Columna, Color), Model),
	findall(
		[I, J], (
			adjacents(Fila, Columna, FilaAdjacent, ColumnaAdjacent),
			member(connectada_a(Fila, Columna, FilaAdjacent, ColumnaAdjacent), Model),
			I is Fila - FilaAdjacent,
			J is Columna - ColumnaAdjacent),
		CasellesAdjacents),
	sort(CasellesAdjacents, CasellesAdjacentsOrdenades),
	escriu_casella(Color, CasellesAdjacentsOrdenades),
	fail.
displaySol(_).

escriu_casella(Color, []):-
	escriu_de_color(Color, ' • ').
escriu_casella(Color, [[0, 1]]):-
	escriu_de_color(Color, '━╸ ').
escriu_casella(Color, [[0, -1]]):-
	escriu_de_color(Color, ' ╺━').
escriu_casella(Color, [[0, -1], [0, 1]]):-
	escriu_de_color(Color, '━━━').
escriu_casella(Color, [[1, 0]]):-
	escriu_de_color(Color, ' ╹ ').
escriu_casella(Color, [[0, 1], [1, 0]]):-
	escriu_de_color(Color, '━┛ ').
escriu_casella(Color, [[0, -1], [1, 0]]):-
	escriu_de_color(Color, ' ┗━').
escriu_casella(Color, [[0, -1], [0, 1], [1, 0]]):-
	escriu_de_color(Color, '━┻━').
escriu_casella(Color, [[-1, 0]]):-
	escriu_de_color(Color, ' ╻ ').
escriu_casella(Color, [[-1, 0], [0, 1]]):-
	escriu_de_color(Color, '━┓ ').
escriu_casella(Color, [[-1, 0], [0, -1]]):-
	escriu_de_color(Color, ' ┏━').
escriu_casella(Color, [[-1, 0], [0, -1], [0, 1]]):-
	escriu_de_color(Color, '━┳━').
escriu_casella(Color, [[-1, 0], [1, 0]]):-
	escriu_de_color(Color, ' ┃ ').
escriu_casella(Color, [[-1, 0], [0, 1], [1, 0]]):-
	escriu_de_color(Color, '━┫ ').
escriu_casella(Color, [[-1, 0], [0, -1], [1, 0]]):-
	escriu_de_color(Color, ' ┣━').
escriu_casella(Color, [[-1, 0], [0, -1], [0, 1], [1, 0]]):-
	escriu_de_color(Color, '━╋━').

escriu_de_color(Color, String):-
	colorCode(Color, Code),
	write('\033[0;38;5;'),
	write(Code),
	write("m"),
	write(String),
	write('\033[0m').

