row(I):- between(1,9,I).
col(J):- between(1,9,J).
val(K):- between(1,9,K).
blockID(Iid,Jid):- member(Iid,[0,1,2]), member(Jid,[0,1,2]).
squareOfBlock(Iid,Jid,I,J):- row(I), col(J), Iid is (I-1) // 3, Jid is (J-1) // 3.

% Define SAT Variables
% ==============================================================================

% x(I,J,K) means "square IJ gets value K",
% 1<=i<=9, 1<=j<=9, 1<=k<=9 
% 9^3= 729 variables
satVariable( x(I,J,K) ):- row(I), col(J), val(K).

% Generate Clauses
% ==============================================================================

writeClauses:- 
    filledInputValues,         % for each filled-in value of the input, add a unit clause
    eachIJexactlyOneK,         % each square IJ gets exactly one value K
    eachJKexactlyOneI,         % each column J each value K in exactly one row I
    eachIKexactlyOneJ,         % each row    I each value K in exactly one column J
    eachBlockEachKexactlyOnce, % each 3x3 block gets each value K exactly once.
    true, !.                   % this way you can comment out ANY previous line of writeClauses
writeClauses:- told, nl, write('writeClauses failed!'), nl, nl, halt.

filledInputValues:-
	entrada(Sudoku), nth1(I,Sudoku,Row), nth1(J,Row,K), integer(K),
	writeClause([x(I,J,K)]),
	fail.
filledInputValues.

eachIJexactlyOneK:-
	row(I), col(J),
	findall(x(I,J,K), val(K), Literals), exactly(1, Literals),
	fail.
eachIJexactlyOneK.

eachJKexactlyOneI:-
	col(J), val(K),
	findall(x(I,J,K), row(I), Literals), exactly(1, Literals),
	fail.
eachJKexactlyOneI.

eachIKexactlyOneJ:-
	row(I), val(K),
	findall(x(I,J,K), col(J), Literals), exactly(1, Literals),
	fail.
eachIKexactlyOneJ.

eachBlockEachKexactlyOnce:-
	blockID(Iid,Jid), val(K),
	findall(x(I,J,K), squareOfBlock(Iid,Jid,I,J), Literals),
	exactly(1, Literals),
	fail.
eachBlockEachKexactlyOnce.

% Display Solution
% ==============================================================================

displaySol(M):-
	row(I), nl,
	line(I), col(J), space(J),
	member(x(I,J,K), M), write(K), write(' '),
	fail.
displaySol(_).

line(I):-
	member(I,[4,7]), nl, !.
line(_).

space(J):-
	member(J,[4,7]), write(' '), !.
space(_).

