equips(N):-
	numEquipos(N).
equip(E):-
	equips(N),
	between(1, N, E).
equips_diferents(E1, E2):-
	equip(E1),
	equip(E2),
	E1 \= E2.

jornades_per_volta(N):-
	equips(M),
	N is M - 1.
jornades(N):-
	jornades_per_volta(M),
	N is 2 * M.
jornada(J):-
	jornades(N),
	between(1, N, J).
jornades_consecutives(1, [J | []]):-
	jornada(J).
jornades_consecutives(K, [J1, J2 | J]):-
	jornada(J1),
	J2 is J1 + 1,
	jornada(J2),
	K1 is K - 1,
	jornades_consecutives(K1, [J2 | J]).
jornades_de_anada_i_tornada(I, J):-
	jornades_per_volta(N),
	jornada(I),
	jornada(J),
	N is abs(I-J).

partit(I, J, K):-
	equips_diferents(I, J),
	jornada(K).

% Define SAT Variables
% =============================================================================

% Sigui N el nombre d'equips, E1 i E2 dos equips diferents i J una jornada,
% x(E1, E2, J) denota que l'equip E1 juga a casa contra l'equip E2
% la jornada J, per a E1 i E2 entre 1 i N i per a J entre 1 i 2 * (N - 1),
% amb E1 diferent de E2. Hi ha 2 * N * (N - 1) ^ 2 variables.
satVariable(x(E1, E2, J)):-
	partit(E1, E2, J).

% Sigui E un equip i J una jornada, y(E, J) denota que l'equip E juga a casa
% la jornada J, per a E entre 1 i N i per a J entre 1 i 2 * (N - 1).
% Hi ha 2 * N * (N - 1) variables. Observació: si un equip no juga a casa, llavors juga fora.
satVariable(y(E, J)):-
	equip(E),
	jornada(J).

% Sigui E un equip i J1 i J2 dues jornades consecutives, z(E, J2) denota que
% l'equip E juga les dues jornades J1 i J2 a casa o que l'equip E juga les dues
% jornades J1 i J2 fora, per a E entre 1 i N i per a J2 entre 2 i 2 * (N - 1).
% Hi ha N * (2 * (N - 1) - 1) variables.
satVariable(z(E, J)):-
	equip(E),
	jornada(J),
	J > 1.

% Generate Clauses
% =============================================================================

writeClauses:-
	el_equip_local_de_un_partit_juga_a_casa,
	un_equip_que_juga_dues_jornades_consecutives_a_casa_o_fora_repeteix,
	no_casa, % XXX
	no_fora, % XXX
	no_repeticions_de_dues_jornades, % XXX
	% sense_equips_amb_mes_de_k_repeticions_de_dues_jornades(4), % Extensió.
	sense_repeticions_de_k_jornades(3), % Generalització.
	si_partit, % XXX
	no_partit, % XXX
	un_equip_juga_contra_exactament_un_equip_diferent_un_jornada, %XXX
	un_equip_juga_a_casa_contra_un_equip_diferent_exactament_una_jornada, %XXX
	la_tornada_es_en_el_mateix_ordre_que_la_anada, %XXX
	true, !.
writeClauses:- told, nl, write('writeClauses failed!'), nl, nl, halt.

% Siguin E1 i E2 dos equips i J una jornada,
% si l'equip E1 juga a casa contra l'equip E2 la jornada J,
% l'equip E1 juga a casa la jornada J.
% Facilita la implementació de les restriccions de repeticions.
el_equip_local_de_un_partit_juga_a_casa:-
	partit(E1, E2, J),
	writeClause([-x(E1, E2, J), y(E1, J)]),
	fail.
el_equip_local_de_un_partit_juga_a_casa.

% Sigui E un equip i J1 i J2 dues jornades,
% si l'equip E juga a casa les jornades J1 i J2,
% l'equip E repeteix les jornades J1 i J2;
% si l'equip E juga fora les jornades J1 i J2,
% l'equip E repeteix les jornades J1 i J2.
un_equip_que_juga_dues_jornades_consecutives_a_casa_o_fora_repeteix:-
	equip(E), jornades_consecutives(2, [J1,J2]),
	writeClause([-y(E, J1), -y(E, J2), z(E, J2)]),
	writeClause([y(E, J1), y(E, J2), z(E, J2)]),
	fail.
un_equip_que_juga_dues_jornades_consecutives_a_casa_o_fora_repeteix.

% Siguin E1 i E2 dos equips i J una jornada,
% l'equip E1 no vol jugar a casa contra l'equip E2 la jornada J.
no_casa:-
	nocasa(E1, J), equips_diferents(E1, E2),
	writeClause([-x(E1, E2, J)]),
	fail.
no_casa.

% Siguin E1 i E2 dos equips i J una jornada,
% l'equip E1 no vol jugar fora contra l'equip E2 la jornada J.
no_fora:-
	nofuera(E1, J), equips_diferents(E1, E2),
	writeClause([-x(E2, E1, J)]),
	fail.
no_fora.

% Siguin J1 i J2 dues jornades i E un equip,
% l'equip E no juga a casa les dues jornades ni juga fora les dues jornades.
no_repeticions_de_dues_jornades:-
	norepes(J1, J2), equip(E),
	writeClause([-y(E, J1), -y(E, J2)]),
	writeClause([y(E, J1), y(E, J2)]),
	fail.
no_repeticions_de_dues_jornades.

% Sigui E un equip i K un nombre natural,
% l'equip E no té més de K repeticions de dues jornades.
sense_equips_amb_mes_de_k_repeticions_de_dues_jornades(K):-
	equip(E),
	findall(z(E, J), (jornada(J), J > 1), L),
	atMost(K, L), % Cost quadràtic: optimitzable.
	fail.
sense_equips_amb_mes_de_k_repeticions_de_dues_jornades(_).

% Siguin J1, ..., JK K jornades consecutives i E un equip,
% l'equip E no juga a casa les K jornades ni juga fora les K jornades.
sense_repeticions_de_k_jornades(K):-
	jornades_consecutives(K, C), equip(E),
	findall(-y(E, J), member(J, C), L1),
	writeClause(L1),
	% Donades K jornades consecutives, un equip juga a casa en alguna d'aquestes.
	% Per tant, no en juga fora K seguides.
	findall(y(E, J), member(J, C), L2),
	writeClause(L2),
	fail.
sense_repeticions_de_k_jornades(_).

% Siguin E1 i E2 dos equips i J una jornada,
% l'equip E1 juga a casa contra l'equip E2 la jornada J.
si_partit:-
	sipartido(E1, E2, J),
	writeClause([x(E1, E2, J)]),
	fail.
si_partit.

% Siguin E1 i E2 dos equips i J una jornada,
% l'equip E1 no juga a casa contra l'equip E2 la jornada J.
no_partit:-
	nopartido(E1, E2, J),
	writeClause([-x(E1, E2, J)]),
	fail.
no_partit.

% Sigui E1 un equip i J una jornada,
% l'equip E1 juga contra exactament un equip la jornada J,
% bé sigui a casa o fora.
un_equip_juga_contra_exactament_un_equip_diferent_un_jornada:-
	equip(E1), jornada(J),
	findall(x(E1, E2, J), equips_diferents(E1, E2), L1),
	findall(x(E2, E1, J), equips_diferents(E1, E2), L2),
	append(L1, L2, L),
	exactly(1, L),
	fail.
un_equip_juga_contra_exactament_un_equip_diferent_un_jornada.

% Siguin E1 i E2 dos equips,
% l'equip E1 juga contra l'equip E2 a casa exactament una jornada.
un_equip_juga_a_casa_contra_un_equip_diferent_exactament_una_jornada:-
	equips_diferents(E1, E2),
	findall(x(E1, E2, J), jornada(J), L),
	exactly(1, L),
	fail.
un_equip_juga_a_casa_contra_un_equip_diferent_exactament_una_jornada.

% Sigui N el nombre d'equips, E1 i E2 dos equips i J1 i J2 dues jornades,
% si l'equip E1 juga a casa contra l'equip E2 la jornada J1 i
% l'equip E1 juga fora contra l'equip E2 la jornada J2, abs(J1 - J2) = N.
la_tornada_es_en_el_mateix_ordre_que_la_anada:-
	equips_diferents(E1, E2),
	jornades_de_anada_i_tornada(J1, J2),
	writeClause([-x(E1, E2, J1), x(E2, E1, J2)]),
	fail.
la_tornada_es_en_el_mateix_ordre_que_la_anada.

% Display Solution
% =============================================================================

calendari(M):-
	jornada(J),
	write("\nJornada "),
	write(J),
	equips_diferents(E1, E2),
	member(x(E1, E2, J), M),
	write("\t"),
	write(E1 - E2),
	fail.
calendari(_).

displaySol(M):-
	calendari(M).

