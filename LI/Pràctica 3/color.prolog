vertex(V):-
	nodeNum(N),
	between(1, N, V).

% Define SAT Variables
% ==============================================================================

% Sigui V un vèrtex i C un color, x(V, C) denota que el vèrtex V és de color C,
% per a v
satVariable(x(V, C)):-
	vertex(V),
	color(C).

% Generate Clauses
% ==============================================================================

writeClauses:-
	one_color,
	different_color,
	true, !.
writeClauses:- told, nl, write('writeClauses failed!'), nl, nl, halt.

% Cada vèrtex és d'exactament un color.
one_color:-
	vertex(V),
	findall(x(V, C), color(C), L),
	exactly(1, L),
	fail.
one_color.

% Cada parell de vèrtexs adjacents no són del mateix color.
different_color:-
	edge(V, U),
	color(C),
	writeClause([-x(V, C), -x(U, C)]),
	fail.
different_color.

% Display Solution
% ==============================================================================

displaySol(M):-
	vertex(V),
	color(C),
	member(x(V, C), M),
	nl,
	write(V - C),
	fail.
displaySol(_).

