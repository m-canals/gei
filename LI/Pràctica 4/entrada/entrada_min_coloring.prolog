numNodes(15).
adjacency(1, [  2,    5,      9,10,11,12,13,14,15]).
adjacency(2, [1,        6,7,  9,   11,         15]).
adjacency(3, [1,2,  4,5,  7,8,  10,   12,   14   ]).
adjacency(4, [1,2,3,    6,7,  9,   11,   13,   15]).
adjacency(5, [1,2,3,4,    7,8,9                  ]).
adjacency(6, [1,  3,                           15]).
adjacency(7, [  2,    5,        10,         14   ]).
adjacency(8, [          6,7,  9,10,      13      ]).
adjacency(9, [    3,4,5,  7,8,     11            ]).
adjacency(10,[      4,                12,      15]).
adjacency(11,[1,    4,  6,7,    10,      13,14   ]).
adjacency(12,[  2,  4,5,           11,   13,14,15]).
adjacency(13,[1,  3,4,5,              12,   14,15]).
adjacency(14,[            7,8,  10,   12,13      ]).
adjacency(15,[1,2,    5,6,  8,9,      12,13,14   ]).
