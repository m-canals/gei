node(I):-
	numNodes(N),
	between(1, N, I).
edge(I, J):-
	adjacency(I, L),
	member(J, L).
color(C):- 
	numNodes(N),
	between(1, N, C).

% Define SAT Variables
% ==============================================================================

satVariable(x(I, C)):-
	node(I),
	color(C).

% Generate Clauses
% ==============================================================================

% Predicate writeClauses(MaxCost) generates the clauses that guarantee that a
% solution with a maximum cost of MaxCost is found.

writeClauses(infinite):- !, numNodes(N), writeClauses(N), !.
writeClauses(MaxColors):-
    eachNodeExactlyOnecolor(MaxColors),
    noAdjacentNodesWithSameColor(MaxColors),
    true, !.
writeClauses(_):- told, nl, write('writeClauses failed!'), nl, nl, halt.

eachNodeExactlyOnecolor(MaxColors):-
	node(I),
	findall(x(I, C), between(1, MaxColors, C), Lits),
	exactly(1, Lits),
	fail.
eachNodeExactlyOnecolor(_).

noAdjacentNodesWithSameColor(MaxColors):-
	edge(I, J),
	between(1, MaxColors, C),
	writeClause([-x(I, C), -x(J, C)]),
	fail.
noAdjacentNodesWithSameColor(_).

% Display Solution
% ==============================================================================

displaySol(M):-
	node(I),
	member(x(I, C), M),
	write(I - C),
	write(' '),
	fail.
displaySol(_):- nl, !.

% Compute Cost
% ==============================================================================

costOfThisSolution(M, Cost):-
	findall(C, member(x(_, C), M), L),
	sort(L, L1),
	length(L1, Cost),
	!.

