coloredOutput(1).

% A factory produces banners using only a set of existing rectangular pieces.
% Our goal is to find out how to use the minimum set of pieces that exactly
% match the design of the banner. Note that pieces can be rotated if necessary.
% Also, note that each piece can be used at most once. That's why there can be
% several identical pieces in the input.

piece(P):-
	pieces(L), member([P,_,_],L).
pieceSize(P,W,H):-
	pieces(L), member([P,W,H],L).
widthBanner(W):-
	banner(B), member(L,B), length(L,W),!.
heightBanner(H):-
	banner(B), length(B,H), !.
contentsCellBanner(X,Y,C):-
	cell(X,Y), banner(B), heightBanner(H), Y1 is H-Y+1,
	nth1(Y1,B,L), nth1(X,L,C).
column(X):-
	widthBanner(W), between(1,W,X).
row(Y):-
	heightBanner(H), between(1,H,Y).
cell(X,Y):-
	column(X), row(Y).
fragment(X1,Y1,W,H,X,Y):-
	cell(X1,Y1), XN is X1 + W - 1, YN is Y1 + H - 1, cell(XN,YN),
	between(X1,XN,X), between(Y1,YN,Y), cell(X,Y).

% Define SAT Variables
% =============================================================================

% Let W be the width of the banner, let H be the height of the banner and let N
% be the number of pieces, then:

% Variable pieceCell(P,X,Y) means "piece P fills cell [X,Y]", with 1 <= X <= W,
% 1 <= Y <= H and 1 <= P <= N. There are W * H * P variables.
satVariable(pieceCell(P,X,Y)):- piece(P), cell(X,Y).

% Variable used(P) means "piece P is used", with 1 <= P <= N. There are N
% variables.
satVariable(used(P)):- piece(P).

% Variable pieceStarts(P,X,Y) mens "bottom-left cell of piece P is in cell
% [X,Y]" and variable pieceStartsRotated(P,X,Y) means "bottom-left cell of
% rotated piece P is in cell [X,Y]", with  1 <= X <= W, 1 <= Y <= H and
% 1 <= P <= N. There are W * H * P variables of each kind.
satVariable(pieceStarts(P,X,Y)):- piece(P), cell(X,Y).
satVariable(pieceStartsRotated(P,X,Y)):- piece(P), cell(X,Y).

% Note: [1,1] is the bottom-left cell of the banner.

% Generate Clauses
% =============================================================================

writeClauses(infinite):- !, pieces(L), length(L,K), writeClauses(K), !.
writeClauses(K):-
	exactlyZeroPieces,
	exactlyOnePiece,
	ifSomePieceCellThenUsed,
	ifUsedThenSomePieceStart,
	onlyValidPieceStarts,
	pieceCellsIffPieceStart,
	atMostAsManyPieces(K),
	true, !.
writeClauses(_):- told, nl, write('writeClauses failed!'), nl, nl, halt.

% Cadascuna de les caselles sense disseny no té ningun tros de peça
% (exactlyZeroPieces), en canvi, cadascuna de les caselles amb disseny té algun
% tros de peça (exactlyOnePiece). Si una casella amb disseny C té un tros de la
% peça P, llavors, s'utilitza la peça P (ifSomePieceCellThenUsed); com que
% s'utilitza la peça P, la peça P està col·locada (ifUsedThenSomePieceStart) de
% forma correcta (onlyValidPieceStarts), girada o sense girar, i no hi ha cap
% tros de la peça P fora del lloc on està col·locada (pieceCellsIffPieceStart),
% per tant, la peça està col·locada sobre la casella C i la peça no està
% col·locada a ningun altre lloc.

exactlyZeroPieces:-
	contentsCellBanner(X,Y,.), piece(P),
	writeClause([-pieceCell(P,X,Y)]),
	fail.
exactlyZeroPieces.

exactlyOnePiece:-
	contentsCellBanner(X,Y,x),
	findall(pieceCell(P,X,Y), piece(P), L),
	exactly(1,L),
	fail.
exactlyOnePiece.

ifSomePieceCellThenUsed:-
	piece(P), cell(X,Y),
	writeClause([-pieceCell(P,X,Y), used(P)]),
	% If and only if.
	% findall(pieceCell(P,X,Y), cell(X,Y), L),
	% expressOr(used(P), L),
	fail.
ifSomePieceCellThenUsed.

ifUsedThenSomePieceStart:-
	piece(P),
	findall(pieceStarts(P,X,Y), cell(X,Y), L1),
	findall(pieceStartsRotated(P,X,Y), cell(X,Y), L2),
	append(L1,L2,L),
	writeClause([-used(P) | L]),
	% If and only if.
	% expressOr(used(P), L),
	fail.
ifUsedThenSomePieceStart.

onlyValidPieceStarts:-
	pieceSize(P,W,H), N is W * H, cell(X1,Y1),
	findall(pieceCell(P,X,Y), fragment(X1,Y1,W,H,X,Y), L),
	\+ length(L,N),
	writeClause([-pieceStarts(P,X1,Y1)]),
	fail.
onlyValidPieceStarts:-
	pieceSize(P,W,H), N is W * H, cell(X1,Y1),
	findall(pieceCell(P,X,Y), fragment(X1,Y1,H,W,X,Y), L),
	\+ length(L,N),
	writeClause([-pieceStartsRotated(P,X1,Y1)]),
	fail.
onlyValidPieceStarts.

pieceCellsIffPieceStart:-
	pieceSize(P,W,H), N is W * H, cell(X1,Y1),
	findall(pieceCell(P,X,Y), fragment(X1,Y1,W,H,X,Y), L),
	length(L,N),
	% Si una peça sense girar està col·locada en un lloc, a les caselles
	% corresponents hi ha un tros de la peça.
	expressAnd(pieceStarts(P,X1,Y1), L),
	% Si una peça sense girar està col·locada en un lloc, no hi ha cap tros de la
	% peça fora d'aquest lloc.
	cell(X2,Y2),
	\+ member(pieceCell(P,X2,Y2), L),
	writeClause([-pieceStarts(P,X1,Y1), -pieceCell(P,X2,Y2)]),
	fail.
pieceCellsIffPieceStart:-
	pieceSize(P,W,H), N is W * H, cell(X1,Y1),
	findall(pieceCell(P,X,Y), fragment(X1,Y1,H,W,X,Y), L),
	length(L,N),
	% Si una peça girada està col·locada en un lloc, a les caselles corresponents
	% hi ha un tros de la peça.
	expressAnd(pieceStartsRotated(P,X1,Y1), L),
	% Si una peça girada està col·locada en un lloc, no hi ha cap tros de la peça
	% fora d'aquest lloc.
	cell(X2,Y2),
	\+ member(pieceCell(P,X2,Y2), L),
	writeClause([-pieceStartsRotated(P,X1,Y1), -pieceCell(P,X2,Y2)]),
	fail.
pieceCellsIffPieceStart.

atMostAsManyPieces(K):-
	findall(used(P), piece(P), L),
	atMost(K,L).

% Display Solution
% =============================================================================

displaySol(M):-
	writeBanner(M).
displaySol(_).

writeBanner(M):-
	heightBanner(H), between(1,H,YB), Y is H - YB + 1,
	writeRow(M,Y), nl,
	fail.
writeBanner(_).

writeRow(M,Y):-
	widthBanner(W), between(1,W,X),
	writeCell(M,X,Y),
	fail.
writeRow(_,_).

writeCell(M,X,Y):-
	member(pieceCell(P,X,Y),M), !, writeCell(P).
writeCell(_,_,_):-
	write("╳").
writeCell(P):-
	coloredOutput(1), colorCode(P,C),
	write("\033["), write(C), write("m \033[0m").
writeCell(P):-
	coloredOutput(0),
	write(P).

colorCode(1,40).
colorCode(2,41).
colorCode(3,42).
colorCode(4,43).
colorCode(5,44).
colorCode(6,45).
colorCode(7,46).
colorCode(8,47).
colorCode(9,100).
colorCode(a,101).
colorCode(b,102).
colorCode(c,103).
colorCode(d,104).
colorCode(e,105).
colorCode(f,106).
colorCode(g,107).

% Compute Cost
% =============================================================================

costOfThisSolution(M,K):-
	findall(P, member(used(P),M), L),
	length(L,K).

