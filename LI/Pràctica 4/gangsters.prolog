% The mafia has a lot gangsters for doing different tasks. These tasks are
% planned every 3 days (72h), according to a forecast of the tasks to be done
% every hour. No gangster can do two different tasks during the same hour or on
% two consecutive hours. Some gangsters are not available on certain hours. We
% want to plan all tasks (which gangster does what task when) and we want to
% find the minimal K such that no gangster works more than K consecutive hours.

task(T):-
	gangstersNeeded(T,_).
differentTasks(T1,T2):-
	task(T1),
	task(T2),
	T1 \= T2.
needed(T,H,N):-
	gangstersNeeded(T,L),
	nth1(H,L,N).
gangster(G):-
	gangsters(L),
	member(G,L).
hour(H):-
	between(1,72,H).
consecutiveHours(1,[X|[]]):-
	hour(X).
consecutiveHours(K,[X,Y|L]):-
	hour(X),
	Y is X + 1,
	hour(Y),
	K1 is K - 1,
	consecutiveHours(K1,[Y|L]).
unavailable(G,H):-
	notAvailable(G,L),
	member(H,L).
available(G,H):-
	gangster(G),
	hour(H),
	\+ unavailable(G,H).

% Define SAT Variables
% =============================================================================

% Let N be the number of gangsters, let M be the number of tasks and let P be
% the number of hours, then variable does(G,T,H) means "gangster G does task T
% at hour H" with 1 <= G <= N, 1 <= T <= M and 1 <= H <= P. There are
% N * M * P variables.
satVariable(does(G,T,H)):-
	gangster(G),
	task(T),
	hour(H).

% Generate Clauses
% =============================================================================

writeClauses(infinite):- !, writeClauses(4), !.
writeClauses(K):-
	exactlyAsManyGangsters,
	onlyAvailableGangsters,
	atMostOneTaskDuringTheSameOur,
	atMostOneTaskOnTwoConsecutiveHours,
	atMostAsManyConsecutiveHours(K),
	true, !.
writeClauses(_):- told, nl, write('writeClauses failed!'), nl, nl, halt.

% Each task is done when it needs to be done by the required number of
% gangsters.
exactlyAsManyGangsters:-
	needed(T,H,N),
	findall(does(G,T,H), gangster(G), Literals),
	exactly(N, Literals),
	fail.
exactlyAsManyGangsters.

% Some gangsters are not available on certain hours.
onlyAvailableGangsters:-
	unavailable(G,H), task(T),
	writeClause([-does(G,T,H)]),
	fail.
onlyAvailableGangsters.

% No gangster can do two different tasks during the same hour.
atMostOneTaskDuringTheSameOur:-
	gangster(G), hour(H),
	findall(does(G,T,H), task(T), Literals),
	atMost(1, Literals),
	fail.
atMostOneTaskDuringTheSameOur.

% No gangster can do two different tasks on two consecutive hours.
atMostOneTaskOnTwoConsecutiveHours:-
	gangster(G), consecutiveHours(2,[H1,H2]), differentTasks(T1,T2),
	writeClause([-does(G,T1,H1), -does(G,T2,H2)]),
	fail.
atMostOneTaskOnTwoConsecutiveHours.

% No gangster works more than K consecutive hours.
atMostAsManyConsecutiveHours(K):-
	gangster(G), task(T), K1 is K + 1, consecutiveHours(K1,L),
	findall(-does(G,T,H), member(H,L), Literals),
	writeClause(Literals),
	fail.
atMostAsManyConsecutiveHours(_).

% Display Solution
% =============================================================================

displaySol(M):-
	write('                      10        20        30        40        50        60        70  '), nl,
	write('              123456789012345678901234567890123456789012345678901234567890123456789012'), nl,
	gangster(G), nl,
	write('gangster '), write(G), write(': '), hour(H), writeIfBusy(G,H,M),
	fail.
displaySol(_):- nl, !.

writeIfBusy(G,H,M):- member(does(G, killing, H), M), write('k'), !.
writeIfBusy(G,H,M):- member(does(G, countingMoney, H), M), write('c'), !.
writeIfBusy(G,H,M):- member(does(G, politics, H), M), write('p'), !.
writeIfBusy(_,_,_):- write('-'), !.

% Compute Cost
% =============================================================================

costOfThisSolution(M,K):-
	findall(N, (gangster(G), longestDoings(M,G,N)), L),
	max_list(L,K).

longestDoings(M,G,N):-
	findall(H, member(does(G,_,H), M), L),
	sort(L,SL),
	maximumConsecutiveHours(SL,_,N).

maximumConsecutiveHours([],0,0).
maximumConsecutiveHours([_],1,1).
maximumConsecutiveHours([X,Y|L],K,N):-
	X is Y - 1,
	maximumConsecutiveHours([Y|L],K1,N1),
	K is K1 + 1,
	N is max(K,N1).
maximumConsecutiveHours([X,Y|L],1,N1):-
	X < Y - 1,
	maximumConsecutiveHours([Y|L],_,N1).

