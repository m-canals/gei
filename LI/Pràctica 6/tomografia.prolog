% A matrix which contains zeroes and ones gets "x-rayed" vertically and
% horizontally, giving the total number of ones in each row and column. The
% problem is to reconstruct the contents of the matrix from this information.

:- use_module(library(clpfd)).

input(
	[0, 0, 8, 2, 6, 4, 5, 3, 7, 0, 0],
	[0, 0, 7, 1, 6, 3, 4, 5, 2, 7, 0, 0]).
input(
	[10, 4, 8, 5, 6],
	[5, 3, 4, 0, 5, 0, 5, 2, 2, 0, 1, 5, 1]).
input(
	[11, 5, 4],
	[3, 2, 3, 1, 1, 1, 1, 2, 3, 2, 1]).

% Set Variables
% ==============================================================================

matrixByRows([], _, []).
matrixByRows(Elements, Width, [Row | Rows]) :-
	append(Row, RemainingElements, Elements),
	length(Row, Width),
	matrixByRows(RemainingElements, Width, Rows).

setVariables(RowSums, ColumnSums, Rows):-
	length(RowSums, Height),
	length(ColumnSums, Width),
	N is Height * Width,
	length(Variables, N),
	Variables ins 0..1,
	matrixByRows(Variables, Width, Rows).

% Set Constraints 
% ==============================================================================

sums([], []).
sums([List | Lists], [Sum | Sums]):-
	sum(List, #=, Sum),
	sums(Lists, Sums).

setConstraints(RowSums, ColumnSums, Rows):-
	sums(Rows, RowSums),
	transpose(Rows, Columns),
	sums(Columns, ColumnSums).

% Set Labeling
% ==============================================================================

setLabeling(_, _, Rows):-
	append(Rows, Variables),
	labeling([], Variables).

% Display Solution
% ==============================================================================

writeBit(1):- write('*  '), !.
writeBit(0):- write('   '), !.

displaySolution(_, ColSums, _):-
	write('     '),
	member(Sum, ColSums),
	writef('%2r ', [Sum]),
	fail.
displaySolution(RowSums, _, Rows):-
	nl,
	nth1(N, Rows, Row),
	nth1(N, RowSums, Sum),
	nl,
	writef('%3r   ', [Sum]),
	member(Bit, Row),
	writeBit(Bit),
	fail.
displaySolution(_, _, _):-
	nl,
	nl.

% ==============================================================================

main:-
	input(RowSums, ColumnSums),
	setVariables(RowSums, ColumnSums, Rows), 
	setConstraints(RowSums, ColumnSums, Rows),
	setLabeling(RowSums, ColumnSums, Rows),
	displaySolution(RowSums, ColumnSums, Rows),
	fail.

