% Given input(SIZE, SIZES), the problem is to fit all squares of sizes SIZES in
% a square of size SIZE.

graphicOutput(1).
:- use_module(library(clpfd)).

input(0, 3, [2, 1, 1, 1, 1, 1]).
input(1, 4, 2, 2, 2, 1, 1, 1, 1]).
input(2, 5, [3, 2, 2, 2, 1, 1, 1, 1]).
input(3, 19, [10, 9, 7, 6, 4, 4, 3, 3, 3, 3, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1]).
input(4, 40, [24, 16, 16, 10, 9, 8, 8, 7, 7, 6, 6, 3, 3, 3, 2, 1, 1]).
input(5, 112, [50, 42, 37, 35, 33, 29, 27, 25, 24, 19, 18, 17, 16, 15, 11, 9, 8, 7, 6, 4, 2]).
input(6, 175, [81, 64, 56, 55, 51, 43, 39, 38, 35, 33, 31, 30, 29, 20, 18, 16, 14, 9, 8, 5, 4, 3, 2, 1]).

% Set Variables
% =============================================================================

matrixByRows([], _, []).
matrixByRows(Elements, Width, [Row | Rows]) :-
	append(Row, RemainingElements, Elements),
	length(Row, Width),
	matrixByRows(RemainingElements, Width, Rows).

setVariables(Size, Sizes, Rows, Columns, InRows, InColumns):-
	length(Sizes, N),
	length(Rows, N),
	length(Columns, N),
	Rows ins 1 .. Size,
	Columns ins 1 .. Size,
	M is Size * N,
	length(InRowVariables, M),
	length(InColumnVariables, M),
	InRowVariables ins 0 .. 1,
	InColumnVariables ins 0 .. 1,
	matrixByRows(InRowVariables, N, InRows),
	matrixByRows(InColumnVariables, N, InColumns).

% Set Constraints 
% =============================================================================

insideBigSquare(_, [], []).
insideBigSquare(BigSquareSize, [Size | Sizes], [Begin | BeginList]):-
	BigSquareSize #>= (Begin + Size - 1),
	insideBigSquare(BigSquareSize, Sizes, BeginList).

nonOverlapping(_, _, _, [], [], []).
nonOverlapping(Size1, Row1, Column1,
 [Size2 | Sizes], [Row2 | Rows], [Column2 | Columns]):-
	% Un quadrat no intersecta amb ningun altre quadrat si i només si
	% no intersecta amb el primer d'aquests (per les clàusules següents) i
	% no intersecta amb els restants (per la hipòtesi d'inducció).
	%
	% Dos quadrats de mida s i s' que comencen a (x,y) i (x',y'), respectivament,
	% intersecten si i només si x < x' + s', x + s > x', y < y' + s' i
	% y + s > y', per tant, no intersecten si i només si, x >= x' + s',
	% x + s <= x', y >= y' + s' o y + s <= y'.
	(Column1 #>= (Column2 + Size2)) #\/
	((Column1 + Size1) #=< Column2) #\/
	(Row1 #>= (Row2 + Size2)) #\/
	((Row1 + Size1) #=< Row2),
	nonOverlapping(Size1, Row1, Column1, Sizes, Rows, Columns).

nonOverlapping([], [], []).
nonOverlapping([Size | Sizes], [Row | Rows], [Column | Columns]):-
	% Un quadrat no intersecta amb ningun altre quadrat si i només si
	% no intersecta amb ningun altre quadrat no vist (per la clàusula següent) i
	% no intersecta amb ningun altre quadrat vist (per la hipòtesi d'inducció).
	nonOverlapping(Size, Row, Column, Sizes, Rows, Columns),
	nonOverlapping(Sizes, Rows, Columns).

inLineDefinition(_, [], [], []).
inLineDefinition(P, [Line | Lines], [Size | Sizes], [InLine | InLines]):-
	((Line #=< P) #/\ (Line #>= (P - Size + 1))) #==> InLine,
	inLineDefinition(P, Lines, Sizes, InLines).

inLineDefinitions(0, _, _, []).
inLineDefinitions(Line, Lines, Sizes, [InLines | InLinesList]):-
	inLineDefinition(Line, Lines, Sizes, InLines),
	PreviousLine is Line - 1,
	inLineDefinitions(PreviousLine, Lines, Sizes, InLinesList).

atMostAsManyPointsPerLine(_, _, []).
atMostAsManyPointsPerLine(Size, Sizes, [InLines | InLinesList]):-
	% Permetem que els quadrats cobreixin el quadrat gran parcialment:
	% el nombre de punts en una línia és menor o igual que la mida.
	scalar_product(Sizes, InLines, #=<, Size),
	atMostAsManyPointsPerLine(Size, Sizes, InLinesList).

setConstraints(Size, Sizes, Rows, Columns, InRowsList, InColumnsList):-
	insideBigSquare(Size, Sizes, Rows),
	insideBigSquare(Size, Sizes, Columns),
	nonOverlapping(Sizes, Rows, Columns),
	% Restricció redudant per a augmentar l'eficiència.
	inLineDefinitions(Size, Rows, Sizes, InRowsList),
	inLineDefinitions(Size, Columns, Sizes, InColumnsList),
	atMostAsManyPointsPerLine(Size, Sizes, InRowsList),
	atMostAsManyPointsPerLine(Size, Sizes, InColumnsList).

% Set Labeling
% =============================================================================

interleave([], [], []).
interleave([], [B | L2], [B | L2]).
interleave([A | L1], [], [A | L1]).
interleave([A | L1], [B | L2], [A, B | L]):-
	interleave(L1, L2, L).

setLabeling(_, _, Rows, Columns, InRowsList, InColumnsList):-
	% Prova de posar primer els quadrats més grans (la llista ja està ordenada).
	%
	% La optimització amb la restricció redundant sembla més eficient quan les
	% variables no s'alternen, segurament perquè es poden propagar les decisions
	% sobre les files sense haver hagut de decidir sobre les columnes.
	%
	% interleave(Rows, Columns, PositionVariables),
	append(InRowsList, InRowVariables),
	append(InColumnsList, InColumnVariables),
	append([Rows, Columns, InRowVariables, InColumnVariables], Variables),
	once(labeling([leftmost], Variables)).

% Display Solution
% =============================================================================

writeCell(_, _, _, N):- graphicOutput(0), writef(' %2l', [N]), !.
writeCell(X - Y, X - Y, X - Y, _):- write('⣏⣹'), !.
writeCell(X - Y, X - Y, _ - _, _):- write('⡏⠉').
writeCell(X - Y, X - B, _ - D, _):- Y > B, Y < D, write('⡇ ').
writeCell(X - Y, X - _, _ - Y, _):- write('⣇⣀').
writeCell(X - Y, A - _, C - Y, _):- X > A, X < C, write('⣀⣀').
writeCell(X - Y, _ - _, X - Y, _):- write('⣀⣸').
writeCell(X - Y, _ - B, X - D, _):- Y > B, Y < D, write(' ⢸').
writeCell(X - Y, _ - Y, X - _, _):- write('⠉⢹').
writeCell(X - Y, A - Y, C - _, _):- X > A, X < C, write('⠉⠉').
writeCell(X - Y, A - B, C - D, _):- X > A, X < C, Y > B, Y < D, write('⠀⠀').

bottomRightCornerOfRectangle(A - B, C - D, W - H):-
	C is A + W - 1,
	D is B + H - 1.

insideRectangle(A - B, C - D, X - Y):-
	between(A, C, X),
	between(B, D, Y).

writeCell(Y, X, Sizes, Rows, Columns):-
	nth1(K, Sizes, Size),
	nth1(K, Rows, B),
	nth1(K, Columns, A),
	bottomRightCornerOfRectangle(A - B, C - D, Size - Size),
  insideRectangle(A - B, C - D, X - Y),
  !,
  writeCell(X - Y, A - B, C - D, Size).
writeCell(_, _, _, _, _):-
	graphicOutput(0),
	write('   '),
	!.
writeCell(_, _, _, _, _):-
	write('⠀⠀').

displaySolution(Size, Sizes, Rows, Columns, _, _):- 
	between(1, Size, Row),
	nl,
	between(1, Size, Column),
	writeCell(Row, Column, Sizes, Rows, Columns),
	fail.
displaySolution(_, _, _, _, _, _):-
	nl.

% =============================================================================

main:-
	input(_, Size, Sizes),
	nl, write('Fitting all squares of sizes '), write(Sizes),
	write(' into big square of size '), write(Size), write('...'), nl,
	setVariables(Size, Sizes, Rows, Columns, InRowsList, InColumnsList),
	setConstraints(Size, Sizes, Rows, Columns, InRowsList, InColumnsList),
	setLabeling(Size, Sizes, Rows, Columns, InRowsList, InColumnsList),
	displaySolution(Size, Sizes, Rows, Columns, InRowsList, InColumnsList),
	fail.

