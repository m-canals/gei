% Funcions matemàtiques =======================================================
% =============================================================================

% factorial(N,F) és cert, si i només si, F és el factorial de N.
factorial(0):-!.
factorial(N,F):-
	N1 is N - 1,
	factorial(N1, F1),
	F is N1 * F1.

% natural(N) és cert, si i només si, N és un nombre natural.
natural(0).
natural(N):-
	natural(N1),
	N is N1 + 1.

% multiple(X,Y,P) és cert, si i només si, P és múltiple de X i Y.
multiple(X,Y,M):-
	natural(N),
	M is N * X,
	0 is M mod Y.

% prime_factor(N,F) és cert, si i només si, F és una llista que conté els
% factors primers de N.
prime_factors(1,[]):-!.
prime_factors(N,[F|L]):-
	natural(F),
	F > 1,
	0 is N mod F,
	N1 is N // F, 
	prime_factors(N1,L),
	!.

% derivative(F,X,D) és cert, si i només si, la derivada de la funció F respecte
% de la variable X és D.
derivative(X,X,1):-!.
derivative(C,_,0):-number(C).
derivative(A+B,X,A1+B1):-derivative(A,X,A1),derivative(B,X,B1).
derivative(A-B,X,A1-B1):-derivative(A,X,A1),derivative(B,X,B1).
derivative(A*B,X,A*B1+B*A1):-derivative(A,X,A1),derivative(B,X,B1).
derivative(sin(A),X,cos(A)*B):-derivative(A,X,B).
derivative(cos(A),X,-sin(A)*B):-derivative(A,X,B).
derivative(e^A,X,B*e^A):-derivative(A,X,B).
derivative(ln(A),X,B*1/A):-derivative(A,X,B).

simplify(E,E1):-simplify_once(E,E2),!,simplify(E2,E1).
simplify(E,E).

simplify_once(A+B,A+C):-simplify_once(B,C),!.
simplify_once(B+A,C+A):-simplify_once(B,C),!.
simplify_once(A*B,A*C):-simplify_once(B,C),!.
simplify_once(B*A,C*A):-simplify_once(B,C),!.
simplify_once(0*_,0):-!.
simplify_once(_*0,0):-!.
simplify_once(1*X,X):-!.
simplify_once(X*1,X):-!.
simplify_once(0+X,X):-!.
simplify_once(X+0,X):-!.
simplify_once(N1+N2,N3):-number(N1),number(N2),N3 is N1+N2,!.
simplify_once(N1*N2,N3):-number(N1),number(N2),N3 is N1*N2,!.
simplify_once(N1*X+N2*X,N3*X):-number(N1),number(N2),N3 is N1+N2,!.
simplify_once(N1*X+X*N2,N3*X):-number(N1),number(N2),N3 is N1+N2,!.
simplify_once(X*N1+N2*X,N3*X):-number(N1),number(N2),N3 is N1+N2,!.
simplify_once(X*N1+X*N2,N3*X):-number(N1),number(N2),N3 is N1+N2,!.

% product(L,P) és cert, si i només si, P és el producte del elements de la
% llista L.
product([],1).
product([X|L],P):-
	product(L,P1),
	P is P1*X.

% dot_product(L1,L2,P) és cert, si i només si, P és el producte escalar dels
% elements de les llistes L1 i L2 i la longitud de L1 i L2 és la mateixa.
dot_product([],[],0).
dot_product([X|L1],[Y|L2],P):-
	dot_product(L1,L2,P1),
	P is P1+X*Y.

% sum(L,S) és cert, si i només si, S és la suma dels elements de la llista L.
sum([],0).
sum([X|L1],S):-
	sum(L1,S1),
	S is S1+X.

% Llistes =====================================================================
% =============================================================================

% list_concatenation(L1,L2,L) és cert, si i només si, L és la llista resultant
% de concatenar les llistes L1 i L2.
list_concatenation([],L,L).
list_concatenation([X|L1],L2,[X|L]):-
	list_concatenation(L1,L2,L).

% list_membership(X,L) és cert, si i només si, L conté X.
list_membership(X,[X|_]).
list_membership(X,[X|L]):-
	list_membership(X,L).

% membership(X,L,R) és cert, si i només si, L conté X i R és la llista resultant
% de la concatenació de les llistes amb els elements anteriors i posteriors a X.
list_membership_with_remaining_items(X,L,R):-
	list_concatenation(L1,[X|L2],L),
	list_concatenation(L1,L2,R).

% Alternativa més eficient.
list_membership_with_remaining_items2(X,[X|L],L).
list_membership_with_remaining_items2(X,[Y|L],[Y|R]):-
	list_membership_with_remaining_items2(X,L,R). 

% length(L,N) és cert, si i només si, la llargada de la lista L és N.
list_length([],0).
list_length([_|L],N):-
	list_length(L,M),N is M + 1.

% sublist(L,S) és cert, si i només si, S és una subllista de la llista L,
% és a dir, S conté n elements consecutius de L, per a n entre zero i el nombre
% d'elements de L, ambdós inclosos.
list_sublist([],[]).
list_sublist([X|L],[X|S]):-
	list_sublist(L,S).
list_sublist([_|L],S):-
	list_sublist(L,S).

% list_permutation(L,P) és cert, si i només si, P és una llista amb els elements
% de la llista L permutats.
list_permutation([],[]).
list_permutation(L,[X|P]):-
	list_membership_with_remaining_items(X,L,R),
	list_permutation(R,P).

% list_last(X,L) és cert, si i només si, X és el darrer element de la llista L.
list_last(X,L):-
	list_concatenation(_,[X],L).

list_last2(X,[X]).
list_last2(X,[_|L1]):-
	list_last2(X,L1).

% list_reversal(L,R) és cert, si i només si, R és la llista L revessada.
list_reversal([],[]).
list_reversal(L,[X|R1]):-
	list_concatenation(L1,[X],L),
	list_reversal(L1,R1).

list_reversal2([],[]).
list_reversal2([X|L1],R):-
	list_concatenation(R1,[X],R),
	list_reversal2(L1,R1).

% count(L,X,N) és cert, si i només si, N és el nombre d'ocurrències de X a la
% llista L.
count([],_,0).
count([X|L1],X,N):-
	count(L1,X,N1),
	N is N1+1.
count([Y|L1],X,N):-
	X\=Y,
	count(L1,X,N).

% is_sorted(L) és cert, si i només si, la llista L està ordenada de forma
% creixent, és a dir, un element de la llista no és major que l'element
% posterior.
is_sorted([]).
is_sorted([_]).
is_sorted([X,Y|L1]):-
	X=<Y,
	is_sorted([Y|L1]).

% list_sorting(L1,L2) és cert, si i només si, la llista L2 és la llista
% resultant d'ordenar la llista L1 de forma creixent, és a dir, L2 és una
% permutació de L1 ordenada de forma creixent.
list_sorting(L1,L2) :-
	list_permutation(L1,L2),
	is_sorted(L2).  

% ordered_insertion(X,L1,L2) és cert, si i només si, L2 és la llista resultant
% de la inseració de X a la posició corresponent de la llista ordenada L1.
ordered_insertion(X,[],[X]). 
ordered_insertion(X,[Y|L],[X,Y|L]):-
	X=<Y. 
ordered_insertion(X,[Y|L],[Y|L1]):-
	X>Y,
	ordered_insertion(X,L,L1). 

% list_insertion_sorting(L1,L2) és cert, si i només si, la llista L2 és la
% llista resultant d'ordenar la llista L1 de forma creixent, és a dir, L2 és la
% llista resultant d'inserir, respectant l'ordre, el primer element de L1 a la
% llista dels elements restants de L1 ordenada de forma creixent.
list_insertion_sorting([],[]). 
list_insertion_sorting([X|L],L1):-
	list_insertion_sorting(L,L2),
	ordered_insertion(X,L2,L1).

% split(A,B,C) és cert, si i només si, les llistes B i C contenen,
% respectivament, ceil(N/2) i floor(N/2) elements de la llista A, sent N la
% longitud d'A.
split([],[],[]).
split([X],[X],[]).
split([X,Y|A],[X|B],[Y|C]):-
	split(A,B,C).

% list_insertion_sorting(L1,L2) és cert, si i només si, la llista L2 és la
% llista resultant d'ordenar la llista L1 de forma creixent.
merge_sort([],[]):-
	!.
merge_sort([X],[X]):-
	!.
merge_sort(A,B) :-
	split(A,A1,A2),
	merge_sort(A1,B1),
	merge_sort(A2,B2), 
  merge(B1,B2,B).

% merge(A,B,C) és cert, si i només si, C és la llista resultant d'ordenar la
% concatenació de les llistes A i B.
merge(A,[],A):-
	!.
merge([],A,A).
merge([X|L1],[Y|L2],[X|L3]):-
	X=<Y,
	!,
	merge(L1,[Y|L2],L3). 
merge([X|L1],[Y|L2],[Y|L3]):- 
	merge([X|L1],L2,L3). 

% list_join(Separator, List, String) és cert, si i només si, String és la cadena
% resultant d'unir els elements de la llista List amb la cadena Separator.
list_join(_,[],"").
list_join(_,[X],X).
list_join(X,[Y|L1],S):-
	L1\=[],
	list_join(X,L1,S1),
	string_concat(Y,X,S2),
	string_concat(S2,S1,S).

% combination(L,N,C) és cert, si i només si, C és una llista que representa una
% combinació de N elements de L, amb repeticions.
combination(_,0,[]).
combination(L,N,[X|L1]):-
	N>0,
	N1 is N-1,
	list_membership_with_remaining_items(X,L,_),
	combination(L,N1,L1).

frequencies([],[]).
frequencies([X|L],[[X,N]|F2]):-
	frequencies(L,F1),
	list_membership_with_remaining_items([X,N1],F1,F2),
	!,
	N is N1+1.
frequencies([X|L],[[X,1]|F1]):-
	frequencies(L,F1).

palindrome([]).
palindrome([_]):-
	!.
palindrome([X|L]):-
	list_concatenation(L1,[X],L),
	palindrome(L1).

% number_as_list(L,N,S) és cert, si i només si, la llista L conté els digits del
% nombre N, amb possibles zeros a l'esquerra, i S és la longitud de L. Genera N.
number_as_list([],0, 0).
number_as_list([X|L1],N,S):-
	number_as_list(L1,N1,S1),
	N is N1+X*10^S1,
	S is S1+1.

% Altres ======================================================================
% =============================================================================

xifres(L,N):-
	list_sublist(L,S),
	list_permutation(S,P),
	expression(P,E), 
	N is E,
	write(E),
	nl,
	fail.

expression([X],X).                                     
expression(L,E1+E2):-
	list_concatenation(L1,L2,L),
	L1 \= [],
	L2 \= [],
	expression(L1,E1),
	expression(L2,E2).
expression(L,E1-E2):-
	list_concatenation(L1,L2,L),
	L1 \= [],
	L2 \= [],         
	expression(L1,E1),
	expression(L2,E2).      
expression(L,E1*E2):-
	list_concatenation(L1,L2,L),
	L1 \= [],
	L2 \= [],         
	expression(L1,E1),
	expression(L2,E2).
expression(L,E1//E2):-
	list_concatenation(L1,L2,L),
	L1 \= [],
	L2 \= [],
	expression(L1,E1),
	expression(L2,E2),
	X is E2,
	X \= 0,
	0 is E1 mod E2. 

% dice_roll_sum(P,N,L) és cert, si i només si, L és una llista amb les
% puntuacions de N daus, de les quals la suma és P, és a dir, P és la suma dels
% elements de L, N és la llargada de L, el mínim de L és 1 i el màxim de L és 6.
dice_roll_sum(P,N,L):-
	dice_roll(N, L),
	sum(L,P).

dice_roll(0,[]).
dice_roll(N,[1|L1]):-N>0,N1 is N-1, dice_roll(N1,L1).
dice_roll(N,[2|L1]):-N>0,N1 is N-1, dice_roll(N1,L1).
dice_roll(N,[3|L1]):-N>0,N1 is N-1, dice_roll(N1,L1).
dice_roll(N,[4|L1]):-N>0,N1 is N-1, dice_roll(N1,L1).
dice_roll(N,[5|L1]):-N>0,N1 is N-1, dice_roll(N1,L1).
dice_roll(N,[6|L1]):-N>0,N1 is N-1, dice_roll(N1,L1).

% XXX Perquè no funciona amb member(X, [1,2,3,4,5,6])?

% Optimització.
dice_roll_sum2(0,0,[]).
dice_roll_sum2(P,N,[1|L1]):-N>0,P>0,N1 is N-1,P1 is P-1,dice_roll_sum(P1,N1,L1).
dice_roll_sum2(P,N,[2|L1]):-N>0,P>1,N1 is N-1,P1 is P-2,dice_roll_sum(P1,N1,L1).
dice_roll_sum2(P,N,[3|L1]):-N>0,P>2,N1 is N-1,P1 is P-3,dice_roll_sum(P1,N1,L1).
dice_roll_sum2(P,N,[4|L1]):-N>0,P>3,N1 is N-1,P1 is P-4,dice_roll_sum(P1,N1,L1).
dice_roll_sum2(P,N,[5|L1]):-N>0,P>4,N1 is N-1,P1 is P-5,dice_roll_sum(P1,N1,L1).
dice_roll_sum2(P,N,[6|L1]):-N>0,P>5,N1 is N-1,P1 is P-6,dice_roll_sum(P1,N1,L1).

% sum_remaining(L) és cert, si i només si, la llista L conté un element igual
% que la suma dels elements restants.
sum_remaining(L) :-
	list_membership_with_remaining_items(X,L,R),
	sum(R,X),
	!.

% sum_previous(L) és cert, si i només si, la llista L conté un element igual
% que la suma dels elements anteriors a aquest.
sum_previous(L) :-
	list_concatenation(L1,[X|_],L),
	sum(L1,X),
	!.

% dictionary(L,N) escriu les combinacions de N elements de L, amb repeticions.
dictionary(L,N):-
	combination(L,N,C),
	list_join("",C,S),
	write(S),
	nl,
	fail.

% palindromes(L) escriu totes les permutacions capicues de L, amb repeticions.
palindromes(L):-
	list_permutation(L,P),
	palindrome(P),
	write(P),
	nl,
	fail.

% distinct_palindromes(L) escriu totes les permutacions capicues de L, sense
% repeticions.
distinct_palindromes(L):-
	setof(P,(permutation(L,P),palindrome(P)),S),
	write(S).

% concat([], L, L).
% concat([X|L1], L2, [X|L3]) :-
% 	concat(L1, L2, L3).

% pert_con_resto(X, L, R) :-
% 	concat(L1, [X|L2], L),
% 	concat(L1, L2, R).

% pert(X, L) :- pert_con_resto(X, L, _).

% no_pert(X, L) :- pert(X, L), !, fail.
% no_pert(_, _).

% permutacion([], []).
% permutacion(L, [X|P]) :-
% 	pert_con_resto(X, L, R),
% 	permutacion(R, P).

% inverso([],[]).
% inverso(L,[X|L1]):- concat(L2,[X],L), inverso(L2,L1).

% permutaciones_distintas(L, A, [P|X]) :-
% 	permutacion(L, P),
% 	no_pert(P, A),
% 	permutaciones_distintas(L, [P|A], X).
% permutaciones_distintas(_, _, []).

% escribir_palindromos(P) :-
% 	pert(X, P),
% 	inverso(X, X),
% 	write(X), nl, fail.
% escribir_palindromos(_).

% palindromos(L) :-
% 	permutaciones_distintas(L, [], P),
% 	escribir_palindromos(P),
% 	!.

% TODO Base i diferents longituds.
digital_sum([],[],[], 0).
digital_sum([X|A],[Y|B],[Z|C], CarryOut):-
	digital_sum(A, B, C, CarryIn),
	Z is (X+Y+CarryIn)mod 10,
	CarryOut is (X+Y+CarryIn)//10. 

% TODO digital_sum que comprovi si el digit I de la suma és correcte
% per tal d'evitar treball innecessari.
sendmory():-
	permutation([0,1,2,3,4,5,6,7,8,9], [_,_,S,E,N,D,M,O,R,Y]),
	digital_sum([S,E,N,D],[M,O,R,E],[O,N,E,Y], M),
	write([S,E,N,D,M,O,R,Y]),
	nl,
	fail.

sendmory2():-
	permutation([0,1,2,3,4,5,6,7,8,9], [_,_,S,E,N,D,M,O,R,Y]),
	N1 is 1000 * S + 100 * E + 10 * N + D,
	N2 is 1000 * M + 100 * O + 10 * R + E,
	N3 is 10000 * M + 1000 * O + 100 * N + 10 * E + Y,
	N3 is N1 + N2,
	write([S,E,N,D,M,O,R,Y]),
	nl,
	fail.

safe(0,_).
safe(M,C):-
	M>=C.

% Definim un vèrtex com una situació en què hi ha M missioners, C caníbals a la
% riba 0 del riu i la canoa és a la riba S del riu.
vertex([M,C,S]):-
	member(M,[0,1,2,3]),
	member(C,[0,1,2,3]),
	member(S,[0,1]),
	safe(M,C),
	M1 is 3-M,
	C1 is 3-C,
	safe(M1,C1).

% Definim una aresta com una travessada d'una o dues persones d'una riba a
% l'altra, tenint en compte que d'on surten n'hi haurà menys i a on arriben
% n'hi haurà més.
edge([M1,C1,0],[M2,C2,1]):-
	member([M,C],[[0,1],[0,2],[1,0],[1,1],[2,0]]),
	M2 is M1-M,
	C2 is C1-C.
edge([M1,C1,1],[M2,C2,0]):-
	member([M,C],[[0,1],[0,2],[1,0],[1,1],[2,0]]),
	M2 is M1+M,
	C2 is C1+C.

% TODO Definir P en comptes d'escriure'l.
% path(X,Z,P) escriu les llistes amb els vèrtexs pels quals transcorren els
% camins que hi ha des del vèrtex X fins al vèrtex Z.
unvisited(X,P):- memberchk(X,P),!,fail.
unvisited(_,_).
path(X,X,P):-list_reversal(P,R),write(R),nl.
path(X,Z,P):-
	vertex(Y),
	unvisited(Y,P),
	edge(X,Y),
	path(Y,Z,[Y|P]).

missionaries_and_cannibals:-path([3,3,0],[0,0,1],[[3,3,0]]).

% Tenint un dau les puntacions 1, ..., 9 i sent un llançament de 3 daus.
% El llançament D1 és millor que el llançament D2 si és més probable que un
% dau de D1 sigui més gran que un dau de D2. Per exemple, D1 = [3,5,7] i
% D2 = [2,1,8], D1 és millor que D2 perquè 3>2,3>1,5>2,5>1,7>2,7>1, és a dir,
% 6/9 vegades és més gran un dau de D1.
esMillor(D1,D2):-
	findall(A-B, (member(A,D1),member(B,D2),A>B), L),
	length(L, K),
	K>=5.

% Quins llançaments R satisfan que R millor que A,
% A millor que N i N millor que R?
f():-
	permutation([1,2,3,4,5,6,7,8,9],[R1,R2,R3,A1,A2,A3,N1,N2,N3]),
	esMillor([R1,R2,R3],[A1,A2,A3]),
	esMillor([A1,A2,A3],[N1,N2,N3]),
	esMillor([N1,N2,N3],[R1,R2,R3]),
	write([R1,R2,R3]).

