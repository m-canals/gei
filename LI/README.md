Pràctiques de la primavera de 2020 de l'assignatura Lògica a la Informàtica (LI) del Grau en Enginyeria Informàtica (GEI) de la Facultat de Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web de l'assignatura](https://www.cs.upc.edu/~li).
