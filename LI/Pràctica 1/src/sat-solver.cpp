# include <iostream>
# include <algorithm>
# include <vector>
# include <climits>

# define UNDEFINED -1
# define TRUE 1
# define FALSE 0

using namespace std;

int numberOfVariables;
int numberOfClauses;
vector <vector <int>> clauses;
vector <int> model;
vector <int> modelStack;
unsigned int indexOfNextLiteralToPropagate;
int decisionLevel;

vector <vector <int>> positiveOccurrences;
vector <vector <int>> negativeOccurrences;
vector <int> orderedVariables;

# define MAXIMUM_CONFLICT_LEVEL INT_MAX
vector <int> conflictLevels;

int currentValueInModel(int literal) {
	if (literal >= 0) return model[literal];
	else if (model[-literal] == UNDEFINED) return UNDEFINED;
	else return 1 - model[-literal];
}

void setLiteralToTrue(int literal) {
	modelStack.push_back(literal);
	if (literal > 0) model[literal] = TRUE;
	else model[-literal] = FALSE;		
}

// Initialization.
// ============================================================================

void readClauses() {
	// Ignora els comentaris.
	char c = cin.get();
	while (c == 'c') {
		while (c != '\n') c = cin.get();
		c = cin.get();
	}	
	
	// Llegeix «cnf numberOfVariables numberOfClauses».
	string aux;
	cin >> aux >> numberOfVariables >> numberOfClauses;
	clauses.resize(numberOfClauses);
	
	// Llegeix les clàusules.
	for (int i = 0; i < numberOfClauses; ++i) {
		int lit;
		while (cin >> lit and lit != 0)
			clauses[i].push_back(lit);
	}
}

void addClauseToOccurrenceLists(int i) {
	for (unsigned int j = 0; j < clauses[i].size(); j++) {
		if (clauses[i][j] > 0) {
			positiveOccurrences[clauses[i][j]].push_back(i);
		} else {
			negativeOccurrences[-clauses[i][j]].push_back(i);
		}
	}
}

bool compareLiterals(const int & a, const int & b) {
	if (a == 0) {
		return false;
	} else if (b == 0) {
		return true;
	} else {
		return min(positiveOccurrences[a].size(), negativeOccurrences[a].size()) >
		       min(positiveOccurrences[b].size(), negativeOccurrences[b].size());
	}
}

// Heuristic for finding the next decision literal.
// ============================================================================

int getFirstUndefinedLiteral() {
	for (int i = 1; i <= numberOfVariables; ++i)
		if (model[i] == UNDEFINED)
			return i;
	
	return 0;
}

int getMostOccurrentUndefinedLiteral() {
	for (int i = 0; i <= numberOfVariables; ++i)
		if (model[orderedVariables[i]] == UNDEFINED)
			return orderedVariables[i];

	return 0;
}

int getMostConflictiveUndefinedLiteral() {
	int literal = 0;
	
	for (int i = 1; i <= numberOfVariables; ++i)
		if (model[i] == UNDEFINED && conflictLevels[i] >= conflictLevels[literal])
				literal = i;

	return literal;
}

// Propagation and backtracking.
// ============================================================================

bool propagateOnClause(int i) {
	bool conflict = false;
	int numberOfTrueLiterals = 0;
	int numberOfUndefinedLiterals = 0;
	int lastUndefinedLiteral = 0;
	
	for (unsigned int k = 0; numberOfTrueLiterals == 0 and k < clauses[i].size(); k++) {
		int value = currentValueInModel(clauses[i][k]);
		numberOfTrueLiterals += value == TRUE;
		numberOfUndefinedLiterals += value == UNDEFINED;
		lastUndefinedLiteral += (value == UNDEFINED) * (clauses[i][k] - lastUndefinedLiteral);
	}
	
	if (numberOfTrueLiterals == 0) {
		// Tots els literals són falsos.
		if (numberOfUndefinedLiterals == 0) {
			conflict = true;
		// Tots els literals són falsos excepte un literal sense definir.
		} else if (numberOfUndefinedLiterals == 1) {
			setLiteralToTrue(lastUndefinedLiteral);
		}
	}
	
	return conflict;
}

bool propagateOnOppositeClauses() {
	bool conflict = false;
	int literal = modelStack[indexOfNextLiteralToPropagate];
	vector <int> occurrences = literal > 0 ? negativeOccurrences[literal] : positiveOccurrences[-literal];
	
	for (unsigned int i = 0; not conflict && i < occurrences.size(); i++) {
		conflict = propagateOnClause(occurrences[i]);
	}

	return conflict;
}

bool propagateOnAllClauses() {
	bool conflict = false;
	
	for (int i = 0; not conflict && i < numberOfClauses; i++) {
		conflict = propagateOnClause(i);
	}

	return conflict;
}

void updateConflictLevels() {
	int literal = modelStack[indexOfNextLiteralToPropagate];
	int variable = abs(literal);
	
	if (conflictLevels[variable] == MAXIMUM_CONFLICT_LEVEL) {
		for (int i = 0; i <= numberOfVariables; i++) {
			conflictLevels[i] /= 2;
		}
	}
	
	conflictLevels[variable]++;
}

bool propagateGivesConflict() {
	bool conflict = false;
	
	while (not conflict && indexOfNextLiteralToPropagate < modelStack.size()) {
		//conflict = propagateOnAllClauses();
		conflict = propagateOnOppositeClauses();
		
		if (conflict) {
			updateConflictLevels();
		}
		
		indexOfNextLiteralToPropagate++;
	}
	
	return conflict;
}

void backtrack() {
	int i = modelStack.size() -1;
	int literal = 0;
	
	// 0 és la marca dels literals decidits.
	while (modelStack[i] != 0) {
		literal = modelStack[i];
		model[abs(literal)] = UNDEFINED;
		modelStack.pop_back();
		i--;
	}
	
	// El literal és el darrer literal decidit.
	modelStack.pop_back(); // Eliminem la marca.
	decisionLevel--;
	indexOfNextLiteralToPropagate = modelStack.size();
	setLiteralToTrue(-literal); // Desfem la darrera decisió.
}

// Conclusion.
// ============================================================================

int checkModel() {
	for (int i = 0; i < numberOfClauses; ++i) {
		bool someTrue = false;
		
		for (unsigned int j = 0; not someTrue and j < clauses[i].size(); ++j) {
			someTrue = currentValueInModel(clauses[i][j]) == TRUE;
		}
		
		if (not someTrue) {
			cout << "Error in model, clause is not satisfied:";
			
			for (unsigned int j = 0; j < clauses[i].size(); ++j) {
				cout << clauses[i][j] << " ";
			}
			
			cout << endl;
			return 1;
		}
	}
	
	return 0;
}

int satisfiable() {
	if (checkModel() == 1) {
		return 1;
	} else {
		cout << "SATISFIABLE" << endl;
		return 20;
	}
}

int unsatisfiable() {
	cout << "UNSATISFIABLE" << endl;
	return 10;
}

// ============================================================================

int main() { 
	readClauses();
	model.resize(numberOfVariables + 1, UNDEFINED);
	indexOfNextLiteralToPropagate = 0;
	decisionLevel = 0;
	positiveOccurrences.resize(numberOfVariables + 1);
	negativeOccurrences.resize(numberOfVariables + 1);
	orderedVariables.resize(numberOfVariables + 1);
	conflictLevels.resize(numberOfVariables + 1, 0);
	
	for (int i = 0; i < numberOfClauses; i++) {
		addClauseToOccurrenceLists(i);

		// Clàusules unitàries.
		if (clauses[i].size() == 1) {
			int literal = clauses[i][0];
			int value = currentValueInModel(literal);
			
			if (value == FALSE) {
				return unsatisfiable();
			} else if (value == UNDEFINED) {
				setLiteralToTrue(literal);
			}
		}
	}

	for (int i = 0; i <= numberOfVariables; i++) {
		orderedVariables[i] = i;
	}
	
	sort(orderedVariables.begin(), orderedVariables.end(), compareLiterals);
	
	// DPLL.
	while (true) {
		// Propaga exhaustivament.
		while (propagateGivesConflict()) {
			// Hi ha un conflicte: si no hem decidit res és insatisfactible, si no,
			// desfem la decisió.
			if (decisionLevel == 0) {
				return unsatisfiable();
			} else {
				backtrack();
			}
		}
		
		// No podem propagar res més: si no hi ha literals sense definir hi ha un
		// model, si no, prèn una decisió.
		//int decisionLiteral = getFirstUndefinedLiteral();
		//int decisionLiteral = getMostOccurrentUndefinedLiteral();
		int decisionLiteral = getMostConflictiveUndefinedLiteral();

		if (decisionLiteral == 0) {
			return satisfiable();
		} else {
			modelStack.push_back(0);
			++indexOfNextLiteralToPropagate;
			++decisionLevel;
			setLiteralToTrue(decisionLiteral);
		}
	}
}
