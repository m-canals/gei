#!/bin/sh

PICOSAT_OUTPUT=tmp/picosat-output.txt

if make "$@"
then
	for input in cnf/vars-*-*.cnf
	do
		  printf '%s\n' $input
		  
		  printf "picosat: "
		  begin=$(date +%s)
		  picosat -v $input > "$PICOSAT_OUTPUT"
		  end=$(date +%s)
		  elapsed=$((end-begin))
		  satisfiable1=$(grep '^s' "$PICOSAT_OUTPUT" | cut -d ' ' -f 2)
		  decisions=$(grep '^c [0-9]* decisions$' "$PICOSAT_OUTPUT"  | cut -d ' ' -f 2)
		  printf '%s, %d seconds, %d decisions\n' $satisfiable1 $elapsed $decisions 
		  
		  for solver
		  do
				printf "%s: " "$solver"
				begin=$(date +%s)
				satisfiable2=$("bin/$solver" < "$input")
				end=$(date +%s)
				elapsed=$((end-begin))
				test $satisfiable1 = $satisfiable2 && color=32 || color=31
				printf '\033[%dm%s\033[0m, %d seconds\n' $color $satisfiable2 $elapsed
			done
			
			printf '\n'
	done
fi

