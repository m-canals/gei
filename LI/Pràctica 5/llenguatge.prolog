% Sintaxi:
%
%  <programa>      begin <instruccions> end
%  <instruccions>  <instrucció>
%  <instruccions>  <instrucció> ; <instruccions>
%  <instrucció>    <variable> = <variable> + <variable>
%  <instrucció>    if <variable> = <variable> then <instruccions>
%                  else <instruccions>
%  <variable>      x
%  <variable>      y
%  <variable>      z

% message(S, L):- write(S), write(L), nl.
message(_, _).

variable(x).
variable(y).
variable(z).

instruction(L):-
	L = [X, =, Y, +, Z],
	variable(X),
	variable(Y),
	variable(Z),
	message("Assignació i operació: ", L).
instruction(L):-
	append([[if, X, =, Y, then], L1, [else], L2, [endif]], L),
	variable(X),
	variable(Y),
	instructions(L1),
	instructions(L2),
	message("Sentència condicional: ", L).

instructions(L):-
	instruction(L),
	message("Instrucció: ", L).
instructions(L):-
	append([X, [;], L1], L),
	instruction(X),
	instructions(L1),
	message("Instruccions: ", L).

program(L):-
	append([[begin], L1, [end]], L),
	instructions(L1),
	message("Programa: ", L),
	write("yes"), nl.
program(_):-
	write("no"), nl.

main:-
	program([begin, x, =, x, +, z, end]),
	program([begin, x, =, x, +, y, ;, z, =, z, +, z, ;, x, =, y, +, x, end]),
	program([begin, x, =, y, +, z, ;, if, z, =, z, then, x, =, x, +, z, ;, y, =, y, +, z, else, z, =, z, +, y, endif, ;, x, =, x, +, z, end]),
	program([begin, z, =, x, +, y, ;, x, =, z, z, end]),
	halt.

% Versió alternativa.

instruction([V1, =, V2, +, V3 | L2], L2) :-
	variable(V1),
	variable(V2),
	variable(V3).
instruction([if, V1, = , V2, then | Rest], L2) :-
	variable(V1),
	variable(V2), 
	instructions(Rest, [else | THEN]), 
	instructions(THEN, [endif|L2]).
instructions(L1, L2):-
	instruction(L1, L2).
instructions(L1, L2):-
	instruction(L1, [; | Rest]),
	instructions(Rest, L2).
program([begin | Rest]):-
	instructions(Rest,[end]), !.  

