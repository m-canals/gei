variable --> [x].
variable --> [y].
variable --> [z].

instruction --> variable, [=], variable, [+], variable.
instruction --> [if], variable, [=], variable, [then], instructions, [else], instructions, [endif].
instructions --> instruction.
instructions --> instruction, [;], instructions.
program --> [begin], instructions, [end], !.

% ?- phrase(program, [begin,x,=,y,+,z,end]).
% true.

% ?- expand_term(variable --> [x], Exp).
% Exp =  (variable([x|_3008], _3008):-true).

% ?- expand_term((program --> [begin], instructions, [end], !), Expansion).
% Expansion =  (program([begin|_3972], _3994):-instructions(_3972, _4034), _4034=[end|_4068], !, _3994=_4068).

% ?- phrase(variable, [x,=,y,+,z], Rest).
% Rest = [=, y, +, z].

% ?- phrase(program, [begin, x, =, y, +, z, ; , if, x, =, z, then, x, =, y, +, z, else, x, =, y, +, z, endif, x, =, y, +, z, end] ).
% false.

% ?- phrase(program, [begin, x, =, y, +, z, ; , if, x, =, z, then, x, =, y, +, z, else, x, =, y, +, z, endif,;, x, =, y, +, z, end] ).
% true.

