% Volem saber la forma més ràpida de que tres missioners i tres caníbals creuin
% un riu amb una canoa de dues places de tal manera que en cada riba hi hagi
% com a mínim tants missioners com caníbals.

% L'estat [A, B, C] és l'estat en què hi ha A missioners i B caníbals a la riba
% 0 i la canoa és a la riba C (0 o 1).

safe(0, _).
safe(3, _).
safe(A, B):-
	0 >= (B - A),
	0 >= (3 - B) - (3 - A).

valid([A, B, C]):-
	between(0, 3, A),
	between(0, 3, B),
	between(0, 1, C),
	safe(A, B).

step([A, B, 0], [D, E, 1]):-
	member([X, Y], [[0, 1], [0, 2], [1, 0], [1, 1], [2, 0]]), 
	D is A - X,
	E is B - Y,
	valid([D, E, 1]).
step([A, B, 1], [D, E, 0]):-
	member([X, Y], [[0, 1], [0, 2], [1, 0], [1, 1], [2, 0]]), 
	D is A + X,
	E is B + Y,
	valid([D, E, 0]).

steps(State, State, Steps, Steps).
steps(State, FinalState, StepsSoFar, AllSteps):-
	step(State, NextState),
	\+ member(NextState, StepsSoFar),
	steps(NextState, FinalState, [NextState | StepsSoFar], AllSteps).

naturalNumber(1).
naturalNumber(N):-
	naturalNumber(M),
	N is M + 1.

main:-
	naturalNumber(N),
	steps([3, 3, 0], [0, 0, 1], [[3, 3, 0]], ReversedSteps),
	length(Steps, N),
	reverse(ReversedSteps, Steps),
	write(Steps),
	nl,
	halt.

