% Disposem d'una aixeta, un cubell de 5 litres i un cubell de 8 litres. Podem
% omplir o buidar completament un cubell o abocar el contingut d'un cubell a
% l'altre cubell, sense que sobreïxi. Volem saber la seqüència mínima
% d'operacions necessàries per tal que al cubell de 8 litres contingui 4 litres.

% L'estat [A, B] és l'estat en què hi ha A litres al cubell de 5 litres i hi ha
% B litres al cubell de 8 litres.

% Aboquem el contingut d'un cubell, que no és buit,
% a l'altre cubell, que no és ple.
step([A, B], [C, D]):-
	A \= 5,
	B \= 0,
	C is min(A + B, 5),
	D is B - (C - A).
step([A, B], [C, D]):-
	A \= 0,
	B \= 8,
	D is min(B + A, 8),
	C is A - (D - B).
% Omplim completament un cubell que no és ple.
step([A, B], [5, B]):-
	A \= 5.
step([A, B], [A, 8]):-
	B \= 8.
% Buidem completament un cubell que no és buit.
step([A, B], [0, B]):-
	A \= 0.
step([A, B], [A, 0]):-
	B \= 0.

steps(State, State, Steps, Steps).
steps(State, FinalState, StepsSoFar, AllSteps):-
	step(State, NextState),
	\+ member(NextState, StepsSoFar),
	steps(NextState, FinalState, [NextState | StepsSoFar], AllSteps).

naturalNumber(0).
naturalNumber(N):-
	naturalNumber(M),
	N is M + 1.

main:-
	naturalNumber(N),
	steps([0, 0], [0, 4], [[0, 0]], ReversedSteps),
	length(Steps, N),
	reverse(ReversedSteps, Steps),
	write(Steps),
	nl,
	halt.

