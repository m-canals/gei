% We have a helicopter that can carry its pilot
% plus at most two passengers at the same time.
% We are given a list of passengers,
% each with his or her origin and destination,
% given as coordinates in the plane (a pair of integers in 0..1000).
% Complete the following program to find the shortest route
% for the helicopter to start from its base, handle all passengers,
% and return to its base.

% Optimal route distance: 3981.30 km.
example(5, [
	[[813,136],[133,888]],
	[[942,399],[600, 89]],
	[[532,241],[ 55, 55]],
	[[498,276],[553,528]],
	[[531,911],[430,545]]]). 

% Optimal route distance: 3889.06 km.
example(8, [
	[[839,731],[925,300]],
	[[888,309],[381,637]],
	[[423,608],[576,715]],
	[[703,709],[397,645]],
	[[353,442],[397,620]],
	[[ 34,945],[401,465]],
	[[885,319],[750,888]],
	[[313,357],[266,749]]]).

% Optimal route distance: not worse than 6214.37 km.
example(10, [
	[[781,230],[204,491]],
	[[424,252],[634,768]],
	[[940, 26],[487,833]],
	[[297,976],[478,536]],
	[[953,998],[224,261]],
	[[287, 81],[189,192]],
	[[716,325],[236,667]],
	[[735,683],[876,967]],
	[[394,749],[533, 52]],
	[[144,620],[558,470]]]). 

% Optimal route distance: not worse than 9175.89 km.
example(12, [
	[[264,384],[ 549,238]],
	[[747,908],[ 887, 67]],
	[[431,918],[ 636, 13]],
	[[ 20,455],[ 658,892]],
	[[136,976],[ 893,570]],
	[[677,750],[ 632,434]],
	[[998,960],[ 390,169]],
	[[740,110],[ 808,811]],
	[[581,198],[ 875,245]],
	[[768, 27],[ 639,556]],
	[[736,381],[1000,631]],
	[[ 93,222],[ 155,301]]]).

helicopterBasePoint([254,372]).

distance([X1, Y1], [X2, Y2], D):-
	D is sqrt(abs(X1 - X2) ** 2 + abs(Y1 - Y2) ** 2).

storeRouteIfBetter(AccumulatedDistance, Route):-
	bestRouteSoFar(BestAccumulatedDistance, _),
	AccumulatedDistance < BestAccumulatedDistance,
	write('Improved solution. New best distance is '),
	write(AccumulatedDistance), write(' km.'), nl,
	retractall(bestRouteSoFar(_, _)),
	assertz(bestRouteSoFar(AccumulatedDistance, Route)),
	!.

% No passengers to pick up and no passengers to drop off:
% return to base and update the best one.
helicopter([], AccumulatedDistance, [CurrentPoint | VisitedPoints], []):-
	helicopterBasePoint(BasePoint),
	distance(CurrentPoint, BasePoint, DistanceToBasePoint),
	NewAccumulatedDistance is AccumulatedDistance + DistanceToBasePoint,
	storeRouteIfBetter(NewAccumulatedDistance, [BasePoint, CurrentPoint | VisitedPoints]),
	fail.

% It is already worse than the best one: discard it.
helicopter(_, AccumulatedDistance, _, _):-
	bestRouteSoFar(BestAccumulatedDistance, _), 
	AccumulatedDistance >= BestAccumulatedDistance,
	!,
	fail.

% Pick up another passenger.
helicopter(Trips, AccumulatedDistance, [CurrentPoint | VisitedPoints], DropOffPoints):-
	length(DropOffPoints, N),
	N < 2,
	select([PickUpPoint, DropOffPoint], Trips, RemainingTrips),
	distance(CurrentPoint, PickUpPoint, DistanceToPickUpPoint),
	NewAccumulatedDistance is AccumulatedDistance + DistanceToPickUpPoint,
	helicopter(RemainingTrips, NewAccumulatedDistance, [PickUpPoint, CurrentPoint | VisitedPoints], [DropOffPoint | DropOffPoints]).

% Drop off one of the passengers in the helicopter.
helicopter(Trips, AccumulatedDistance, [CurrentPoint | VisitedPoints], DropOffPoints):-
	length(DropOffPoints, N),
	N > 0,
	select(DropOffPoint, DropOffPoints, RemainingDropOffPoints),
	distance(CurrentPoint, DropOffPoint, DistanceToDropOffPoint),
	NewAccumulatedDistance is AccumulatedDistance + DistanceToDropOffPoint,
	helicopter(Trips, NewAccumulatedDistance, [DropOffPoint, CurrentPoint | VisitedPoints], RemainingDropOffPoints).

main:-
	N = 5,
	% Initially the best route is the worst route (infinite distance).
	retractall(bestRouteSoFar(_, _)),
	assertz(bestRouteSoFar(100000, [])),
	example(N, Trips),
	helicopterBasePoint(CurrentPoint),
	helicopter(Trips, 0, [CurrentPoint], []).
main:-
	bestRouteSoFar(AccumulatedDistance, ReverseRoute),
	reverse(ReverseRoute, Route),
	nl, write('Optimal route: '), write(Route), write('. '),
	write(AccumulatedDistance), write(' km.'), nl, nl,
	halt.

