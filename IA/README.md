Treball d'innovació de la tardor de 2020 de l'assignatura Intel·ligència Artificial (IA) del Grau en Enginyeria Informàtica (GEI) de la Facultat de Informàtica de Barcelona (FIB) de la Universitat Politècnica de Catalunya (UPC).

Més informació al [web de l'assignatura](https://sites.google.com/upc.edu/intelligencia-artificial).
